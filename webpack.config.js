const path = require("path");

// BABEL CONFIG MODULE
let babelConfigModule = {
    module: {
        rules: [
            {
                // COMPILE ES6
                test: /\.js$/i,
                exclude: /(node_modules)/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: [
                            "@babel/preset-env"
                        ]
                    }
                }
            }
        ]
    }
};

// SASS CONFIG MODULE
let sassConfigModule = {
    module: {
        rules: [
            {
                // COMPILE SASS/SCSS FILES AND PREFIX STYLES
                test: /\.scss$/i,
                exclude: /(node_modules)/,
                use: [
                    {
                        loader: "style-loader" // FOR COMPILING SCSS
                    },
                    {
						loader: 'file-loader', // FOR COMPILING SCSS TO SEPARATE CSS FILE
						options: {
							name(resourcePath, resourceQuery) {
                                resourcePath = resourcePath.replace(/\\/g, "/"); // CHECK FOR BACKSLASHES TO ACCOUNT FOR WINDOWS FILE PATHS

                                let filePath = resourcePath.split("/");
                                let file = filePath[filePath.length - 1].replace("-to-compile.scss", ".css");
                                return file;
                            }
						}
                    },
                    {
                        loader: "postcss-loader", // FOR AUTO-PREFIXING STYLES
                        options: {
                            postcssOptions: {
                                plugins: [
                                    [
                                        "autoprefixer",
                                        {
                                            // OPTIONS
                                        }
                                    ]
                                ]
                            }
                        }
                    },
                    {
                        loader: "sass-loader", // FOR COMPILING SCSS
                        options: {
                            implementation: require("sass"),
                        }
                    }
                ]
            }
        ]
    }
};

// SPLITTING UP THE CONFIGURATIONS SO THAT WE CAN HAVE MULTIPLE ENTIRES AND OUTPUT LOCATIONS
module.exports = [
    // COMPILE TEMPLATE JAVASCRIPT CONFIGURATION
    {
        name: "compileTemplateJS",
        mode: 'none',
        entry: [
            './src/template/compiled/js/head-to-compile.js'
        ],
        output: {
            filename: "head.js",
            path: path.resolve(__dirname, "src/template/compiled/js")
        },
        ...babelConfigModule
    },

    // BUILD TEMPLATE JAVASCRIPT CONFIGURATION
    {
        name: "buildTemplateJS",
        mode: 'none',
        entry: [
            './src/build/build-template/template-files/head-to-compile.js'
        ],
        output: {
            filename: "head.js",
            path: path.resolve(__dirname, "src/build/build-template/template-files")
        },
        ...babelConfigModule
    },

    // COMPILE TEMPLATE CSS CONFIGURATION
    {
        name: "compileTemplateCSS",
        mode: 'none',
        entry: [
            './src/template/compiled/css/1024-to-compile.scss',
            './src/template/compiled/css/768-to-compile.scss',
            './src/template/compiled/css/640-to-compile.scss',
            './src/template/compiled/css/480-to-compile.scss',
            './src/template/compiled/css/320-to-compile.scss'
        ],
        output: {
            filename: "css-compile.js", // WILL GET REMOVED AFTER COMPILE COMPLETES
            path: path.resolve(__dirname, "src/template/compiled/css")
        },
        ...sassConfigModule
    },

    // BUILD TEMPLATE CSS CONFIGURATION
    {
        name: "buildTemplateCSS",
        mode: 'none',
        entry: [
            './src/build/build-template/template-files/1024-to-compile.scss',
            './src/build/build-template/template-files/768-to-compile.scss',
            './src/build/build-template/template-files/640-to-compile.scss',
            './src/build/build-template/template-files/480-to-compile.scss',
            './src/build/build-template/template-files/320-to-compile.scss'
        ],
        output: {
            filename: "css-compile.js", // WILL GET REMOVED AFTER COMPILE COMPLETES
            path: path.resolve(__dirname, "src/build/build-template/template-files")
        },
        ...sassConfigModule
    }
];
