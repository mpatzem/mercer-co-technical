<?php

// SETTING UP THIS WAY HELPS KEEP THINGS OUT OF THE GLOBAL SPACE WHILE HAVING GLOBAL ACCESS
class Variables {

    function __construct() {
        $this->variables = array(
            "siteName" => "Mercer Co Technical School",
            "siteDomain" => "https://mpatzem.schoolwires.net",
            "homepageUrl" => "/Page/1",
            "subpageUrl" => "/Page/273",
            "subpageNoNavUrl" => "/Page/556",
            "siteAlias" => "dev1",
            "siteAddress" => "129 Bull Run Rd",
            "siteCity" => "Pennington",
            "siteState" => "NJ",
            "siteZip" => "08534",
            "sitePhone" => "609.737.9784",
            "siteFax" => "012.345.6789",
        );
    }

    public function Get($variable) {
        if(array_key_exists($variable, $this->variables)) {
            return $this->variables[$variable];
        } else {
            return null;
        }
    }

}

?>