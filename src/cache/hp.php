
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">

<html lang="en">
<head>
    <title>Fletch_Test / Homepage</title>
    <!--
    <PageMap>
    <DataObject type="document">
    <Attribute name="siteid">4</Attribute>
    </DataObject>
    </PageMap>
    -->
    

<script>
(function(apiKey){
    (function(p,e,n,d,o){var v,w,x,y,z;o=p[d]=p[d]||{};o._q=[];
    v=['initialize','identify','updateOptions','pageLoad'];for(w=0,x=v.length;w<x;++w)(function(m){
        o[m]=o[m]||function(){o._q[m===v[0]?'unshift':'push']([m].concat([].slice.call(arguments,0)));};})(v[w]);
        y=e.createElement(n);y.async=!0;y.src='https://cdn.pendo.io/agent/static/'+apiKey+'/pendo.js';
        z=e.getElementsByTagName(n)[0];z.parentNode.insertBefore(y,z);})(window,document,'script','pendo');

        // Call this whenever information about your visitors becomes available
        // Please use Strings, Numbers, or Bools for value types.
        pendo.initialize({
            visitor: {
                id: 'SWCS000010-cngv0iEqRbHQc+9Am84vTQ==',   // Required if user is logged in
                role: 'Anonymous',
                isPartOfGroup: 'False',
                // email:        // Optional
                // You can add any additional visitor level key-values here,
                // as long as it's not one of the above reserved names.
            },

            account: {
                id: 'SWCS000010', // Highly recommended
                version: '2.69',
                name:         'mpatzem.schoolwires.net'
                // planLevel:    // Optional
                // planPrice:    // Optional
                // creationDate: // Optional

                // You can add any additional account level key-values here,
                // as long as it's not one of the above reserved names.
            }
        });
})('ca0f531d-af61-45a7-7c9a-079f24d9128a');
</script>

    
    <meta property="og:type" content="website" />
<meta property="fb:app_id" content="411584262324304" />
<meta property="og:url" content="http%3A%2F%2Fmpatzem.schoolwires.net%2Fsite%2Fdefault.aspx%3FPageID%3D1" />
<meta property="og:title" content="Fletch_Test / Homepage" />
<meta name="twitter:card" value="summary" />
<meta name="twitter:title" content="Fletch_Test / Homepage" />
<meta itemprop="name" content="Fletch_Test / Homepage" />

    <!-- Begin swuc.GlobalJS -->
<script type="text/javascript">
 staticURL = "https://mpatzem.schoolwires.net/Static/";
 SessionTimeout = "50";
 BBHelpURL = "";
</script>
<!-- End swuc.GlobalJS -->

    <script src='https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/min/sri-failover.min.js' type='text/javascript'></script>

    <!-- Stylesheets -->
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-Light.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-Italic.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-Regular.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/GlobalAssets/webfonts/OpenSans-SemiBold.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/shepherd/shepherd-theme-default.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/App_Themes/SW/jquery.jgrowl.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static//site/assets/styles/system_2660.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static//site/assets/styles/apps_2590.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/App_Themes/SW/jQueryUI.css" />
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/GlobalAssets/webfonts/SchoolwiresMobile_2320.css" />
    
    <link rel="Stylesheet" type="text/css" href="https://mpatzem.schoolwires.net/Static/GlobalAssets/Styles/Grid.css" />

    <!-- Scripts -->
    <script src="https://mpatzem.schoolwires.net/Static/GlobalAssets/WCM-2680/WCM.js" type="text/javascript"></script>
    <script src="https://mpatzem.schoolwires.net/Static/GlobalAssets/WCM-2680/API.js" type="text/javascript"></script>
    <script src='https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/min/jquery-3.0.0.min.js' type='text/javascript'></script>
    <script src='https://mpatzem.schoolwires.net/Static//GlobalAssets/Scripts/min/jquery-migrate-1.4.1.min.js' type='text/javascript'></script
    <script src='https://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js' type='text/javascript'
        integrity='sha384-JO4qIitDJfdsiD2P0i3fG6TmhkLKkiTfL4oVLkVFhGs5frz71Reviytvya4wIdDW' crossorigin='anonymous'
        data-sri-failover='https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/min/swfobject.js'></script>
    <script src='https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/tether/tether.min.js' type='text/javascript'></script>
    <script src='https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/shepherd/shepherd.min.js' type='text/javascript'></script>
   
    <script type="text/javascript">
        $(document).ready(function () {
            SetCookie('SWScreenWidth', screen.width);
            SetCookie('SWClientWidth', document.body.clientWidth);
            
            $("div.ui-article:last").addClass("last-article");
            $("div.region .app:last").addClass("last-app");

            // get on screen alerts
            var isAnyActiveOSA = 'False';
            var onscreenAlertCookie = GetCookie('Alerts');

            if (onscreenAlertCookie == '' || onscreenAlertCookie == undefined) {
                onscreenAlertCookie = "";
            }
            if (isAnyActiveOSA == 'True') {
                GetContent(homeURL + "/cms/Tools/OnScreenAlerts/UserControls/OnScreenAlertDialogListWrapper.aspx?OnScreenAlertCookie=" + onscreenAlertCookie + "&SiteID=4", "onscreenalert-holder", 2, "OnScreenAlertCheckListItem();");
            }            

        });

    // ADA SKIP NAV
    $(document).ready(function () {
        $(document).on('focus', '#skipLink', function () {
            $("div.sw-skipnav-outerbar").animate({
                marginTop: "0px"
            }, 500);
        });

        $(document).on('blur', '#skipLink', function () {
            $("div.sw-skipnav-outerbar").animate({
                marginTop: "-30px"
            }, 500);
        });
    });

    // ADA MYSTART
    $(document).ready(function () {
        var top_level_nav = $('.sw-mystart-nav');

        // Set tabIndex to -1 so that top_level_links can't receive focus until menu is open
        // school dropdown
        $(top_level_nav).find('ul').find('a').attr('tabIndex', -1);

        // my account dropdown
        $(top_level_nav).next('ul').find('a').attr('tabIndex', -1);

        var openNavCallback = function(e, element) {
             // hide open menus
            hideMyStartBarMenu();

            // show school dropdown
            if ($(element).find('ul').length > 0) {
                $(element).find('.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false');
                $(element).find('.sw-dropdown').find('li:first-child a').focus()
            }

            // show my account dropdown
            if ($(element).next('ul').length > 0) {
                $(element).next('.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false');
                $(element).next('.sw-dropdown').find('li:first-child a').focus();
                $('#sw-mystart-account').addClass("clicked-state");
            }
        }

        $(top_level_nav).click(function (e) {
            openNavCallback(e, this);
        });

        $('.sw-dropdown-list li').click(function(e) {
            e.stopImmediatePropagation();
            $(this).focus();
        });
        
        // Bind arrow keys for navigation
        $(top_level_nav).keydown(function (e) {
            if (e.keyCode == 37) { //key left
                e.preventDefault();

                // This is the first item
                if ($(this).prev('.sw-mystart-nav').length == 0) {
                    $(this).parents('div').find('.sw-mystart-nav').last().focus();
                } else {
                    $(this).prev('.sw-mystart-nav').focus();
                }
            } else if (e.keyCode == 38) { //key up
                e.preventDefault();

                // show school dropdown
                if ($(this).find('ul').length > 0) {
                    $(this).find('div.sw-dropdown').css('display', 'block').find('ul').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).last().focus();
                }

                // show my account dropdown
                if ($(this).find('ul').length > 0) {
                    $(this).find('ul.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).last().focus();
                }
            } else if (e.keyCode == 39) { //key right
                e.preventDefault();

                // This is the last item
                if ($(this).next('.sw-mystart-nav').length == 0) {
                    $(this).parents('div').find('.sw-mystart-nav').first().focus();
                } else {
                    $(this).next('.sw-mystart-nav').focus();
                }
            } else if (e.keyCode == 40) { //key down
                e.preventDefault();

                // show school dropdown
                if ($(this).find('ul').length > 0) {
                    $(this).find('div.sw-dropdown').css('display', 'block').find('ul').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).first().focus();
                }

                // show my account dropdown
                if ($(this).next('ul').length > 0) {
                    $(this).next('ul.sw-dropdown').css('display', 'block').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).first().focus();
                }
            } else if (e.keyCode == 13 || e.keyCode == 32) { //enter key
                // If submenu is hidden, open it
                e.preventDefault();

                
                openNavCallback(e, this);
                $(this).parent('li').find('ul[aria-hidden=true]').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).first().focus();
            } else if (e.keyCode == 27) { //escape key
                e.preventDefault();
                hideMyStartBarMenu();
            } else {
                $(this).parent('.sw-mystart-nav').find('ul[aria-hidden=false] a').each(function () {
                    if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                        $(this).focus();
                        return false;
                    }
                });
            }
        });

        // school dropdown
        var startbarlinks = $(top_level_nav).find('ul').find('a');
        bindMyStartBarLinks(startbarlinks);

        // my account dropdown
        var myaccountlinks = $(top_level_nav).next('ul').find('a');
        bindMyStartBarLinks(myaccountlinks);

        function bindMyStartBarLinks(links) {
            $(links).keydown(function (e) {
                e.stopPropagation();

                if (e.keyCode == 38) { //key up
                    e.preventDefault();

                    // This is the first item
                    if ($(this).parent('li').prev('li').length == 0) {
                        if ($(this).parents('ul').parents('.sw-mystart-nav').length > 0) {
                            $(this).parents('ul').parents('.sw-mystart-nav').focus();
                        } else {
                            $(this).parents('ul').prev('.sw-mystart-nav').focus();
                        }
                    } else {
                        $(this).parent('li').prev('li').find('a').first().focus();
                    }
                } else if (e.keyCode == 40) { //key down
                    e.preventDefault();

                    if ($(this).parent('li').next('li').length == 0) {
                        if ($(this).parents('ul').parents('.sw-mystart-nav').length > 0) {
                            $(this).parents('ul').parents('.sw-mystart-nav').focus();
                        } else {
                            $(this).parents('ul').prev('.sw-mystart-nav').focus();
                        }
                    } else {
                        $(this).parent('li').next('li').find('a').first().attr('tabIndex', 0);
                        $(this).parent('li').next('li').find('a').first().focus();
                    }
                } else if (e.keyCode == 27 || e.keyCode == 37) { // escape key or key left
                    e.preventDefault();
                    hideMyStartBarMenu();
                } else if (e.keyCode == 32) { //enter key
                    e.preventDefault();
                    window.location = $(this).attr('href');
                } else {
                    var found = false;

                    $(this).parent('div').nextAll('li').find('a').each(function () {
                        if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                            $(this).focus();
                            found = true;
                            return false;
                        }
                    });

                    if (!found) {
                        $(this).parent('div').prevAll('li').find('a').each(function () {
                            if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                                $(this).focus();
                                return false;
                            }
                        });
                    }
                }
            });
        }
        
        // Hide menu if click or focus occurs outside of navigation
        $('#sw-mystart-inner').find('.sw-mystart-nav').last().keydown(function (e) {
            if (e.keyCode == 9) {
                // If the user tabs out of the navigation hide all menus
                hideMyStartBarMenu();
            }
        });

        /*$(document).click(function() { 
            hideMyStartBarMenu();
        });*/

        // try to capture as many custom MyStart bars as possible
        $('.sw-mystart-button').find('a').focus(function () {
            hideMyStartBarMenu();
        });

        $('#sw-mystart-inner').click(function (e) {
            e.stopPropagation();
        });

        $('ul.sw-dropdown-list').blur(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-mypasskey').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-sitemanager').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-myview').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-signin').focus(function () {
            hideMyStartBarMenu();
        });

        $('#ui-btn-register').focus(function () {
            hideMyStartBarMenu();
        });

        // button click events
        $('div.sw-mystart-button.home a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.pw a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.manage a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('#sw-mystart-account').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).addClass('clicked-state');
                $('#sw-myaccount-list').show();
            }
        });

        $('#sw-mystart-mypasskey a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.signin a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });

        $('div.sw-mystart-button.register a').keydown(function (e) {
            e.stopImmediatePropagation();

            if (e.keyCode == 13) {
                $(this).click();
            }
        });
    });

    function hideMyStartBarMenu() {
        $('.sw-dropdown').attr('aria-hidden', 'true').css('display', 'none');
        $('#sw-mystart-account').removeClass("clicked-state");
    }

    // ADA CHANNEL NAV
    $(document).ready(function() {
        var channelCount;
        var channelIndex = 1;
        var settings = {
            menuHoverClass: 'hover'
        };

        // Add ARIA roles to menubar and menu items
        $('[id="channel-navigation"]').attr('role', 'menubar').find('li a').attr('role', 'menuitem').attr('tabindex', '0');

        var top_level_links = $('[id="channel-navigation"]').find('> li > a');
        channelCount = $(top_level_links).length;


        $(top_level_links).each(function() {
            $(this).attr('aria-posinset', channelIndex).attr('aria-setsize', channelCount);
            $(this).next('ul').attr({ 'aria-hidden': 'true', 'role': 'menu' });

            if ($(this).parent('li.sw-channel-item').children('ul').length > 0) {
                $(this).attr('aria-haspopup', 'true');
            }

            var sectionCount = $(this).next('ul').find('a').length;
            var sectionIndex = 1;
            $(this).next('ul').find('a').each(function() {
                $(this).attr('tabIndex', -1).attr('aria-posinset', sectionIndex).attr('aria-setsize', sectionCount);
                sectionIndex++;
            });
            channelIndex++;

        });

        $(top_level_links).focus(function () {
            //hide open menus
            hideChannelMenu();

            if ($(this).parent('li').find('ul').length > 0) {
                $(this).parent('li').addClass(settings.menuHoverClass).find('ul').attr('aria-hidden', 'false').css('display', 'block');
            }
        });

        // Bind arrow keys for navigation
        $(top_level_links).keydown(function (e) {
            if (e.keyCode == 37) { //key left
                e.preventDefault();

                // This is the first item
                if ($(this).parent('li').prev('li').length == 0) {
                    $(this).parents('ul').find('> li').last().find('a').first().focus();
                } else {
                    $(this).parent('li').prev('li').find('a').first().focus();
                }
            } else if (e.keyCode == 38) { //key up
                e.preventDefault();

                if ($(this).parent('li').find('ul').length > 0) {
                    $(this).parent('li').addClass(settings.menuHoverClass).find('ul').css('display', 'block').attr('aria-hidden', 'false').find('a').attr('tabIndex', 0).last().focus();
                }
            } else if (e.keyCode == 39) { //key right
                e.preventDefault();

                // This is the last item
                if ($(this).parent('li').next('li').length == 0) {
                    $(this).parents('ul').find('> li').first().find('a').first().focus();
                } else {
                    $(this).parent('li').next('li').find('a').first().focus();
                }
            } else if (e.keyCode == 40) { //key down
                e.preventDefault();

                if ($(this).parent('li').find('ul').length > 0) {
                    $(this).parent('li')
                         .addClass(settings.menuHoverClass)
                         .find('ul.sw-channel-dropdown').css('display', 'block')
                         .attr('aria-hidden', 'false')
                         .find('a').attr('tabIndex', 0)
                         .first().focus();
                }
            } else if (e.keyCode == 13 || e.keyCode == 32) { //enter key
                // If submenu is hidden, open it
                e.preventDefault();

                $(this).parent('li').find('ul[aria-hidden=true]').attr('aria-hidden', 'false').addClass(settings.menuHoverClass).find('a').attr('tabIndex', 0).first().focus();
            } else if (e.keyCode == 27) { //escape key
                e.preventDefault();
                hideChannelMenu();
            } else {
                $(this).parent('li').find('ul[aria-hidden=false] a').each(function () {
                    if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                        $(this).focus();
                        return false;
                    }
                });
            }
        });

        var links = $(top_level_links).parent('li').find('ul').find('a');

        $(links).keydown(function (e) {
            if (e.keyCode == 38) {
                e.preventDefault();

                // This is the first item
                if ($(this).parent('li').prev('li').length == 0) {
                    $(this).parents('ul').parents('li').find('a').first().focus();
                } else {
                    $(this).parent('li').prev('li').find('a').first().focus();
                }
            } else if (e.keyCode == 40) {
                e.preventDefault();

                if ($(this).parent('li').next('li').length == 0) {
                    $(this).parents('ul').parents('li').find('a').first().focus();
                } else {
                    $(this).parent('li').next('li').find('a').first().focus();
                }
            } else if (e.keyCode == 27 || e.keyCode == 37) {
                e.preventDefault();
                $(this).parents('ul').first().prev('a').focus().parents('ul').first().find('.' + settings.menuHoverClass).removeClass(settings.menuHoverClass);
            } else if (e.keyCode == 32) {
                e.preventDefault();
                window.location = $(this).attr('href');
            } else {
                var found = false;

                $(this).parent('li').nextAll('li').find('a').each(function () {
                    if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                        $(this).focus();
                        found = true;
                        return false;
                    }
                });

                if (!found) {
                    $(this).parent('li').prevAll('li').find('a').each(function () {
                        if (typeof keyCodeMap != "undefined" && $(this).text().substring(0, 1).toLowerCase() == keyCodeMap[e.keyCode]) {
                            $(this).focus();
                            return false;
                        }
                    });
                }
            }
        });

        function hideChannelMenu() {
            $('li.sw-channel-item.' + settings.menuHoverClass).removeClass(settings.menuHoverClass).find('ul').attr('aria-hidden', 'true').css('display', 'none').find('a').attr('tabIndex', -1);
        }
        
        // Hide menu if click or focus occurs outside of navigation
        $('[id="channel-navigation"]').find('a').last().keydown(function (e) {
            if (e.keyCode == 9) {
                // If the user tabs out of the navigation hide all menus
                hideChannelMenu();
            }
        });

        $('[id="channel-navigation"]').find('a').first().keydown(function (e) {
            if (e.keyCode == 9) {
                // hide open MyStart Bar menus
                hideMyStartBarMenu();
            }
        });

        /*$(document).click(function() {
            hideChannelMenu();
        });*/

        $('[id="channel-navigation"]').click(function (e) {
            e.stopPropagation();
        });
    });

    $(document).ready(function() {
        $('input.required').each(function() {
            if ($('label[for="' + $(this).attr('id') + '"]').length > 0) {
                if ($('label[for="' + $(this).attr('id') + '"]').html().indexOf('recStar') < 0) {
                    $('label[for="' + $(this).attr('id') + '"]').prepend('<span class="recStar" aria-label="required item">*</span> ');
                }
            }
        });

        $(document).ajaxComplete(function() {
            $('input.required').each(function() {
                if ($('label[for="' + $(this).attr('id') + '"]').length > 0) {
                    if ($('label[for="' + $(this).attr('id') + '"]').html().indexOf('recStar') < 0) {
                        $('label[for="' + $(this).attr('id') + '"]').prepend('<span class="recStar" aria-label="required item">*</span> ');
                    }
                }
            });
        });
    });
    </script>

    <!-- Page -->
    
    <style type="text/css">/* MedaiBegin Standard *//* TEMPLATE CSS COMPILED FROM SASS */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ---------------------------- ### GLOBAL ### ---------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* GroupBegin Global */
/* ----- FONTS ----- */
@font-face {
    font-family: "template-icon";
    src: url("data:application/x-font-ttf;charset=utf-8;base64,AAEAAAALAIAAAwAwT1MvMg8SBigAAAC8AAAAYGNtYXAXVtKjAAABHAAAAFRnYXNwAAAAEAAAAXAAAAAIZ2x5ZvIg44EAAAF4AAASUGhlYWQl+KJcAAATyAAAADZoaGVhCq8GzwAAFAAAAAAkaG10eHiJCqAAABQkAAAAhGxvY2E5lD4GAAAUqAAAAERtYXhwAC8AuQAAFOwAAAAgbmFtZZlKCfsAABUMAAABhnBvc3QAAwAAAAAWlAAAACAAAwPiAZAABQAAApkCzAAAAI8CmQLMAAAB6wAzAQkAAAAAAAAAAAAAAAAAAAABEAAAAAAAAAAAAAAAAAAAAABAAADpHAPA/8AAQAPAAEAAAAABAAAAAAAAAAAAAAAgAAAAAAADAAAAAwAAABwAAQADAAAAHAADAAEAAAAcAAQAOAAAAAoACAACAAIAAQAg6Rz//f//AAAAAAAg6QD//f//AAH/4xcEAAMAAQAAAAAAAAAAAAAAAQAB//8ADwABAAAAAAAAAAAAAgAANzkBAAAAAAEAAAAAAAAAAAACAAA3OQEAAAAAAQAAAAAAAAAAAAIAADc5AQAAAAABAAD/wAJ3A8AABgAANwkBNwkBJwABh/55eAH//gF4OgGHAYd4/gH9/3gAAAEAAP+/BAADwAA4AAABHgEVFAYHDgEjIREUBgcOASMiJicuATURISImJy4BNTQ2Nz4BMyERNDY3PgEzMhYXHgEVESEyFhcD5g0NDQ0NIRX+uA0NDCIUFSAODQ3+uxQhDg4MDQ0NIRUBSA0NDCIUFSAODQ0BSBMiDQIBDCIUFSAODQ3+uxQiDQ4NDQ4OIRQBRQ0NDSEVFCENDgwBSBQhDg0NDQ0NIRX+uA0NAAABAAD/wAN3A8AAAgAAEwERAAN3AcACAPwAAAAAAQAA/8ACygPAAAIAABMRAQACygPA/AACAAAAAA0AAP/AA9sDwAAYABwAIAAkACgALAAwADQAOAA8AEAARABIAAABIzUjFSE1IxUjIgYVERQWMyEyNjURNCYjASM1MzUjNTM1IzUzEyM1MzUjNTM1IzUzEyM1MzUjNTM1IzUzEyM1MzUjNTM1IzUzA3Z1Vv5pVlgqPDwqAxAqOzsq/XGqqqqqqqrloaGhoaGh5KCgoKCgoO6qqqqqqqoDZFxcXFw8Kf0nKjw8KgLYKjz8malEkkSi/ZupRJJEov2bqUOTRKL9m6hEk0SiAAEAAP/AA8sDwAA0AAABMhYVFAYjIiYnBR4BFRQGBwU+ATMyFhUUBiMiJjU0NjclDgEjIiY1NDYzMhYXJS4BNTQ2MwMhR2NjRyZCF/6tAwMDAwFTF0ImR2NjR0dkAwP+rhhCJUdkZEcmQRgBUgMDZEcDwGRHRmQeG7cLFgwLFwq3Gh5jR0dkZEcLFwq3Gh9kR0dkHxq3CxYLR2QAAAEAAP/IA2QDuAAVAAAJAQYiJy4BNRE0Njc2MhcBHgEVFAYHA0X9Fw8gDg8QEA8OIA8C6Q4REQ4Biv4+CAgJHRADhBEdCAgI/j4IHRARHQgAAgAA/8AEAAPAAAMACAAAEyEVIQEzESMRAAQA/AABpba2Ahu2Alv8AAQAAAACAQwBSANjAlMADAAZAAABFAYjIiY1NDYzMhYVNyIGFRQWMzI2NTQmIwIXTjc4Tk44N07GN05ONzhOTjgBzTdOTjc4Tk44hk44N05ONzhOAAEAAP/BBu0DwAAtAAABLgEjIgYHIgcOAQcGMQEuASMiBgcOAR0BFBYXFBceARcWFzY3PgE3Njc+AT0BBu0UQzAUJBIBamr9aWr9sCpMIwoUCTQzCAc3Ntmjo9puaGjDXFxVNDUDUjc3BwdjY+5jYwIoMDABAxMzIDQJFAwCNTTOmpnMZ2JhuVdXUShNIxgAAAEBZACIAp0C0wAVAAABNyMwNDE0NjsBNSMiBh0BIxUzETMRAokUYhohJ048TWJidQGaYk4XEGJGQ05i/u4BEgACAAD/2AK2A8AAHgAqAAABIgcOAQcGFRQXHgEXFhcWMjc2Nz4BNzY1NCcuAScmAyImNTQ2MzIWFRQGAVtIPz9fGxsiIV8zMyQROxEkMzRfISIbHF4/P0ZGYmJGRWNjA8AbG18/P0hRXl6zTk4xGBgxTk6zXl5RSD8/Xxsb/f9jRUZiYkZGYgAABQAO/84D8gO7ABEAJwA5AGQAbwAAATc2FhcWFAcxBwYiJyY0NzkBJyYGByIUDwEGFhcWNjcxPgE/ATYmJxMHDgEXHgE/AT4BNzYmJy4BBwMWFAcGIi8BBxYGBwYmLwEHFgYHMQYiLwEmNDc+ARcBJyY0Nz4BMzIWFwEFJwcXFjI3NjQnMQLNaA8nDAsLZw4nDg4OAxElCAEBOwcMEA4eCwMFAjsHDBH0iRENCQghEYoEBwMOAg4KGwweDw8OKQ4djygVODJ6K0eFDAMNDikOkQ4PDSMPAQodDg4HEgoJEgcCIf66B4ktFDoVFBQC0WcMBQ8NIQ1nDg4OJw7qCQwSAQGKECMJBwULAwcEihEiCf7lOwkkEhANBjwBBQQOJw0JBQX+oQ4pDw4OHT84iCgkCixGOw4lDQ4OkQ8oDg0CCgJWHQ4pDwYICAb935cGPSwVFRQ6FQAAAAAFAPYAmANAAuMASACSAJ4AqgC2AAABMhYXHgEXHgEXHgEXHgEXFhQVHAEHDgEHDgEHDgEHDgEHDgEjIiYnLgEnLgEnLgEnLgEnLgE1NDY3PgE3PgE3PgE3PgE3PgEzNSIGBw4BBw4BBw4BBw4BBw4BFRQWFx4BFx4BFx4BFx4BFx4BMzI2Nz4BNz4BNz4BNz4BNzY0NTwBJy4BJy4BJy4BJy4BJy4BIzEVIgYVFBYzMjY1NCYHIiY1NDYzMhYVFAY3FAYjIiY1NDYzMhYCGzskGBUZCAoQCAcLBAIHAQEBAQYDBAsHCBAKCBkVGCQ7OiUXFhkIChAHCAoEAwYBAQEBAQEGAwQKCAcQCggZFhclOjwlGBciDg4ZDQwQBQYHAQEBAQEBBwYFEAwNGQ4OIhcYJTw8JhcYIQ4PGQwMEAYFBwIBAQIHBQYQDAwZDw4iFxcmPD5YWD4+WVk+KDk5KCg6OpgVDg8VFQ8OFQKuAQEBBgMECggHEAsHGhUXJTo7JRcVGgcLDwgICgQDBgEBAQEBAQYDBAoICA8LCBkVFyU7OiUXFhkHCxAHCAoEAwYBAQE1AQEBBwYFEAwNGQ4OIhcYJjs8JhcYIQ4PGQwNDwYFCAEBAQEBAQcGBhAMDBkPDiEYFyY8OyYYFyIODhkNDBAFBgcBAQGPWD4/WFg/Plj4OSgpOTkpKDn+DxQUDw8UFAADAQwAoANMAusAFAAYACQAAAEzFT4BMzIWHQEjNTQmIyIGHQEjEQczESMTFAYjIiY1NDYzMhYB2XUMPS1cLHUMLzsTdcR2doApHBwoKBwcKQInOxgjYU/WwyJAPCbDAYcB/nkCBhwoKBwdKCgAAAAAAQEkAKwC+QL3AEkAAAEUBw4BBwYjIiYnMAYHDgEjIjY3Njc+ATc2MTAmNTQ2MzIWBw4BBwYWMzI2NTQmIyIGFRQWFRQGIyImNTQ3PgE3NjMyFx4BFxYVAvgMDC4hICkbPggQBB0kDg8PCQMICBIHBxQ0GxcVBQ8RBwcnGjBFRERNdycYDw8rERFBMC88MCgnOQ8QAjMxKys/EhIWETALSUBbKQoeHj4ZGCMYJSknEzsbIBsfZ0k3UVxALCcPDgVKKyQmJj8UFBARNiQjJQAAAAEAAP/AA3cDwAACAAAJAREDd/yJAcACAPwAAAACAAD/wALqA8AAAwAHAAATMxEjATMRIwC0tAI2tLQDwPwABAD8AAAAAQE4AJYDFQLmACcAAAEwFhcwBw4BFxYXMDY3PgE3NicuAScmIzA2NycwBgcwJicwBhcwJgcBODUnFxcDLCyFNxkZYwkFExI3Hh4TOioLRCcRTiUHWU4CZ2MWISFlPTw4Ig0OQ10uIiEsCgsqASMVKW1EV0IdAwAABAAA/8ACmAPAABEAIwAxAD8AACUxFAYjISImNRE0NjMhMhYVEScRNCYjISIGFREUFjMhMjY1MQUzMjY1NCYrASIGFRQWEzMyNjU0JisBIgYVFBYCmDsq/jEpOzspAdApO0AfFP5OFB8fFAGyFR7+v2kLEBALaQsPDwtpBAUFBGkDBQUlKjs7KgM2Kjs7KvzKZQKqFRsbFf1WFB4eFJIPCwsPDwsLDwOIBQMEBAQEAwUAAAAAAQAA/8AENQPAAAkAAAETDQETJQUTLQECGocBlP6/c/60/rNz/r8BlAPA/oMK9v595eUBg/YKAAABAAD/vwQAA8AAOAAAAR4BFRQGBw4BIyERFAYHDgEjIiYnLgE1ESEiJicuATU0Njc+ATMhETQ2Nz4BMzIWFx4BFREhMhYXA+YNDQ0NDSEV/rgNDQwiFBUgDg0N/rsUIQ4ODA0NDSEVAUgNDQwiFBUgDg0NAUgTIg0CAQwiFBUgDg0N/rsUIg0ODQ0ODiEUAUUNDQ0hFRQhDQ4MAUgUIQ4NDQ0NDSEV/rgNDQAAAwEXAJUDYALrADYAZABxAAABPAE1NDYzHgEXHgEXHgEXHgEXHgEXFAYjKgEjIiY1NCYnLgEnLgEnLgEnLgEnLgEjIiY1NiY1FTwBNTQ2Mx4BFx4BFx4BFx4BFxQGIyoBIyImNS4BJy4BJy4BJy4BByImNTwBNRcyFhUUBiMiJjU0NjMBFwIDJkklLlQoIj0bJDYSDxABAgIaNBoEAQMDBRIODiUXGDYeGzofHj4gBAMBAQIDIkMgKEYdHCoODAwBAgMaMxoDAgESEhEuHRk3HgcPBwQBTiAtLSEgLS0hArENGg0EAgEKCg0nGxc4ICtgNS5fMQMCAwITJBIhPh4fOxobLhMRGwkJCQMEDRkNygwaDQQCAQ0MEDAfHkQmIEIiAwIDAiVDIB4wEhETBQEBAQMDDRoNsy4hIC8uISItAAACAAD/1APsA8AAJABCAAAlDgEjIicuAScmNTQ3PgE3NjMyFx4BFxYVFAYHARYUDwEGIicBJzI3PgE3NjU0Jy4BJyYjIgcOAQcGFRQXHgEXFjMxAoIyeEFVSkpuICAgIG5KSlRUSkpuICAnJAELFBQBEzgT/vXrQDk4VBkYGBlUOThBQDk4VRgYGBhVODlA3iMoICBvSkpUVEpKbiAgICBuSkpUQngy/vUTOBMBFBQBCxQYGVQ5OEBBODlUGRgYGFU4OUBAOTlUGBkAAAACAAD/wANJA70ALwBdAAAFIiYnJS4BJy4BNxM+ARceAQ8BJT4BNz4BLwEmNjc2Fh8BFgYHDgEHBRceAQcOASMBIiYvASY3PgE3NjclJy4BNz4BFwUeARceAQcDDgEnLgE/AQUOAR8BFgYHDgEjAicECQT+4QkPBAQBA2QHJRQTEgc8ASweMA0HCwkqCQ4TEycJKRIGFxZGLP7XqxMSBwUbD/5eDhkHSBECAyckJDABKqsUEggHJRQBHwkPBAQBA2QHJhMUEgc9/tM4PBFICQ4TBQsFQAIBZgMOCQkUCQEaFBIHByYTq48OKhgMKBVWEycJCQ4TViVXKSY/FI49ByUUDxIBAg8OlyUpKU0iIReOPQclFBMSB2YDDgkJFAn+5hQSBwcmE6uPG1sjlxMnCQIDAAAAAAEA0wClAx0CjgA/AAABDgEHPgE3DgEHJy4BIyIGFRQWFwciJicOARUUFhcuAScUFhcGJiceARcOASMiJiceATMyNz4BNzY1PAE1PgE3Ax0QLBISIgYOIBEPECQaMkMBAQJYeiwIDCMYDyAMYCkPTQYMQigfSisHDgcoaDNTPz5TFRURHQwCRQcQAgsuFQgUBg8RFkMyBgkEASs3Dh0QHzIRAQwHLFUIDg0BJCkBGBkBARoZHh1fPDs8BAsEDSESAAIAAf/BBAADvwBIAGQAAAE0NjcuAScGJicuATcuAScOASMiJicOAQcWBgcOAScOAQceARUUBgceARc2FhceAQceARc+ATMyFhc+ATcmNjc+ARc+ATcuATUBIicuAScmNTQ3PgE3NjMyFx4BFxYVFAcOAQcGA542LAgVDTFSIiIVDRgzGxpWMDBXGho0GA0VISJSMg0VCCw2NiwIFQ0xUiIiFQ0YMxoaWC8vWBoaNBcNFSIiUjENFgcsNv5jNjAwRxUUFBVHMDA2NjAwRxUUFBVHMDABwDBLGho0Fw0hIiJSMQ0WByw2NiwIFQ0xUiIiIQ0YMxoaSzAwVxoaNBcNFSIiUjENFgcsNjYsCBUNMVIiIiENGDMaGksw/voUFUcwMDY2MDBHFRQUFUcwMDY2MDBHFRQAAQDoAKYDMAKgADUAABMwNhcWFx4BFxYXHgEzMjY3Njc+ATc2NzYnJgYHBgc2FgcOASMiJicmJy4BJyYjIgYxDgExF/w/DwcLCxsODg0ONh0dNRAUHB04GRgMDCIiYC0uCjIrDw4xDwwgDgcIBxcPEBUsIik5FAIELCwdJyhSJiYaHR0aDRAcHFE2N0RFHyAFISI8HkArLEk9OBolJEQXGBQhVBMAAAAAAgDxAPwDPAKXADYAOQAAATAmJy4BJy4BMTAGBw4BBw4BMTAGHQEUFjEwFhceARcWFx4BMzIxMDI3PgE3PgExMDY9ATQmMQU1FwM2Cg0RIQk9kJA+CCIRDQoGBgoNESYKEiEiQhgZkD0JIRENCgYG/pOcAj4sDhEHAQQCAgQBBxEOLDQjOyM1LQ4RBgIBAQEBBQEGEQ4tNSM7JDPDnE4AAAEAAAABAAAUoBwVXw889QALBAAAAAAA3/Yu8AAAAADf9i7wAAD/vwbtA8AAAAAIAAIAAAAAAAAAAQAAA8D/wAAABu0AAAAABu0AAQAAAAAAAAAAAAAAAAAAACEEAAAAAAAAAAAAAAACAAAAAnkAAAQAAAADdwAAAsoAAAPbAAADzAAAA2QAAAQAAAAEdgEMBu0AAAR2AWQCtgAAA/8ADgR2APYEdgEMBHYBJAN3AAAC6gAABHYBOAKYAAAENQAABAAAAAR2ARcEAAAAA1gAAAR2ANMEAAABBHYA6AR2APEAAAAAAAoAFAAeADQAigCYAKYBDAFaAYIBmAHAAggCKAJsAxgEIARYBMAEzgTiBR4FeAWUBeoGigbwB4oH6giECNgJKAABAAAAIQC3AA0AAAAAAAIAAAAAAAAAAAAAAAAAAAAAAAAADgCuAAEAAAAAAAEABwAAAAEAAAAAAAIABwBgAAEAAAAAAAMABwA2AAEAAAAAAAQABwB1AAEAAAAAAAUACwAVAAEAAAAAAAYABwBLAAEAAAAAAAoAGgCKAAMAAQQJAAEADgAHAAMAAQQJAAIADgBnAAMAAQQJAAMADgA9AAMAAQQJAAQADgB8AAMAAQQJAAUAFgAgAAMAAQQJAAYADgBSAAMAAQQJAAoANACkaWNvbW9vbgBpAGMAbwBtAG8AbwBuVmVyc2lvbiAxLjAAVgBlAHIAcwBpAG8AbgAgADEALgAwaWNvbW9vbgBpAGMAbwBtAG8AbwBuaWNvbW9vbgBpAGMAbwBtAG8AbwBuUmVndWxhcgBSAGUAZwB1AGwAYQByaWNvbW9vbgBpAGMAbwBtAG8AbwBuRm9udCBnZW5lcmF0ZWQgYnkgSWNvTW9vbi4ARgBvAG4AdAAgAGcAZQBuAGUAcgBhAHQAZQBkACAAYgB5ACAASQBjAG8ATQBvAG8AbgAuAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==") format("truetype");
    font-weight: normal;
    font-style: normal;
    font-display: block;
  }
  /* ----- GENERAL ----- */
  @-ms-viewport {
    width: device-width;
  }
  body {
    -webkit-text-size-adjust: none;
    -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
  }
  
  body.nav-open {
    overflow: hidden;
  }
  
  body:before {
    content: "desktop";
    display: none;
  }
  
  *[data-toggle=false],
  *[data-content=""] {
    display: none !important;
  }
  
  .hide1024 {
    display: none;
  }
  
  .show1024 {
    display: block;
  }
  
  .flex-cont {
    display: -ms-flexbox;
    display: -webkit-box;
    display: flex;
  }
  
  .inline-flex-cont {
    display: -webkit-inline-box;
    display: -ms-inline-flexbox;
    display: inline-flex;
  }
  
  .jc-sb {
    -webkit-box-pack: justify;
        -ms-flex-pack: justify;
            justify-content: space-between;
  }
  
  .jc-sa {
    -ms-flex-pack: distribute;
        justify-content: space-around;
  }
  
  .jc-c {
    -webkit-box-pack: center;
        -ms-flex-pack: center;
            justify-content: center;
  }
  
  .jc-fs {
    -webkit-box-pack: start;
        -ms-flex-pack: start;
            justify-content: flex-start;
  }
  
  .jc-fe {
    -webkit-box-pack: end;
        -ms-flex-pack: end;
            justify-content: flex-end;
  }
  
  .ai-fs {
    -webkit-box-align: start;
        -ms-flex-align: start;
            align-items: flex-start;
  }
  
  .ai-fe {
    -webkit-box-align: end;
        -ms-flex-align: end;
            align-items: flex-end;
  }
  
  .ai-c {
    -webkit-box-align: center;
        -ms-flex-align: center;
            align-items: center;
  }
  
  .ai-b {
    -webkit-box-align: baseline;
        -ms-flex-align: baseline;
            align-items: baseline;
  }
  
  .as-s {
    -ms-flex-item-align: stretch;
        align-self: stretch;
  }
  
  .as-c {
    -ms-flex-item-align: center;
        align-self: center;
  }
  
  .fd-col {
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
        -ms-flex-direction: column;
            flex-direction: column;
  }
  
  .fw-w {
    -ms-flex-wrap: wrap;
        flex-wrap: wrap;
  }
  
  .sticky {
    position: fixed;
  }
  
  /* HIDDEN ELEMENTS */
  #sw-mystart-outer,
  #sw-footer-outer,
  .dropdown,
  .hidden {
    display: none;
  }
  
  #gb-icons-sticky {
    opacity: 0;
  }
  
  .ui-clear:after {
    content: "";
  }
  
  .content-wrap {
    max-width: 1202px;
    padding: 0px 30px;
    margin: 0 auto;
    position: relative;
  }
  
  button {
    cursor: pointer;
    background: transparent;
    border: 0px;
    padding: 0px;
    margin: 0px;
  }
  
  h1, h2, h3, h4 {
    margin: 0;
  }
  
  li {
    list-style: none;
  }
  
  li::marker {
    display: none;
  }
  
  #gb-page {
    width: 100%;
    position: relative;
    overflow: hidden;
    background: #fff;
  }
  #gb-page::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-image: url("https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/bg-pattern.svg");
    background-repeat: no-repeat;
    position: fixed;
    background-repeat-x: repeat;
  }
  
  .dropdown {
    background: #fff;
    position: absolute;
  }
  
  .custom-dropdown {
    position: relative;
    height: 100%;
  }
  
  .custom-dropdown .dropdown {
    display: none;
    position: absolute;
    top: auto;
    z-index: 8002;
    list-style: none;
    margin: 0;
    width: -webkit-fit-content;
    width: -moz-fit-content;
    width: fit-content;
    color: #333;
    -webkit-box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.3);
            box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.3);
    -webkit-box-sizing: border-box;
            box-sizing: border-box;
    overflow: auto;
  }
  
  .custom-dropdown ul {
    margin: 0;
    padding: 0;
  }
  
  .custom-dropdown ul li a {
    display: block;
    width: 100%;
    padding: 5px 30px;
    font-family: "Sarabun", sans-serif;
    font-weight: 600;
    font-size: 0.875rem;
    color: #000;
    text-decoration: none;
    -webkit-box-sizing: border-box;
            box-sizing: border-box;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  
  .custom-dropdown ul li a:hover,
  .custom-dropdown ul li a:focus {
    background: #305288;
  }
  
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  /* ------------------------- ### HEADER ### ------------------------------- */
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  .mystartbar {
    height: 54px;
    background: #4567A9;
    position: relative;
    z-index: 6; =
  }
  .mystartbar .content-wrap {
    height: 100%;
  }
  
  .nav-section {
    position: relative;
    height: 100%;
  }
  .nav-section.right {
    padding-right: 30px;
    margin-left: -19px;
  }
  .nav-section.right::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background: #305288;
    right: 0;
    left: auto;
    width: 100vw;
    -webkit-clip-path: polygon(0 0, 100% 0%, 98% 100%, 0% 100%);
            clip-path: polygon(0 0, 100% 0%, 98% 100%, 0% 100%);
  }
  .nav-section.left {
    margin-right: 13px;
  }
  .nav-section .cs-button-section {
    padding-right: 70px;
    margin-top: 86px;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
        -ms-flex-direction: column;
            flex-direction: column;
    z-index: 10;
  }
  
  .nav-group {
    position: relative;
    height: 100%;
  }
  .nav-group.right {
    padding-left: 20px;
  }
  .nav-group.right::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background: #4567A9;
    height: 100%;
    width: 10px;
    position: absolute;
    width: 20px;
    position: absolute;
    left: -20px;
    top: 0;
    -webkit-clip-path: polygon(80% 0%, 100% 0%, 18% 100%, 0% 100%);
            clip-path: polygon(80% 0%, 100% 0%, 18% 100%, 0% 100%);
  }
  .nav-group a {
    text-decoration: none;
    height: 100%;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
        -ms-flex-align: center;
            align-items: center;
  }
  .nav-group span {
    color: #FFFFFF;
    font-size: 0.875rem;
    text-transform: uppercase;
    font-weight: 700;
    padding: 0 20px;
    font-family: "Nunito", sans-serif;
  }
  .nav-group .extra-nav-links {
    position: relative;
  }
  .nav-group .extra-nav-links::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    width: 0;
    height: 0;
    left: calc(50% - 9px);
    top: 0;
    border-left: 9px solid transparent;
    border-right: 9px solid transparent;
    border-top: 9px solid transparent;
    background-color: transparent;
    -webkit-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
  }
  .nav-group .extra-nav-links:hover::before,
  .nav-group .extra-nav-links:focus::before {
    border-left: 9px solid transparent;
    border-right: 9px solid transparent;
    border-top: 9px solid #fff;
    background-color: transparent;
  }
  
  .header-logo {
    padding: 23px 0 21px 0;
    min-height: 165px;
  }
  
  #gb-logo {
    opacity: 1;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  
  .main-header {
    background: #4567A9;
  }
  
  /* GENERAL MYSTART ITEMS */
  .mystart-selector {
    position: relative;
    height: 100%;
  }
  .mystart-selector span {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
        -ms-flex-align: center;
            align-items: center;
    position: relative;
    padding: 0px 5px;
  }
  .mystart-selector span::after {
    font-family: "template-icon" !important;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    color: #fff;
    content: "\e909";
    font-size: 8px;
    padding-left: 9px;
  }
  .mystart-selector[aria-expanded=true] span::after {
    -webkit-animation: rotate-center 0.6s ease-in-out both;
    animation: rotate-center 0.6s ease-in-out both;
    -webkit-transform-origin: 72.4% 45.6%;
            transform-origin: 72.4% 45.6%;
  }
  .mystart-selector[aria-expanded=false] span::after {
    -webkit-animation: rotate-reverse 0.6s ease-in-out both;
    animation: rotate-reverse 0.6s ease-in-out both;
    -webkit-transform-origin: 72.4% 45.6%;
            transform-origin: 72.4% 45.6%;
  }
  
  #cs-ab {
    position: relative;
  }
  #cs-ab #ab-dropdown {
    min-width: 300px;
    padding: 0;
    left: -37px;
  }
  
  #cs-ar {
    position: relative;
    margin-left: 8px;
    z-index: 1;
  }
  #cs-ar .mystart-selector {
    padding-left: 38px;
  }
  #cs-ar .mystart-selector::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background: #FFDD00;
    top: 0;
    -webkit-clip-path: polygon(7% 0, 100% 0%, 92% 100%, 0% 100%);
            clip-path: polygon(7% 0, 100% 0%, 92% 100%, 0% 100%);
    width: 266px;
    height: 61px;
    left: 0;
  }
  #cs-ar span {
    color: #221F1F;
    text-transform: uppercase;
    font-family: "Barlow Condensed", sans-serif;
    font-weight: 500;
    font-size: 1.5625rem;
    letter-spacing: -0.3px;
  }
  #cs-ar span::after {
    color: #221F1F;
    padding-left: 8px;
  }
  
  #ar-dropdown {
    width: 100%;
    padding: 20px 0;
    background: #FFDD00;
    top: calc(100% + 7px);
  }
  #ar-dropdown ul li a span {
    font-family: "Sarabun", sans-serif;
    font-weight: 600;
    font-size: 0.875rem;
    color: #000;
    text-decoration: none;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  #ar-dropdown ul li a:hover,
  #ar-dropdown ul li a:focus {
    background: rgba(255, 255, 255, 0.56);
  }
  
  /* GroupEnd */
  /* SEARCH */
  #gb-search-submit {
    height: 60px;
    width: 60px;
    position: relative;
    border-radius: 50%;
    border: 4px solid white;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  #gb-search-submit::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background: #5F9F43;
    border-radius: 50%;
    top: -2px;
    left: -2px;
    border: 2px solid transparent;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  #gb-search-submit::after {
    font-family: "template-icon" !important;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    content: "\e917";
    color: #fff;
    position: absolute;
    left: 14px;
    font-size: 26px;
    top: 13px;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  #gb-search-submit:hover, #gb-search-submit:focus {
    border: 4px solid #fff;
  }
  #gb-search-submit:hover::before, #gb-search-submit:focus::before {
    background: #fff;
    border: 2px solid #5F9F43;
  }
  #gb-search-submit:hover::after, #gb-search-submit:focus::after {
    color: #5F9F43;
  }
  
  #search-input-wrapper {
    padding-bottom: 12px;
    min-height: 40px;
  }
  
  #gb-search-input {
    position: relative;
    background: transparent;
    border: none;
    min-height: 40px;
    color: #fff;
    font-family: "Nunito", sans-serif;
    font-size: 0.875em;
    font-weight: 700;
    padding-left: 10px;
  }
  #gb-search-input.focus {
    background: #fff;
    color: #4567AA;
  }
  
  /* GroupEnd */
  /* TRANSLATE */
  #gb-translate {
    position: relative;
    width: 100%;
  }
  
  .translate-icon {
    position: relative;
    height: 55px;
    min-width: 55px;
    padding-right: 5px;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  .translate-icon::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background: #4567AA;
    border-radius: 50%;
    height: 55px;
    width: 55px;
    top: -2px;
    left: -2px;
    border: 2px solid transparent;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  .translate-icon::after {
    font-family: "template-icon" !important;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    content: "\e918";
    position: absolute;
    left: 17px;
    font-size: 28px;
    color: #fff;
    top: calc(100% - 41px);
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  .translate-icon.hover::before {
    background: #fff;
    border: 2px solid #4567A9;
  }
  .translate-icon.hover::after {
    color: #4567A9;
  }
  
  .translate-selector {
    width: 100%;
    height: 100%;
    color: #4567AA;
    font-family: "Nunito", sans-serif;
    font-size: 0.875em;
    font-weight: 700;
    text-align: left;
    padding-left: 5px;
  }
  .translate-selector:hover {
    color: black;
  }
  
  #translate-dropdown {
    padding: 20px;
    -webkit-box-sizing: border-box;
            box-sizing: border-box;
    text-align: center;
    width: -webkit-fit-content;
    width: -moz-fit-content;
    width: fit-content;
    max-height: 86px;
    top: 76%;
    left: 64px;
  }
  
  #google_translate_element .goog-te-gadget,
  #google_translate_element .goog-logo-link,
  #google_translate_element .goog-logo-link:link,
  #google_translate_element .goog-logo-link:visited,
  #google_translate_element .goog-logo-link:hover,
  #google_translate_element .goog-logo-link:active {
    padding: 0;
    margin-top: 0;
  }
  #google_translate_element .goog-te-gadget a,
  #google_translate_element .goog-logo-link a,
  #google_translate_element .goog-logo-link:link a,
  #google_translate_element .goog-logo-link:visited a,
  #google_translate_element .goog-logo-link:hover a,
  #google_translate_element .goog-logo-link:active a {
    font-size: 12px;
    font-weight: bold;
    color: #444;
    text-decoration: none;
  }
  
  #google_translate_element {
    text-transform: none;
    position: relative;
  }
  
  #school-list {
    background: #4567A9;
    min-width: 226px;
    max-width: 230px;
    padding: 10px 0;
  }
  #school-list li a {
    color: #fff;
  }
  
  /* GroupEnd */
  /* CHANNEL BAR */
  #gb-channel-bar {
    position: relative;
  }
  
  #channel-navigation {
    min-height: 55px;
  }
  
  #sw-channel-list-container {
    display: block;
    width: auto;
  }
  
  ul.sw-channel-dropdown {
    background: rgba(69, 103, 170, 0.95);
    border: transparent;
    padding: 10px 20px;
    top: 100%;
    left: -17px;
    margin-top: 5px;
  }
  
  ul.sw-channel-dropdown li a {
    color: #fff;
    border-top: 1px solid #A7C6E7;
    font-size: 1rem;
    font-family: "Nunito", sans-serif;
    font-weight: 500;
    padding-left: 0;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  
  ul.sw-channel-dropdown li a:hover,
  ul.sw-channel-dropdown li a:focus {
    color: #FFE947;
    background: rgba(69, 103, 170, 0.95);
  }
  
  ul.sw-channel-dropdown li:first-child a {
    border-top: none;
  }
  
  ul.sw-channel-dropdown li {
    width: -webkit-fit-content;
    width: -moz-fit-content;
    width: fit-content;
  }
  
  ul.sw-channel-list {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
        flex-wrap: wrap;
    margin: 0;
    padding: 0;
    border: none;
  }
  
  ul.sw-channel-list li.sw-channel-item {
    position: relative;
    float: none;
    height: auto;
    margin: 0;
    list-style: none outside none;
  }
  ul.sw-channel-list li.sw-channel-item::before {
    display: block;
    content: "";
    position: absolute;
    top: auto;
    width: 100%;
    height: 5px;
    background: transparent;
    bottom: -5px;
    left: 0;
  }
  
  li.sw-channel-item > a {
    display: block;
    height: 100%;
    color: #FFF;
    padding: 10px 15px;
    text-decoration: none;
    -webkit-box-sizing: border-box;
            box-sizing: border-box;
    font-family: "Nunito", sans-serif;
    font-size: 1rem;
    font-weight: 700;
    text-transform: uppercase;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
        -ms-flex-align: center;
            align-items: center;
  }
  li.sw-channel-item > a::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    width: 0;
    height: 0;
    left: calc(50% - 9px);
    top: 0;
    border-left: 9px solid transparent;
    border-right: 9px solid transparent;
    border-top: 9px solid transparent;
    background-color: transparent;
    -webkit-transition: all 0.3s ease 0s;
    transition: all 0.3s ease 0s;
  }
  
  li.sw-channel-item > a span {
    display: block;
    padding: 0;
  }
  
  li.sw-channel-item.hover > a,
  li.sw-channel-item.active > a,
  li.sw-channel-item > a:focus {
    color: #FFF;
  }
  li.sw-channel-item.hover > a::before,
  li.sw-channel-item.active > a::before,
  li.sw-channel-item > a:focus::before {
    border-left: 9px solid transparent;
    border-right: 9px solid transparent;
    border-top: 9px solid #fff;
    background-color: transparent;
  }
  
  /* MEGA MENU */
  ul.sw-channel-list li.sw-channel-item.csmc ul.sw-channel-dropdown {
    width: -webkit-fit-content;
    width: -moz-fit-content;
    width: fit-content;
    padding: 0 4px 10px 14px;
    top: 100%;
    left: -17px;
  }
  ul.sw-channel-list li.sw-channel-item.csmc ul.sw-channel-dropdown li {
    padding: 6px 0;
    border-top: 1px solid #ccc;
    width: 100%;
  }
  ul.sw-channel-list li.sw-channel-item.csmc ul.sw-channel-dropdown li a {
    width: 100%;
    padding-right: 0;
  }
  ul.sw-channel-list li.sw-channel-item.csmc ul.sw-channel-dropdown .csmc-section-columns {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    border-top: 1px solid transparent;
    padding-top: 0;
  }
  ul.sw-channel-list li.sw-channel-item.csmc ul.sw-channel-dropdown .csmc-column {
    padding: 12px 16px 0px;
    min-width: 280px;
  }
  ul.sw-channel-list li.sw-channel-item.csmc ul.sw-channel-dropdown .csmc-section.column-heading {
    padding-bottom: 16px;
    padding-top: 0px;
    border-top: 1px solid transparent;
  }
  ul.sw-channel-list li.sw-channel-item.csmc ul.sw-channel-dropdown .csmc-section.column-heading h2 {
    font-family: "Nunito", sans-serif;
    font-weight: 700;
    color: #FFE947;
    font-size: 1.1875rem;
  }
  
  /* GroupEnd */
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  /* ------------------------ ### HOMEPAGE ### ------------------------------ */
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  /* MMG AREA*/
  #hp-slideshow-outer {
    position: relative;
  }
  
  #hp-slideshow {
    overflow: hidden;
    --mask: conic-gradient(from -45deg at bottom,#0000,#000 1deg 89deg,#0000 90deg) 50%/16.00px 100%;
    -webkit-mask: var(--mask);
    mask: var(--mask);
    -webkit-mask-repeat: repeat-x;
  }
  #hp-slideshow .mmg-description-wrapper {
    position: absolute;
    bottom: 93px;
    left: 0;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    width: 50vw;
    max-width: 580px;
  }
  #hp-slideshow .mmg-description-outer {
    position: relative;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    max-height: 100%;
    padding: 24px 43px 29px;
    background: rgba(255, 255, 255, 0.87);
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    z-index: 10;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  #hp-slideshow .mmg-description-title {
    font-family: "Nunito", sans-serif;
    font-size: 1.25rem;
    font-weight: 700;
    letter-spacing: 0.6px;
  }
  #hp-slideshow .mmg-description-caption {
    font-family: "Nunito", sans-serif;
    font-size: 0.875rem;
    font-weight: 600;
    letter-spacing: -0.1px;
    line-height: 24px;
  }
  #hp-slideshow .mmg-description-links {
    position: absolute;
    bottom: -28px;
    left: calc(12% - 23px);
  }
  #hp-slideshow .mmg-description-links .mmg-description-link {
    background: #4567A9;
    padding: 10px;
    color: #FFFFFF;
    border: 1px solid transparent;
    padding: 14px 20px;
    margin: 0;
    font-family: "Sarabun", sans-serif;
    font-weight: 600;
    text-decoration: none;
    font-size: 1rem;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  #hp-slideshow .mmg-description-links .mmg-description-link:hover, #hp-slideshow .mmg-description-links .mmg-description-linkfocus {
    background: #fff;
    color: #4567A9;
    border: 1px solid #4567A9;
  }
  #hp-slideshow .mmg-description-links a:nth-child(2) {
    margin-left: 5px;
  }
  #hp-slideshow .mmg-controls {
    position: absolute;
    top: -28px;
    left: calc(12% - 41px);
  }
  #hp-slideshow .mmg-controls .mmg-control {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    position: relative;
    top: auto;
    left: auto;
    right: auto;
    height: 40px;
    width: 40px;
    margin: 0 6px;
    background: none;
  }
  #hp-slideshow .mmg-controls .mmg-control::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background: #fff;
    border-radius: 50%;
    -webkit-box-sizing: border-box;
            box-sizing: border-box;
    -webkit-box-shadow: 0px 3px 6px #00000029;
            box-shadow: 0px 3px 6px #00000029;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  #hp-slideshow .mmg-controls .mmg-control::after {
    font-family: "template-icon" !important;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    position: relative;
    color: #000;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  #hp-slideshow .mmg-controls .mmg-control:hover::before, #hp-slideshow .mmg-controls .mmg-control:focus::before {
    background: #4567AA;
  }
  #hp-slideshow .mmg-controls .mmg-control:hover::after, #hp-slideshow .mmg-controls .mmg-control:focus::after {
    color: #fff;
  }
  #hp-slideshow .mmg-controls .mmg-control.back::after {
    content: "\e902";
  }
  #hp-slideshow .mmg-controls .mmg-control.back:hover::before, #hp-slideshow .mmg-controls .mmg-control.back:focus::before {
    background: #4567AA;
  }
  #hp-slideshow .mmg-controls .mmg-control.back:hover::after, #hp-slideshow .mmg-controls .mmg-control.back:focus::after {
    color: #fff;
  }
  #hp-slideshow .mmg-controls .mmg-control.play-pause.playing::after {
    content: "\e911";
  }
  #hp-slideshow .mmg-controls .mmg-control.play-pause.paused::after {
    content: "\e910";
  }
  #hp-slideshow .mmg-controls .mmg-control.next::after {
    content: "\e910";
  }
  #hp-slideshow .mmg-controls .mmg-control.play-pause span {
    display: none;
  }
  #hp-slideshow .mmg-container[data-active-slide-title=false][data-active-slide-caption=false][data-active-slide-link=false][data-active-slide-video=false] .mmg-description-outer {
    background: transparent;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  #hp-slideshow .mmg-container .mmg-description {
    padding: 0;
  }
  
  /* GroupEnd */
  /* MAIN HOMEPAGE */
  .hp-row {
    position: relative;
  }
  .hp-row.one {
    z-index: 5;
  }
  .hp-row.two {
    padding: 0px 0 30px 0;
  }
  .hp-row.three, .hp-row.five {
    background: #4567A9;
    position: relative;
  }
  .hp-row.three::before, .hp-row.five::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    top: auto;
    bottom: -10px;
    background: #4567A9;
    --mask: conic-gradient(from -45deg at bottom,#0000,#000 1deg 89deg,#0000 90deg) 50%/16.00px 100%;
    -webkit-mask: var(--mask);
    mask: var(--mask);
    -webkit-mask-repeat: repeat-x;
  }
  .hp-row.three {
    padding: 36px 0 44px;
  }
  .hp-row.five {
    padding: 27px 0 45px;
  }
  .hp-row.four {
    padding: 55px 0 63px;
  }
  .hp-row.four .content-wrap.columns {
    -ms-flex-wrap: wrap;
        flex-wrap: wrap;
    max-width: 1300px;
    -webkit-box-sizing: border-box;
            box-sizing: border-box;
  }
  .hp-row.four .content-wrap.extra-row {
    padding-top: 101px;
    max-width: 1208px;
  }
  .hp-row.four .hp-column {
    width: 33.333%;
    padding: 0 25px;
    -webkit-box-sizing: border-box;
            box-sizing: border-box;
  }
  .hp-row.six {
    padding: 12px 0 0px 0;
    position: relative;
    --mask: conic-gradient(from -45deg at bottom,#0000,#000 1deg 89deg,#0000 90deg) 50%/16.00px 100%;
    -webkit-mask: var(--mask);
    mask: var(--mask);
    -webkit-mask-repeat: repeat-x;
  }
  .hp-row.six::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background: white;
    background: -webkit-gradient(linear, left top, left bottom, color-stop(65%, rgba(255, 255, 255, 0)), to(rgba(89, 142, 246, 0.5)));
    background: linear-gradient(180deg, rgba(255, 255, 255, 0) 65%, rgba(89, 142, 246, 0.5) 100%);
    pointer-events: none;
  }
  
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  /* ------------------------### REGION APPS ###---------------------------- */
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  .hp [data-region=b] .ui-widget.app.upcomingevents .ui-widget-detail {
    background: #305288;
    padding: 20px 30px;
  }
  .hp [data-region=b] .ui-widget.app.upcomingevents .ui-widget-detail::after {
    display: none;
  }
  .hp [data-region=b] .ui-widget.app.upcomingevents .ui-widget-detail .ui-articles li {
    padding: 0;
    margin: 0;
    background: transparent;
  }
  .hp [data-region=b] .ui-widget.app.upcomingevents .ui-widget-detail .ui-articles .adam-hug {
    padding: 0;
  }
  .hp [data-region=b] .ui-widget.app.upcomingevents .ui-widget-detail .ui-articles .adam-hug .joel-month {
    padding: 0;
    font-size: 1.5rem;
  }
  .hp [data-region=b] .ui-widget.app.upcomingevents .ui-widget-detail .ui-articles .adam-hug .jeremy-date {
    font-size: 1.5rem;
  }
  .hp [data-region=b] .ui-widget.app.upcomingevents .ui-widget-detail .ui-articles .upcoming-column.right {
    padding-top: 0px;
  }
  .hp [data-region=b] .ui-widget.app.upcomingevents .ui-widget-detail .ui-articles .sw-calendar-block-time {
    display: none;
  }
  .hp [data-region=b] .ui-widget.app.upcomingevents .ui-widget-detail .ui-articles .sw-calendar-block-title {
    color: #FFE947;
    padding: 0;
  }
  .hp [data-region=b] .ui-widget.app.upcomingevents .ui-widget-detail .ui-articles .sw-calendar-block-title a {
    font-size: 1.5rem;
    padding: 0;
  }
  .hp [data-region=b] .ui-widget.app.upcomingevents .ui-widget-detail .view-calendar-link {
    display: -webkit-inline-box;
    display: -ms-inline-flexbox;
    display: inline-flex;
    background: transparent;
    border: 1px solid #fff;
  }
  .hp [data-region=b] .ui-widget.app.upcomingevents .ui-widget-footer {
    display: none;
  }
  .hp [data-region=c] .ui-widget.app .ui-widget-header h1 {
    color: #fff;
    font-size: 4.375em;
  }
  .hp [data-region=c] .ui-widget.app.upcomingevents .ui-widget-header.icon::before {
    content: "\e904";
    color: #fd0;
    top: 13px;
    left: 0;
  }
  .hp [data-region=c] .ui-widget.app.upcomingevents .ui-widget-detail {
    margin-top: -10px;
    margin-right: -13px;
  }
  .hp [data-region=c] .ui-widget.app.upcomingevents .ui-articles li {
    background: #fff;
    -webkit-box-flex: 1;
        -ms-flex: 1 1;
            flex: 1 1;
    padding: 20px;
    margin-top: 0;
    margin-bottom: 0;
  }
  .hp [data-region=c] .ui-widget.app.upcomingevents .ui-articles li:nth-child(n+6) {
    display: none;
  }
  .hp [data-region=c] .ui-widget.app.upcomingevents .ui-articles .ui-article-title.sw-calendar-block-date {
    color: #221F1F;
  }
  .hp [data-region=c] .ui-widget.app.upcomingevents .ui-articles .upcoming-column.right .ui-article-description {
    color: #221F1F;
  }
  .hp [data-region=c] .ui-widget.app .more-link,
  .hp [data-region=c] .ui-widget.app .view-calendar-link,
  .hp [data-region=c] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss {
    background: #fff;
    color: #4666A9;
    position: relative;
    margin-right: 66px;
  }
  .hp [data-region=c] .ui-widget.app .more-link:hover, .hp [data-region=c] .ui-widget.app .more-link:focus,
  .hp [data-region=c] .ui-widget.app .view-calendar-link:hover,
  .hp [data-region=c] .ui-widget.app .view-calendar-link:focus,
  .hp [data-region=c] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss:hover,
  .hp [data-region=c] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss:focus {
    border: 1px solid #fff;
    color: #fff;
    background: #4666A9;
  }
  .hp [data-region=c] .ui-widget.app .more-link::before,
  .hp [data-region=c] .ui-widget.app .view-calendar-link::before,
  .hp [data-region=c] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-image: url("https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/btn-stars.svg");
    right: -75px;
    left: auto;
    background-repeat: no-repeat;
    width: 66px;
    height: 46px;
  }
  .hp [data-region=d] .ui-widget.app .ui-widget-header.icon::before {
    content: "\e905";
    font-size: 49px;
    left: 5px;
    top: -6px;
  }
  .hp [data-region=d] .ui-widget.app .ui-widget-header.icon::after {
    display: block;
  }
  .hp [data-region=e] .regionE-button {
    padding: 5px 0px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
  }
  .hp [data-region=e] .regionE-button a {
    font-family: "Nunito", sans-serif;
    font-weight: 400;
    text-decoration: none;
    color: #fff;
    padding: 15px 15px 12px;
    background: #4666A9;
    text-align: center;
    text-transform: capitalize;
    border: 1px solid transparent;
    -webkit-box-sizing: border-box;
            box-sizing: border-box;
    opacity: 1;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  .hp [data-region=e] .regionE-button a span {
    height: auto;
  }
  .hp [data-region=e] .regionE-button a:hover, .hp [data-region=e] .regionE-button a:focus {
    border: 1px solid #4666A9;
    color: #4666A9;
    background: #fff;
  }
  .hp [data-region=e] .ui-widget.app .ui-widget-header {
    padding-bottom: 30px;
  }
  .hp [data-region=e] .ui-widget.app .ui-widget-header.icon::before {
    content: "\e906";
    left: 18px;
    top: 1px;
    font-size: 38px;
  }
  .hp [data-region=e] .ui-widget.app .ui-widget-header.icon::after {
    display: block;
  }
  .hp [data-region=f] .ui-widget.app .ui-widget-header {
    padding-bottom: 15px;
  }
  .hp [data-region=f] .ui-widget.app .ui-widget-header.icon {
    padding-left: 79px;
  }
  .hp [data-region=f] .ui-widget.app .ui-widget-header.icon::before {
    content: "\e914";
    left: 15px;
    top: -5px;
    font-size: 34px;
  }
  .hp [data-region=f] .ui-widget.app .ui-widget-header.icon::after {
    display: block;
    border-radius: 14%;
    border-bottom-left-radius: 0%;
    border-bottom-right-radius: 0%;
    background: #5F9F43;
    width: 70px;
    height: 90px;
    left: -2px;
    -webkit-clip-path: polygon(100% 0, 100% 100%, 50% 75%, 0% 100%, 0 48%, 0% 0%);
            clip-path: polygon(100% 0, 100% 100%, 50% 75%, 0% 100%, 0 48%, 0% 0%);
  }
  .hp [data-region=g] .ui-widget.app {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
        -ms-flex-pack: justify;
            justify-content: space-between;
  }
  .hp [data-region=g] .ui-widget.app .ui-widget-header {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
        -ms-flex-direction: column;
            flex-direction: column;
    -webkit-box-pack: center;
        -ms-flex-pack: center;
            justify-content: center;
    -webkit-box-align: start;
        -ms-flex-align: start;
            align-items: flex-start;
    -ms-flex-preferred-size: 36%;
        flex-basis: 36%;
    margin-top: -80px;
  }
  .hp [data-region=g] .ui-widget.app .ui-widget-header.icon {
    padding-left: 0;
    padding-right: 10px;
  }
  .hp [data-region=g] .ui-widget.app .ui-widget-header.icon::before {
    content: "\e90c";
    color: #5F9F43;
    position: relative;
    font-size: 130px;
    top: -15px;
    left: 8px;
  }
  .hp [data-region=g] .ui-widget.app .ui-widget-header h1 {
    font-size: 4.875em;
    line-height: 80px;
  }
  .hp [data-region=g] .ui-widget.app.headlines .ui-widget-detail {
    width: 100%;
  }
  .hp [data-region=g] .ui-widget.app.headlines .ui-articles {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
        flex-wrap: wrap;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
        -ms-flex-direction: row;
            flex-direction: row;
  }
  .hp [data-region=g] .ui-widget.app.headlines .ui-articles li {
    -webkit-box-flex: 1;
        -ms-flex: 1 1;
            flex: 1 1;
    -ms-flex-preferred-size: calc(50% - 10px);
        flex-basis: calc(50% - 10px);
    padding: 5px;
  }
  .hp [data-region=g] .ui-widget.app.headlines .ui-article {
    position: relative;
    width: 100%;
    height: 300px;
    padding: 0;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
  }
  .hp [data-region=g] .ui-widget.app.headlines .ui-article-thumb {
    display: none !important;
  }
  .hp [data-region=g] .ui-widget.app.headlines .headline-description {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: rgba(70, 102, 169, 0.94);
    padding: 25px 35px;
    text-align: center;
    opacity: 0;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  .hp [data-region=g] .ui-widget.app.headlines .headline-description > div {
    position: relative;
    top: 10px;
    max-height: 100%;
    overflow: auto;
    opacity: 0;
    text-align: left;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  .hp [data-region=g] .ui-widget.app.headlines .ui-article-description {
    padding-bottom: 10px;
  }
  .hp [data-region=g] .ui-widget.app.headlines .headline-description:hover,
  .hp [data-region=g] .ui-widget.app.headlines .headline-description.focused,
  .hp [data-region=g] .ui-widget.app.headlines .headline-description.no-image {
    opacity: 1;
  }
  .hp [data-region=g] .ui-widget.app.headlines .headline-description:hover > div,
  .hp [data-region=g] .ui-widget.app.headlines .headline-description.focused > div,
  .hp [data-region=g] .ui-widget.app.headlines .headline-description.no-image > div {
    top: 0;
    opacity: 1;
  }
  .hp [data-region=g] .ui-widget.app.headlines .headline-description.no-image {
    background: #0A2CD8;
  }
  .hp [data-region=g] .ui-widget.app.headlines .headline-card-trigger {
    position: absolute;
    top: 20px;
    left: 20px;
    height: 0;
    padding: 0;
    border: none;
    color: #fff;
    opacity: 0;
    border-radius: 6px;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  .hp [data-region=g] .ui-widget.app.headlines .headline-card-trigger:focus,
  .hp [data-region=g] .ui-widget.app.headlines .headline-card-trigger[aria-expanded=true] {
    height: auto;
    padding: 9px 12px;
    opacity: 1;
    z-index: 2;
  }
  .hp [data-region=g] .ui-widget.app.headlines .ui-article-description {
    color: #fff;
  }
  .hp [data-region=g] .ui-widget.app.headlines .ui-widget-detail .ui-articles .ui-article .ui-article-title a {
    color: #fff;
  }
  .hp [data-region=g] .ui-widget.app .more-link,
  .hp [data-region=g] .ui-widget.app .view-calendar-link,
  .hp [data-region=g] .ui-widget.app .div.app-level-social-rss a.ui-btn-toolbar.rs {
    margin-top: 16px;
    position: relative;
  }
  .hp [data-region=g] .ui-widget.app .more-link::before,
  .hp [data-region=g] .ui-widget.app .view-calendar-link::before,
  .hp [data-region=g] .ui-widget.app .div.app-level-social-rss a.ui-btn-toolbar.rs::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-image: url("https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/btn-stars-2.svg");
    top: 29px;
    right: -100px;
    left: auto;
    background-repeat: no-repeat;
    width: 148px;
    height: 83px;
  }
  .hp [data-region=h] .ui-widget.app .ui-widget-header h1 {
    color: #fff;
    font-size: 4.875rem;
    margin: 0;
    margin-right: 37px;
    position: relative;
  }
  .hp [data-region=h] .ui-widget.app .ui-widget-header h1::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-image: url("https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/btn-stars-head.svg");
    right: -37px;
    left: auto;
    background-repeat: no-repeat;
    width: 37px;
    height: 37px;
    top: 16px;
  }
  .hp [data-region=h] .ui-widget.app .more-link,
  .hp [data-region=h] .ui-widget.app .view-calendar-link,
  .hp [data-region=h] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss {
    background: #fff;
    color: #4666A9;
    position: relative;
    margin-right: 66px;
  }
  .hp [data-region=h] .ui-widget.app .more-link:hover, .hp [data-region=h] .ui-widget.app .more-link:focus,
  .hp [data-region=h] .ui-widget.app .view-calendar-link:hover,
  .hp [data-region=h] .ui-widget.app .view-calendar-link:focus,
  .hp [data-region=h] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss:hover,
  .hp [data-region=h] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss:focus {
    border: 1px solid #fff;
    color: #fff;
    background: #4666A9;
  }
  .hp [data-region=h] .ui-widget.app .more-link::before,
  .hp [data-region=h] .ui-widget.app .view-calendar-link::before,
  .hp [data-region=h] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-image: url("https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/btn-stars.svg");
    right: -75px;
    left: auto;
    background-repeat: no-repeat;
    width: 66px;
    height: 46px;
  }
  .hp [data-region=h] .ui-widget.app.lightbox .ui-widget-header.icon::before {
    content: "\e906";
    color: #4567A9;
    top: 32px;
    left: 24px;
    font-size: 36px;
  }
  .hp [data-region=h] .ui-widget.app.lightbox .ui-widget-header.icon::after {
    display: block;
    width: 65px;
    height: 65px;
    top: 18px;
    left: 1px;
    background: #fd0;
  }
  .hp [data-region=h] .ui-widget.app.lightbox .ui-widget-detail .ui-articles {
    -webkit-box-pack: justify !important;
        -ms-flex-pack: justify !important;
            justify-content: space-between !important;
    margin: 0 !important;
  }
  .hp [data-region=h] .ui-widget.app.lightbox .ui-widget-detail .ui-articles li {
    -webkit-box-flex: 1;
        -ms-flex: 1 1 20%;
            flex: 1 1 20%;
    margin: 0 !important;
    width: 230px !important;
    height: 230px !important;
    min-width: auto !important;
    max-width: 230px !important;
  }
  .hp [data-region=h] .ui-widget.app.lightbox .ui-widget-detail .ui-articles li .ui-article {
    height: 100% !important;
    padding: 0 !important;
  }
  .hp [data-region=h] .ui-widget.app.lightbox .ui-widget-detail .ui-articles li img {
    max-width: 230px !important;
    max-height: 230px !important;
  }
  
  /* GroupEnd */
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  /* ----------------------------- ### APPS ### ----------------------------- */
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  /* HOMEPAGE STLES */
  .hp .ui-widget.app .ui-widget-header {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
        -ms-flex-direction: row;
            flex-direction: row;
    -webkit-box-align: center;
        -ms-flex-align: center;
            align-items: center;
    -webkit-box-pack: justify;
        -ms-flex-pack: justify;
            justify-content: space-between;
    letter-spacing: 0.6px;
  }
  .hp .ui-widget.app .ui-widget-header.icon {
    padding-left: 87px;
  }
  .hp .ui-widget.app .ui-widget-header.icon::before {
    font-family: "template-icon" !important;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    position: absolute;
    font-size: 66px;
    color: #fff;
    z-index: 1;
  }
  .hp .ui-widget.app .ui-widget-header.icon::after {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    border-radius: 50%;
    background: #5F9F43;
    display: none;
    width: 80px;
    height: 80px;
    top: -20px;
    left: -10px;
  }
  
  /* GROUPEND */
  /* GENERAL */
  .ui-widget-footer {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
  }
  
  .more-link-under {
    display: none;
  }
  
  div.ui-widget-header p {
    margin-top: 10px;
  }
  
  .ui-widget-header.ui-helper-hidden {
    margin: 0px !important;
    padding: 0px !important;
  }
  
  div.ui-widget.app div.ui-widget-header:empty {
    display: none;
  }
  
  .ui-widget.app.flexpage div.ui-article p {
    line-height: 27px;
  }
  
  .sp div.ui-widget.app .ui-widget-header {
    margin-bottom: 10px;
  }
  
  .ui-widget.app .ui-widget-header {
    position: relative;
    text-transform: uppercase;
  }
  .ui-widget.app .ui-widget-header h1 {
    font-family: "Nunito", sans-serif;
    font-weight: 700;
    font-size: 1.875rem;
    color: #4567A9;
  }
  .ui-widget.app .more-link,
  .ui-widget.app .view-calendar-link,
  .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss,
  .ui-widget.app .regionE-button a {
    font-family: "Nunito", sans-serif;
    font-weight: 400;
    text-decoration: none;
    color: #fff;
    padding: 15px 15px 12px;
    background: #4666A9;
    text-align: center;
    text-transform: capitalize;
    border: 1px solid transparent;
    -webkit-box-sizing: border-box;
            box-sizing: border-box;
    opacity: 1;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  .ui-widget.app .more-link span,
  .ui-widget.app .view-calendar-link span,
  .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss span,
  .ui-widget.app .regionE-button a span {
    height: auto;
  }
  .ui-widget.app .more-link:hover, .ui-widget.app .more-link:focus,
  .ui-widget.app .view-calendar-link:hover,
  .ui-widget.app .view-calendar-link:focus,
  .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss:hover,
  .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss:focus,
  .ui-widget.app .regionE-button a:hover,
  .ui-widget.app .regionE-button a:focus {
    border: 1px solid #4666A9;
    color: #4666A9;
    background: #fff;
  }
  .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss {
    margin-left: 0;
  }
  .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss span {
    padding: 0;
    background: none;
    font-size: 1rem;
  }
  .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss span::after {
    font-family: "template-icon" !important;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    content: "\e916";
    margin: 0;
    font-size: 1rem;
  }
  .ui-widget.app.navigation li a {
    margin-bottom: 7px;
  }
  .ui-widget.app.navigation li::after {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    width: 100%;
    height: 1px;
    background: #C0C0C0;
    position: relative;
  }
  .ui-widget.app.pagenavigation li:last-child::after {
    display: none;
  }
  .ui-widget.app.content-accordion .ui-widget-header {
    margin-bottom: 11px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
        -ms-flex-align: center;
            align-items: center;
    -webkit-box-pack: justify;
        -ms-flex-pack: justify;
            justify-content: space-between;
  }
  .ui-widget.app.content-accordion .ui-widget-header::after {
    position: absolute !important;
    top: auto !important;
    bottom: -6px;
  }
  .ui-widget.app.content-accordion .ui-widget-header .content-accordion-toolbar a {
    text-decoration: none;
    background: #4267A8;
    padding: 14px;
    color: #fff;
    font-family: "Nunito", sans-serif;
    font-weight: 400;
    font-size: 1rem;
    border: 1px solid #4666A9;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  .ui-widget.app.content-accordion .ui-widget-header .content-accordion-toolbar a:hover, .ui-widget.app.content-accordion .ui-widget-header .content-accordion-toolbar a:focus {
    background: #fff;
    color: #4666A9;
  }
  .ui-widget.app.content-accordion .ui-widget-detail .ui-articles li {
    border-bottom: none;
    background: #E3E0DF;
    margin: 0;
    padding: 0;
  }
  .ui-widget.app.content-accordion .ui-widget-detail .ui-articles li .ui-article {
    margin-bottom: 5px;
    padding: 12px 21px;
  }
  .ui-widget.app.content-accordion .ui-widget-detail .content-accordion-button::after {
    color: #221F1F;
    font-size: 14px;
    font-weight: 700;
    top: 6px;
    right: 0;
  }
  .ui-widget.app.content-accordion .ui-widget-detail .content-accordion-button span {
    font-weight: 400;
    font-family: "Nunito", sans-serif;
    color: #010101;
    text-transform: none;
    font-size: 1.25rem;
  }
  .ui-widget.app.tabbed-content .ui-widget-header {
    margin: 0;
  }
  .ui-widget.app.tabbed-content .ui-widget-tabs {
    margin-bottom: 0px;
  }
  .ui-widget.app.upcomingevents .ui-articles {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
        flex-wrap: wrap;
  }
  .ui-widget.app.upcomingevents .ui-articles li {
    margin: 2px 10px 9px 0px;
    width: 100%;
    background: #4267A8;
    padding: 30px 20px;
  }
  .ui-widget.app.upcomingevents .ui-articles li:nth-child(n+6) {
    display: none;
  }
  .ui-widget.app.upcomingevents .ui-articles .ui-article-title.sw-calendar-block-date {
    font-family: "Nunito", sans-serif;
    font-weight: 700;
    color: #fff;
    font-size: 1.5rem;
    text-transform: uppercase;
  }
  .ui-widget.app.upcomingevents .ui-articles .ui-article-title.sw-calendar-block-date .joel-day {
    display: none;
  }
  .ui-widget.app.upcomingevents .ui-articles .ui-article-title.sw-calendar-block-date .adam-hug {
    position: relative;
  }
  .ui-widget.app.upcomingevents .ui-articles .ui-article-title.sw-calendar-block-date .adam-hug::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background: #CECECC;
    width: 35%;
    height: 1.5px;
    top: auto;
    bottom: -3px;
  }
  .ui-widget.app.upcomingevents .ui-articles .ui-article-title.sw-calendar-block-date .adam-hug .joel-month {
    padding-right: 8px;
  }
  .ui-widget.app.upcomingevents .ui-articles .ui-article-title.sw-calendar-block-date .adam-hug .jeremy-day {
    padding-left: 8px;
  }
  .ui-widget.app.upcomingevents .ui-articles .upcoming-column.right {
    padding-top: 13px;
    font-family: "Nunito", sans-serif;
    font-weight: 500;
    font-size: 0.875rem;
  }
  .ui-widget.app.upcomingevents .ui-articles .upcoming-column.right .ui-article-description {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: reverse;
        -ms-flex-direction: column-reverse;
            flex-direction: column-reverse;
    color: #fff;
  }
  .ui-widget.app.upcomingevents .ui-articles .upcoming-column.right .ui-article-description .sw-calendar-block-time {
    font-weight: 700;
    font-size: 0.8125rem;
  }
  .ui-widget.app.upcomingevents .ui-articles .upcoming-column.right .ui-article-description:nth-child(n+3) {
    display: none;
  }
  .ui-widget.app.upcomingevents .ui-articles .upcoming-column.right .all-events-link {
    display: block;
    position: relative;
    width: 17px;
    height: 17px;
    margin-top: 15px;
    border-radius: 50%;
    border: 1px solid #000;
    color: #000;
  }
  .ui-widget.app.upcomingevents .ui-articles .upcoming-column.right .all-events-link::before {
    font-family: "template-icon" !important;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    content: "\e915";
    position: absolute;
    left: 5px;
    top: 5px;
    font-size: 8px;
  }
  .ui-widget.app.headlines .ui-widget-detail .ui-articles .ui-article .ui-article-thumb {
    padding-bottom: 10px;
  }
  .ui-widget.app.headlines .ui-widget-detail .ui-articles .ui-article .ui-article-title {
    font-family: "Nunito", sans-serif;
    font-size: 1.25rem;
    font-weight: 700;
  }
  .ui-widget.app.headlines .ui-widget-detail .ui-articles .ui-article .ui-article-title a {
    color: #014F7F;
  }
  .ui-widget.app.headlines .ui-widget-detail .ui-articles .ui-article .ui-article-description {
    font-family: "Nunito", sans-serif;
    font-size: 1rem;
    font-weight: 400;
  }
  
  /* TAB CONTENT - TO OVERWRITE SYSTEM SETTINGS */
  #gb-page .ui-widget.app.tabbed-content [aria-selected=true].tabbed-content-tab {
    background: #4267A8;
  }
  #gb-page .ui-widget.app.tabbed-content .tabbed-content-tab {
    border-bottom: none;
    background: #000;
    padding: 11px 29px 13px;
    margin-right: 3px;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  #gb-page .ui-widget.app.tabbed-content .tabbed-content-tab span {
    color: #fff;
    text-transform: none;
    font-family: "Nunito", sans-serif;
    font-weight: 400;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  #gb-page .ui-widget.app.tabbed-content .tabbed-content-tab:hover, #gb-page .ui-widget.app.tabbed-content .tabbed-content-tab:focus {
    background: #4267A8;
  }
  
  /* GROUPEND */
  /* BULLETS FOR ALL LISTS */
  div.ui-widget.app.navigation li div.bullet {
    background: none;
    position: relative;
  }
  
  div.ui-widget.app.navigation li div.bullet.expandable {
    background: none;
  }
  div.ui-widget.app.navigation li div.bullet.expandable::before {
    font-family: "template-icon" !important;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    content: "\e915";
    position: absolute;
    font-size: 11px;
    color: #000;
    top: 5px;
  }
  
  div.ui-widget.app.navigation li div.bullet.collapsible {
    background: none;
  }
  div.ui-widget.app.navigation li div.bullet.collapsible::before {
    font-family: "template-icon" !important;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    content: "\e915";
    position: absolute;
    font-size: 11px;
    color: #000;
    top: 5px;
  }
  
  /* END */
  /* GROUPEND */
  /* ANNOUNCEMENTS */
  .ui-widget.app.announcements .ui-articles li {
    border-bottom: 1px solid #C0C0C0;
  }
  .ui-widget.app.announcements .ui-articles li:last-child {
    border-bottom: 1px solid transparent;
  }
  
  /* GroupEnd */
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  /* ------------------------ ### SUBPAGES ### ------------------------------ */
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  #sp-content-outer {
    position: relative;
    display: block;
  }
  
  #sp-content-outer div[id*=module-content-] {
    padding: 0 0 25px 27px;
    margin: 0;
    -webkit-box-sizing: border-box;
            box-sizing: border-box;
  }
  
  #spn-content-outer div[id*=module-content-] {
    padding: 0 27px 25px 0;
    margin: 0;
    -webkit-box-sizing: border-box;
            box-sizing: border-box;
  }
  
  #sp-content {
    padding-top: 10px;
  }
  
  .sp-column.one {
    width: 273px;
    padding: 0;
  }
  
  .sp-column.two {
    width: calc(100% - 273px);
    min-height: 500px;
    padding: 0 0 0 10px;
  }
  
  ul.ui-breadcrumbs {
    padding-top: 10px;
  }
  
  ul.ui-breadcrumbs > li > a > span {
    color: #767576;
    font-family: "Nunito", sans-serif;
    font-weight: 600;
    font-size: 1rem;
  }
  
  ul.ui-breadcrumbs > li > a {
    background-image: none;
    position: relative;
    text-decoration: none;
  }
  ul.ui-breadcrumbs > li > a::after {
    position: absolute;
    font-family: "template-icon" !important;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    content: "\e900";
    color: #767576;
    left: auto;
    font-size: 10px;
    right: 2px;
    top: 6px;
  }
  
  .ui-breadcrumb-last span {
    color: #4C4C4C;
  }
  
  /* SIDE NAV */
  .ui-widget.app.pagenavigation {
    background: #4666A9;
    --mask: conic-gradient(from -45deg at bottom,#0000,#000 1deg 89deg,#0000 90deg) 50%/16.00px 100%;
    -webkit-mask: var(--mask);
    mask: var(--mask);
    -webkit-mask-repeat: repeat-x;
    padding: 20px;
  }
  .ui-widget.app.pagenavigation .ui-widget-header h1 {
    color: #FFE947;
    font-size: 1.5625rem;
  }
  .ui-widget.app.pagenavigation .ui-widget-header::after {
    display: none !important;
  }
  .ui-widget.app.pagenavigation li a {
    color: #fff;
  }
  
  div.ui-widget.app.pagenavigation li div.bullet.collapsible::before,
  div.ui-widget.app.pagenavigation li div.bullet.expandable::before {
    color: #fff;
  }
  
  /* HEADING UNDERLINES */
  .sp .ui-widget.app .ui-widget-header::after,
  .spn .ui-widget.app .ui-widget-header::after {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    width: 100%;
    height: 1px;
    background: #C0C0C0;
    position: relative;
  }
  
  /* END */
  .sp .nav-section .cs-button-section {
    padding-right: 0;
  }
  .sp .nav-group.right {
    display: none;
  }
  .sp .gb-footer-row #gb-icons {
    position: relative;
    padding-bottom: 30px;
  }
  .sp .gb-footer-row #gb-icons::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    width: 100%;
    height: 2px;
    background: #CECECC;
    top: auto;
    left: auto;
    bottom: 0;
  }
  .sp #gb-icons {
    visibility: visible;
  }
  
  /* GROUPEND */
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  /* ------------------------ ### GB ICONS ### ------------------------------ */
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  #gb-icons .cs-global-icons {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
        -ms-flex-pack: justify;
            justify-content: space-between;
    -ms-flex-wrap: wrap;
        flex-wrap: wrap;
  }
  #gb-icons .cs-global-icons li .cs-global-icon {
    display: -ms-flexbox;
    display: -webkit-box;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
        -ms-flex-direction: column;
            flex-direction: column;
    -webkit-box-align: center;
        -ms-flex-align: center;
            align-items: center;
    text-decoration: none;
    color: #4567A9;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  #gb-icons .cs-global-icons li .cs-global-icon:hover, #gb-icons .cs-global-icons li .cs-global-iconfocus {
    color: #5F9F43;
  }
  #gb-icons .cs-global-icons li .cs-global-icon:hover .text, #gb-icons .cs-global-icons li .cs-global-iconfocus .text {
    color: #606060;
  }
  #gb-icons .cs-global-icons li .icon {
    font-size: 40px;
    padding: 30px 30px 17px;
  }
  #gb-icons .cs-global-icons li .text {
    font-family: "Akshar", sans-serif;
    font-size: 0.875rem;
    text-transform: uppercase;
  }
  
  #gb-icons-sticky .cs-global-icons {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
        -ms-flex-align: center;
            align-items: center;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
        -ms-flex-direction: row;
            flex-direction: row;
    -webkit-box-pack: justify;
        -ms-flex-pack: justify;
            justify-content: space-between;
    position: fixed;
    z-index: 100000;
    background: rgba(69, 103, 170, 0.94);
    bottom: 0;
    right: 42px;
    padding: 10px 13px;
  }
  #gb-icons-sticky .cs-global-icons li {
    width: 30px;
    height: 30px;
    padding: 5px 9px;
  }
  #gb-icons-sticky .cs-global-icons .cs-global-icon {
    width: 100%;
    height: 100%;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
        -ms-flex-align: center;
            align-items: center;
    -webkit-box-pack: center;
        -ms-flex-pack: center;
            justify-content: center;
    text-decoration: none;
    color: #fff;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  #gb-icons-sticky .cs-global-icons .cs-global-icon:hover, #gb-icons-sticky .cs-global-icons .cs-global-icon:focus {
    color: #5F9F43;
  }
  #gb-icons-sticky .cs-global-icons .cs-global-icon .icon {
    font-size: 21px;
  }
  #gb-icons-sticky .cs-global-icons .cs-global-icon .text {
    display: none;
  }
  
  /* GroupEnd */
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  /* ---------------------------- ### MAGIC ### ----------------------------- */
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  .mg-header {
    position: relative;
    color: #4567A9;
  }
  .mg-header .mg-head-1 {
    font-family: "Oooh Baby", cursive;
    font-size: 13.4375rem;
    font-weight: 400;
    margin-right: 30px;
  }
  .mg-header .mg-header-icon {
    max-width: 248px;
    max-height: 213px;
    position: absolute;
    top: 33px;
  }
  .mg-header .mg-header-icon-1024 {
    min-width: 248px;
    min-height: 213px;
    position: absolute;
    top: 33px;
    right: 360px;
  }
  .mg-header .mg-header-icon-1024 .mg-wand-icon {
    max-width: 248px;
    max-height: 213px;
    width: 248px;
    height: 213px;
    left: -8px;
    position: absolute;
  }
  .mg-header .mg-header-icon-1024 .mg-star {
    position: absolute;
  }
  .mg-header .mg-header-icon-1024 .mg-star.i1 {
    max-width: 67px;
    max-height: 57px;
    width: 67px;
    height: 57px;
    left: 14px;
    top: 52px;
  }
  .mg-header .mg-header-icon-1024 .mg-star.i2 {
    max-width: 96px;
    max-height: 75px;
    width: 96px;
    height: 75px;
    left: -10px;
    top: 28px;
  }
  .mg-header .mg-header-icon-1024 .mg-star.i3 {
    max-width: 131px;
    max-height: 65px;
    width: 131px;
    height: 65px;
    top: 0;
    left: 102px;
  }
  .mg-header .mg-header-icon-1024 .mg-star.i4 {
    max-width: 125px;
    max-height: 53px;
    width: 125px;
    height: 53px;
    right: 11px;
    bottom: 35px;
  }
  .mg-header .mg-header-icon-1024 .mg-star.i5 {
    max-width: 87px;
    max-height: 55px;
    width: 87px;
    height: 55px;
    bottom: 0;
    right: 74px;
  }
  .mg-header .mg-head-2 {
    font-family: "Nunito", sans-serif;
    font-size: 3.3125rem;
    position: absolute;
    right: 0;
    top: 111px;
    text-transform: uppercase;
  }
  
  #mg-section {
    padding: 0 10px 0 2px;
    -webkit-box-sizing: border-box;
            box-sizing: border-box;
    -webkit-box-align: center;
        -ms-flex-align: center;
            align-items: center;
    z-index: 100;
    position: relative;
    min-height: 760px;
  }
  
  #mg-titles {
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
        -ms-flex-direction: column;
            flex-direction: column;
    -ms-flex-preferred-size: 50%;
        flex-basis: 50%;
    -webkit-box-sizing: border-box;
            box-sizing: border-box;
    padding-right: 10px;
    padding-left: 69px;
    padding-top: 8px;
  }
  
  #mg-descriptions {
    -ms-flex-preferred-size: 50%;
        flex-basis: 50%;
    padding-left: 74px;
    -webkit-box-sizing: border-box;
            box-sizing: border-box;
    margin-top: -16px;
  }
  
  .tablink {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    margin-bottom: 24px;
    position: relative;
    padding: 5px 10px;
  }
  .tablink .t0_1 {
    stroke-dasharray: 230;
    -webkit-animation: t0_1-out 0.5s ease-out;
            animation: t0_1-out 0.5s ease-out;
    -webkit-animation-fill-mode: backwards;
            animation-fill-mode: backwards;
    opacity: 0;
  }
  .tablink .t0_2 {
    stroke-dasharray: 70;
    -webkit-animation: t0_2-out 0.5s ease-out;
            animation: t0_2-out 0.5s ease-out;
    -webkit-animation-fill-mode: backwards;
            animation-fill-mode: backwards;
    opacity: 0;
  }
  .tablink .t1_1 {
    stroke-dasharray: 30;
    -webkit-animation: t1_1-out 0.5s ease-out;
            animation: t1_1-out 0.5s ease-out;
    -webkit-animation-fill-mode: backwards;
            animation-fill-mode: backwards;
    opacity: 0;
  }
  .tablink .t1_2 {
    stroke-dasharray: 330;
    -webkit-animation: t1_2-out 0.5s ease-out;
            animation: t1_2-out 0.5s ease-out;
    -webkit-animation-fill-mode: backwards;
            animation-fill-mode: backwards;
    opacity: 0;
  }
  .tablink .t2_1 {
    stroke-dasharray: 90;
    -webkit-animation: t2_1-out 0.5s ease-out;
            animation: t2_1-out 0.5s ease-out;
    -webkit-animation-fill-mode: backwards;
            animation-fill-mode: backwards;
    opacity: 0;
  }
  .tablink .t2_2 {
    stroke-dasharray: 437;
    -webkit-animation: t2_2-out 0.5s ease-out;
            animation: t2_2-out 0.5s ease-out;
    -webkit-animation-fill-mode: backwards;
            animation-fill-mode: backwards;
    opacity: 0;
  }
  .tablink .t3_1 {
    stroke-dasharray: 21;
    -webkit-animation: t3_1-out 0.3s ease-out;
            animation: t3_1-out 0.3s ease-out;
    -webkit-animation-fill-mode: backwards;
            animation-fill-mode: backwards;
    opacity: 0;
  }
  .tablink .t3_2 {
    stroke-dasharray: 782;
    -webkit-animation: t3_2-out 0.5s ease-out;
            animation: t3_2-out 0.5s ease-out;
    -webkit-animation-fill-mode: backwards;
            animation-fill-mode: backwards;
    opacity: 0;
  }
  .tablink.active p {
    color: #4567A9;
  }
  .tablink.active .t0_1 {
    -webkit-animation: t0_1-in 0.5s ease-in-out;
            animation: t0_1-in 0.5s ease-in-out;
    -webkit-animation-fill-mode: forwards;
            animation-fill-mode: forwards;
  }
  .tablink.active .t0_2 {
    -webkit-animation: t0_2-in 0.5s ease-in-out;
            animation: t0_2-in 0.5s ease-in-out;
    -webkit-animation-fill-mode: forwards;
            animation-fill-mode: forwards;
  }
  .tablink.active .t1_1 {
    -webkit-animation: t1_1-in 0.5s ease-in-out;
            animation: t1_1-in 0.5s ease-in-out;
    -webkit-animation-fill-mode: forwards;
            animation-fill-mode: forwards;
  }
  .tablink.active .t1_2 {
    -webkit-animation: t1_2-in 0.5s ease-in-out;
            animation: t1_2-in 0.5s ease-in-out;
    -webkit-animation-fill-mode: forwards;
            animation-fill-mode: forwards;
  }
  .tablink.active .t2_1 {
    -webkit-animation: t2_1-in 0.5s ease-in-out;
            animation: t2_1-in 0.5s ease-in-out;
    -webkit-animation-fill-mode: forwards;
            animation-fill-mode: forwards;
  }
  .tablink.active .t2_2 {
    -webkit-animation: t2_2-in 0.5s ease-in-out;
            animation: t2_2-in 0.5s ease-in-out;
    -webkit-animation-fill-mode: forwards;
            animation-fill-mode: forwards;
  }
  .tablink.active .t3_1 {
    -webkit-animation: t3_1-in 0.6s ease-in-out;
            animation: t3_1-in 0.6s ease-in-out;
    -webkit-animation-fill-mode: forwards;
            animation-fill-mode: forwards;
  }
  .tablink.active .t3_2 {
    -webkit-animation: t3_2-in 0.5s ease-in-out;
            animation: t3_2-in 0.5s ease-in-out;
    -webkit-animation-fill-mode: forwards;
            animation-fill-mode: forwards;
  }
  .tablink.active p {
    color: #4567A9;
  }
  .tablink.active p::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    height: 5.5px;
    width: 73px;
    background: #FFE947;
    right: -88px;
    left: auto;
    top: 45%;
  }
  .tablink p {
    margin: 0;
    text-align: left;
    font-family: "Nunito", sans-serif;
    font-size: 3.5625rem;
    font-weight: 400;
  }
  
  .tabcontent {
    position: relative;
    padding: 18px 0px 12px 30px;
  }
  .tabcontent::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    width: 7%;
    border-top: 5.5px solid #FFE947;
    border-bottom: 5.5px solid #FFE947;
    border-left: 5.5px solid #FFE947;
  }
  .tabcontent.show {
    display: block;
  }
  .tabcontent.no-show {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
  }
  .tabcontent p {
    margin: 0;
    font-family: "Nunito", sans-serif;
    font-size: 1.75rem;
    font-weight: 400;
  }
  
  /* INDIVIDUAL CARDS - ONLY IF ACTIVE */
  .tablink#title-0 #title-0_1 {
    position: absolute;
    left: 67px;
    top: auto;
    bottom: -8px;
    height: 23px;
  }
  .tablink#title-0 #title-0_2 {
    position: absolute;
    left: -27px;
    top: -8px;
    height: 100%;
    height: 44px;
  }
  .tablink#title-1 #title-1_1 {
    position: absolute;
    height: 39px;
    left: -42px;
    top: 29px;
  }
  .tablink#title-1 #title-1_2 {
    position: absolute;
    top: auto;
    bottom: -10px;
    height: 26px;
  }
  .tablink#title-2 #title-2_1 {
    position: absolute;
    height: 39px;
    left: -71px;
    top: 30px;
  }
  .tablink#title-2 #title-2_2 {
    position: absolute;
    top: auto;
    bottom: -8px;
    height: 24px;
    left: 20px;
  }
  .tablink#title-3 #title-3_1 {
    position: absolute;
    left: -70px;
    height: 57px;
    top: auto;
    bottom: -47px;
  }
  .tablink#title-3 #title-3_2 {
    position: absolute;
    left: -24px;
    height: 100%;
    top: 2px;
  }
  
  /* GEN STYLE FOR SVGS */
  .cls {
    stroke: #FFE947;
    fill: transparent;
    stroke-width: 5px;
    stroke-linecap: round;
  }
  
  /* END */
  /* GroupEnd*/
  /* CITY SECTION */
  #animation-graphic {
    padding-top: 100px;
    position: relative;
  }
  #animation-graphic::after {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background: #FFE000;
    width: 100%;
    height: 25px;
    height: 38px;
    bottom: 0;
    top: auto;
  }
  #animation-graphic #city-bg {
    position: relative;
    width: 100%;
    max-width: 1500px;
    margin: 0 auto;
    min-height: 300px;
    background-image: url("https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/City-MinusCar-Windmills.svg");
    background-repeat: no-repeat;
    background-size: contain;
    background-position-y: bottom;
    background-position-x: center;
  }
  #animation-graphic #city-bg-mobile {
    display: none;
    position: relative;
    width: 100%;
    min-height: 300px;
    background-image: url("https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/city-all.svg");
    background-repeat: no-repeat;
    background-size: contain;
    background-position-y: bottom;
    background-position-x: center;
  }
  #animation-graphic #city-bg-mobile::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background: #FFE000;
    width: 100%;
    height: 16px;
    bottom: -7px;
    top: auto;
  }
  #animation-graphic .mg-svgs {
    position: absolute;
    z-index: 2;
    bottom: 0;
  }
  #animation-graphic #cloud-1 {
    -webkit-animation: none;
            animation: none;
  }
  #animation-graphic #cloud-2 {
    -webkit-animation: none;
            animation: none;
  }
  #animation-graphic #cloud-3 {
    -webkit-animation: none;
            animation: none;
  }
  #animation-graphic #car {
    -webkit-animation: none;
            animation: none;
  }
  #animation-graphic #windmill-1 {
    -webkit-animation: none;
            animation: none;
    -webkit-transform-origin: 81.2% 67.7%;
            transform-origin: 81.2% 67.7%;
  }
  #animation-graphic #windmill-2 {
    -webkit-animation: none;
            animation: none;
    -webkit-transform-origin: 82.8% 50%;
            transform-origin: 82.8% 50%;
  }
  #animation-graphic #windmill-3 {
    -webkit-animation: none;
            animation: none;
    -webkit-transform-origin: 95.4% 45.6%;
            transform-origin: 95.4% 45.6%;
  }
  
  /* GroupEnd*/
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  /* ------------------------ ### SOCIAL MEDIA ### -------------------------- */
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  /*FOR HOMEPAGE*/
  .gb-socials.sticky {
    z-index: 1000;
    background: #FFFFFF;
    margin-left: 5px;
    -webkit-box-shadow: 0px 0px 2px 2px rgba(0, 0, 0, 0.18);
    box-shadow: 0px 0px 2px 2px rgba(0, 0, 0, 0.18);
    padding: 3px;
    top: 246px;
  }
  .gb-socials.region-e {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
        -ms-flex-direction: row;
            flex-direction: row;
    -ms-flex-wrap: wrap;
        flex-wrap: wrap;
    -webkit-box-pack: center;
        -ms-flex-pack: center;
            justify-content: center;
  }
  .gb-socials.region-e .gb-social-icon {
    width: 52px;
    height: 48px;
  }
  .gb-socials.region-e .gb-social-icon .gb-social-icons::before {
    font-size: 40px;
  }
  .gb-socials .gb-social-icon {
    height: 41px;
    width: 38px;
    position: relative;
  }
  .gb-socials .gb-social-icon .gb-social-icons {
    text-decoration: none;
    position: absolute;
    height: 100%;
    width: 100%;
    text-align: center;
  }
  .gb-socials .gb-social-icon .gb-social-icons::before {
    font-family: "template-icon" !important;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    font-size: 35px;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  .gb-socials .gb-social-icon .gb-social-icons.facebook::before {
    content: "\e90a";
    color: #476999;
  }
  .gb-socials .gb-social-icon .gb-social-icons.facebook:hover::before, .gb-socials .gb-social-icon .gb-social-icons.facebook:focus::before {
    color: #5F9F43;
  }
  .gb-socials .gb-social-icon .gb-social-icons.twitter::before {
    content: "\e919";
    color: #476999;
  }
  .gb-socials .gb-social-icon .gb-social-icons.twitter:hover::before, .gb-socials .gb-social-icon .gb-social-icons.twitter:focus::before {
    color: #5F9F43;
  }
  .gb-socials .gb-social-icon .gb-social-icons.youtube::before {
    content: "\e91c";
    color: #E22C34;
  }
  .gb-socials .gb-social-icon .gb-social-icons.youtube:hover::before, .gb-socials .gb-social-icon .gb-social-icons.youtube:focus::before {
    color: #5F9F43;
  }
  .gb-socials .gb-social-icon .gb-social-icons.instagram::before {
    content: "\e90d";
    color: #4D9DCF;
  }
  .gb-socials .gb-social-icon .gb-social-icons.instagram:hover::before, .gb-socials .gb-social-icon .gb-social-icons.instagram:focus::before {
    color: #5F9F43;
  }
  .gb-socials .gb-social-icon .gb-social-icons.linkedin::before {
    content: "\e90e";
    color: #0E76A8;
  }
  .gb-socials .gb-social-icon .gb-social-icons.linkedin:hover::before, .gb-socials .gb-social-icon .gb-social-icons.linkedin:focus::before {
    color: #5F9F43;
  }
  .gb-socials .gb-social-icon .gb-social-icons.vimeo::before {
    content: "\e91b";
    color: #86c9ef;
  }
  .gb-socials .gb-social-icon .gb-social-icons.vimeo:hover::before, .gb-socials .gb-social-icon .gb-social-icons.vimeo:focus::before {
    color: #5F9F43;
  }
  .gb-socials .gb-social-icon .gb-social-icons.flickr::before {
    content: "\e908";
    color: #476999;
  }
  .gb-socials .gb-social-icon .gb-social-icons.flickr:hover::before, .gb-socials .gb-social-icon .gb-social-icons.flickr:focus::before {
    color: #5F9F43;
  }
  .gb-socials .gb-social-icon .gb-social-icons.pinterest::before {
    content: "\e90f";
    color: #c8232c;
  }
  .gb-socials .gb-social-icon .gb-social-icons.pinterest:hover::before, .gb-socials .gb-social-icon .gb-social-icons.pinterest:focus::before {
    color: #5F9F43;
  }
  .gb-socials .gb-social-icon .gb-social-icons.peachjar::before {
    content: "\e912";
    color: #F2A11A;
  }
  .gb-socials .gb-social-icon .gb-social-icons.peachjar:hover::before, .gb-socials .gb-social-icon .gb-social-icons.peachjar:focus::before {
    color: #5F9F43;
  }
  .gb-socials .gb-social-icon .gb-social-icons.rss::before {
    content: "\e916";
    color: #000;
  }
  .gb-socials .gb-social-icon .gb-social-icons.rss:hover::before, .gb-socials .gb-social-icon .gb-social-icons.rss:focus::before {
    color: #5F9F43;
  }
  
  /*FOR SUBPAGES/
  
  /* GroupEnd */
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  /* ------------------------ ### FOOTER STYLES ### ------------------------- */
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  #gb-footer {
    padding: 30px 0;
  }
  
  .gb-footer-row {
    position: relative;
  }
  .gb-footer-row.one {
    padding-bottom: 38px;
  }
  .gb-footer-row.one::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    width: 100%;
    height: 2px;
    background: #CECECC;
    top: auto;
    left: auto;
    bottom: 0;
  }
  
  .footer-address {
    -webkit-box-flex: 1;
        -ms-flex: 1 1;
            flex: 1 1;
    padding: 0 0 0 38px;
  }
  .footer-address .footer-info-wrap {
    padding: 0px 60px 0 49px;
    position: relative;
  }
  .footer-address .footer-info-wrap .footer-info-header {
    font-family: "Nunito", sans-serif;
    font-weight: 600;
    font-size: 2.0625rem;
    color: #4267A8;
    margin: 0;
  }
  .footer-address .footer-info-wrap .footer-info-header::before {
    font-family: "template-icon" !important;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    position: absolute;
    left: 0;
    top: 0;
    font-size: 43px;
    top: 14px;
  }
  .footer-address .footer-info-wrap .footer-info-header.visit::before {
    content: "\e90b";
  }
  .footer-address .footer-info-wrap .footer-info-header.contact::before {
    content: "\e913";
    top: 11px;
    font-size: 40px;
  }
  .footer-address .footer-info-wrap .footer-info-wrapper .info-head {
    color: #4267A8;
  }
  .footer-address .footer-info-wrap .footer-info-wrapper p {
    margin: 7px 0px;
  }
  .footer-address .footer-info-wrap .footer-info-wrapper p .address-wrap {
    text-decoration: none;
    color: #000;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  .footer-address .footer-info-wrap .footer-info-wrapper p .address-wrap:hover, .footer-address .footer-info-wrap .footer-info-wrapper p .address-wrap:focus {
    color: #4A8136;
  }
  
  .footer-link-container {
    padding-left: 30px;
  }
  .footer-link-container #footer-link-list {
    padding: 0;
    margin: 0;
    -webkit-column-count: 2;
       -moz-column-count: 2;
            column-count: 2;
    -webkit-column-gap: 58px;
       -moz-column-gap: 58px;
            column-gap: 58px;
    margin-top: 18px;
  }
  .footer-link-container #footer-link-list li {
    margin: 10px 0;
    position: relative;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  .footer-link-container #footer-link-list li::before {
    font-family: "template-icon" !important;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    content: "\e903";
    color: #757575;
    left: -20px;
    top: 7px;
    position: absolute;
    font-size: 10px;
  }
  .footer-link-container #footer-link-list li:hover::before, .footer-link-container #footer-link-list li:focus::before {
    color: #4A8136;
  }
  .footer-link-container #footer-link-list li a {
    text-decoration: none;
    color: #221F1F;
    font-family: "Nunito", sans-serif;
    font-size: 0.875rem;
    font-weight: 400;
    text-transform: uppercase;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  .footer-link-container #footer-link-list li a:hover, .footer-link-container #footer-link-list li a:focus {
    color: #4A8136;
  }
  .footer-link-container #footer-link-list li:first-child {
    margin-top: 0;
  }
  
  #disclaimer-row {
    -webkit-box-orient: horizontal;
    -webkit-box-direction: reverse;
        -ms-flex-direction: row-reverse;
            flex-direction: row-reverse;
  }
  
  #footer-disclaimer p {
    color: #5B5B5B;
    font-family: "Nunito", sans-serif;
    font-size: 0.875rem;
    font-weight: 500;
  }
  
  .cs-bb-footer-outer {
    padding: 0;
    -ms-flex-line-pack: start;
        align-content: flex-start;
    padding-right: 59px;
  }
  
  .cs-bb-footer.info p {
    font-family: "Nunito", sans-serif;
    font-size: 0.6875rem;
    font-weight: 500;
    color: #5B5B5B;
  }
  
  .cs-mystart-dropdown.user-options {
    position: relative;
  }
  .cs-mystart-dropdown.user-options .cs-dropdown-selector {
    position: relative;
  }
  .cs-mystart-dropdown.user-options .cs-dropdown-selector span {
    font-family: "Nunito", sans-serif;
    font-size: 0.875rem;
    font-weight: 600;
    color: #5B5B5B;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  .cs-mystart-dropdown.user-options .cs-dropdown-selector:hover span, .cs-mystart-dropdown.user-options .cs-dropdown-selector:focus span {
    text-decoration: underline;
  }
  .cs-mystart-dropdown.user-options .cs-dropdown-selector::before {
    font-family: "template-icon" !important;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    content: "\e91a";
    position: absolute;
    left: -20px;
    top: 2px;
    font-size: 12px;
    color: #5B5B5B;
  }
  .cs-mystart-dropdown.user-options .cs-dropdown-selector::after {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    top: auto;
    width: 125%;
    height: 100%;
    background-color: transparent;
    bottom: 100%;
    left: -20px;
  }
  .cs-mystart-dropdown.user-options .cs-dropdown {
    position: absolute;
    top: auto;
    right: 0;
    bottom: calc(100% + 12px);
    left: auto;
    min-width: 182px;
    background: #DCDCDC;
    -webkit-box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.3);
            box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.3);
  }
  .cs-mystart-dropdown.user-options .cs-dropdown ul {
    padding: 10px 0;
    margin: 0;
  }
  .cs-mystart-dropdown.user-options .cs-dropdown ul li a {
    color: #000;
    text-decoration: none;
    font-family: "Nunito", sans-serif;
    text-transform: uppercase;
    font-weight: 600;
    font-size: 0.8125rem;
    padding: 6px 15px 4px;
    display: block;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  .cs-mystart-dropdown.user-options .cs-dropdown ul li a:hover, .cs-mystart-dropdown.user-options .cs-dropdown ul li a:focus {
    background: rgba(255, 255, 255, 0.87);
  }

  div#ui-mypasskey-overlay{
      padding: 0;
      margin-top: -304px;
  }
  #sw-mystart-passkey {
  	margin-left: 5px;
  }
  .cs-mystart-dropdown.passkeys {
    position: relative;
  }
  .cs-mystart-dropdown.passkeys #ui-btn-mypasskey {
    position: relative;
    text-decoration: none;
  }
  .cs-mystart-dropdown.passkeys #ui-btn-mypasskey span {
    font-family: "Nunito", sans-serif;
    font-size: 0.875rem;
    font-weight: 600;
    color: #5B5B5B;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  .cs-mystart-dropdown.passkeys #ui-btn-mypasskey:hover span, .cs-mystart-dropdown.passkeys #ui-btn-mypasskey:focus span {
    text-decoration: underline;
  }
  /* GroupEnd */
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  /* ------------------------ ### EDITOR STYLES ### ------------------------- */
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  html {
    font-size: 16px;
    line-height: 1.375;
  }
  
  /* GroupBegin EditorStyles */
  body {
    font-family: "Nunito", sans-serif;
    font-weight: 400;
    color: #231f20;
    text-rendering: optimizeLegibility !important;
    -webkit-font-smoothing: antialiased !important;
    -moz-font-smoothing: antialiased !important;
    font-smoothing: antialiased !important;
    -moz-osx-font-smoothing: grayscale;
  }
  
  h1, h2, h3, h4 {
    color: #4666A9;
    font-family: "Nunito", sans-serif;
  }
  
  h1 {
    font-size: 1.75rem;
  }
  
  h2 {
    font-size: 1.625rem;
  }
  
  h3 {
    font-size: 1.5rem;
  }
  
  h4 {
    font-size: 1.25rem;
  }
  
  p {
    font-family: "Nunito", sans-serif;
    font-size: 0.9375rem;
    color: #000;
  }
  
  .Border_Box {
    border: 2px solid #DDDDDC;
    background: #fff;
    padding: 27px 30px;
    display: block;
  }
  
  .Primary_Box {
    background: #4666A9;
    padding: 27px 30px;
    display: block;
  }
  
  .Secondary_Box {
    background: #E4E4E4;
    padding: 27px 30px;
    display: block;
  }
  
  .Call_To_Action_Button {
    padding: 15px 15px 12px;
    background: #4666A9;
    border: 1px solid transparent;
    -webkit-box-sizing: border-box;
            box-sizing: border-box;
    position: relative;
    opacity: 1;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  
  /* GroupEnd */
  .Border_Box,
  .Border_Box h1,
  .Border_Box h2,
  .Border_Box h3,
  .Border_Box h4,
  .Border_Box h5,
  .Border_Box h6,
  .Border_Box span,
  .Border_Box a,
  .Border_Box p {
    color: #4666A9;
  }
  
  .Primary_Box,
  .Primary_Box h1,
  .Primary_Box h2,
  .Primary_Box h3,
  .Primary_Box h4,
  .Primary_Box h5,
  .Primary_Box h6,
  .Primary_Box span,
  .Primary_Box a,
  .Primary_Box p {
    color: #fff;
  }
  
  .Secondary_Box,
  .Secondary_Box h1,
  .Secondary_Box h2,
  .Secondary_Box h3,
  .Secondary_Box h4,
  .Secondary_Box h5,
  .Secondary_Box h6,
  .Secondary_Box span,
  .Secondary_Box a,
  .Secondary_Box p {
    color: #000;
  }
  
  .Call_To_Action_Button a {
    font-family: "Nunito", sans-serif;
    font-weight: 400;
    text-decoration: none;
    color: #fff;
    text-align: center;
    text-transform: capitalize;
    -webkit-transition: all 0.4s ease 0s;
    transition: all 0.4s ease 0s;
  }
  
  .Call_To_Action_Button:hover a {
    color: #4666A9;
  }
  
  .Call_To_Action_Button::before {
    display: block;
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-image: url("https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/btn-stars.svg");
    right: -75px;
    left: auto;
    background-repeat: no-repeat;
    width: 66px;
    height: 46px;
  }
  
  .Call_To_Action_Button span {
    height: auto;
  }
  
  .Call_To_Action_Button:hover,
  .Call_To_Action_Button:focus {
    border: 1px solid #4666A9;
    background: #fff;
  }
  
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  /* ------------------------ ### MEDIA QUERIES ### ------------------------- */
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  /* GroupBegin Media Queries */
  @media (max-width: 1292px) {
    .mg-header .mg-head-1 {
      font-size: 14.3vw;
    }
  
    .mg-header .mg-head-2 {
      font-size: 3.52vw;
    }
  }
  @media (max-width: 1300px) {
    .content-wrap {
      padding: 0 55px;
    }
  }
  @media (max-width: 1232px) {
    #gb-search-form {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -ms-flex-wrap: nowrap;
          flex-wrap: nowrap;
    }
  
    .hp [data-region=c] .ui-widget.app .ui-widget-header h1 {
      font-size: 3.875em;
    }
  }
  @media (max-width: 1259px) {
    .hp [data-region=h] .ui-widget.app.lightbox .ui-widget-detail .ui-articles li:nth-child(n+5) {
      display: none;
    }
  
    .mg-header .mg-head-2 {
      right: 60px;
    }
  }
  @media (max-width: 1081px) {
    .hp [data-region=h] .ui-widget.app.lightbox .ui-widget-detail .ui-articles {
      -ms-flex-wrap: nowrap !important;
          flex-wrap: nowrap !important;
    }
  
    .hp [data-region=c] .ui-widget.app.upcomingevents .ui-articles li:nth-child(n+5) {
      -webkit-box-flex: 0;
          -ms-flex: 0 1;
              flex: 0 1;
      margin-top: 10px;
    }
  
    .mg-header .mg-head-2 {
      top: 67px;
      right: 90px;
    }
  
    .mg-header-icon-1024 {
      display: none;
    }
  
    .mg-header .mg-header-icon {
      display: block;
      max-width: 188px;
      max-height: 163px;
      position: absolute;
      top: 13px;
      right: 340px;
    }
  }
  @media (max-width: 1136px) {
    .nav-section .cs-button-section {
      padding-right: 0;
    }
  }
  @media (max-width: 1111px) {
    .hp [data-region=c] .ui-widget.app.upcomingevents .ui-articles li:nth-child(n+5) {
      display: none;
    }
  
    .hp [data-region=h] .ui-widget.app.lightbox .ui-widget-detail .ui-articles li {
      margin: 0 4px !important;
    }
  }
  @media (max-width: 1147px) {
    #cs-ar {
      position: absolute;
      top: 100%;
      left: auto;
      right: 0;
    }
  }
  @media (max-width: 1026px) {
    #animation-graphic #city-bg-mobile {
      display: block;
    }
  
    #animation-graphic #city-bg {
      display: none;
    }
  
    .mg-svgs {
      display: none;
    }
  
    .tablink p {
      font-size: 2.9625rem;
    }
  
    .tabcontent p {
      font-size: 1.65rem;
    }
  
    .footer-address {
      display: block;
    }
  }
  /* GroupEnd */
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  /* ------------------------ ### ANIMATIONS ### ---------------------------- */
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  /* ------------------------------------------------------------------------ */
  /* SCROLL ANIMATIONS */
  #gb-icons {
    visibility: hidden;
    -webkit-animation: none;
            animation: none;
    z-index: 2;
  }
  
  #gb-icons.hello {
    visibility: visible;
    -webkit-animation: slide-in-top 0.5s cubic-bezier(0.25, 0.46, 0.45, 0.94) 0.5s both;
    animation: slide-in-top 0.5s cubic-bezier(0.25, 0.46, 0.45, 0.94) 0.5s both;
  }
  
  #gb-icons-sticky.hello {
    opacity: 1;
  }
  
  .hp [data-region=c] .ui-widget.app .ui-widget-header h1,
  .hp [data-region=c] .ui-widget.app.upcomingevents .ui-widget-header.icon::before,
  .hp [data-region=c] .ui-widget.app .more-link,
  .hp [data-region=c] .ui-widget.app .view-calendar-link,
  .hp [data-region=c] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss,
  .hp [data-region=c] .ui-widget.app.upcomingevents .ui-widget-detail,
  .hp [data-region=d] .ui-widget.app .ui-widget-header h1,
  .hp [data-region=d] .ui-widget.app .ui-widget-header.icon::before,
  .hp [data-region=d] .ui-widget.app .ui-widget-header.icon::after,
  .hp [data-region=e] .ui-widget.app .ui-widget-header h1,
  .hp [data-region=e] .ui-widget.app .ui-widget-header.icon::before,
  .hp [data-region=e] .ui-widget.app .ui-widget-header.icon::after,
  .hp [data-region=f] .ui-widget.app .ui-widget-header h1,
  .hp [data-region=f] .ui-widget.app .ui-widget-header.icon::before,
  .hp [data-region=f] .ui-widget.app .ui-widget-header.icon::after,
  .hp [data-region=d] .ui-widget.app .ui-widget-detail,
  .hp [data-region=e] .ui-widget.app .ui-widget-detail,
  .hp [data-region=f] .ui-widget.app .ui-widget-detail,
  .hp [data-region=d] .ui-widget.app .more-link,
  .hp [data-region=d] .ui-widget.app .view-calendar-link,
  .hp [data-region=d] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss,
  .hp [data-region=e] .ui-widget.app .more-link,
  .hp [data-region=e] .ui-widget.app .view-calendar-link,
  .hp [data-region=e] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss,
  .hp [data-region=f] .ui-widget.app .more-link,
  .hp [data-region=f] .ui-widget.app .view-calendar-link,
  .hp [data-region=f] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss,
  .hp [data-region=g] .ui-widget.app .ui-widget-header h1,
  .hp [data-region=g] .ui-widget.app .ui-widget-header.icon,
  .hp [data-region=g] .ui-widget.app.headlines .ui-widget-detail,
  .hp [data-region=h] .ui-widget.app .ui-widget-header.icon::before,
  .hp [data-region=h] .ui-widget.app .ui-widget-header.icon::after,
  .hp [data-region=h] .ui-widget.app .ui-widget-header h1,
  .hp [data-region=h] .ui-widget.app .ui-widget-header h1::before,
  .hp [data-region=h] .ui-widget.app .more-link,
  .hp [data-region=h] .ui-widget.app .view-calendar-link,
  .hp [data-region=h] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss,
  .hp [data-region=h] .ui-widget.app .ui-widget-detail,
  .mg-header .mg-head-1,
  .mg-header-icon-1024 .mg-wand-icon,
  .mg-header-icon-1024 .mg-star.i1,
  .mg-header-icon-1024 .mg-star.i2,
  .mg-header-icon-1024 .mg-star.i3,
  .mg-header-icon-1024 .mg-star.i4,
  .mg-header-icon-1024 .mg-star.i5 {
    visibility: hidden;
    -webkit-animation: none;
            animation: none;
  }
  
  .hp [data-region=c] .ui-widget.app.hello .ui-widget-header h1,
  .hp [data-region=g] .ui-widget.app.hello .ui-widget-header h1,
  .hp [data-region=h] .ui-widget.app.hello .ui-widget-header h1 {
    visibility: visible;
    -webkit-animation: slide-in-left 1s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
    animation: slide-in-left 1s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
  }
  
  .hp [data-region=c] .ui-widget.app.hello.upcomingevents .ui-widget-header.icon::before {
    visibility: visible;
    -webkit-animation: slide-in-top 1.3s cubic-bezier(0.25, 0.46, 0.45, 0.94) 0.5s both;
    animation: slide-in-top 1.3s cubic-bezier(0.25, 0.46, 0.45, 0.94) 0.5s both;
  }
  
  .hp [data-region=c] .ui-widget.ap.hello .more-link,
  .hp [data-region=c] .ui-widget.app.hello .view-calendar-link,
  .hp [data-region=c] .ui-widget.app.hello div.app-level-social-rss a.ui-btn-toolbar.rss,
  .hp [data-region=h] .ui-widget.app.hello .more-link,
  .hp [data-region=h] .ui-widget.app.hello .view-calendar-link,
  .hp [data-region=h] .ui-widget.app.hello div.app-level-social-rss a.ui-btn-toolbar.rss {
    visibility: visible;
    -webkit-animation: fade-in 2s cubic-bezier(0.39, 0.575, 0.565, 1) both;
    animation: fade-in 2s cubic-bezier(0.39, 0.575, 0.565, 1) both;
  }
  
  .hp [data-region=c] .ui-widget.app.hello.upcomingevents .ui-widget-detail,
  .hp [data-region=g] .ui-widget.app.hello.headlines .ui-widget-detail,
  .hp [data-region=h] .ui-widget.app.hello .ui-widget-detail {
    visibility: visible;
    -webkit-animation: slide-in-bottom 2.2s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
    animation: slide-in-bottom 2.2s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
  }
  
  .hp [data-region=d] .ui-widget.app.hello .ui-widget-header.icon::before,
  .hp [data-region=e] .ui-widget.app.hello .ui-widget-header.icon::before,
  .hp [data-region=f] .ui-widget.app.hello .ui-widget-header.icon::before,
  .hp [data-region=d] .ui-widget.app.hello .ui-widget-header.icon::after,
  .hp [data-region=e] .ui-widget.app.hello .ui-widget-header.icon::after,
  .hp [data-region=f] .ui-widget.app.hello .ui-widget-header.icon::after,
  .hp [data-region=g] .ui-widget.app.hello .ui-widget-header.icon {
    visibility: visible;
    -webkit-animation: fade-in 2s cubic-bezier(0.39, 0.575, 0.565, 1) both;
    animation: fade-in 2s cubic-bezier(0.39, 0.575, 0.565, 1) both;
  }
  
  .hp [data-region=d] .ui-widget.app.hello .ui-widget-header h1,
  .hp [data-region=e] .ui-widget.app.hello .ui-widget-header h1,
  .hp [data-region=f] .ui-widget.app.hello .ui-widget-header h1 {
    visibility: visible;
    -webkit-animation: slide-in-bottom 1.5s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
    animation: slide-in-bottom 1.5s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
  }
  
  .hp [data-region=d] .ui-widget.app.hello .ui-widget-detail,
  .hp [data-region=e] .ui-widget.app.hello .ui-widget-detail,
  .hp [data-region=f] .ui-widget.app.hello .ui-widget-detail {
    visibility: visible;
    -webkit-animation: slide-in-bottom 2.5s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
    animation: slide-in-bottom 2.5s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
  }
  
  .hp [data-region=d] .ui-widget.app.hello .more-link,
  .hp [data-region=d] .ui-widget.app.hello .view-calendar-link,
  .hp [data-region=d] .ui-widget.app.hello div.app-level-social-rss a.ui-btn-toolbar.rss,
  .hp [data-region=e] .ui-widget.app.hello .more-link,
  .hp [data-region=e] .ui-widget.app.hello .view-calendar-link,
  .hp [data-region=e] .ui-widget.app.hello div.app-level-social-rss a.ui-btn-toolbar.rss,
  .hp [data-region=f] .ui-widget.app.hello .more-link,
  .hp [data-region=f] .ui-widget.app.hello .view-calendar-link,
  .hp [data-region=f] .ui-widget.app.hello div.app-level-social-rss a.ui-btn-toolbar.rss,
  .hp [data-region=h] .ui-widget.app.hello .ui-widget-header.icon::before,
  .hp [data-region=h] .ui-widget.app.hello .ui-widget-header.icon::after {
    visibility: visible;
    -webkit-animation: fade-in 2s cubic-bezier(0.39, 0.575, 0.565, 1) both;
    animation: fade-in 2s cubic-bezier(0.39, 0.575, 0.565, 1) both;
  }
  
  .hp [data-region=h] .ui-widget.app.hello .ui-widget-header h1::before {
    visibility: visible;
    -webkit-animation: fade-in 9s cubic-bezier(0.39, 0.575, 0.565, 1) both;
    animation: fade-in 9s cubic-bezier(0.39, 0.575, 0.565, 1) both;
  }
  
  .mg-header.hello .mg-head-1,
  .mg-header.hello .mg-head-2 {
    visibility: visible;
    -webkit-animation: fade-in 2s cubic-bezier(0.39, 0.575, 0.565, 1) both;
    animation: fade-in 2s cubic-bezier(0.39, 0.575, 0.565, 1) both;
  }
  
  .mg-header-icon-1024 .mg-wand-icon {
    visibility: visible;
    -webkit-animation: slide-in-top 1.3s cubic-bezier(0.25, 0.46, 0.45, 0.94) 0.5s both;
    animation: slide-in-top 1.3s cubic-bezier(0.25, 0.46, 0.45, 0.94) 0.5s both;
    -webkit-animation-delay: 0.75s;
            animation-delay: 0.75s;
  }
  
  .mg-header-icon-1024.hello .mg-star.i1 {
    visibility: visible;
    -webkit-animation: fade-in 1.5s cubic-bezier(0.39, 0.575, 0.565, 1) both;
    animation: fade-in 1.5s cubic-bezier(0.39, 0.575, 0.565, 1) both;
    -webkit-animation-delay: 1s;
            animation-delay: 1s;
  }
  
  .mg-header-icon-1024.hello .mg-star.i2 {
    visibility: visible;
    -webkit-animation: fade-in 1.5s cubic-bezier(0.39, 0.575, 0.565, 1) both;
    animation: fade-in 1.5s cubic-bezier(0.39, 0.575, 0.565, 1) both;
    -webkit-animation-delay: 2.7s;
            animation-delay: 2.7s;
  }
  
  .mg-header-icon-1024.hello .mg-star.i3 {
    visibility: visible;
    -webkit-animation: fade-in 1.5s cubic-bezier(0.39, 0.575, 0.565, 1) both;
    animation: fade-in 1.5s cubic-bezier(0.39, 0.575, 0.565, 1) both;
    -webkit-animation-delay: 2s;
            animation-delay: 2s;
  }
  
  .mg-header-icon-1024.hello .mg-star.i4 {
    visibility: visible;
    -webkit-animation: fade-in 1.5s cubic-bezier(0.39, 0.575, 0.565, 1) both;
    animation: fade-in 1.5s cubic-bezier(0.39, 0.575, 0.565, 1) both;
    -webkit-animation-delay: 3s;
            animation-delay: 3s;
  }
  
  .mg-header-icon-1024.hello .mg-star.i5 {
    visibility: visible;
    -webkit-animation: fade-in 1.5s cubic-bezier(0.39, 0.575, 0.565, 1) both;
    animation: fade-in 1.5s cubic-bezier(0.39, 0.575, 0.565, 1) both;
    -webkit-animation-delay: 1.7s;
            animation-delay: 1.7s;
  }
  
  #animation-graphic.hello #cloud-1 {
    -webkit-animation: slide-left-long 800s cubic-bezier(0.25, 0.46, 0.45, 0.94) infinite both;
    animation: slide-left-long 800s cubic-bezier(0.25, 0.46, 0.45, 0.94) infinite both;
  }
  
  #animation-graphic.hello #cloud-2,
  #animation-graphic.hello #cloud-3 {
    -webkit-animation: slide-right-long 800s cubic-bezier(0.25, 0.46, 0.45, 0.94) infinite both;
    animation: slide-right-long 800s cubic-bezier(0.25, 0.46, 0.45, 0.94) infinite both;
  }
  
  #animation-graphic.hello #car {
    -webkit-animation-delay: 1s;
            animation-delay: 1s;
    -webkit-animation: slide-left-long 150s cubic-bezier(0.25, 0.46, 0.45, 0.94) infinite both;
    animation: slide-left-long 150s cubic-bezier(0.25, 0.46, 0.45, 0.94) infinite both;
  }
  
  #animation-graphic.hello #windmill-1 {
    -webkit-animation: rotate-center 40s ease-in-out infinite both;
    animation: rotate-center 40s ease-in-out infinite both;
  }
  
  #animation-graphic.hello #windmill-2 {
    -webkit-animation: rotate-center 60s ease-in-out infinite both;
    animation: rotate-center 60s ease-in-out infinite both;
  }
  
  #animation-graphic.hello #windmill-3 {
    -webkit-animation: rotate-center 55s ease-in-out infinite both;
    animation: rotate-center 55s ease-in-out infinite both;
  }
  
  /* SCROLL END */
  /* MAGIC ANIMATIONS */
  @-webkit-keyframes t0_1-in {
    from {
      stroke-dashoffset: 230;
      opacity: 0;
    }
    to {
      stroke-dashoffset: 0;
      opacity: 1;
    }
  }
  @keyframes t0_1-in {
    from {
      stroke-dashoffset: 230;
      opacity: 0;
    }
    to {
      stroke-dashoffset: 0;
      opacity: 1;
    }
  }
  @-webkit-keyframes t0_1-out {
    from {
      stroke-dashoffset: 0;
      opacity: 1;
    }
    to {
      stroke-dashoffset: 230;
      opacity: 0;
    }
  }
  @keyframes t0_1-out {
    from {
      stroke-dashoffset: 0;
      opacity: 1;
    }
    to {
      stroke-dashoffset: 230;
      opacity: 0;
    }
  }
  @-webkit-keyframes t0_2-in {
    from {
      stroke-dashoffset: 70;
      opacity: 0;
    }
    to {
      stroke-dashoffset: 0;
      opacity: 1;
    }
  }
  @keyframes t0_2-in {
    from {
      stroke-dashoffset: 70;
      opacity: 0;
    }
    to {
      stroke-dashoffset: 0;
      opacity: 1;
    }
  }
  @-webkit-keyframes t0_2-out {
    from {
      stroke-dashoffset: 0;
      opacity: 1;
    }
    to {
      stroke-dashoffset: 70;
      opacity: 0;
    }
  }
  @keyframes t0_2-out {
    from {
      stroke-dashoffset: 0;
      opacity: 1;
    }
    to {
      stroke-dashoffset: 70;
      opacity: 0;
    }
  }
  @-webkit-keyframes t1_1-in {
    from {
      stroke-dashoffset: 30;
      opacity: 0;
    }
    to {
      stroke-dashoffset: 0;
      opacity: 1;
    }
  }
  @keyframes t1_1-in {
    from {
      stroke-dashoffset: 30;
      opacity: 0;
    }
    to {
      stroke-dashoffset: 0;
      opacity: 1;
    }
  }
  @-webkit-keyframes t1_1-out {
    from {
      stroke-dashoffset: 0;
      opacity: 1;
    }
    to {
      stroke-dashoffset: 30;
      opacity: 0;
    }
  }
  @keyframes t1_1-out {
    from {
      stroke-dashoffset: 0;
      opacity: 1;
    }
    to {
      stroke-dashoffset: 30;
      opacity: 0;
    }
  }
  @-webkit-keyframes t1_2-in {
    from {
      stroke-dashoffset: 330;
      opacity: 0;
    }
    to {
      stroke-dashoffset: 0;
      opacity: 1;
    }
  }
  @keyframes t1_2-in {
    from {
      stroke-dashoffset: 330;
      opacity: 0;
    }
    to {
      stroke-dashoffset: 0;
      opacity: 1;
    }
  }
  @-webkit-keyframes t1_2-out {
    from {
      stroke-dashoffset: 0;
      opacity: 1;
    }
    to {
      stroke-dashoffset: 330;
      opacity: 0;
    }
  }
  @keyframes t1_2-out {
    from {
      stroke-dashoffset: 0;
      opacity: 1;
    }
    to {
      stroke-dashoffset: 330;
      opacity: 0;
    }
  }
  @-webkit-keyframes t2_1-in {
    from {
      stroke-dashoffset: 90;
      opacity: 0;
    }
    to {
      stroke-dashoffset: 0;
      opacity: 1;
    }
  }
  @keyframes t2_1-in {
    from {
      stroke-dashoffset: 90;
      opacity: 0;
    }
    to {
      stroke-dashoffset: 0;
      opacity: 1;
    }
  }
  @-webkit-keyframes t2_1-out {
    from {
      stroke-dashoffset: 0;
      opacity: 1;
    }
    to {
      stroke-dashoffset: 90;
      opacity: 0;
    }
  }
  @keyframes t2_1-out {
    from {
      stroke-dashoffset: 0;
      opacity: 1;
    }
    to {
      stroke-dashoffset: 90;
      opacity: 0;
    }
  }
  @-webkit-keyframes t2_2-in {
    from {
      stroke-dashoffset: 437;
      opacity: 0;
    }
    to {
      stroke-dashoffset: 0;
      opacity: 1;
    }
  }
  @keyframes t2_2-in {
    from {
      stroke-dashoffset: 437;
      opacity: 0;
    }
    to {
      stroke-dashoffset: 0;
      opacity: 1;
    }
  }
  @-webkit-keyframes t2_2-out {
    from {
      stroke-dashoffset: 0;
      opacity: 1;
    }
    to {
      stroke-dashoffset: 437;
      opacity: 0;
    }
  }
  @keyframes t2_2-out {
    from {
      stroke-dashoffset: 0;
      opacity: 1;
    }
    to {
      stroke-dashoffset: 437;
      opacity: 0;
    }
  }
  @-webkit-keyframes t3_1-in {
    from {
      stroke-dashoffset: 21;
      opacity: 0;
    }
    to {
      stroke-dashoffset: 0;
      opacity: 1;
    }
  }
  @keyframes t3_1-in {
    from {
      stroke-dashoffset: 21;
      opacity: 0;
    }
    to {
      stroke-dashoffset: 0;
      opacity: 1;
    }
  }
  @-webkit-keyframes t3_1-out {
    from {
      stroke-dashoffset: 0;
      opacity: 1;
    }
    to {
      stroke-dashoffset: 21;
      opacity: 0;
    }
  }
  @keyframes t3_1-out {
    from {
      stroke-dashoffset: 0;
      opacity: 1;
    }
    to {
      stroke-dashoffset: 21;
      opacity: 0;
    }
  }
  @-webkit-keyframes t3_2-in {
    from {
      stroke-dashoffset: 782;
      opacity: 0;
    }
    to {
      stroke-dashoffset: 0;
      opacity: 1;
    }
  }
  @keyframes t3_2-in {
    from {
      stroke-dashoffset: 782;
      opacity: 0;
    }
    to {
      stroke-dashoffset: 0;
      opacity: 1;
    }
  }
  @keyframes t1_2-out {
    from {
      stroke-dashoffset: 0;
      opacity: 1;
    }
    to {
      stroke-dashoffset: 782;
      opacity: 0;
    }
  }
  /* MAGIC END */
  /**
   * ----------------------------------------
   * animation slide-in-top
   * ----------------------------------------
   */
  @-webkit-keyframes slide-in-top {
    0% {
      -webkit-transform: translateY(-100px);
      transform: translateY(-100px);
      opacity: 0;
    }
    100% {
      -webkit-transform: translateY(0);
      transform: translateY(0);
      opacity: 1;
    }
  }
  @keyframes slide-in-top {
    0% {
      -webkit-transform: translateY(-100px);
      transform: translateY(-100px);
      opacity: 0;
    }
    100% {
      -webkit-transform: translateY(0);
      transform: translateY(0);
      opacity: 1;
    }
  }
  /**
   * ----------------------------------------
   * animation fade-in-bottom
   * ----------------------------------------
   */
  @-webkit-keyframes fade-in-bottom {
    0% {
      -webkit-transform: translateY(50px);
      transform: translateY(50px);
      opacity: 0;
    }
    100% {
      -webkit-transform: translateY(0);
      transform: translateY(0);
      opacity: 1;
    }
  }
  @keyframes fade-in-bottom {
    0% {
      -webkit-transform: translateY(50px);
      transform: translateY(50px);
      opacity: 0;
    }
    100% {
      -webkit-transform: translateY(0);
      transform: translateY(0);
      opacity: 1;
    }
  }
  /**
   * ----------------------------------------
   * animation slide-in-left
   * ----------------------------------------
   */
  @-webkit-keyframes slide-in-left {
    0% {
      -webkit-transform: translateX(-1000px);
      transform: translateX(-1000px);
      opacity: 0;
    }
    100% {
      -webkit-transform: translateX(0);
      transform: translateX(0);
      opacity: 1;
    }
  }
  @keyframes slide-in-left {
    0% {
      -webkit-transform: translateX(-1000px);
      transform: translateX(-1000px);
      opacity: 0;
    }
    100% {
      -webkit-transform: translateX(0);
      transform: translateX(0);
      opacity: 1;
    }
  }
  /**
  * ----------------------------------------
  * animation fade-in
  * ----------------------------------------
  */
  @-webkit-keyframes fade-in {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }
  @keyframes fade-in {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }
  /**
   * ----------------------------------------
   * animation slide-in-bottom
   * ----------------------------------------
   */
  @-webkit-keyframes slide-in-bottom {
    0% {
      -webkit-transform: translateY(1000px);
      transform: translateY(1000px);
      opacity: 0;
    }
    100% {
      -webkit-transform: translateY(0);
      transform: translateY(0);
      opacity: 1;
    }
  }
  @keyframes slide-in-bottom {
    0% {
      -webkit-transform: translateY(1000px);
      transform: translateY(1000px);
      opacity: 0;
    }
    100% {
      -webkit-transform: translateY(0);
      transform: translateY(0);
      opacity: 1;
    }
  }
  /**
   * ----------------------------------------
   * animation slide-left-long
   * ----------------------------------------
   */
  @-webkit-keyframes slide-left-long {
    0% {
      -webkit-transform: translate(0px, 0px);
      transform: translate(0px, 0px);
    }
    100% {
      -webkit-transform: translate(1500px, 0px);
      transform: translate(1500px, 0px);
    }
  }
  @keyframes slide-left-long {
    0% {
      -webkit-transform: translate(0px, 0px);
      transform: translate(0px, 0px);
    }
    100% {
      -webkit-transform: translate(1500px, 0px);
      transform: translate(1500px, 0px);
    }
  }
  /**
  * ----------------------------------------
  * animation slide-right-long
  * ----------------------------------------
  */
  @-webkit-keyframes slide-right-long {
    0% {
      -webkit-transform: translate(0px, 0px);
      transform: translate(0px, 0px);
    }
    100% {
      -webkit-transform: translate(-1500px, 0px);
      transform: translate(-1500px, 0px);
    }
  }
  @keyframes slide-right-long {
    0% {
      -webkit-transform: translate(0px, 0px);
      transform: translate(0px, 0px);
    }
    100% {
      -webkit-transform: translate(-1500px, 0px);
      transform: translate(-1500px, 0px);
    }
  }
  /**
   * ----------------------------------------
   * animation rotate-center
   * ----------------------------------------
   */
  @-webkit-keyframes rotate-center {
    0% {
      -webkit-transform: rotate(0);
      transform: rotate(0);
    }
    100% {
      -webkit-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
  @keyframes rotate-center {
    0% {
      -webkit-transform: rotate(0);
      transform: rotate(0);
    }
    100% {
      -webkit-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
  /**
   * ----------------------------------------
   * animation rotate-center
   * ----------------------------------------
   */
  @-webkit-keyframes rotate-center {
    0% {
      -webkit-transform: rotate(0);
      transform: rotate(0);
    }
    100% {
      -webkit-transform: rotate(180deg);
      transform: rotate(180deg);
    }
  }
  @keyframes rotate-center {
    0% {
      -webkit-transform: rotate(0);
      transform: rotate(0);
    }
    100% {
      -webkit-transform: rotate(180deg);
      transform: rotate(180deg);
    }
  }
  /**
  * ----------------------------------------
  * animation rotate-reverse
  * ----------------------------------------
  */
  @-webkit-keyframes rotate-reverse {
    0% {
      -webkit-transform: rotate(180deg);
      transform: rotate(180deg);
    }
    100% {
      -webkit-transform: rotate(0);
      transform: rotate(0);
    }
  }
  @keyframes rotate-reverse {
    0% {
      -webkit-transform: rotate(180deg);
      transform: rotate(180deg);
    }
    100% {
      -webkit-transform: rotate(0);
      transform: rotate(0);
    }
  }
  /* GROUPEND*//* MediaEnd *//* MediaBegin 768+ */ @media (max-width: 1023px) {@charset "UTF-8";
/* GroupBegin Global */
body:before {
  content: "768";
}

.show768 {
  display: block;
}

.hide768 {
  display: none;
}

.ui-column-one-quarter.region {
  width: 50%;
  float: left;
  clear: left;
}

.ui-column-one-third.region {
  width: 50%;
  float: left;
}

.ui-column-two-thirds.region {
  width: 50%;
  float: left;
}

.region.right {
  float: left;
}

.region.clearleft {
  clear: none;
}

/* GroupEnd */
.nav-section .cs-button-section {
  display: none;
}

.content-wrap {
  padding: 0 35px;
}

/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------- ### HEADER ### ------------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
.nav-section.right {
  padding-left: 29px;
}
.nav-section.left {
  margin-right: 9px;
}
.nav-section .cs-button-section {
  display: none;
}

.mystartbar .content-wrap {
  padding: 0;
}

#gb-header-mobile {
  height: 100%;
  padding-right: 29px;
}
#gb-header-mobile #rs-menu-btn {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  height: 100%;
}
#gb-header-mobile #rs-menu-btn::before {
  background: transparent;
  font-size: 21px;
}
#gb-header-mobile #rs-menu-btn span {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-line-pack: center;
      align-content: center;
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
  font-weight: 400;
  font-family: "Bebas Neue", cursive;
  font-size: 1.3125rem;
  color: #fff;
  padding: 0;
}

#gb-header .mobile-logo-flex {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  padding: 0 32px;
}

.header-logo {
  min-height: auto;
  padding: 14px 0 13px 0;
  z-index: 10;
}

#cs-ar {
  position: absolute;
  bottom: -57px;
  right: 72px;
  margin-left: 0;
}
#cs-ar .mystart-selector {
  padding: 0;
}
#cs-ar .mystart-selector::before {
  width: 236px;
  height: 57px;
  left: -31px;
  top: -3px;
}
#cs-ar span {
  font-size: 1.3125rem;
  margin-top: -9px;
  letter-spacing: 0.1px;
}
#cs-ar span::after {
  padding-left: 10px;
}
#cs-ar #ar-dropdown {
  top: 100%;
  z-index: 5000;
}

#school-list {
  top: 95%;
}

.rs-menu-group.quicklinks {
  display: none;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------ ### HOMEPAGE  ### ----------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
.hp-row.one {
  margin-top: -50px;
}
.hp-row.one .content-wrap {
  padding: 0;
}
.hp-row.three {
  padding: 27px 0 20px;
}
.hp-row.four {
  padding-bottom: 57px;
}
.hp-row.four .hp-column {
  padding: 0 11px 0 0;
}
.hp-row.four .hp-column.one, .hp-row.four .hp-column.two {
  width: 50%;
}
.hp-row.four .hp-column.two {
  padding-right: 0px;
  padding-left: 11px;
}
.hp-row.four .hp-column.three {
  padding-top: 129px;
  width: 100%;
}
.hp-row.four .content-wrap.extra-row {
  padding-top: 53px;
  padding-right: 18px;
}
.hp-row.five {
  padding-bottom: 30px;
}

/* MMG AREA*/
#hp-slideshow {
  overflow: hidden;
  --mask: none;
  -webkit-mask: var(--mask);
  mask: var(--mask);
  -webkit-mask-repeat: repeat-x;
}
#hp-slideshow .mmg-description-outer {
  padding: 21px 37px 5px;
  border-radius: 0;
}
#hp-slideshow .mmg-description-wrapper {
  position: relative;
  top: auto;
  right: auto;
  bottom: auto;
  width: auto;
  max-width: none;
}
#hp-slideshow .mmg-description {
  display: inline-block;
  position: relative;
  width: 100%;
  max-height: 100%;
  padding: 0;
  text-align: left;
  border-radius: 4px;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  opacity: 1;
}
#hp-slideshow .mmg-description-caption {
  margin-bottom: 12px;
}
#hp-slideshow .mmg-description-links {
  position: relative;
  left: auto;
  bottom: auto;
}
#hp-slideshow .mmg-controls {
  top: -33px;
  right: calc(12% - 41px);
  left: auto;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ----------------------- ### HP REGION APPS ### ------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
.hp [data-region=c] .ui-widget.app .ui-widget-header h1 {
  font-size: 2.25rem;
  margin-top: 0;
}
.hp [data-region=c] .ui-widget.app.upcomingevents .ui-widget-header {
  padding-bottom: 37px;
}
.hp [data-region=c] .ui-widget.app.upcomingevents .ui-widget-header.icon {
  padding-left: 60px;
}
.hp [data-region=c] .ui-widget.app.upcomingevents .ui-widget-header.icon::before {
  top: 0px;
  font-size: 44px;
  left: 3px;
}
.hp [data-region=c] .ui-widget.app.upcomingevents .ui-articles li {
  padding: 11px 25px;
  margin: 2px 15px 9px 0px;
}
.hp [data-region=c] .ui-widget.app.upcomingevents .ui-articles li:nth-child(n+4) {
  display: none;
}
.hp [data-region=c] .ui-widget.app.upcomingevents .ui-articles .upcoming-column.right {
  padding-top: 15px;
}
.hp [data-region=c] .ui-widget.app .more-link,
.hp [data-region=c] .ui-widget.app .view-calendar-link,
.hp [data-region=c] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss {
  margin-right: 0;
}
.hp [data-region=c] .ui-widget.app .more-link::before,
.hp [data-region=c] .ui-widget.app .view-calendar-link::before,
.hp [data-region=c] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss::before {
  display: none;
}
.hp [data-region=d] .ui-widget.app .ui-widget-header h1 {
  margin-top: -4px;
  padding-bottom: 8px;
}
.hp [data-region=d] .ui-widget.app .ui-widget-header.icon {
  padding-left: 74px;
}
.hp [data-region=d] .ui-widget.app .ui-widget-header.icon::before {
  font-size: 38px;
  left: 9px;
  top: -5px;
}
.hp [data-region=d] .ui-widget.app .ui-widget-header.icon::after {
  width: 61px;
  height: 61px;
  top: -15px;
  left: 1px;
}
.hp [data-region=e] .ui-widget.app .ui-widget-header h1 {
  margin-top: -4px;
  padding-bottom: 8px;
}
.hp [data-region=e] .ui-widget.app .ui-widget-header.icon {
  padding-left: 72px;
  padding-bottom: 11px;
}
.hp [data-region=e] .ui-widget.app .ui-widget-header.icon::before {
  font-size: 30px;
  left: 15px;
  top: -1px;
}
.hp [data-region=e] .ui-widget.app .ui-widget-header.icon::after {
  width: 61px;
  height: 61px;
  top: -15px;
  left: -7px;
}
.hp [data-region=f] .ui-widget.app .ui-widget-header h1 {
  margin-top: -4px;
  padding-bottom: 8px;
}
.hp [data-region=f] .ui-widget.app .ui-widget-header.icon {
  padding-left: 75px;
  padding-bottom: 11px;
}
.hp [data-region=f] .ui-widget.app .ui-widget-header.icon::before {
  left: 15px;
  top: -2px;
  font-size: 30px;
}
.hp [data-region=f] .ui-widget.app .ui-widget-header.icon::after {
  width: 61px;
  height: 74px;
  left: -1px;
  top: -14px;
}
.hp [data-region=g] .ui-widget.app {
  -webkit-box-align: start;
      -ms-flex-align: start;
          align-items: flex-start;
}
.hp [data-region=g] .ui-widget.app .ui-widget-header {
  margin-top: 0;
  padding-top: 38px;
}
.hp [data-region=g] .ui-widget.app .ui-widget-header.icon::before {
  font-size: 80px;
  top: -10px;
  left: 8px;
}
.hp [data-region=g] .ui-widget.app .ui-widget-header h1 {
  font-size: 1.9375rem;
  line-height: 40px;
  margin-top: 9px;
}
.hp [data-region=g] .ui-widget.app .more-link,
.hp [data-region=g] .ui-widget.app .view-calendar-link,
.hp [data-region=g] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss {
  margin-right: 0;
  padding: 13px 17px;
}
.hp [data-region=g] .ui-widget.app.headlines .ui-widget-header {
  -ms-flex-preferred-size: 36%;
      flex-basis: 36%;
  padding-right: 3px;
}
.hp [data-region=g] .ui-widget.app.headlines .ui-widget-detail {
  -ms-flex-preferred-size: 54%;
      flex-basis: 54%;
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
}
.hp [data-region=g] .ui-widget.app.headlines .ui-article-thumb .img img {
  max-width: none;
  max-height: none;
}
.hp [data-region=g] .ui-widget.app.headlines .ui-article {
  height: auto;
  background-image: none !important;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}
.hp [data-region=g] .ui-widget.app.headlines .ui-article .headline-description {
  position: relative;
  opacity: 1;
  background-color: transparent;
  text-align: left;
  padding: 0;
}
.hp [data-region=g] .ui-widget.app.headlines .ui-article .headline-description > div {
  opacity: 1;
  top: auto;
}
.hp [data-region=g] .ui-widget.app.headlines .ui-articles {
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}
.hp [data-region=g] .ui-widget.app.headlines .ui-articles li:nth-child(n+3) {
  display: none;
}
.hp [data-region=g] .ui-widget.app.headlines .ui-articles .ui-article-thumb {
  display: block !important;
}
.hp [data-region=g] .ui-widget.app.headlines .ui-articles .ui-article-description {
  color: #000;
}
.hp [data-region=h] .ui-widget.app .ui-widget-header {
  padding-bottom: 28px;
  padding-left: 77px;
}
.hp [data-region=h] .ui-widget.app .ui-widget-header h1 {
  font-size: 1.9375rem;
}
.hp [data-region=h] .ui-widget.app .ui-widget-header h1::before {
  right: -27px;
  width: 27px;
  height: 27px;
  top: -6px;
}
.hp [data-region=h] .ui-widget.app .ui-widget-header .more-link,
.hp [data-region=h] .ui-widget.app .ui-widget-header .view-calendar-link,
.hp [data-region=h] .ui-widget.app .ui-widget-header div.app-level-social-rss a.ui-btn-toolbar.rss {
  margin-right: 0;
  padding: 13px 17px;
}
.hp [data-region=h] .ui-widget.app .ui-widget-header .more-link::before,
.hp [data-region=h] .ui-widget.app .ui-widget-header .view-calendar-link::before,
.hp [data-region=h] .ui-widget.app .ui-widget-header div.app-level-social-rss a.ui-btn-toolbar.rss::before {
  display: none;
}
.hp [data-region=h] .ui-widget.app.lightbox .ui-widget-header.icon::before {
  top: 9px;
  left: 21px;
  font-size: 32px;
}
.hp [data-region=h] .ui-widget.app.lightbox .ui-widget-header.icon::after {
  width: 61px;
  height: 61px;
  top: -6px;
}
.hp [data-region=h] .ui-widget.app.lightbox .ui-widget-detail .ui-articles {
  margin: 0 !important;
  -webkit-box-pack: justify !important;
      -ms-flex-pack: justify !important;
          justify-content: space-between !important;
}
.hp [data-region=h] .ui-widget.app.lightbox .ui-widget-detail .ui-articles li {
  -ms-flex-preferred-size: 33.33%;
      flex-basis: 33.33%;
}
.hp [data-region=h] .ui-widget.app.lightbox .ui-widget-detail .ui-articles li:nth-child(n+4) {
  display: none;
}

.hp [data-region=g] .ui-widget.app.headlines .ui-widget-detail .ui-articles .ui-article .ui-article-title a {
  color: #014F7F;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------ ### GENERAL APPS ### -------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
.ui-widget.app .ui-widget-header h1 {
  font-size: 1.9375rem;
  letter-spacing: 0.1px;
}
.ui-widget.app .more-link,
.ui-widget.app .view-calendar-link,
.ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss {
  padding: 13px 13px 12px;
}

/* GroupEnd */
/* GroupBegin Subpage */
#sp-content {
  display: block;
}

.sp-column.one,
.sp-column.two {
  min-height: 0px;
  float: none;
  width: 100%;
  padding: 0;
}

.sp-column.one .ui-widget.app div.ui-widget-header {
  padding: 15px 20px;
  background: #F7F7F7;
  border: solid 1px #cccccc;
  border-radius: 4px;
}

.sp-column.one .ui-widget.app div.ui-widget-header h1 {
  position: relative;
  padding-right: 25px;
}

.sp-column.one .ui-widget.app div.ui-widget-header h1:before {
  content: "";
  font-family: "-icons";
  font-size: 1.125rem;
  color: #000;
  display: block;
  position: absolute;
  top: calc(50% - 9px);
  right: 0;
}

.sp-column.one .ui-widget.app.open > div.ui-widget-header h1:before {
  content: "";
  -webkit-transform: rotate(90deg);
          transform: rotate(90deg);
}

.sp-column.one .ui-widget.app div.ui-widget-detail {
  padding-bottom: 35px;
}

.sp-column.two {
  width: calc(100% + 42px);
  margin-left: -42px;
  padding-top: 35px;
}

/* GroupEnd */
/* GroupBegin Subpage No Nav */
#spn-content {
  min-height: 0px;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------ ### MAGIC SECTION ### ------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
.mg-header {
  padding-top: 12px;
  padding-bottom: 16px;
}
.mg-header .mg-head-1 {
  font-size: 7.6875rem;
  margin-right: 21px;
  margin-left: 11px;
}
.mg-header .mg-header-icon {
  max-width: 140px;
  max-height: 127px;
  right: auto;
  left: 350px;
}
.mg-header .mg-head-2 {
  font-size: 1.875rem;
  top: 60px;
  left: 460px;
  right: auto;
}

#magic-mobile {
  margin-left: 5px;
  margin-bottom: -9px;
}
#magic-mobile .mg-title-mobile {
  position: relative;
  padding-bottom: 10px;
}
#magic-mobile .mg-title-mobile h2 {
  font-family: "Nunito", sans-serif;
  font-weight: 400;
  font-size: 3.5625rem;
  color: #4666A9;
}
#magic-mobile .mg-title-mobile::before {
  display: block;
  content: "";
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background: #FFDD00;
  height: 5px;
  width: 570px;
  top: auto;
  bottom: 0;
}
#magic-mobile .mg-description-mobile {
  margin-top: -12px;
}
#magic-mobile .mg-description-mobile p {
  font-family: "Nunito", sans-serif;
  font-weight: 400;
  font-size: 1.75rem;
  color: #000;
}

#animation-graphic {
  padding-top: 0;
  padding-bottom: 5px;
}
#animation-graphic::before {
  background: -webkit-gradient(linear, left top, left bottom, color-stop(85%, rgba(255, 255, 255, 0)), to(rgba(89, 142, 246, 0.5)));
  background: linear-gradient(180deg, rgba(255, 255, 255, 0) 85%, rgba(89, 142, 246, 0.5) 100%);
}
#animation-graphic::after {
  height: 17px;
  bottom: -13px;
}
#animation-graphic #city-bg-mobile {
  min-height: 230px;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------- ### FOOTER ### ------------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
.gb-footer-row.one {
  padding-bottom: 0px;
}
.gb-footer-row.one::before {
  display: none;
}

.gb-logo-footer {
  display: none;
}

.footer-address {
  padding: 0;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
}
.footer-address .footer-info-wrap {
  padding: 0px 23px 0 49px;
}
.footer-address .footer-info-wrap .footer-info-wrapper {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: horizontal;
  -webkit-box-direction: normal;
      -ms-flex-direction: row;
          flex-direction: row;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
}
.footer-address .footer-info-wrap .footer-info-wrapper p {
  -webkit-box-flex: 1;
      -ms-flex-positive: 1;
          flex-grow: 1;
  font-size: 1.125rem;
  margin: -1px 15px 15px 0px;
}
.footer-address .footer-info-wrap.top {
  padding-bottom: 36px;
  padding-top: 4px;
}

.footer-link-container {
  padding-left: 30px;
  width: 153px;
}
.footer-link-container #footer-link-list {
  -webkit-column-count: auto;
     -moz-column-count: auto;
          column-count: auto;
  -webkit-column-gap: 0;
     -moz-column-gap: 0;
          column-gap: 0;
}

#disclaimer-row {
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------ ### GB ICONS ### ------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
#gb-icons .cs-global-icons {
  -webkit-box-pack: start;
      -ms-flex-pack: start;
          justify-content: flex-start;
}
#gb-icons .cs-global-icons li {
  -ms-flex-preferred-size: 20%;
      flex-basis: 20%;
  margin-bottom: 13px;
}
#gb-icons .cs-global-icons li .icon {
  padding: 23px 30px 10px;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------ ### SOCIAL MEDIA ### -------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
.gb-socials.mobile {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
  -webkit-box-flex: 1;
      -ms-flex: 1 1;
          flex: 1 1;
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
  padding-left: 11px;
}

/* GroupEnd */
/* UNDO SCROLL ANIMATIONS */
#gb-icons,
.hp [data-region=c] .ui-widget.app .ui-widget-header h1,
.hp [data-region=c] .ui-widget.app.upcomingevents .ui-widget-header.icon::before,
.hp [data-region=c] .ui-widget.app .more-link,
.hp [data-region=c] .ui-widget.app .view-calendar-link,
.hp [data-region=c] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss,
.hp [data-region=c] .ui-widget.app.upcomingevents .ui-widget-detail,
.hp [data-region=d] .ui-widget.app .ui-widget-header h1,
.hp [data-region=d] .ui-widget.app .ui-widget-header.icon::before,
.hp [data-region=d] .ui-widget.app .ui-widget-header.icon::after,
.hp [data-region=e] .ui-widget.app .ui-widget-header h1,
.hp [data-region=e] .ui-widget.app .ui-widget-header.icon::before,
.hp [data-region=e] .ui-widget.app .ui-widget-header.icon::after,
.hp [data-region=f] .ui-widget.app .ui-widget-header h1,
.hp [data-region=f] .ui-widget.app .ui-widget-header.icon::before,
.hp [data-region=f] .ui-widget.app .ui-widget-header.icon::after,
.hp [data-region=d] .ui-widget.app .ui-widget-detail,
.hp [data-region=e] .ui-widget.app .ui-widget-detail,
.hp [data-region=f] .ui-widget.app .ui-widget-detail,
.hp [data-region=d] .ui-widget.app .more-link,
.hp [data-region=d] .ui-widget.app .view-calendar-link,
.hp [data-region=d] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss,
.hp [data-region=e] .ui-widget.app .more-link,
.hp [data-region=e] .ui-widget.app .view-calendar-link,
.hp [data-region=e] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss,
.hp [data-region=f] .ui-widget.app .more-link,
.hp [data-region=f] .ui-widget.app .view-calendar-link,
.hp [data-region=f] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss,
.hp [data-region=g] .ui-widget.app .ui-widget-header h1,
.hp [data-region=g] .ui-widget.app .ui-widget-header.icon,
.hp [data-region=g] .ui-widget.app.headlines .ui-widget-detail,
.hp [data-region=h] .ui-widget.app .ui-widget-header.icon::before,
.hp [data-region=h] .ui-widget.app .ui-widget-header.icon::after,
.hp [data-region=h] .ui-widget.app .ui-widget-header h1,
.hp [data-region=h] .ui-widget.app .ui-widget-header h1::before,
.hp [data-region=h] .ui-widget.app .more-link,
.hp [data-region=h] .ui-widget.app .view-calendar-link,
.hp [data-region=h] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss,
.hp [data-region=h] .ui-widget.app .ui-widget-detail,
.mg-header .mg-head-1,
.mg-header-icon-1024 .mg-wand-icon,
.mg-header-icon-1024 .mg-star.i1,
.mg-header-icon-1024 .mg-star.i2,
.mg-header-icon-1024 .mg-star.i3,
.mg-header-icon-1024 .mg-star.i4,
.mg-header-icon-1024 .mg-star.i5 {
  visibility: visible;
}

/* SCROLL END */} /* MediaEnd *//* MediaBegin 640+ */ @media (max-width: 767px) {/* GroupBegin Global */
body:before {
  content: "640";
}

.show640 {
  display: block;
}

.hide640 {
  display: none;
}

.ui-spn .ui-column-one-third {
  width: auto !important;
  float: none !important;
}

.ui-column-one.region {
  width: auto;
  clear: none;
}

.ui-column-one-quarter.region {
  width: auto;
  float: none;
}

.ui-column-one-half.region {
  width: auto;
  float: none;
}

.ui-column-one-third.region {
  width: auto;
  float: none;
}

.ui-column-two-thirds.region {
  width: auto;
  float: none;
}

.region.right {
  float: none;
}

/* GroupEnd */
/* GroupBegin Header */
/* GroupEnd */
/* GroupBegin Footer */
.gb-bb-footer.links li:nth-child(2) a::after {
  display: none;
}

.gb-bb-footer.links ul {
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
  -webkit-flex-wrap: wrap;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------ ### MAGIC SECTION ### ------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
.mg-header.mobile .mg-head-1 {
  font-size: 6.4375rem;
  margin-right: auto;
  margin-left: -1px;
}
.mg-header.mobile .mg-header-wrap {
  display: block;
  position: relative;
  margin-top: -36px;
}
.mg-header.mobile .mg-header-wrap .mg-header-icon {
  max-width: 110px;
  max-height: 83px;
  position: relative;
  right: auto;
  top: auto;
  bottom: auto;
  left: auto;
}
.mg-header.mobile .mg-header-wrap .mg-head-2 {
  font-size: 1.5rem;
  top: 30px;
  right: auto;
  left: 100px;
}

#magic-mobile {
  margin-top: 8px;
}
#magic-mobile .mg-title-mobile {
  padding-bottom: 7px;
}
#magic-mobile .mg-title-mobile::before {
  width: 269px;
  height: 2px;
}
#magic-mobile .mg-title-mobile h2 {
  font-size: 1.75rem;
}
#magic-mobile .mg-description-mobile {
  margin-top: -6px;
}
#magic-mobile .mg-description-mobile p {
  font-size: 0.9375rem;
}

/* GroupBegin Apps */
/* GroupEnd */
/* GroupBegin Homepage */
.nav-group span {
  font-size: 0.775rem;
}

.hp [data-region=h] .ui-widget.app .ui-widget-footer {
  padding-top: 10px;
}
.hp [data-region=h] .ui-widget.app.lightbox .ui-widget-detail .ui-articles li:nth-child(n+3) {
  display: none;
}

#gb-icons .cs-global-icons li .text {
  text-align: center;
}

/* GroupEnd */
/* GroupBegin Subpage */
/* GroupEnd */} /* MediaEnd *//* MediaBegin 480+ */ @media (max-width: 639px) {/* GroupBegin Global */
body:before {
  content: "480";
}

.show480 {
  display: block;
}

.hide480 {
  display: none;
}

.content-wrap {
  padding: 0 24px;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------- ### HEADER ### ------------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
.rs-menu-group.quicklinks {
  display: block;
}

.nav-section.right {
  margin-left: 0;
}

.mystartbar {
  height: auto;
}
.mystartbar .content-wrap {
  padding: 0;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  -webkit-box-align: start;
      -ms-flex-align: start;
          align-items: flex-start;
}

.nav-section.left {
  -ms-flex-wrap: wrap;
      flex-wrap: wrap;
  width: 100%;
  margin-right: 0;
}

.nav-section.right {
  padding-left: 0;
  margin-left: 0;
  height: 49px;
}
.nav-section.right::before {
  -webkit-clip-path: polygon(0 0, 100% 0%, 95% 100%, 0% 100%);
          clip-path: polygon(0 0, 100% 0%, 95% 100%, 0% 100%);
}

.nav-group.left {
  display: none;
}

#cs-ab #ab-dropdown {
  left: auto;
  right: -50px;
}

#cs-ar {
  display: none;
}

#cs-ar-mobile {
  height: 48px;
  background: #FFDD00;
  width: 100%;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}
#cs-ar-mobile span {
  color: #221F1F;
  text-transform: uppercase;
  font-family: "Barlow Condensed", sans-serif;
  font-size: 1.3125rem;
}
#cs-ar-mobile span::after {
  color: #221F1F;
}
#cs-ar-mobile #ar-dropdown {
  top: 100%;
}

#gb-logo img {
  max-width: 142px;
  max-height: 92px;
}

#gb-header-mobile #rs-menu-btn span {
  padding-top: 5px;
}

#gb-header .mobile-logo-flex {
  padding: 0 14px;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------ ### HOMEPAGE  ### ----------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
.hp-row.one {
  margin-top: 0;
}
.hp-row.two {
  padding-bottom: 0;
}
.hp-row.three {
  padding-top: 19px;
}
.hp-row.four {
  padding-top: 50px;
  padding-bottom: 28px;
}
.hp-row.four .content-wrap {
  display: block;
}
.hp-row.four .content-wrap.extra-row {
  padding-top: 16px;
}
.hp-row.four .hp-column {
  padding-right: 0 !important;
  padding-left: 0 !important;
  padding-bottom: 25px;
}
.hp-row.four .hp-column.one {
  padding-bottom: 48px;
  width: 100%;
}
.hp-row.four .hp-column.two {
  padding-top: 21px;
  width: 100%;
}
.hp-row.four .hp-column.three {
  width: 100%;
  padding-top: 29px;
}
.hp-row.five {
  padding-bottom: 6px;
}
.hp-row.six {
  padding: 2px 0 0 0;
  --mask: conic-gradient(from -45deg at bottom,#0000,#000 1deg 89deg,#0000 90deg) 50%/11px 100%;
}
.hp-row.six::before {
  background: -webkit-gradient(linear, left top, left bottom, color-stop(90%, rgba(255, 255, 180, 0)), to(rgba(89, 142, 246, 0.5)));
  background: linear-gradient(180deg, rgba(255, 255, 180, 0) 90%, rgba(89, 142, 246, 0.5) 100%);
}
.hp-row.six .content-wrap {
  padding: 0 16px;
}

.hp-row.three::before, .hp-row.five::before {
  --mask: conic-gradient(from -45deg at bottom,#0000,#000 1deg 89deg,#0000 90deg) 50%/11px 100%;
}

/* MMG AREA*/
#hp-slideshow .mmg-description-outer {
  padding: 23px 24px;
}
#hp-slideshow .mmg-description-title {
  font-size: 1rem;
}
#hp-slideshow .mmg-description-caption {
  font-size: 0.875rem;
  margin-top: -4px;
  margin-bottom: 6px;
}
#hp-slideshow .mmg-description-links .mmg-description-link {
  font-size: 0.8125rem;
}
#hp-slideshow .mmg-controls {
  width: 100%;
  right: 0;
  left: 0;
  top: -10px;
}
#hp-slideshow .mmg-controls .mmg-control {
  width: 27px;
  height: 27px;
}
#hp-slideshow .mmg-controls .mmg-control::after {
  font-size: 12px;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ----------------------- ### HP REGION APPS ### ------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
.hp .ui-widget.app .ui-widget-header.icon {
  padding-left: 61px;
  word-break: break-word;
}
.hp .ui-widget.app .ui-widget-header.icon::after {
  width: 61px;
  height: 61px;
  top: -8px;
}
.hp [data-region=c] .ui-widget.app .ui-widget-header h1 {
  font-size: 1.6875rem;
}
.hp [data-region=c] .ui-widget.app.upcomingevents .ui-widget-header {
  padding-bottom: 18px;
}
.hp [data-region=c] .ui-widget.app.upcomingevents .ui-widget-header.icon {
  padding-left: 72px;
}
.hp [data-region=c] .ui-widget.app.upcomingevents .ui-widget-header.icon::before {
  top: -8px;
  font-size: 47px;
}
.hp [data-region=c] .ui-widget.app.upcomingevents .ui-widget-detail {
  margin-right: 0;
}
.hp [data-region=c] .ui-widget.app.upcomingevents .ui-widget-detail .ui-articles li {
  margin-right: 0;
  margin-bottom: 21px;
}
.hp [data-region=c] .ui-widget.app.upcomingevents .ui-widget-detail .ui-articles li:nth-child(n+2) {
  display: none;
}
.hp [data-region=d] .ui-widget.app .ui-widget-header.icon::before {
  font-size: 38px;
  top: -10px;
}
.hp [data-region=d] .ui-widget.app .ui-widget-header.icon::after {
  top: -21px;
}
.hp [data-region=d] .ui-widget.app .ui-widget-header h1 {
  font-size: 1.6875rem;
}
.hp [data-region=e] .ui-widget.app .ui-widget-header.icon::before {
  font-size: 30px;
  top: -4px;
  left: 22px;
}
.hp [data-region=e] .ui-widget.app .ui-widget-header.icon::after {
  left: 0;
  top: -21px;
}
.hp [data-region=e] .ui-widget.app .ui-widget-header h1 {
  font-size: 1.6875rem;
}
.hp [data-region=f] .ui-widget.app .ui-widget-header.icon {
  padding-left: 54px;
  padding-bottom: 0;
}
.hp [data-region=f] .ui-widget.app .ui-widget-header.icon::before {
  font-size: 25px;
  top: -8px;
  left: 10px;
}
.hp [data-region=f] .ui-widget.app .ui-widget-header.icon::after {
  width: 50px;
  height: 62px;
  left: -2px;
  top: -18px;
}
.hp [data-region=f] .ui-widget.app .ui-widget-header h1 {
  font-size: 1.6875rem;
  letter-spacing: -1.3px;
}
.hp [data-region=g] .ui-widget.app {
  display: block;
}
.hp [data-region=g] .ui-widget.app .ui-widget-header {
  display: block;
}
.hp [data-region=g] .ui-widget.app .ui-widget-header h1 {
  font-size: 1.6375rem;
}
.hp [data-region=g] .ui-widget.app .ui-widget-header.icon {
  padding-top: 35px;
  padding-left: 65px;
}
.hp [data-region=g] .ui-widget.app .ui-widget-header.icon::before {
  font-size: 52px;
  top: 30px;
  left: 2px;
  position: absolute;
}
.hp [data-region=g] .ui-widget.app .ui-widget-footer .clear {
  display: none;
}
.hp [data-region=h] .ui-widget.app .ui-widget-footer {
  padding-top: 10px;
}
.hp [data-region=h] .ui-widget.app.lightbox .ui-widget-header {
  padding-bottom: 29px;
  padding-top: 7px;
  padding-left: 88px;
}
.hp [data-region=h] .ui-widget.app.lightbox .ui-widget-header.icon::before {
  font-size: 30px;
  top: 12px;
  left: 33px;
}
.hp [data-region=h] .ui-widget.app.lightbox .ui-widget-header.icon::after {
  left: 11px;
  top: -4px;
}
.hp [data-region=h] .ui-widget.app.lightbox .ui-widget-header h1 {
  font-size: 1.9375rem;
}
.hp [data-region=h] .ui-widget.app.lightbox .ui-widget-detail .ui-articles li {
  width: 270px !important;
  max-width: 270px !important;
  height: 270px !important;
}
.hp [data-region=h] .ui-widget.app.lightbox .ui-widget-detail .ui-articles li:nth-child(n+2) {
  display: none;
}

.hp [data-region=c] .ui-widget.app .more-link,
.hp [data-region=c] .ui-widget.app .view-calendar-link,
.hp [data-region=c] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss {
  padding: 13px 14px 8px;
}

.hp [data-region=g] .ui-widget.app.headlines .ui-articles li:nth-child(n+2) {
  display: none;
}

.hp [data-region=g] .ui-widget.app .more-link::before, .hp [data-region=g] .ui-widget.app .view-calendar-link::before, .hp [data-region=g] .ui-widget.app .div.app-level-social-rss a.ui-btn-toolbar.rs::before {
  display: none;
}

.hp [data-region=g] .ui-widget.app.headlines .ui-articles .ui-article-description {
  font-size: 0.875rem;
}

.hp [data-region=g] .ui-widget.app .more-link, .hp [data-region=g] .ui-widget.app .view-calendar-link, .hp [data-region=g] .ui-widget.app .div.app-level-social-rss a.ui-btn-toolbar.rs {
  margin-top: 12px;
}

.hp [data-region=g] .ui-widget.app.headlines .ui-articles li {
  padding: 0;
}

.ui-widget.app .more-link, .ui-widget.app .view-calendar-link, .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss {
  font-size: 0.859375rem;
}

.hp [data-region=h] .ui-widget.app .ui-widget-header h1::before {
  display: none;
}

.hp [data-region=h] .ui-widget.app.lightbox .ui-widget-detail .ui-articles li img {
  max-width: 270px !important;
  max-height: 270px !important;
}

.hp [data-region=h] .ui-widget.app .more-link::before, .hp [data-region=h] .ui-widget.app .view-calendar-link::before, .hp [data-region=h] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss::before {
  display: none;
}

.hp [data-region=h] .ui-widget.app .more-link, .hp [data-region=h] .ui-widget.app .view-calendar-link, .hp [data-region=h] .ui-widget.app div.app-level-social-rss a.ui-btn-toolbar.rss {
  margin-right: 0;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------ ### GENERAL APPS ### -------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
.ui-widget.app .ui-widget-header h1 {
  font-size: 1.5625rem;
}

/* GroupEnd */
/* GroupBegin Subpage No Nav */
/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------ ### MAGIC SECTION ### ------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
#animation-graphic {
  padding-bottom: 10px;
}
#animation-graphic::before {
  background: -webkit-gradient(linear, left top, left bottom, color-stop(90%, rgba(255, 255, 255, 0)), to(rgba(89, 142, 246, 0.5)));
  background: linear-gradient(180deg, rgba(255, 255, 255, 0) 90%, rgba(89, 142, 246, 0.5) 100%);
}
#animation-graphic #city-bg-mobile {
  min-height: 150px;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------- ### FOOTER ### ------------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
#gb-footer {
  padding: 16px 0 20px;
}

.gb-footer-row.one {
  display: block;
  padding-bottom: 4px;
}

.footer-address .footer-info-wrap {
  padding: 0 30px 0 50px;
  padding-right: 0;
}
.footer-address .footer-info-wrap.top {
  padding-bottom: 22px;
}

.footer-address .footer-info-wrap .footer-info-header.visit::before {
  top: 13px;
  left: 3px;
}

.footer-address .footer-info-wrap .footer-info-header.contact::before {
  top: 6px;
  left: 3px;
}

.footer-link-container {
  padding-top: 0;
  padding-left: 18px;
  width: auto;
}
.footer-link-container #footer-link-list {
  -webkit-column-count: 2;
     -moz-column-count: 2;
          column-count: 2;
  -webkit-column-gap: 44px;
     -moz-column-gap: 44px;
          column-gap: 44px;
}
.footer-link-container #footer-link-list li::before {
  left: -17px;
  font-size: 12px;
}

.cs-bb-footer-outer {
  padding-right: 50px;
}
.cs-bb-footer-outer .cs-bb-footer.info p {
  -webkit-box-pack: start;
      -ms-flex-pack: start;
          justify-content: flex-start;
}

.cs-mystart-dropdown.user-options .cs-dropdown-selector {
  text-align: left;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------ ### GB ICONS ### ------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
#gb-icons .cs-global-icons li {
  -ms-flex-preferred-size: 33.33%;
      flex-basis: 33.33%;
  margin-bottom: 21px;
}
#gb-icons .cs-global-icons li .icon {
  padding: 12px 20px;
}
#gb-icons .cs-global-icons li .text {
  text-align: center;
}

/* GroupEnd */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
/* ------------------------ ### SOCIAL MEDIA ### -------------------------- */
/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */
.gb-socials .gb-social-icon .gb-social-icons::before {
  font-size: 25px;
}
.gb-socials.mobile {
  -webkit-box-pack: end;
      -ms-flex-pack: end;
          justify-content: flex-end;
}

.gb-socials.region-e {
  margin-top: -6px;
}
.gb-socials.region-e .gb-social-icon {
  width: 45px;
  height: 45px;
}

/* GroupEnd */} /* MediaEnd *//* MediaBegin 320+ */ @media (max-width: 479px) {/* GroupBegin Default */
body:before {
  content: "320";
}

.show320 {
  display: block;
}

.hide320 {
  display: none;
}

/* GroupEnd */
/* GroupBegin Header */
/* GroupEnd */
/* GroupBegin Footer */
/* GroupEnd */
/* GroupBegin Apps */
/* GroupEnd */
/* GroupBegin Homepage */
.hp [data-region=c] .ui-widget.app.upcomingevents .ui-widget-header.icon::before {
  top: 6px;
  font-size: 56px;
}

/* GroupEnd */
/* GroupBegin Subpage */
/* GroupEnd */} /* MediaEnd */</style>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" type="text/css" href="https://extend.schoolwires.com/creative/scripts/creative/tools/creative-icons-v4/css/creativeIcons.v4.min.css" />
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&family=Source+Sans+Pro:wght@400;600;700&display=swap" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="https://extend.schoolwires.com/creative/scripts/creative/global/css/cs.global.min.css" />
<link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Akshar&family=Barlow+Condensed:wght@500&family=Bebas+Neue&family=Nunito:wght@400;600;700&family=Oooh+Baby&family=Sarabun:wght@600&display=swap" rel="stylesheet">

<script type="text/javascript" src="https://extend.schoolwires.com/creative/scripts/creative/global/js/cs.global.min.js"></script>
<script type="text/javascript" src="https://extend.schoolwires.com/creative/scripts/creative/tools/head.min.js"></script>
<script type="text/javascript" src="https://extend.schoolwires.com/creative/scripts/creative/tools/creative-icons-v4/creativeIcons.v4.min.js"></script>
<script type="text/javascript" src="https://extend.schoolwires.com/creative/scripts/joel/mod-events/joel.mod-events.min.js"></script>
<script type="text/javascript" src="https://extend.schoolwires.com/creative/scripts/creative/responsive/creative-app-accordion/creative.app.accordion.min.js"></script>
<script type="text/javascript" src="https://extend.schoolwires.com/creative/scripts/creative/responsive/creative-responsive-menu-v3/creative.responsive.menu.v3.min.js"></script>
<script type="text/javascript" src="https://extend.schoolwires.com/creative/scripts/creative/tools/mega-channel-menu/cs.mega.channel.menu.min.js"></script>
<script type="text/javascript" src="https://extend.schoolwires.com/creative/scripts/creative/tools/creative-translate/creative.translate.min.js"></script>
<script type="text/javascript" src="https://extend.schoolwires.com/creative/creative_services/blackboard_footer/cs.blackboard.footer.min.js"></script>
<!-- UPDATE WITH SLM CONFIG FILE -->
<script type='text/javascript' src='https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/domain/4/slm/school-list-config.js'></script>


<script type="text/javascript">/******/ (function() { // webpackBootstrap
$(window).on("load", function () {
  CreativeTemplate.WindowLoad();
});
$(function () {
  CreativeTemplate.Init();
});
window.CreativeTemplate = {
  //PROPERTIES
  "KeyCodes": {
    'tab': 'Tab',
    'enter': 'Enter',
    'esc': 'Escape',
    'space': ' ',
    'end': 'End',
    'home': 'Home',
    'left': 'ArrowLeft',
    'up': 'ArrowUp',
    'right': 'ArrowRight',
    'down': 'ArrowDown'
  },
  "IsMyViewPage": false,
  "ShowDistrtictHome": false,
  "ShowSchoolList": true,
  "ShowTranslate": true,
  "DefaultLogoSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/default.png",
  //METHODS
  "Init": function Init() {
    //FOR SCOPE
    var template = this;
    this.SetTemplateProps();
    this.MyStart();
    this.RegisterLinks();
    this.JsMediaQueries();
    this.Header();
    this.RsMenu();
    this.ChannelBar();

    if ($("#gb-page.hp").length) {
      this.Homepage();
      this.Magic();
      this.Headlines();
      this.Animations();
      this.MoveButtons();
    }

    this.Body();
    this.GlobalIcons();
    this.SocialIcons();
    this.MegaMenu();
    this.MMGPlugin();
    this.ModEvents();
    this.Search();
    this.Footer(); //WINDOW RESIZE AND SCROLL METHOD

    $(window).resize(function () {
      template.WindowResize();
    });
    $(window).scroll(function () {
      template.WindowScroll();
    });
  },
  "WindowLoad": function WindowLoad() {},
  "WindowResize": function WindowResize() {
    //RUN QUERIES ON RESIZE
    this.JsMediaQueries();
    this.MoveButtons();
  },
  "WindowScroll": function WindowScroll() {},
  "JsMediaQueries": function JsMediaQueries() {
    switch (this.GetBreakPoint()) {
      case "desktop":
        break;

      case "768":
        break;

      case "640":
        break;

      case "480":
        break;

      case "320":
        break;
    }
  },
  "SetTemplateProps": function SetTemplateProps() {
    // FOR SCOPE
    var template = this; //MYVIEW PAGE CHECK

    if ($('#pw-body').length) this.IsMyViewPage = true; //SCHOOL LIST CHECK

    if ($(".sw-mystart-dropdown.schoollist").length) this.ShowSchoolList = true;
  },
  "MyStart": function MyStart() {
    //FOR SCOPE
    var template = this; //BUILD USER OPTIONS DROPDOWN

    var userOptionsItems = "";
    
    // SIGNIN BUTTON


    if ($(".sw-mystart-button.signin").length) {
      userOptionsItems += "<li>" + $(".sw-mystart-button.signin").html() + "</li>";
    } // REGISTER BUTTON


    if ($(".sw-mystart-button.register").length) {
      userOptionsItems += "<li>" + $(".sw-mystart-button.register").html() + "</li>";
    }
    
    
    // ADD USER OPTIONS DROPDOWN TO THE DOM
    $(".cs-mystart-dropdown.user-options .cs-dropdown-list").html(userOptionsItems); 
    
    //CLONE AND DUMP SCHOOLLIST INTO DOM
	//REPLACE WITH NEW SLM
    //$('.sw-mystart-dropdown.schoollist .sw-dropdown-list').clone().appendTo('#school-list'); 
                //SLM CONFIG
    var schools = '';

    for(var i in schoolList) {
        if(schoolList[i].schoolVisible.toLowerCase() == 'true') {
            schools += '<li><a target="_blank" href=\'' + schoolList[i].schoolUrl + '\' tabindex=\'-1\'>' + schoolList[i].schoolName + '</a></li>';
        }
    }
	//MOVE ARRAY INTO SCHOOLS LIST
    $('#school-list').html(schools);
    
    //MOVE SCHOOLS FROM OFFSCREEN
    $("#cs-schools").on('click', function () {
      //FOR SCOPE
      var subList = $(this).children('ul');
      var itemPosLeft = $(this).offset().left;
      var itemPosRight = itemPosLeft + subList.outerWidth();

      if (itemPosRight >= $(window).width()) {
        $('#school-list', this).addClass('overflowing').css({
          'left': -(itemPosRight - $(window).width())
        });
      } else if (itemPosLeft < 0) {
        $('#school-list', this).addClass('overflowing').css({
          'left': -itemPosLeft
        });
      } else {
        $('#school-list', this).removeClass('overflowing').css({
          'left': ''
        });
      }
    }); //RUN TRANSLATE FUNCTION

    this.Translate(); // BIND DROPDOWN EVENTS

    template.DropdownActions({
      //USER OPTIONS
      "dropdownParent": ".cs-mystart-dropdown.user-options",
      "dropdownSelector": ".cs-dropdown-selector",
      "dropdown": ".cs-dropdown",
      "dropdownList": ".cs-dropdown-list"
    });
    template.DropdownActions({
      //TRANSLATE
      "dropdownParent": "#gb-translate",
      "dropdownSelector": ".translate-selector",
      "dropdown": "#translate-dropdown"
    });
    template.DropdownActions({
      //A&B DROPDOWN
      "dropdownParent": "#cs-ab",
      "dropdownSelector": ".mystart-selector",
      "dropdown": "#ab-dropdown"
    });
    template.DropdownActions({ //SCHOOLS DROPDOWN
        "dropdownParent": "#cs-schools",
        "dropdownSelector": ".mystart-selector",
        "dropdown": "#school-dropdown",
        "dropdownList": "#school-list"
    });
    template.DropdownActions({ //A&R DROPDOWN
        "dropdownParent": "#cs-ar",
        "dropdownSelector": ".mystart-selector",
        "dropdown": "#ar-dropdown",
        "dropdownList": "#ar-list"
    });
    template.DropdownActions({ //A&R MOBILE DROPDOWN
        "dropdownParent": "#cs-ar-mobile",
        "dropdownSelector": ".mystart-selector",
        "dropdown": "#ar-dropdown",
        "dropdownList": "#ar-list"
    });
  },
  "RegisterLinks": function RegisterLinks() {
    //FOR SCOPE
    var template = this; //BUILD LINK LIST ARRAY

    //BUILD LINK LIST ARRAY
    var listItem = [
        [//LINK LIST ONE
           'true', //LINK TOGGLE
           'Link 1', //LINK NAME
           '#', //LINK URL
           '_blank' //LINK TARGET
        ],
        [//LINK LIST TWO
           'true', //LINK TOGGLE
           'Link 2', //LINK NAME
           '#', //LINK URL
           '_blank' //LINK TARGET
        ],
        [//LINK LIST THREE
           'true', //LINK TOGGLE
           'Link 3', //LINK NAME
           '#', //LINK URL
           '_blank' //LINK TARGET
        ],
        [//LINK LIST FOUR
           'true', //LINK TOGGLE
           'Link 4', //LINK NAME
           '#', //LINK URL
           '_blank' //LINK TARGET
        ],
        [//LINK LIST FIVE
           'true', //LINK TOGGLE
           'Link 5', //LINK NAME
           '#', //LINK URL
           '_blank' //LINK TARGET
        ],
        [//LINK LIST SIX
           'true', //LINK TOGGLE
           'Link 6', //LINK NAME
           '#', //LINK URL
           '_blank' //LINK TARGET
        ],
        [//LINK LIST SEVEN
           'true', //LINK TOGGLE
           'Link 7', //LINK NAME
           '#', //LINK URL
           '_blank' //LINK TARGET
        ],
        [//LINK LIST EIGHT
           'true', //LINK TOGGLE
           'Link 8', //LINK NAME
           '#', //LINK URL
           '_blank' //LINK TARGET
        ]
    ]

    var linkHTML = '';
    $.each(listItem, function (index) {
      //BUILD LIST ITEMS
      linkHTML += '<li class="ar-list=item" data-toggle="' + listItem[index][0] + '"><a class="ar-link" href="' + listItem[index][2] + '" target="' + listItem[index][3] + '"  aria-label="' + listItem[index][1] + '"><span>' + listItem[index][1] + '</span></a></li>';
    }); //PUSH HTML TO DOM

    $('#ar-list').html(linkHTML);
    $('#cs-ar-mobile #ar-list').html(linkHTML);
  },
  "DropdownActions": function DropdownActions(params) {
    // FOR SCOPE
    var template = this;
    var dropdownParent = params.dropdownParent;
    var dropdownSelector = params.dropdownSelector;
    var dropdown = params.dropdown;
    var dropdownList = params.dropdownList;
    $(dropdownParent + " " + dropdownList + " a").attr("tabindex", "-1"); // MYSTART DROPDOWN SELECTOR CLICK EVENT

    $(dropdownParent).on("click", dropdownSelector, function (e) {
      e.preventDefault();

      if ($(this).parent().hasClass("open")) {
        $("+ " + dropdownList + " a").attr("tabindex", "-1");
        $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
      } else {
        $(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden", "false").slideDown(300, "swing");
      }
    }); // MYSTART DROPDOWN SELECTOR KEYDOWN EVENTS

    $(dropdownParent).on("keydown", dropdownSelector, function (e) {
      // CAPTURE KEY CODE
      switch (e.key) {
        // CONSUME LEFT AND UP ARROWS
        case template.KeyCodes.enter:
        case template.KeyCodes.space:
          e.preventDefault(); // IF THE DROPDOWN IS OPEN, CLOSE IT

          if ($(dropdownParent).hasClass("open")) {
            $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
            $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
          } else {
            $(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden", "false").slideDown(300, "swing", function () {
              if ($(dropdownParent).hasClass("translate")) {
                $("#cs-branded-translate-dropdown").focus();
              } else {
                $(dropdownList + " li:first-child a", this).attr("tabindex", "0").focus();
              }
            });
          }

          break;
        // CONSUME TAB KEY

        case template.KeyCodes.tab:
          if ($("+ " + dropdown + " " + dropdownList + " a").length) {
            $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
            $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
          }

          break;
        // CONSUME LEFT AND UP ARROWS

        case template.KeyCodes.down:
        case template.KeyCodes.right:
          e.preventDefault();
          $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
          $("+ " + dropdown + " " + dropdownList + " li:first-child > a", this).attr("tabindex", "0").focus();
          break;
      }
    }); // MYSTART DROPDOWN LINK KEYDOWN EVENTS

    $(dropdownParent).on("keydown", dropdownList + " li a", function (e) {
      // CAPTURE KEY CODE
      switch (e.key) {
        // CONSUME LEFT AND UP ARROWS
        case template.KeyCodes.left:
        case template.KeyCodes.up:
          e.preventDefault(); // IS FIRST ITEM

          if ($(this).parent().is(":first-child")) {
            // FOCUS DROPDOWN BUTTON
            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
            $(this).closest(dropdownParent).find(dropdownSelector).focus();
          } else {
            // FOCUS PREVIOUS ITEM
            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
            $(this).parent().prev("li").find("> a").attr("tabindex", "0").focus();
          }

          break;
        // CONSUME RIGHT AND DOWN ARROWS

        case template.KeyCodes.right:
        case template.KeyCodes.down:
          e.preventDefault(); // IS LAST ITEM

          if ($(this).parent().is(":last-child")) {
            // FOCUS FIRST ITEM
            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
            $(this).closest(dropdownList).find("li:first-child > a").attr("tabindex", "0").focus();
          } else {
            // FOCUS NEXT ITEM
            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
            $(this).parent().next("li").find("> a").attr("tabindex", "0").focus();
          }

          break;
        // CONSUME TAB KEY

        case template.KeyCodes.tab:
          if (e.shiftKey) {
            e.preventDefault(); // FOCUS DROPDOWN BUTTON

            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
            $(this).closest(dropdownParent).find(dropdownSelector).focus();
          }

          break;
        // CONSUME HOME KEY

        case template.KeyCodes.home:
          e.preventDefault(); // FOCUS FIRST ITEM

          $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
          $(this).closest(dropdownList).find("li:first-child > a").attr("tabindex", "0").focus();
          break;
        // CONSUME END KEY

        case template.KeyCodes.end:
          e.preventDefault(); // FOCUS LAST ITEM

          $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
          $(this).closest(dropdownList).find("li:last-child > a").attr("tabindex", "0").focus();
          break;
        // CONSUME ESC KEY

        case template.KeyCodes.esc:
          e.preventDefault(); // FOCUS DROPDOWN BUTTON AND CLOSE DROPDOWN

          $(this).closest(dropdownParent).find(dropdownSelector).focus();
          $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
          $(dropdownSelector).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
          break;
      }
    });
    $(dropdownParent).mouseleave(function () {
      $(dropdownList + " a", this).attr("tabindex", "-1");
      $(dropdownSelector, this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
    }).focusout(function () {
      var thisDropdown = this;
      setTimeout(function () {
        if (!$(thisDropdown).find(":focus").length) {
          $(dropdownSelector, thisDropdown).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
        }
      }, 500);
    });
  },
  "CheckElementPosition": function CheckElementPosition(element) {
    //CALL FOR SCROLL ANIMATIONS
    var template = this; //CHECK IF ELEMENT IS IN VIEWPORT

    var elementTop = $(element).offset().top;
    var elementLeft = $(element).offset().left;
    var elementBottom = elementTop + $(element).outerHeight();
    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();
    var viewportWidth = $(window).width();
    return elementBottom > viewportTop && elementTop < viewportBottom && elementLeft < viewportWidth;
  },
  "ChannelBar": function ChannelBar() {
    $(".sw-channel-item").off('hover');
    $(".sw-channel-item").hover(function () {
      $("ul", this).stop(true, true);
      var subList = $(this).children('ul');

      if ($.trim(subList.html()) !== "") {
        subList.slideDown(300, "swing");
      }

      $(this).addClass("hover");
      var itemPosLeft = $(this).offset().left;
      var itemPosRight = itemPosLeft + subList.outerWidth();

      if (itemPosRight >= $(window).width()) {
        $('.sw-channel-dropdown', this).addClass('overflowing').css({
          'left': -(itemPosRight - $(window).width())
        });
      } else if (itemPosLeft < 0) {
        $('.sw-channel-dropdown', this).addClass('overflowing').css({
          'left': -itemPosLeft
        });
      } else {
        $('.sw-channel-dropdown', this).removeClass('overflowing').css({
          'left': ''
        });
      }
    }, function () {
      $(".sw-channel-dropdown").slideUp(300, "swing");
      $(this).removeClass("hover");
    });
  },
  "Header": function Header() {
    // FOR SCOPE
    var template = this; //ADD SITE URL

    var homeLink = '<a style="display:inline-block" href="https://mpatzem.schoolwires.net/site">'; //LOGO DIMENSIONS

    var imgWidth = 255;
    var imgHeight = 165; //ADD LOGO TO HEADER

    var logoSrc = $.trim('https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/GlobalAssets/images//Faces/default-man.jpg');
    var srcSplit = logoSrc.split("/");
    var srcSplitLen = srcSplit.length; //CHECK IF LOGO IS EMPTY

    if (logoSrc == '' || srcSplit[srcSplitLen - 1] == 'default-man.jpg') {
      logoSrc = template.DefaultLogoSrc;
    } //ADD CUSTOM SCHOOL LOGO


    $('#gb-logo').append(homeLink + '<img width="' + imgWidth + '" height="' + imgHeight + '" src="' + logoSrc + '" alt="Fletch_Test Logo"></a>'); //WRAP IN H1

    $('#gb-logo > a').wrap('<h1 />'); //ADD TRANSLATE HOVER STATE FOR ICON

    $('.translate-selector').hover(function () {
      $('.translate-icon').addClass('hover');
    }, function () {
      $('.translate-icon').removeClass("hover");
    });
  },
  "Homepage": function Homepage() {
    //FOR SCOPE
    var template = this; //LOOP EACH APP ON H1

    $(".app").each(function () {
      //FOR SCOPE
      var thisApp = $(this); //CHECK FOR H1

      if ($(thisApp).find(".ui-widget-header h1").length) {
        //IF H1 EXISTS ADD CLASS
        $(thisApp).find(".ui-widget-header").addClass("icon");
      } else {
        //IF H1 DOESN'T EXIST REMOVE CLASS
        $(thisApp).find(".ui-widget-header").removeClass("icon");
      }
    }); //LOOP EACH ARTICLES IN LIGHTBOX

    $(".app.lightbox .ui-articles").each(function (props) {
      //FOR SCOPE
      var thisApp = $(this); //CHECK FOR H1

      if ($(thisApp).find(".ui-article img").length) {
        $(thisApp).find(".ui-article img", props.element).wrap("<span class='light-box-img'></span>"); //IF H1 EXISTS ADD CLASS

        $('.ui-article').hover(function () {
          $(thisApp).find(".ui-article").addClass("hover");
        }, function () {
          $(thisApp).find(".ui-article").removeClass("hover");
        });
      }
    }); //ADD BUTTON IN REGION E

    var buttonToggle ='true';
    var buttonLink ='#';
    var buttonTarget ='_blank';
    var buttonText ='More';

    if ($('.hp-row.four .ui-widget.app').length) {
      $(".hp-column.two").append("<div class='regionE-button' role='button' data-toggle='" + buttonToggle + "'><a href='" + buttonLink + "' target='" + buttonTarget + "'><span>" + buttonText + "</span></a></div>");
    }
  },
  "Animations": function Animations() {
    //FOR SCOPE
    var template = this; //SCROLL ANIMATIONS
    //VAR FOR HOW FAR HEADER LOGO NEED TO DISAPPEAR

    var checkpoint = 155;

    switch (this.GetBreakPoint()) {
      case "desktop":
        if ($(".hp").length) {
          //FIRE GB ICONS ANIMATIONS SINCE THEY ARE ALWAYS IN VIEW ON LOAD
          $(document).ready(function () {
            $("#gb-icons ").addClass("hello");
          }); //ON SCROLL

          $(window).scroll(function () {
            //CHECK Y SCROLL DISTANCE
            var currentScroll = window.pageYOffset;

            if (currentScroll <= checkpoint) {
              //HAVE WE HIT CHECKPOINT
              opacity = 1 - currentScroll / checkpoint;
            } else {
              //HAVE WE GONE PAST CHECK POINT
              opacity = 0;
            }

            $('#gb-logo').css('opacity', opacity); //PASS VARIABLE TO CSS
            //CHECK FOR GB ICONS

            if (!template.CheckElementPosition($("#gb-icons"))) {
              setTimeout(function () {
                $('#gb-icons-sticky').addClass('hello');
              }, 100);
            } else {
              $('#gb-icons-sticky').removeClass('hello');
            } //CHECK FOR HP ROW THREE


            if (template.CheckElementPosition($('.hp-row.three'))) {
              setTimeout(function () {
                $('.hp-row.three .ui-widget.app').each(function () {
                  if (template.CheckElementPosition($(".ui-widget-detail", this)) && !$(this).hasClass("hello")) {
                    $(this).addClass("hello");
                  }
                });
              }, 300);
            } //CHECK FOR HP ROW FOUR


            if (template.CheckElementPosition($('.hp-row.four'))) {
              setTimeout(function () {
                $('.hp-row.four .ui-widget.app').each(function () {
                  if (template.CheckElementPosition($(".ui-widget-detail", this)) && !$(this).hasClass("hello")) {
                    $(this).addClass("hello");
                  }
                });
              }, 300);
            } //CHECK FOR HP ROW FIVE


            if (template.CheckElementPosition($('.hp-row.five'))) {
              setTimeout(function () {
                $('.hp-row.five .ui-widget.app').each(function () {
                  if (template.CheckElementPosition($(".ui-widget-detail", this)) && !$(this).hasClass("hello")) {
                    $(this).addClass("hello");
                  }
                });
              }, 300);
            } //CHECK FOR HP ROW SIX


            if (template.CheckElementPosition($('.hp-row.six'))) {
              setTimeout(function () {
                if (template.CheckElementPosition($(".mg-header-icon-1024")) && !$('.mg-header-icon-1024').hasClass("hello")) {
                  $('.mg-header-icon-1024').addClass("hello");
                }

                if (template.CheckElementPosition($(".mg-header")) && !$('.mg-header').hasClass("hello")) {
                  $('.mg-header').addClass("hello");
                } //CHECK MAGIC SECTION  


                if (template.CheckElementPosition($("#animation-graphic")) && !$('#animation-graphic').hasClass("hello")) {
                  $('#animation-graphic').addClass("hello");
                }
              }, 300);
            }
          });
        }

        break;

      case "768":
        break;

      case "640":
        break;

      case "480":
        break;

      case "320":
        break;
    }
  },
  "MoveButtons": function() {
      switch(this.GetBreakPoint()) {
          case "desktop": case "768":
              //MOVE LINKS TO HEADERS
              $("[data-region='c'] .app, [data-region='g'] .app, [data-region='h'] .app").each(function(){
                  //FOR SCOPE
                  var thisApp = $(this);

                  //MOVE CALENDAR LINK IF IT EXISTS
                  if ($(thisApp).find(".view-calendar-link").length){
                      //IF IT ISNT WHERE IT SHOULD BE
                      if (!$(thisApp).find(".ui-widget-header .view-calendar-link").length) {
                          // move
                          $(thisApp).find(".view-calendar-link").insertAfter($(thisApp).find(".ui-widget-header h1"));
                      } //else do nothing
                  }

                  //MOVE MORE LINK IF IT EXISTS
                  if ($(thisApp).find(".more-link").length){
                      //IF IT ISNT WHERE IT SHOULD BE
                      if (!$(thisApp).find(".ui-widget-header .more-link").length) {
                          // move
                          $(thisApp).find(".more-link").insertAfter($(thisApp).find(".ui-widget-header h1"));
                      } //else do nothing
                  }
                  //MOVE RSS LINK IF IT EXISTS
                  if ($(thisApp).find(".app-level-social-rss").length){
                      if (!$(thisApp).find(".ui-widget-header .app-level-social-rss").length) {
                          // move
                          $(thisApp).find(".app-level-social-rss").insertAfter($(thisApp).find(".ui-widget-header h1"));
                      } //else do nothing
                  }
              });

          break;

          case "640":
              //MOVE LINKS TO HEADERS
              $("[data-region='g'] .app").each(function(){
                  //FOR SCOPE
                  var thisApp = $(this);

                  //MOVE CALENDAR LINK IF IT EXISTS
                  if ($(thisApp).find(".view-calendar-link").length){
                      //IF IT ISNT WHERE IT SHOULD BE
                      if (!$(thisApp).find(".ui-widget-header .view-calendar-link").length) {
                          // move it
                          $(thisApp).find(".view-calendar-link").insertAfter($(thisApp).find(".ui-widget-header h1"));
                      } //else do nothing
                  }

                  //MOVE MORE LINK IF IT EXISTS
                  if ($(thisApp).find(".more-link").length){
                      //IF IT ISNT WHERE IT SHOULD BE
                      if (!$(thisApp).find(".ui-widget-header .more-link").length) {
                          // move
                          $(thisApp).find(".more-link").insertAfter($(thisApp).find(".ui-widget-header h1"));
                      } //else do nothing
                  }

                  //MOVE RSS LINK IF IT EXISTS
                  if ($(thisApp).find(".app-level-social-rss").length){
                      //IF IT ISNT WHERE IT SHOULD BE
                      if (!$(thisApp).find(".ui-widget-header .app-level-social-rss").length) {
                          // move
                          $(thisApp).find(".app-level-social-rss").insertAfter($(thisApp).find(".ui-widget-header h1"));
                      } //else do nothing
                  }
              });

              //MOVE LINKS TO FOOTERS
              $("[data-region='c'] .app, [data-region='h'] .app").each(function(){
                  //FOR SCOPE
                  var thisApp = $(this);

                  //MOVE LINK IF IT EXISTS
                  if ($(thisApp).find(".view-calendar-link").length){
                      //IF IT ISNT WHERE IT SHOULD BE
                      if (!$(thisApp).find(".ui-widget-footer .view-calendar-link").length) {
                          // move
                          $(thisApp).find(".view-calendar-link").appendTo($(thisApp).find(".ui-widget-footer"));
                      } //else do nothing
                  }

                  //MOVE LINK IF IT EXISTS
                  if ($(thisApp).find(".more-link").length){
                      //IF IT ISNT WHERE IT SHOULD BE
                      if (!$(thisApp).find(".ui-widget-footer .more-link").length) {
                          // move
                          $(thisApp).find(".more-link").appendTo($(thisApp).find(".ui-widget-footer"));
                      } //else do nothing
                  }

                  //MOVE LINK IF IT EXISTS
                  if ($(thisApp).find(".app-level-social-rss").length){
                      //IF IT ISNT WHERE IT SHOULD BE
                      if (!$(thisApp).find(".ui-widget-footer .app-level-social-rss").length) {
                          // move
                          $(thisApp).find(".app-level-social-rss").appendTo($(thisApp).find(".ui-widget-footer"));
                      } //else do nothing
                  }
              });

          break;

          case "480": case "320":

              //MOVE LINKS TO FOOTERS
              $("[data-region='c'] .app, [data-region='g'] .app, [data-region='h'] .app").each(function(){
                  //FOR SCOPE
                  var thisApp = $(this);

                  //MOVE LINK IF IT EXISTS
                  if ($(thisApp).find(".view-calendar-link").length){
                      //IF IT ISNT WHERE IT SHOULD BE
                      if (!$(thisApp).find(".ui-widget-footer .view-calendar-link").length) {
                          // move
                          $(thisApp).find(".view-calendar-link").appendTo($(thisApp).find(".ui-widget-footer"));
                      } //else do nothing
                  }

                  //MOVE LINK IF IT EXISTS
                  if ($(thisApp).find(".more-link").length){
                      //IF IT ISNT WHERE IT SHOULD BE
                      if (!$(thisApp).find(".ui-widget-footer .more-link").length) {
                          // move
                          $(thisApp).find(".more-link").appendTo($(thisApp).find(".ui-widget-footer"));
                      } //else do nothing
                  }

                  //MOVE LINK IF IT EXISTS
                  if ($(thisApp).find(".app-level-social-rss").length){
                      //IF IT ISNT WHERE IT SHOULD BE
                      if (!$(thisApp).find(".ui-widget-footer .app-level-social-rss").length) {
                          // move
                          $(thisApp).find(".app-level-social-rss").appendTo($(thisApp).find(".ui-widget-footer"));
                      } //else do nothing
                  }

              });
          break;
      }
  },
  "Headlines": function Headlines() {
    //HEADLINE CARDS
    $("[data-region='g'] .headlines .ui-article").each(function (articleIndex, article) {
      //LOOP THROUGH EACH ARTICLE
      $('.ui-article-controls, .clear', article).remove(); //REMOVE
      //WRAP LIST ITEMS THAT DONT HAVE IMGS

      $(article).children().not('.ui-article-thumb').wrapAll('<div class="headline-description flex-cont jc-c ai-c fd-col"><div></div></div>'); //SET TABINDEX

      $('.headline-description a', article).attr('tabindex', -1);
    }); //LOOP THROUGH EACH ARTICLE

    $("[data-region='g'] .headlines").each(function (appIndex, app) {
      $('.ui-article', app).each(function (articleIndex, article) {
        var imgSrc = $('.ui-article-thumb img', article).attr('src') == undefined ? '' : $('.ui-article-thumb img', article).attr('src');

        if (imgSrc != '' && imgSrc.indexOf('default-man') < 0) {
          $(article).css('background-image', 'url("' + imgSrc + '")'); //SET UNIQUE ID FOR ALLY CONTROL

          var uniqId = 'hp-headlines-' + appIndex + '-desc-' + articleIndex;
          $('.headline-description', article).attr('id', uniqId).before('<button class="headline-card-trigger" aria-controls="' + uniqId + '" aria-expanded="false">Toggle Description</button>');
        } else {
          $('.headline-description', article).addClass('no-image');
        }
      });
    }); //CLICK EVENTS FOR HEADLINES

    $("[data-region='g'] .headline-card-trigger").on('click', function (e) {
      var ctrlObj = $('#' + $(this).attr('aria-controls'));

      if ($(this).attr('aria-expanded') == 'true') {
        ctrlObj.removeClass('focused');
        $(this).attr('aria-expanded', false);
        $('a', ctrlObj).attr('tabindex', -1);
      } else {
        ctrlObj.addClass('focused');
        $(this).attr('aria-expanded', true);
        $('a', ctrlObj).attr('tabindex', 0);
      }
    });
  },
  "Magic": function Magic() {
    //FOR SCOPE
    var template = this;
    var magicContent = [{
      'show':true,
      'text':'Career Technical Education',
      'description':'Career and Technical Education (CTE) is an educational option that provides learners the opportunity to earn industry-valued credentials, college credit, and workplace experiences incorporating a rigorous academic core coupled with a high-level technical curriculum. 70% of CTE students attend post-secondary education. CTE students have a 93% graduation rate compared to the national average of 80%. A CTE student will make twice as much money as a student with a high school diploma.'
    }, {
      'show':true,
      'text':'Student Success',
      'description':'Our students earn college credit while still in high school and gain practical skills for life or any career. Articulation Agreements with The Art Institute of Philadelphia, The Culinary Institute of America, Mercer County Community College, and other schools allow students to earn valuable college credit upon the successful completion of high school classes.'
    }, {
      'show':true,
      'text':'Work-Based Learning',
      'description':'Our business partners give our students work-based learning, including career exploration opportunities such as career preparation and career training. We create an environment to let our students succeed in work-based learning and propel their careers.'
    }, {
      'show':true,
      'text':'Our Mission',
      'description':'Mercer County Technical Schools’ mission is to produce a community of inspired, compassionate learners who are knowledgeable, and skilled and possess the competencies that will prepare them for success in an ever-changing technological world. We provide our students with educational opportunities in preparing for careers in business and industry while emphasizing a culture of personal attention that focuses on the individual learner. Our goal is to foster a school climate that emphasizes the importance of the teaching-learning process and provides youth and adults the opportunities to maximize their potential, reflect and offer solutions to challenges posed by society.'
    }]; //CREATE VARIABLE FOR ITEMS

    var igTitles = '';
    var igDescrip = '';
    var igMobile = ''; //LOOP THROUGH EACH ITEM

    $.each(magicContent, function (index, items) {
      //BUILD ITEMS
      if (items.show) {
        //IS TOGGLED ON
        //FOR DESKTOP
        if (index === 0) {
          //IS BUTTON 1
          //BUILD TITLE
          igTitles += '<button id="title-' + index + '" class="tablink active" aria-labelledby="tab-' + index + '" aria-selected="true" tabindex="0" aria-controls="tabpanel-' + index + '"><p>' + items.text + '</p><svg id="title-0_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 181.19 21.91"><g><path class="cls t0_1" d="m2.81,18.95S36.95-2.8,33.27,6.13c-5.61,13.63-8.82,14.43,15.58,0,15.78-9.33,6.06,3.21,6.87,10.43s12.03-2.41,24.86-10.42c12.83-8.02,9.62,10.42,28.06,12.03,18.44,1.6,17.01-13.83,28.06-4.01,7.22,6.41,41.69,1.6,41.69,1.6"/></g></svg><svg id="title-0_2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 59.52 48.25"><g><line class="cls t0_2" x1="51.79" y1="23.31" x2="56.72" y2="2.81"/><line class="cls t0_2" x1="28.27" y1="26.55" x2="18.26" y2="5.56"/><line class="cls t0_2" x1="24.72" y1="45.45" x2="2.81" y2="37.68"/></g></svg></button>'; //BUILD DESCRIPTION

          igDescrip += ' <div id="tabpanel-' + index + '" class="tabcontent show" aria-expanded="true" aria-labelledby="tabpanel-' + index + '"><p>' + items.description + '</p></div>';
        } else {
          //BUILD TITLE
          igTitles += '<button id="title-' + index + '" class="tablink" aria-selected="false" tabindex="-1" aria-labelledby="tab-' + index + '" aria-controls="tabpanel-' + index + '"><p>' + items.text + '</p>';

          if (index === 1) {
            //IS BUTTON 2
            igTitles += '<svg id="title-1_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.35 35.61"><g><line class="cls t1_1" x1="16.68" y1="2.81" x2="18.68" y2="32.81"/><line class="cls t1_1" x1="32.54" y1="15.62" x2="2.81" y2="20"/></g></svg><svg id="title-1_2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 333.61 26.44"><g><path class="cls t1_2" d="m2.81,23.64S176.8-6.36,330.8,5.64"/></g></svg>';
          }

          if (index === 2) {
            //IS BUTTON 3
            igTitles += '<svg id="title-2_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 73.61 37.61"><g><line class="cls t2_1" x1="2.81" y1="24.8" x2="61.81" y2="16.8"/><polyline class="cls t2_1" points="39.81 2.81 70.8 14.81 52.81 34.81 49.81 10.81"/></g></svg><svg id="title-2_2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 362.61 24.19"><g><path class="cls t2_2" d="m2.81,19.6S45.81-5.4,38.81,5.6c-7,11-14,18,17,4,31-14-11,17,13,11,24-6,291-12,291-12l-27,9"/></g></svg>';
          }

          if (index === 3) {
            //IS BUTTON 4
            igTitles += '<svg id="title-3_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 47.25 60.56"><g><line class="cls t3_1" x1="23.15" y1="8.35" x2="2.81" y2="2.81"/><line class="cls t3_1" x1="25.68" y1="31.96" x2="4.38" y2="41.33"/><line class="cls t3_1" x1="44.45" y1="36.09" x2="36" y2="57.75"/></g></svg><svg id="title-3_2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 393.61 75.61"><g><polyline class="cls t3_2"points="326.8 72.8 2.81 72.8 2.81 2.81 390.8 2.81"/></g></svg>';
          }

          igTitles += '</button>'; //BUILD DESCRIPTION

          igDescrip += '<div id="tabpanel-' + index + '" class="tabcontent no-show" aria-expanded="false"  aria-labelledby="tabpanel-' + index + '"><p>' + items.description + '</p></div>';
        } //FOR MOBILE


        igMobile += '<div class="mg-mobile-wrapper"><div class="mg-title-mobile"><h2>' + items.text + '</h2></div><div class="mg-description-mobile"><p>' + items.description + '</p></div></div>';
      }
    }); //IF ITEMS EXIST, PUSH

    if (igTitles.length && igDescrip.length) {
      $('#mg-titles').prepend(igTitles);
      $('#mg-descriptions').prepend(igDescrip); //PUSH FOR MOBILE

      $('#magic-mobile').prepend(igMobile);
    } //SPOTLIGHT TABS INTERACTION


    $(".tablink").on('focus mouseover', function (e) {
      var currentTab = $(this); //curent tab

      var currentTabPanel = $('#' + $(this).attr('aria-controls')); //corresponding tabpanel

      $(".tablink").removeClass('active'); //remove active from all tabs

      $(".tablink").attr({
        'aria-selected': 'false',
        'tabindex': '-1'
      }); //update all tabs to selected false

      currentTab.addClass('active'); //find current tab, make active

      currentTab.attr({
        'aria-selected': 'true',
        'tabindex': '0'
      }); //find current tab, make selected

      $(".tabcontent").addClass('no-show'); //add no-show to all tabpanels

      $(".tabcontent").removeClass('show'); //remove show from all tabpanels

      $(".tabcontent").attr('aria-expanded', 'false'); //update all tabpanels to selected false

      currentTabPanel.removeClass('no-show'); //remove no-show from current panel

      currentTabPanel.addClass('show'); //add show to current panel

      currentTabPanel.attr('aria-expanded', 'true'); //update all tabpanels to selected false
    }); //SPOTLIGHT TAB INTERACTION ONCE FOCUSED

    $(".tablink").on("keydown", function (e) {
      var currentTab = $(this); //curent tab

      var currentTabPanel = $('#' + $(this).attr('aria-controls')); //corresponding tabpanel
      //ARROW KEY MOVEMENTS

      switch (e.key) {
        //CONSUME DOWN ARROW KEY
        case template.KeyCodes.right:
        case template.KeyCodes.down:
          e.preventDefault();

          if ($(currentTab).is(":last-child")) {
            //IF WE ARE AT BOTTOM OF LIST
            $(".tablink:first-child").focus(); //GO TO FIRST
          } else {
            $(currentTab).next().focus(); //IF NOT GO TO NEXT DOWN
          }

          break;
        //CONSUME UP ARROW KEY

        case template.KeyCodes.left:
        case template.KeyCodes.up:
          e.preventDefault();

          if ($(currentTab).is(":first-child")) {
            //IF WE ARE AT TOP OF LIST
            $(".tablink:last-child").focus(); //GO TO FIRST
          } else {
            $(currentTab).prev().focus(); //ESLE GO
          }

          break;
      }
    });
  },
  "Body": function Body() {
    // FOR SCOPE
    var template = this; //AUTO FOUCS SIGN IN FIELD

    $("swsignin-txt-username").focus(); // APPLY RESPONSIVE DIMENSIONS TO CONTENT IMAGES

    $('div.ui-widget.app .ui-widget-detail img').not($('div.ui-widget.app.cs-rs-multimedia-rotator .ui-widget-detail img')).not($('div.ui-widget.app.gallery.json .ui-widget-detail img')).each(function () {
      if ($(this).attr('width') !== undefined && $(this).attr('height') !== undefined) {
        // IMAGE HAS INLINE DIMENSIONS
        $(this).css({
          'display': 'inline-block',
          'width': '100%',
          'max-width': $(this).attr('width') + 'px',
          'height': 'auto',
          'max-height': $(this).attr('height') + 'px'
        });
      }
    }); // ADJUST FIRST BREADCRUMB

    $('li.ui-breadcrumb-first > a > span').text('Home'); // CHECK PAGELIST HEADER

    if ($.trim($(".ui-widget.app.pagenavigation .ui-widget-header").text()) == "") {
      $(".ui-widget.app.pagenavigation .ui-widget-header").html("<h1></h1>");
    } //MOVE BUTTONS INTO UI-WIDGET-FOOTER


    $('.sp div.ui-widget.app, .spn div.ui-widget.app').each(function (appIndex, app) {
      //more link
      if ($('.more-link', app).length > 0) {
        $('.more-link', app).prependTo($('.ui-widget-footer', app));
        $('.more-link-under', app).parent().remove();
      } //view calendar link


      if ($('.view-calendar-link', app).length > 0) {
        $('.view-calendar-link', app).prependTo($('.ui-widget-footer', app));
      }

      if ($('.app-level-social-follow', app).length > 0 && $('.app-level-social-follow', app).children().length > 0) {
        $('.app-level-social-follow', app).prependTo($('.ui-widget-footer', app));
      } else {
        $('.app-level-social-follow', app).remove();
      } //Hide empty footer


      if ($('.ui-widget-footer', app).children().not('.clear').length <= 0) {
        $('.ui-widget-footer', app).addClass('no-children').hide();
      } else {
        $('.ui-widget-footer', app).addClass('has-children');
        $('.ui-widget-footer .clear', app).remove();
      } //move button to header on accordion app


      if ($(app).hasClass("content-accordion")) {
        var thisApp = $(app);
        $('.content-accordion-toolbar', app).appendTo($('.ui-widget-header', app));
      }
    }); //CONST FOR TOGGLE ATTR

    var toggle ='true'; //FIND SIDE NAV

    if ($('.sp-column.one .pagenavigation').length) {
      //ADD ATTR
      $('.sp-column.one .pagenavigation .ui-widget-header').attr('data-toggle', toggle);
    }

    $(".sp-column.one").csAppAccordion({
      "accordionBreakpoints": [768, 640, 480, 320]
    });
  },
  "GlobalIcons": function GlobalIcons() {
    $("#gb-icons, #gb-icons-sticky").creativeIcons({
      "iconNum":'10',
      "defaultIconSrc": '',
      "icons"             : [
          {
              "image"     : 'Student Program/19.png', 
              "showText"  : true,
              "text"      : 'District Reports',
              "url"       : '#',
              "target"    : '_self'
          },
          {
              "image"     : 'Student Program/19.png', 
              "showText"  : true,
              "text"      : 'Career Board',
              "url"       : '#',
              "target"    : '_self'
          },
          {
              "image"     : 'Student Program/19.png', 
              "showText"  : true,
              "text"      : 'Food Services',
              "url"       : '#',
              "target"    : '_self'
          },
          {
              "image"     : 'Student Program/19.png', 
              "showText"  : true,
              "text"      : 'Directory',
              "url"       : '#',
              "target"    : '_self'
          },
          {
              "image"     : 'Student Program/19.png', 
              "showText"  : true,
              "text"      : 'Employment',
              "url"       : '#',
              "target"    : '_self'
          },
          {
              "image": 'Student Program/19.png',
              "showText": true,
              "text": 'Calendar',
              "url": '#',
              "target": '_self'
          },
          {
              "image": 'Student Program/19.png',
              "showText": true,
              "text": 'Contact',
              "url": '#',
              "target": '_self'
          },
          {
              "image": 'Student Program/19.png',
              "showText": true,
              "text": 'Parent Powerschool',
              "url": '#',
              "target": '_self'
          },
          {
              "image": 'Student Program/19.png',
              "showText": true,
              "text": 'Locations',
              "url": '#',
              "target": '_self'
          },
          {
              "image": 'Student Program/19.png',
              "showText": true,
              "text": 'Mobile App',
              "url": '#',
              "target": '_self'
          }
      ],
      "siteID": "4",
      "siteAlias": "site",
      "calendarLink": "/Page/2",
      "contactEmail": "emailname@fsusd.com",
      "allLoaded": function allLoaded() {}
    });
  },
  "SocialIcons": function SocialIcons() {
    var socialIcons = [{
      "show":true,
      "label": "Facebook",
      "class": "facebook",
      "url":'#',
      "target":'_blank'
    }, {
      "show":true,
      "label": "Twitter",
      "class": "twitter",
      "url":'#',
      "target":'_blank'
    }, {
      "show":true,
      "label": "YouTube",
      "class": "youtube",
      "url":'#',
      "target":'_blank'
    }, {
      "show":true,
      "label": "Instagram",
      "class": "instagram",
      "url":'#',
      "target":'_blank'
    }, {
      "show":true,
      "label": "LinkedIn",
      "class": "linkedin",
      "url":'#',
      "target":'_blank'
    }, {
      "show":true,
      "label": "Vimeo",
      "class": "vimeo",
      "url":'#',
      "target":'_blank'
    }, {
      "show":true,
      "label": "Flickr",
      "class": "flickr",
      "url":'#',
      "target":'_blank'
    }, {
      "show":true,
      "label": "Pinterest",
      "class": "pinterest",
      "url":'#',
      "target":'_blank'
    }, {
      "show":true,
      "label": "Peachjar",
      "class": "peachjar",
      "url":'#',
      "target":'_blank'
    }]; //CREATE ICON VARIABLE

    var icons = ""; //LOOP THROUGH EACH AND BUILD

    $.each(socialIcons, function (index, icon) {
      if (icon.show) {
        icons += '<li class="gb-social-icon" data-toggle=' + icon.show + '><a class="gb-social-icons ' + icon.class + ' district" href="' + icon.url + '" target="' + icon.target + '" aria-label="' + icon.label + '"></a></li>';
      }
    }); //AFTER LOOP PUSH ICONS TO DOM

    if (icons.length) {
      $(".gb-socials").prepend(icons);
    }
  },
  // "StickyScroll": function() {
  //     var template = this;
  //     //CHECK IF ELEMENT IS IN VIEWPORT
  //     $.fn.isInViewport = function(offset) {
  //         let elementTriggerPoint = $(this).offset().top + ($(this).outerHeight()/2);
  //         let viewportMiddle = $(window).scrollTop() + ($(window).height()/2);
  //         let offsetSize = $(window).height() * offset;
  //         return elementTriggerPoint >= (viewportMiddle - offsetSize) && elementTriggerPoint <= (viewportMiddle + offsetSize);
  //     };
  //     if(this.GetBreakPoint() === 'desktop') {
  //         console.log('desktop');
  //         if(!$('#gb-icons-sticky').isInViewport(0.42)) {
  //             console.log('notInView');
  //             $(this).addClass('hello');
  //         } else {
  //             $(this).removeClass('hello');
  //             console.log('inView');
  //         }
  //     }
  // },
  "MegaMenu": function MegaMenu() {
    $("#sw-channel-list-container").megaChannelMenu({
      "numberOfChannels": 10,
      // integer 
      "maxSectionColumnsPerChannel": 3,
      // integer (this does NOT include the extra, non-section column)
      "extraColumn": false,
      // boolean
      "extraColumnTitle": false,
      //bolean
      "extraColumnDescription": false,
      //bolean
      "extraColumnImage": false,
      //bolean
      "extraColumnLink": false,
      //bolean
      "extraColumnPosition": "ABOVE",
      // string, "ABOVE", "BELOW", "LEFT", "RIGHT"
      "sectionHeadings": true,
      // boolean
      "sectionHeadingLinks": false,
      // boolean
      "maxNumberSectionHeadingsPerColumn": 3,
      // integer 
      "megaMenuElements": [{
        //CHANNEL 1
        "channelName":'',
        "channelColumns": [[//COLUMN 1
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }], [//COLUMN 2
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }], [//COLUMN 3
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }]]
      }, {
        //CHANNEL 2
        "channelName":'',
        "channelColumns": [[//COLUMN 1
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }], [//COLUMN 2
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }], [//COLUMN 3
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }]]
      }, {
        //CHANNEL 3
        "channelName":'',
        "channelColumns": [[//COLUMN 1
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }], [//COLUMN 2
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }], [//COLUMN 3
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }]]
      }, {
        //CHANNEL 4
        "channelName":'',
        "channelColumns": [[//COLUMN 1
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }], [//COLUMN 2
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }], [//COLUMN 3
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }]]
      }, {
        //CHANNEL 5
        "channelName":'',
        "channelColumns": [[//COLUMN 1
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }], [//COLUMN 2
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }], [//COLUMN 3
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }]]
      }, {
        //CHANNEL 6
        "channelName":'',
        "channelColumns": [[//COLUMN 1
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }], [//COLUMN 2
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }], [//COLUMN 3
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }]]
      }, {
        //CHANNEL 7
        "channelName":'',
        "channelColumns": [[//COLUMN 1
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }], [//COLUMN 2
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }], [//COLUMN 3
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }]]
      }, {
        //CHANNEL 8
        "channelName":'',
        "channelColumns": [[//COLUMN 1
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }], [//COLUMN 2
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }], [//COLUMN 3
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }]]
      }, {
        //CHANNEL 9
        "channelName":'',
        "channelColumns": [[//COLUMN 1
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }], [//COLUMN 2
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }], [//COLUMN 3
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }]]
      }, {
        //CHANNEL 10
        "channelName":'',
        "channelColumns": [[//COLUMN 1
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }], [//COLUMN 2
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }], [//COLUMN 3
        {
          //SECTION 1
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 2
          "heading": [{
            "text":''
          }],
          "sections":''
        }, {
          //SECTION 3
          "heading": [{
            "text":''
          }],
          "sections":''
        }]]
      }],
      "allLoaded": function allLoaded() {}
    });
  },
  "MMGPlugin": function MMGPlugin() {
    // FOR SCOPE
    var template = this;

    if ($("#sw-content-container10 .ui-widget.app.multimedia-gallery").length) {
      var mmg = eval("multimediaGallery" + $("#sw-content-container10 .ui-widget.app.multimedia-gallery:first").attr("data-pmi"));
      mmg.props.defaultGallery = false;
      $("#sw-content-container10 .ui-widget.app.multimedia-gallery:first").csMultimediaGallery({
        "efficientLoad": true,
        "imageWidth": 1200,
        "imageHeight": 655,
        "mobileDescriptionContainer": [960, 768, 640, 480, 320],
        // [960, 768, 640, 480, 320]
        "galleryOverlay": false,
        "linkedElement": [],
        // ["image", "title", "overlay"]
        "playPauseControl": true,
        "backNextControls": true,
        "bullets": false,
        "thumbnails": false,
        "thumbnailViewerNum": [4, 4, 3, 3, 2],
        // NUMERICAL - [960 view, 768 view, 640 view, 480 view, 320 view]
        "autoRotate": false,
        "hoverPause": true,
        "transitionType": "slide",
        // fade, slide, custom
        "transitionSpeed": 1.5,
        "transitionDelay": 4,
        "fullScreenRotator": false,
        "fullScreenBreakpoints": [960],
        // NUMERICAL - [960, 768, 640, 480, 320]
        "onImageLoad": function onImageLoad(props) {},
        // props.element, props.recordIndex, props.mmgRecords
        "allImagesLoaded": function allImagesLoaded(props) {},
        // props.element, props.mmgRecords
        "onTransitionStart": function onTransitionStart(props) {},
        // props.element, props.currentRecordIndex, props.currentGalleryIndex, props.nextRecordIndex, props.nextGalleryIndex, props.mmgRecords
        "onTransitionEnd": function onTransitionEnd(props) {},
        // props.element, props.currentRecordIndex, props.currentGalleryIndex, props.mmgRecords
        "allLoaded": function allLoaded(props) {
          //WRAP DESCRIPTION IN WRAPPER
          $('.mmg-description-outer', props.element).wrap('<div class="mmg-description-wrapper flex-cont fd-col jc-sb" />'); //IF CONTROLS EXIST, THEN WRAP THEM

          if ($('.mmg-control', props.element).length > 0) {
            $('.mmg-description-outer', props.element).append('<div class="mmg-controls flex-cont ai-c jc-c"></div>');
            $('.mmg-controls', props.element).append($('#hp-slideshow .mmg-control.back'));
            $('.mmg-controls', props.element).append($('#hp-slideshow .mmg-control.play-pause'));
            $('.mmg-controls', props.element).append($('#hp-slideshow .mmg-control.next'));
          }
        },
        // props.element, props.mmgRecords
        "onWindowResize": function onWindowResize(props) {} // props.element, props.mmgRecords

      });
    }
  },
  "ModEvents": function ModEvents() {
    // FOR SCOPE
    var template = this;
    $(".ui-widget.app.upcomingevents").modEvents({
      columns: "yes",
      monthLong: "yes",
      dayWeek: "yes"
    });
    eventsByDay(".upcomingevents .ui-articles");

    function eventsByDay(container) {
      $(".ui-article", container).each(function () {
        if (!$(this).find("h1.ui-article-title.sw-calendar-block-date").size()) {
          var moveArticle = $(this).html();
          $(this).parent().prev().children().children().next().append(moveArticle);
          $(this).parent().remove();
        }

        ;
      });
      $(".ui-article", container).each(function () {
        var newDateTime = $('.upcoming-column.left h1', this).html().toLowerCase().split("</span>");
        var newDate = '';
        var dateIndex = newDateTime.length > 2 ? 2 : 1; //if the dayWeek is set to yes we need to account for that in our array indexing
        //if we have the day of the week make sure to include it.

        if (dateIndex > 1) {
          newDate += newDateTime[0] + "</span>";
        } //add in the month


        newDate += newDateTime[dateIndex - 1] + '</span>'; //the month is always the in the left array position to the date
        //wrap the date in a new tag

        newDate += '<span class="jeremy-date">' + newDateTime[dateIndex] + '</span>'; //append the date and month back into their columns

        $('.upcoming-column.left h1', this).html(newDate); //add an ALL DAY label if no time was given

        $('.upcoming-column.right .ui-article-description', this).each(function () {
          if ($('.sw-calendar-block-time', this).length < 1) {
            //if it doesnt exist add it
            $(this).prepend('<span class="sw-calendar-block-time">ALL DAY</span>');
          }

          var text = $(".sw-calendar-block-title", this).text();
          var text = text.split("@");

          if (text.length == 2) {
            $(".sw-calendar-block-title a", this).text(text[0]);
            $(this).append("<span class='sw-calendar-block-location'>" + text[1] + "</span>");
          }

          $(".sw-calendar-block-time", this).appendTo($(this));
        }); //WRAP DATE AND MONTH IN A CONTAINER

        $(".jeremy-date, .joel-month", this).wrapAll("<span class='adam-hug'></span>");
        var eventDay = $.trim($(this).find(".joel-day").html().toLowerCase());
        var eventMonth = $.trim($(this).find(".joel-month").text().toLowerCase());
        var eventNumberDay = $.trim($(this).find(".jeremy-date").text());
        var ariaMonth = "";
        var ariaDate = "";

        switch (eventMonth) {
          case "jan":
            ariaMonth = "January";
            break;

          case "feb":
            ariaMonth = "February";
            break;

          case "mar":
            ariaMonth = "March";
            break;

          case "apr":
            ariaMonth = "April";
            break;

          case "may":
            ariaMonth = "May";
            break;

          case "jun":
            ariaMonth = "June";
            break;

          case "jul":
            ariaMonth = "July";
            break;

          case "aug":
            ariaMonth = "August";
            break;

          case "sep":
            ariaMonth = "September";
            break;

          case "oct":
            ariaMonth = "October";
            break;

          case "nov":
            ariaMonth = "November";
            break;

          case "dec":
            ariaMonth = "December";
            break;

          default:
            ariaMonth = eventMonth;
            break;
        }

        switch (eventNumberDay) {
          case "1":
            ariaDate = "First";
            break;

          case "2":
            ariaDate = "Second";
            break;

          case "3":
            ariaDate = "Third";
            break;

          case "4":
            ariaDate = "Fourth";
            break;

          case "5":
            ariaDate = "Fifth";
            break;

          case "6":
            ariaDate = "Sixth";
            break;

          case "7":
            ariaDate = "Seventh";
            break;

          case "8":
            ariaDate = "Eighth";
            break;

          case "9":
            ariaDate = "Ninth";
            break;

          case "10":
            ariaDate = "Tenth";
            break;

          case "11":
            ariaDate = "Eleventh";
            break;

          case "12":
            ariaDate = "Twelveth";
            break;

          case "13":
            ariaDate = "Thirteenth";
            break;

          case "14":
            ariaDate = "Fourteenth";
            break;

          case "15":
            ariaDate = "Fifteenth";
            break;

          case "16":
            ariaDate = "Sixteenth";
            break;

          case "17":
            ariaDate = "Seventeenth";
            break;

          case "18":
            ariaDate = "Eighteenth";
            break;

          case "19":
            ariaDate = "Nineteenth";
            break;

          case "20":
            ariaDate = "Twentieth";
            break;

          case "21":
            ariaDate = "Twenty-first";
            break;

          case "22":
            ariaDate = "Twenty-second";
            break;

          case "23":
            ariaDate = "Twenty-third";
            break;

          case "24":
            ariaDate = "Twenty-fourth";
            break;

          case "25":
            ariaDate = "Twenty-fifth";
            break;

          case "26":
            ariaDate = "Twenty-sixth";
            break;

          case "27":
            ariaDate = "Twenty-seventh";
            break;

          case "28":
            ariaDate = "Twenty-eighth";
            break;

          case "29":
            ariaDate = "Twenty-ninth";
            break;

          case "30":
            ariaDate = "Thirtieth";
            break;

          case "31":
            ariaDate = "Thirty-first";
            break;

          default:
            ariaDate = eventNumberDay;
            break;
        } //ADD MORE EVENTS LINK


        if ($(this).find(".ui-article-description").length > 2) {
          var moreEventsLink = $(this).find(".ui-article-description:nth-child(1) a").attr("href");
          moreEventsLink = moreEventsLink.split("/").slice(0, -2).join("/");
          moreEventsLink = moreEventsLink + "/day";
          $(this).find(".upcoming-column.right").append("<a href='" + moreEventsLink + "' target='blank' aria-label='View all events for " + eventDay + ", " + ariaMonth + " " + ariaDate + ".' class='all-events-link'></a>");
        }
      });
    } //ADD NO EVENTS TEXT


    $(".upcomingevents").each(function () {
      if (!$(this).find(".ui-article").length) {
        $("<li><p>There are no upcoming events to display.</p></l1>").appendTo($(this).find(".ui-articles"));
      }
    });
  },
  "RsMenu": function RsMenu() {
    //FOR SCOPE
    var template = this;
    var usedSearchLinks = [];
    var searchLinks = [{
      "text":'Career',
      "url":'#',
      "target":'_self'
    }, {
      "text":'Calendar',
      "url":'#',
      "target":'_self'
    }, {
      "text":'Staff',
      "url":'#',
      "target":'_self'
    }];
    searchLinks.forEach(checkIfUsed);
    var extraOptions = {}; //ARE THEY USED, IF SO ASSIGN THEM

    if (usedSearchLinks.length > 0) {
      Object.assign(extraOptions, {
        "Quick Links": usedSearchLinks
      });
    }

    ; //CHECK IF ASSIGNED

    function checkIfUsed(item) {
      //TRIM
      if ($.trim(item.text) != "") {
        var itemToPush = {
          "text": item.text,
          "url": item.url,
          "target": item.target
        }; //PLACE USED LINKS

        usedSearchLinks.push(itemToPush);
      }
    }

    $.csRsMenu({
      "breakPoint": 768,
      // SYSTEM BREAK POINTS - 768, 640, 480, 320
      "slideDirection": "left-to-right",
      // OPTIONS - left-to-right, right-to-left
      "menuButtonParent": "#gb-header-mobile",
      "menuBtnText": "Menu",
      "colors": {
        "pageOverlay": '#000000',
        "menuBackground": '#FFFFFF',
        "menuText": '#333333',
        "menuTextAccent": '#333333',
        "dividerLines": '#E6E6E6',
        "buttonBackground": '#E6E6E6',
        "buttonText": '#333333'
      },
      "showDistrictHome": template.ShowDistrictHome,
      "districtHomeText": "District",
      "showSchools": template.ShowSchoolList,
      "schoolMenuText": "Schools",
      "showTranslate": true,
      "translateMenuText": "Translate",
      "translateVersion": 2,
      // 1 = FRAMESET, 2 = BRANDED
      "translateId": "",
      "translateLanguages": [// ["ENGLISH LANGUAGE NAME", "TRANSLATED LANGUAGE NAME", "LANGUAGE CODE"]
      ["Afrikaans", "Afrikaans", "af"], ["Albanian", "shqiptar", "sq"], ["Amharic", "አማርኛ", "am"], ["Arabic", "العربية", "ar"], ["Armenian", "հայերեն", "hy"], ["Azerbaijani", "Azərbaycan", "az"], ["Basque", "Euskal", "eu"], ["Belarusian", "Беларуская", "be"], ["Bengali", "বাঙালি", "bn"], ["Bosnian", "bosanski", "bs"], ["Bulgarian", "български", "bg"], ["Burmese", "မြန်မာ", "my"], ["Catalan", "català", "ca"], ["Cebuano", "Cebuano", "ceb"], ["Chichewa", "Chichewa", "ny"], ["Chinese Simplified", "简体中文", "zh-CN"], ["Chinese Traditional", "中國傳統的", "zh-TW"], ["Corsican", "Corsu", "co"], ["Croatian", "hrvatski", "hr"], ["Czech", "čeština", "cs"], ["Danish", "dansk", "da"], ["Dutch", "Nederlands", "nl"], ["Esperanto", "esperanto", "eo"], ["Estonian", "eesti", "et"], ["Filipino", "Pilipino", "tl"], ["Finnish", "suomalainen", "fi"], ["French", "français", "fr"], ["Galician", "galego", "gl"], ["Georgian", "ქართული", "ka"], ["German", "Deutsche", "de"], ["Greek", "ελληνικά", "el"], ["Gujarati", "ગુજરાતી", "gu"], ["Haitian Creole", "kreyòl ayisyen", "ht"], ["Hausa", "Hausa", "ha"], ["Hawaiian", "ʻŌlelo Hawaiʻi", "haw"], ["Hebrew", "עִברִית", "iw"], ["Hindi", "हिंदी", "hi"], ["Hmong", "Hmong", "hmn"], ["Hungarian", "Magyar", "hu"], ["Icelandic", "Íslenska", "is"], ["Igbo", "Igbo", "ig"], ["Indonesian", "bahasa Indonesia", "id"], ["Irish", "Gaeilge", "ga"], ["Italian", "italiano", "it"], ["Japanese", "日本語", "ja"], ["Javanese", "Jawa", "jw"], ["Kannada", "ಕನ್ನಡ", "kn"], ["Kazakh", "Қазақ", "kk"], ["Khmer", "ភាសាខ្មែរ", "km"], ["Korean", "한국어", "ko"], ["Kurdish", "Kurdî", "ku"], ["Kyrgyz", "Кыргызча", "ky"], ["Lao", "ລາວ", "lo"], ["Latin", "Latinae", "la"], ["Latvian", "Latvijas", "lv"], ["Lithuanian", "Lietuvos", "lt"], ["Luxembourgish", "lëtzebuergesch", "lb"], ["Macedonian", "Македонски", "mk"], ["Malagasy", "Malagasy", "mg"], ["Malay", "Malay", "ms"], ["Malayalam", "മലയാളം", "ml"], ["Maltese", "Malti", "mt"], ["Maori", "Maori", "mi"], ["Marathi", "मराठी", "mr"], ["Mongolian", "Монгол", "mn"], ["Myanmar", "မြန်မာ", "my"], ["Nepali", "नेपाली", "ne"], ["Norwegian", "norsk", "no"], ["Nyanja", "madambwe", "ny"], ["Pashto", "پښتو", "ps"], ["Persian", "فارسی", "fa"], ["Polish", "Polskie", "pl"], ["Portuguese", "português", "pt"], ["Punjabi", "ਪੰਜਾਬੀ ਦੇ", "pa"], ["Romanian", "Română", "ro"], ["Russian", "русский", "ru"], ["Samoan", "Samoa", "sm"], ["Scottish Gaelic", "Gàidhlig na h-Alba", "gd"], ["Serbian", "Српски", "sr"], ["Sesotho", "Sesotho", "st"], ["Shona", "Shona", "sn"], ["Sindhi", "سنڌي", "sd"], ["Sinhala", "සිංහල", "si"], ["Slovak", "slovenský", "sk"], ["Slovenian", "slovenski", "sl"], ["Somali", "Soomaali", "so"], ["Spanish", "Español", "es"], ["Sundanese", "Sunda", "su"], ["Swahili", "Kiswahili", "sw"], ["Swedish", "svenska", "sv"], ["Tajik", "Тоҷикистон", "tg"], ["Tamil", "தமிழ்", "ta"], ["Telugu", "తెలుగు", "te"], ["Thai", "ไทย", "th"], ["Turkish", "Türk", "tr"], ["Ukrainian", "український", "uk"], ["Urdu", "اردو", "ur"], ["Uzbek", "O'zbekiston", "uz"], ["Vietnamese", "Tiếng Việt", "vi"], ["Welsh", "Cymraeg", "cy"], ["Western Frisian", "Western Frysk", "fy"], ["Xhosa", "isiXhosa", "xh"], ["Yiddish", "ייִדיש", "yi"], ["Yoruba", "yorùbá", "yo"], ["Zulu", "Zulu", "zu"]],
      "showAccount": true,
      "accountMenuText": "User Options",
      "usePageListNavigation": false,
      "extraMenuOptions": extraOptions,
      "siteID": "4",
      "allLoaded": function allLoaded() {}
    });
  },
  "Translate": function Translate() {
    $('#translate-dropdown').creativeTranslate({
      'type': 2,
      // 1 = FRAMESET, 2 = BRANDED, 3 = API
      'advancedOptions': {
        'addMethod': 'append',
        'removeBrandedDefaultStyling': false,
        'useTranslatedNames': true // ONLY FOR FRAMESET AND API VERSIONS - true = TRANSLATED LANGUAGE NAME, false = ENGLISH LANGUAGE NAME

      },
      'translateLoaded': function translateLoaded() {} // CALLBACK TO RUN YOUR CUSTOM CODE AFTER EVERYTHING IS READY               

    });
  },
  "Search": function Search() {
    var defSearchText = 'Search...';
    var searchBox = $('#gb-search-input');
    searchBox.attr('placeholder', '').val(defSearchText).focus(function () {
      if ($(this).val() == defSearchText) {
        $(this).val('');
      }
    }).blur(function () {
      if ($(this).val() == '') {
        $(this).val(defSearchText);
      }
    });
    $('#gb-search-form').submit(function (e) {
      e.preventDefault();

      if ($('#gb-search-input').val() != '' && $('#gb-search-input').val() != defSearchText) {
        window.location.href = '/site/Default.aspx?PageType=6&SiteID=4&SearchString=' + $('#gb-search-input').val();
      }
    });
  },
  "Footer": function Footer() {
    //FOR SCOPE
    var template = this; //ADD LOGO

    var homeLink = '<a style="display:inline-block" href="https://mpatzem.schoolwires.net/site">'; //SITE URL
    //LOGO DIMENSIONS

    var imgWidth = 255;
    var imgHeight = 165; //ADD LOGO TO HEADER

    var logoSrc = $.trim('https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/GlobalAssets/images//Faces/default-man.jpg');
    var srcSplit = logoSrc.split("/");
    var srcSplitLen = srcSplit.length; //CHECK IF LOGO IS EMPTY

    if (logoSrc == '' || srcSplit[srcSplitLen - 1] == 'default-man.jpg') {
      logoSrc = template.DefaultLogoSrc;
    } //ADD CUSTOM SCHOOL LOGO


    $('.gb-logo-footer').append(homeLink + '<img width="' + imgWidth + '" height="' + imgHeight + '" src="' + logoSrc + '" alt="Fletch_Test Logo"></a>'); //ADD BLACKBOARD FOOTER

    $(document).csBbFooter({
      'footerContainer': '#disclaimer-row',
      'useDisclaimer': false,
      'disclaimerText': ''
    });
  },
  "AllyClick": function AllyClick(event) {
    if (event.type == 'click') {
      return true;
    } else if (event.type == 'keydown' && (event.key == this.KeyCodes.space || event.key == this.KeyCodes.enter)) {
      return true;
    } else {
      return false;
    }
  },
  "GetBreakPoint": function GetBreakPoint() {
    return window.getComputedStyle(document.querySelector("body"), ":before").getPropertyValue("content").replace(/"|'/g, "");
    /*"*/
  }
};
/******/ })()
;</script>

    <!-- App Preview -->
    


    <style type="text/css">
        /* HOMEPAGE EDIT THUMBNAIL STYLES */

        div.region {
            ;
        }

            div.region span.homepage-thumb-region-number {
                font: bold 100px verdana;
                color: #fff;
            }

        div.homepage-thumb-region {
            background: #264867; /*dark blue*/
            border: 5px solid #fff;
            text-align: center;
            padding: 40px 0 40px 0;
            display: block;
        }

            div.homepage-thumb-region.region-1 {
                background: #264867; /*dark blue*/
            }

            div.homepage-thumb-region.region-2 {
                background: #5C1700; /*dark red*/
            }

            div.homepage-thumb-region.region-3 {
                background: #335905; /*dark green*/
            }

            div.homepage-thumb-region.region-4 {
                background: #B45014; /*dark orange*/
            }

            div.homepage-thumb-region.region-5 {
                background: #51445F; /*dark purple*/
            }

            div.homepage-thumb-region.region-6 {
                background: #3B70A0; /*lighter blue*/
            }

            div.homepage-thumb-region.region-7 {
                background: #862200; /*lighter red*/
            }

            div.homepage-thumb-region.region-8 {
                background: #417206; /*lighter green*/
            }

            div.homepage-thumb-region.region-9 {
                background: #D36929; /*lighter orange*/
            }

            div.homepage-thumb-region.region-10 {
                background: #6E5C80; /*lighter purple*/
            }

        /* END HOMEPAGE EDIT THUMBNAIL STYLES */
    </style>

    <style type="text/css" media="print">
        .noprint {
            display: none !important;
        }
    </style>

    <style type ="text/css">
        .ui-txt-validate {
            display: none;
        }
    </style>

    

<!-- Begin Schoolwires Traffic Code --> 

<script type="text/javascript">

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date(); a = s.createElement(o),
        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-5173826-6', 'auto', 'BBTracker' );
    ga('BBTracker.set', 'dimension1', 'AWS');
    ga('BBTracker.set', 'dimension2', 'False');
    ga('BBTracker.set', 'dimension3', 'SWCS000010');
    ga('BBTracker.set', 'dimension4', '4');
    ga('BBTracker.set', 'dimension5', '1');
    ga('BBTracker.set', 'dimension6', '1');

    ga('BBTracker.send', 'pageview');

</script>

<!-- End Schoolwires Traffic Code --> 

    <!-- Ally Alternative Formats Loader    START   -->
    
    <!-- Ally Alternative Formats Loader    END     -->

</head>

        <body data-translate="google">
    

    <input type="hidden" id="hidFullPath" value="https://mpatzem.schoolwires.net/" />
    <input type="hidden" id="hidActiveChannelNavType" value="-1" />
    <input type="hidden" id="hidActiveChannel" value ="0" />
    <input type="hidden" id="hidActiveSection" value="0" />

    <!-- OnScreen Alert Dialog Start -->
    <div id="onscreenalert-holder"></div>
    <!-- OnScreen Alert Dialog End -->

    <!-- ADA Skip Nav -->
    <div class="sw-skipnav-outerbar">
        <a href="#sw-maincontent" id="skipLink" class="sw-skipnav" tabindex="0">Skip to Main Content</a>
    </div>

    <!-- DashBoard SideBar Start -->
    
    <!-- DashBoard SideBar End -->

    <!-- off-canvas menu enabled-->
    

    

<style type="text/css">
	/* SPECIAL MODE BAR */
	div.sw-special-mode-bar {
		background: #FBC243 url('https://mpatzem.schoolwires.net/Static//GlobalAssets/Images/special-mode-bar-background.png') no-repeat;
		height: 30px;
		text-align: left;
		font-size: 12px;
		position: relative;
		z-index: 10000;
	}
	div.sw-special-mode-bar > div {
		padding: 8px 0 0 55px;
		font-weight: bold;
	}
	div.sw-special-mode-bar > div > a {
		margin-left: 20px;
		background: #A0803D;
		-moz-border-radius: 5px;
		-webkit-border-radius: 5px;
		color: #fff;
		padding: 4px 6px 4px 6px;
		font-size: 11px;
	}

	/* END SPECIAL MODE BAR */
</style>

<script type="text/javascript">
	
	function SWEndPreviewMode() { 
		var data = "{}";
		var success = "window.location='';";
		var failure = "CallControllerFailure(result[0].errormessage);";
		CallController("https://mpatzem.schoolwires.net/site/SiteController.aspx/EndPreviewMode", data, success, failure);
	}
	
    function SWEndEmulationMode() {
        var data = "{}";
        var success = "DeleteCookie('SourceEmulationUserID');DeleteCookie('SidebarIsClosed');window.location='https://mpatzem.schoolwires.net/ums/Users/Users.aspx';";
        var failure = "CallControllerFailure(result[0].errormessage);";
        CallController("https://mpatzem.schoolwires.net/site/SiteController.aspx/EndEmulationMode", data, success, failure);
	}

	function SWEndPreviewConfigMode() {
	    var data = "{}";
	    var success = "window.location='';";
	    var failure = "CallControllerFailure(result[0].errormessage);";
	    CallController("https://mpatzem.schoolwires.net/site/SiteController.aspx/EndPreviewConfigMode", data, success, failure);
	}
</script>
            

    <!-- BEGIN - MYSTART BAR -->
<div id='sw-mystart-outer' class='noprint'>
<div id='sw-mystart-inner'>
<div id='sw-mystart-left'>
<div class='sw-mystart-nav sw-mystart-button home'><a tabindex="0" href="https://mpatzem.schoolwires.net/Domain/4" alt="District Home" title="Return to the homepage on the district site."><span>District Home<div id='sw-home-icon'></div>
</span></a></div>
<div class='sw-mystart-nav sw-mystart-dropdown schoollist' tabindex='0' aria-label='Select a School' role='navigation'>
<div class='selector' aria-hidden='true'>Select a School...</div>
<div class='sw-dropdown' aria-hidden='false'>
<div class='sw-dropdown-selected' aria-hidden='true'>Select a School</div>
<ul class='sw-dropdown-list' aria-hidden='false' aria-label='Schools'>
<li><a href="https://mpatzem.schoolwires.net/Domain/1503">Humble ISD</a></li>
<li><a href="https://mpatzem.schoolwires.net/Domain/1580">Fletch_Test_2</a></li>
<li><a href="https://mpatzem.schoolwires.net/Domain/1634">Grosse Pointe</a></li>
</ul>
</div>
<div class='sw-dropdown-arrow' aria-hidden='true'></div>
</div>
</div>
<div id='sw-mystart-right'>
<div id='ui-btn-signin' class='sw-mystart-button signin'><a href="https://mpatzem.schoolwires.net/site/Default.aspx?PageType=7&SiteID=4&IgnoreRedirect=true"><span>Sign In</span></a></div>
<div id='ui-btn-register' class='sw-mystart-button register'><a href="https://mpatzem.schoolwires.net/site/Default.aspx?PageType=10&SiteID=4"><span>Register</span></a></div>
<div id='sw-mystart-search' class='sw-mystart-nav'>
<script type="text/javascript">
$(document).ready(function() {
    $('#sw-search-input').keyup(function(e) {        if (e.keyCode == 13) {
            SWGoToSearchResultsPageswsearchinput();
        }
    });
    $('#sw-search-input').val($('#swsearch-hid-word').val())});
function SWGoToSearchResultsPageswsearchinput() {
window.location.href="https://mpatzem.schoolwires.net/site/Default.aspx?PageType=6&SiteID=4&SearchString=" + $('#sw-search-input').val();
}
</script>
<label for="sw-search-input" class="hidden-label">Search Our Site</label>
<input id="sw-search-input" type="text" title="Search Term" aria-label="Search Our Site" placeholder="Search this Site..." />
<a tabindex="0" id="sw-search-button" title="Search" href="javascript:;" role="button" aria-label="Submit Site Search" onclick="SWGoToSearchResultsPageswsearchinput();"><span><img src="https://mpatzem.schoolwires.net/Static//globalassets/images/sw-mystart-search.png" alt="Search" /></span></a><script type="text/javascript">
$(document).ready(function() {
    $('#sw-search-button').keyup(function(e) {        if (e.keyCode == 13) {
            SWGoToSearchResultsPageswsearchinput();        }
    });
});
</script>

</div>
<div class='clear'></div>
</div>
</div>
</div>
<!-- END - MYSTART BAR -->
<div id="gb-page" class="hp site">
    <header id="gb-header">
        <div class="mystartbar">
            <div class="content-wrap flex-cont jc-sb ai-c">
                <div class="nav-section right flex-cont ai-c">
                    <div id="gb-header-mobile"></div>
                    <div class="cs-button-section flex-cont jc-c ai-fs">
                        <div id="search-input-wrapper" class="flex-cont ai-c">
                            <!-- BODY OF SEARCH-->
                            <form id="gb-search-form" aria-hidden="false">
                                <button type="submit" id="gb-search-submit" aria-label="Submit the search input"></button>
                                <label for="gb-search-input" class="hidden">Search...</label>		
                                <input type="text" id="gb-search-input" class="gb-search-input" value="Search..." />
                            </form>
                        </div>
                        <div id="gb-translate" class="custom-dropdown flex-cont jc-fs ai-c">
                            <div class="translate-icon"></div>
                            <button class="translate-selector" aria-controls="translate-dropdown" aria-expanded="false" aria-haspopup="true">Translate This Page</button>
                            <div id="translate-dropdown" class="dropdown" aria-hidden="true"></div>
                        </div>
                    </div>
                    <div class="nav-group right flex-cont ai-c">
                        <div id="cs-ab" class="custom-dropdown">
                            <button class="mystart-selector" aria-controls="a&b days" aria-expanded="false" aria-haspopup="true">
                                <span>A & B Day</span>
                            </button>
                            <div id="ab-dropdown" class="dropdown" aria-hidden="true" data-region="b">
                                <div id="sw-content-container1" class="region ui-hp"><div id='pmi-1948'>
<div id="module-content-1174" ><div class="ui-widget app upcomingevents">
 <div class="ui-widget-header ui-helper-hidden"></div>
 <div class="ui-widget-detail">
		<ul class="ui-articles">
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">Today</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1783/20230215/event/2646">B Day</a></span>
     </p>
</div>
</li>
	</ul>
<a class='view-calendar-link' href="https://mpatzem.schoolwires.net/Page/2"><span>View Calendar</span></a>
 </div>
 <div class="ui-widget-footer">
 </div>
</div>
</div>
</div>
</div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="nav-section left flex-cont ai-c">
                    <div class="nav-group left flex-cont ai-c">
                        <a class="extra-nav-links" data-toggle="true" href="#" target="_self" aria-label="Career Link">
                            <span>Career</span>
                        </a> 
    
                        <a class="extra-nav-links" data-toggle="true" href="#" target="_self" aria-label="Calendar Link">
                            <span>Calendar</span>
                        </a> 
    
                        <a class="extra-nav-links" data-toggle="true" href="#" target="_self" aria-label="Staff Link">
                            <span>Staff</span>
                        </a> 
    
                        <div id="cs-schools" class="custom-dropdown">
                            <button class="mystart-selector" aria-controls="Our Schools" aria-expanded="false">
                                <span>Our Schools</span>
                            </button>
                            <div id="school-dropdown" class="dropdown" aria-hidden="true">
                                <ul id="school-list"></ul>
                            </div>
                        </div>
    
                        <div id="cs-ar" class="custom-dropdown">
                            <button class="mystart-selector" aria-controls="Apply and Register" aria-expanded="false">
                                <span>Apply & Register</span>
                            </button>
                            <div id="ar-dropdown" class="dropdown" aria-hidden="true">
                                <ul id="ar-list"></ul>
                            </div>
                        </div>
                    </div>
                    <div id="cs-ar-mobile" class="custom-dropdown jc-c hide1024 show480">
                        <button class="mystart-selector" aria-controls="Apply and Register" aria-expanded="false">
                            <span>Apply & Register</span>
                        </button>
                        <div id="ar-dropdown" class="dropdown" aria-hidden="true">
                            <ul id="ar-list"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-wrap mobile-logo-flex">
            <div class="header-logo flex-cont jc-c ai-c" data-toggle="true">
                <div id="gb-logo" class="logo-container"></div>
            </div>
            <section class="gb-socials mobile hide1024 show768"></section>
            <div class="main-header ai-c show1024 hide768">
                <nav id="gb-channel-bar" title="main navigation">
                    <section id="gb-channel-list" class="flex-cont jc-c ai-c">
                        <div id="sw-channel-list-container" role="navigation">
<ul id="channel-navigation" class="sw-channel-list" role="menubar">
<li id="navc-HP" class="sw-channel-item" ><a href="https://mpatzem.schoolwires.net/Page/1" aria-label="Home"><span>Home</span></a></li>
<li id="navc-1173" class="sw-channel-item">
<a href="https://mpatzem.schoolwires.net/site/Default.aspx?PageType=1&SiteID=4&ChannelID=1173&DirectoryType=6
"">
<span>Discover</span></a>
<div class="hidden-sections"><ul>"
<li id="navs-1184" class="hidden-section"><a href="https://mpatzem.schoolwires.net/domain/1184"><span>Our Programs</span></a></li>
<li id="navs-1185" class="hidden-section"><a href="https://mpatzem.schoolwires.net/domain/1185"><span>Classes and Schedules</span></a></li>
<li id="navs-1186" class="hidden-section"><a href="https://mpatzem.schoolwires.net/domain/1186"><span>Locations</span></a></li>
<li id="navs-1187" class="hidden-section"><a href="https://mpatzem.schoolwires.net/domain/1187"><span>Teacher Information</span></a></li>
<li id="navs-1188" class="hidden-section"><a href="https://mpatzem.schoolwires.net/domain/1188"><span>School Locator</span></a></li>
<li id="navs-1189" class="hidden-section"><a href="https://mpatzem.schoolwires.net/domain/1189"><span>Discover Why</span></a></li>
<li id="navs-1207" class="hidden-section"><a href="https://mpatzem.schoolwires.net/domain/1207"><span>Subpage</span></a></li>
<li id="navs-1654" class="hidden-section"><a href="https://mpatzem.schoolwires.net/domain/1654"><span>Section 8</span></a></li>
<li id="navs-1655" class="hidden-section"><a href="https://mpatzem.schoolwires.net/domain/1655"><span>Section 9</span></a></li>
<li id="navs-1656" class="hidden-section"><a href="https://mpatzem.schoolwires.net/domain/1656"><span>Section 10</span></a></li>
<li id="navs-1657" class="hidden-section"><a href="https://mpatzem.schoolwires.net/domain/1657"><span>Section 11</span></a></li>
<li id="navs-1658" class="hidden-section"><a href="https://mpatzem.schoolwires.net/domain/1658"><span>Section 12</span></a></li>
<li id="navs-1659" class="hidden-section"><a href="https://mpatzem.schoolwires.net/domain/1659"><span>Section 13</span></a></li>
<li id="navs-1660" class="hidden-section"><a href="https://mpatzem.schoolwires.net/domain/1660"><span>Section 14</span></a></li>
<li id="navs-1661" class="hidden-section"><a href="https://mpatzem.schoolwires.net/domain/1661"><span>Section 15</span></a></li>
<li id="navs-1662" class="hidden-section"><a href="https://mpatzem.schoolwires.net/domain/1662"><span>Section 16</span></a></li>
<li id="navs-1663" class="hidden-section"><a href="https://mpatzem.schoolwires.net/domain/1663"><span>Section 17</span></a></li>
<li id="navs-1664" class="hidden-section"><a href="https://mpatzem.schoolwires.net/domain/1664"><span>Section 18</span></a></li>
<li id="navs-1665" class="hidden-section"><a href="https://mpatzem.schoolwires.net/domain/1665"><span>Section 19</span></a></li>
<li id="navs-1666" class="hidden-section"><a href="https://mpatzem.schoolwires.net/domain/1666"><span>Section 20</span></a></li>
<li id="navs-1667" class="hidden-section"><a href="https://mpatzem.schoolwires.net/domain/1667"><span>Section 21</span></a></li>

</ul></div>
<ul class="dropdown-hidden">
</ul>
</li><li id="navc-1174" class="sw-channel-item">
<a href="https://mpatzem.schoolwires.net/domain/1190"">
<span>Our Schools</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-1190"><a href="https://mpatzem.schoolwires.net/domain/1190"><span>Section Title</span></a></li>
<li id="navs-1191"><a href="https://mpatzem.schoolwires.net/domain/1191"><span>Headlines and Features</span></a></li>
<li id="navs-1192"><a href="https://mpatzem.schoolwires.net/domain/1192"><span>Human Resources</span></a></li>
<li id="navs-1193"><a href="https://mpatzem.schoolwires.net/domain/1193"><span>Employment Opportunities</span></a></li>
</ul>
</li><li id="navc-1175" class="sw-channel-item">
<a href="https://mpatzem.schoolwires.net/domain/1194"">
<span>Family Resources</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-1194"><a href="https://mpatzem.schoolwires.net/domain/1194"><span>Our Programs</span></a></li>
<li id="navs-1195"><a href="https://mpatzem.schoolwires.net/domain/1195"><span>Classes and Schedules</span></a></li>
<li id="navs-1196"><a href="https://mpatzem.schoolwires.net/domain/1196"><span>Locations</span></a></li>
<li id="navs-1197"><a href="https://mpatzem.schoolwires.net/domain/1197"><span>Teacher Information</span></a></li>
<li id="navs-1198"><a href="https://mpatzem.schoolwires.net/domain/1198"><span>School Locator</span></a></li>
<li id="navs-1199"><a href="https://mpatzem.schoolwires.net/domain/1199"><span>Discover Why</span></a></li>
</ul>
</li><li id="navc-1176" class="sw-channel-item">
<a href="https://mpatzem.schoolwires.net/domain/1200"">
<span>Clubs</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="sw-channel-dropdown">
<li id="navs-1200"><a href="https://mpatzem.schoolwires.net/domain/1200"><span>Our Programs</span></a></li>
<li id="navs-1201"><a href="https://mpatzem.schoolwires.net/domain/1201"><span>Classes and Schedules</span></a></li>
<li id="navs-1202"><a href="https://mpatzem.schoolwires.net/domain/1202"><span>Locations</span></a></li>
<li id="navs-1203"><a href="https://mpatzem.schoolwires.net/domain/1203"><span>Teacher Information</span></a></li>
<li id="navs-1204"><a href="https://mpatzem.schoolwires.net/domain/1204"><span>School Locator</span></a></li>
<li id="navs-1205"><a href="https://mpatzem.schoolwires.net/domain/1205"><span>Discover Why</span></a></li>
</ul>
</li><li id="navc-1288" class="hidden-channel">
<a href="https://mpatzem.schoolwires.net/domain/1288">
<span>Classrooms</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
</li><li id="navc-1434" class="sw-channel-item">
<a href="https://mpatzem.schoolwires.net/domain/1435"">
<span>Technology</span></a>
<div class="hidden-sections"><ul>"

</ul></div>
<ul class="dropdown-hidden">
<li id="navs-1435"><a href="https://mpatzem.schoolwires.net/domain/1435"><span>Technology</span></a></li>
</ul>
</li></ul><div class='clear'></div>
</div>



<script type="text/javascript">
    $(document).ready(function() {
        channelHoverIE();
        channelTouch();
        closeMenuByPressingKey();
    });

    function channelTouch() {
        // this will change the dropdown behavior when it is touched vs clicked.
        // channels will be clickable on second click. first click simply opens the menu.
        $('#channel-navigation > .sw-channel-item > a').on({
            'touchstart': function (e) {
                
                // see if has menu
                if ($(this).siblings('ul.sw-channel-dropdown').children('li').length > 0)  {
                    var button = $(this);

                    // add href as property if not already set
                    // then remove href attribute
                    if (!button.prop('linkHref')) {
                        button.prop('linkHref', button.attr('href'));
                        button.removeAttr('href');
                    }

                    // check to see if menu is already open
                    if ($(this).siblings('ul.sw-channel-dropdown').is(':visible')) {
                        // if open go to link
                        window.location.href = button.prop('linkHref');
                    } 

                } 
            }
        });
    }


    
    function channelHoverIE(){
		// set z-index for IE7
		var parentZindex = $('#channel-navigation').parents('div:first').css('z-index');
		var zindex = (parentZindex > 0 ? parentZindex : 8000);
		$(".sw-channel-item").each(function(ind) {
			$(this).css('z-index', zindex - ind);
			zindex --;
		});
	    $(".sw-channel-item").hover(function(){
	        var subList = $(this).children('ul');
	        if ($.trim(subList.html()) !== "") {
	            subList.show();
	            subList.attr("aria-hidden", "false").attr("aria-expanded", "true");
		    }
		    $(this).addClass("hover");
	    }, function() {
	        $(".sw-channel-dropdown").hide();
	        $(this).removeClass("hover");
	        var subList = $(this).children('ul');
	        if ($.trim(subList.html()) !== "") {
	            subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
	        }
	    });
    }

    function closeMenuByPressingKey() {
        $(".sw-channel-item").each(function(ind) {
            $(this).keyup(function (event) {
                if (event.keyCode == 27) { // ESC
                    $(".sw-channel-dropdown").hide();
                    $(this).removeClass("hover");
                    var subList = $(this).children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                }
                if (event.keyCode == 13 || event.keyCode == 32) { //enter or space
                    $(this).find('a').get(0).click();
                }
            }); 
        });

        $(".sw-channel-item a").each(function (ind) {
            $(this).parents('.sw-channel-item').keydown(function (e) {
                if (e.keyCode == 9) { // TAB
                    $(".sw-channel-dropdown").hide();
                    $(this).removeClass("hover");
                    var subList = $(this).children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                }
            });
        });

        $(".sw-channel-dropdown li").each(function(ind) {
            $(this).keydown(function (event) {
                if (event.keyCode == 9) { // TAB
                    $(".sw-channel-dropdown").hide();
                    var parentMenuItem = $(this).parent().closest('li');
                    parentMenuItem.removeClass("hover");
                    var subList = parentMenuItem.children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                    parentMenuItem.next().find('a:first').focus();
                    event.preventDefault();
                    event.stopPropagation();
                }

                if (event.keyCode == 37 || // left arrow
                    event.keyCode == 39) { // right arrow
                    $(".sw-channel-dropdown").hide();
                    var parentMenuItem = $(this).parent().closest('li');
                    parentMenuItem.removeClass("hover");
                    var subList = parentMenuItem.children('ul');
                    if ($.trim(subList.html()) !== "") {
                        subList.attr("aria-hidden", "true").attr("aria-expanded", "false");
                    }
                    if (event.keyCode == 37) {
                        parentMenuItem.prev().find('a:first').focus();
                    } else {
                        parentMenuItem.next().find('a:first').focus();
                    }
                    event.preventDefault();
                    event.stopPropagation();
                }
            });
        });
    }

</script>


                    </section>
                </nav>
            </div>
        </div>
    </header>

    <main id="gb-main">
        <section class="hp-row one">
            <a id="sw-maincontent" name="sw-maincontent" tabindex="-1"></a>
            <div class="content-wrap">
                <div id="hp-slideshow-outer" data-region="a">
                    <div id="hp-slideshow"><div id="sw-content-container10" class="region ui-hp"><div id='pmi-1232'>



<div id='sw-module-12280'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '4';
            var PageID = '1';
            var RenderLoc = '0';
            var MIID = '1228';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1228" >
<div class="ui-widget app multimedia-gallery" data-pmi="1232" data-mi="1228">
	<div class="ui-widget-header ui-helper-hidden">
		
	</div>
    <div class="ui-widget-detail">
    	<div id="mmg-container-1232" class="mmg-container" data-gallery-type="default" data-transition="fade" data-record-num="0" data-is-animating="false" data-play-state="playing" data-is-hovering="false" data-has-focus="false" data-is-touching="false" data-active-record-index="0" data-active-gallery-index="0">
            <div class="mmg-viewer">
                <div class="mmg-slides-outer">
                    <ul class="mmg-slides"></ul>
                </div>
                <div class="mmg-live-feedback mmg-ally-hidden" aria-live="polite"></div>
            </div>
        </div>
        
        <script type="text/javascript">
            var multimediaGallery1232 = {
            	"props": {
                	"defaultGallery": true,
                    "imageWidth" : 960,
                	"imageHeight" : 500,
                	"pageModuleInstanceId": "1232",
                    "moduleInstanceId": "1228",
                    "virtualFolderPath": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/ModuleInstance/1228/"
                },
				"records": [
{
                    "flexDataId": "1117",
                    "hideTitle": "False",
                    "title": "Alumni Program Successfully Launches",
                    "hideCaption": "False",
                    "caption": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/4/0.png",
                    "imageWidth": "999",
                    "imageHeight": "545",
                    "imageAltText": "cyberpunk",
                    "isLinked": "True",
                    "linkUrl": "#",
                    "linkText": "Discover More",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "",
                    "videoIsEmbedded": "False",
                    "videoIframe": ""
                },
{
                    "flexDataId": "1460",
                    "hideTitle": "False",
                    "title": "Lorem Ipsum",
                    "hideCaption": "False",
                    "caption": "Nunc sed augue lacus viverra vitae. Lectus magna fringilla urna porttitor rhoncus dolor purus.",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/4/1.png",
                    "imageWidth": "999",
                    "imageHeight": "545",
                    "imageAltText": "arcade",
                    "isLinked": "False",
                    "linkUrl": "",
                    "linkText": "",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "",
                    "videoIsEmbedded": "False",
                    "videoIframe": ""
                },
{
                    "flexDataId": "1459",
                    "hideTitle": "True",
                    "title": "Diam phasellus vestibulum",
                    "hideCaption": "False",
                    "caption": "",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/4/2.png",
                    "imageWidth": "999",
                    "imageHeight": "545",
                    "imageAltText": "arcade 1",
                    "isLinked": "False",
                    "linkUrl": "",
                    "linkText": "",
                    "openLinkInNewWindow": "False",
                    "videoLinkText": "",
                    "videoIsEmbedded": "False",
                    "videoIframe": ""
                },
{
                    "flexDataId": "1128",
                    "hideTitle": "False",
                    "title": "Quam vulputate dignissim suspendisse",
                    "hideCaption": "False",
                    "caption": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.",
                    "imageSrc": "https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/4/4.png",
                    "imageWidth": "999",
                    "imageHeight": "545",
                    "imageAltText": "tokyo",
                    "isLinked": "True",
                    "linkUrl": "https://loremipsum.io/",
                    "linkText": "Link Text",
                    "openLinkInNewWindow": "True",
                    "videoLinkText": "Video Text",
                    "videoIsEmbedded": "True",
                    "videoIframe": ""
                },
{}]
			};
		</script>
	</div>
	<div class="ui-widget-footer ui-clear"></div>
</div>

<script type="text/javascript" src="//extend.schoolwires.com/creative/scripts/creative/rotate/multimedia-gallery/cs.multimedia.gallery.min.js"></script>
<script type="text/javascript">
	$(function() {
    	if(multimediaGallery1232.props.defaultGallery) {
        	$("#pmi-1232 .ui-widget.app.multimedia-gallery").csMultimediaGallery({
                "pageModuleInstanceId": 1232,
                "moduleInstanceId": 1228,
                "imageWidth" : multimediaGallery1232.props.imageWidth,
                "imageHeight" : multimediaGallery1232.props.imageHeight,
                "playPauseControl" : true,
                "bullets" : true
            });
        }
    });
</script>

<style type="text/css">
#pmi-1232:before {
    content: "960";
    display: none;
}
@media (max-width: 1023px) {
    #pmi-1232:before {
        content: "768";
    }
}
@media (max-width: 767px) {
    #pmi-1232:before {
        content: "640";
    }
}
@media (max-width: 639px) {
    #pmi-1232:before {
        content: "480";
    }
}
@media (max-width: 479px) {
    #pmi-1232:before {
        content: "320";
    }
}
</style>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div></div>
                </div>
            </div>
        </section>
        <section class="gb-socials sticky show1024 hide768"></section>
        <section class="hp-row two">
            <div class="content-wrap">
                <div id="gb-icons"></div>
            </div>
        </section>
        <section class="hp-row three" data-region="c">
            <div class="content-wrap">
                <div id="sw-content-container2" class="region ui-hp"><div id='pmi-1949'>
<div id="module-content-1787" ><div class="ui-widget app upcomingevents">
 <div class="ui-widget-header">
     <h1>Mark Your Calendars</h1>
 </div>
 <div class="ui-widget-detail">
		<ul class="ui-articles">
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">Today</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230215/event/1165">La Luna</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">Friday</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230217/event/600">Dad Joke Day</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230217/event/1609">Okay Sure</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230217/event/2809">Hidden Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">Monday</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230220/event/1610">Okay Sure</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">2:00 PM - 3:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230220/event/291">Sure, Why Not?</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230220/event/2810">Hidden Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">February 22, 2023</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230222/event/1166">La Luna</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">February 24, 2023</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230224/event/601">Dad Joke Day</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230224/event/1611">Okay Sure</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230224/event/2811">Hidden Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">February 27, 2023</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230227/event/1612">Okay Sure</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">2:00 PM - 3:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230227/event/292">Sure, Why Not?</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">7:00 PM - 8:00 PM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230227/event/2812">Hidden Event</a></span>
     </p>
</div>
</li>
<li>
<div class="ui-article">
     <h1 class="ui-article-title sw-calendar-block-date">March 1, 2023</h1>
     <p class="ui-article-description">
         <span class="sw-calendar-block-time">8:00 AM - 9:00 AM</span>
         <span class="sw-calendar-block-title"><a href="
https://mpatzem.schoolwires.net/site/Default.aspx?PageID=2&DomainID=4#calendar1/20230301/event/1167">La Luna</a></span>
     </p>
</div>
</li>
	</ul>
<a class='view-calendar-link' href="https://mpatzem.schoolwires.net/Page/2"><span>View Calendar</span></a>
 </div>
 <div class="ui-widget-footer">
 </div>
</div>
</div>
</div>
</div>
            </div>
        </section>
        <section class="hp-row four">
            <div class="content-wrap columns flex-cont">
                <div class="hp-column one" data-region="d">
                    <div id="sw-content-container3" class="region ui-hp"><div id='pmi-1943'>



<div id='sw-module-17840'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '4';
            var PageID = '1';
            var RenderLoc = '0';
            var MIID = '1784';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1784" >
<div class="ui-widget app flexpage">
	<div class="ui-widget-header">
		<h1>Connect</h1>
	</div>
	
	<div class="ui-widget-detail">
<div class="ui-article"><span ><blockquote class="twitter-tweet"><p lang="en" dir="ltr">Tuesday is just second Monday.<br><br>📸: <a href="https://twitter.com/WindCaveNPS?ref_src=twsrc%5Etfw">@WindCaveNPS</a> <a href="https://t.co/GWGOuX2KAQ">pic.twitter.com/GWGOuX2KAQ</a></p>&mdash; National Park Service (@NatlParkService) <a href="https://twitter.com/NatlParkService/status/1615379440053243906?ref_src=twsrc%5Etfw">January 17, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></span></div> 
<div class="clear"></div>
</div>
	<div class="ui-widget-footer">
		
			
		
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div>
                    <div class="gb-socials region-e"></div>
                </div>
                <div class="hp-column two" data-region="e">
                    <div id="sw-content-container4" class="region ui-hp"><div id='pmi-1944'>



<div id='sw-module-17850'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '4';
            var PageID = '1';
            var RenderLoc = '0';
            var MIID = '1785';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1785" >
<div class="ui-widget app flexpage">
	<div class="ui-widget-header">
		<h1>Podcasts</h1>
	</div>
	
	<div class="ui-widget-detail">
<div class="ui-article"><span ><iframe frameborder="0" height="200" scrolling="no" src="https://embed.radiopublic.com/e?if=5178a14b-f106-4d89-9274-b8b0cbf26293" width="100%"></iframe></span></div> 
<div class="clear"></div>
</div>
	<div class="ui-widget-footer">
		
			
		
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
<div id='pmi-1953'>



<div id='sw-module-17900'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '4';
            var PageID = '1';
            var RenderLoc = '0';
            var MIID = '1790';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1790" >
<div class="ui-widget app flexpage">
	<div class="ui-widget-header ui-helper-hidden">
		
	</div>
	
	<div class="ui-widget-detail">
<div class="ui-article"><span ><iframe frameborder="0" height="200" scrolling="no" src="https://embed.radiopublic.com/e?if=5178a14b-f106-4d89-9274-b8b0cbf26293" width="100%"></iframe></span></div> 
<div class="clear"></div>
</div>
	<div class="ui-widget-footer">
		
			
		
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div>
                </div>
                <div class="hp-column three" data-region="f">
                    <div id="sw-content-container5" class="region ui-hp"><div id='pmi-1946'>



<div id='sw-module-11050'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '4';
            var PageID = '1';
            var RenderLoc = '0';
            var MIID = '1105';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1105" >
<div class="ui-widget app announcements">
	<div class="ui-widget-header">
		<h1>Announcements</h1>
	</div>
	
	<div class="ui-widget-detail">
		<ul class="ui-articles">
<li>
    <div class="ui-article">
		<div class="ui-article-description">
			<span><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p></span>
		</div>
		<div class="ui-article-controls">
        	<a class="sub-link" title="Go to comments for Hello World!" href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&ModuleInstanceID=1105&ViewID=ed695a1c-ef13-4546-b4eb-4fefcdd4f389&RenderLoc=0&FlexDataID=1504&PageID=1&Comments=true"><span>Comments (-1)</span></a>
			
		</div>
		<div class="clear"></div>
	</div>
</li>
<li>
    <div class="ui-article">
		<div class="ui-article-description">
			<span><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></span>
		</div>
		<div class="ui-article-controls">
        	<a class="sub-link" title="Go to comments for This is a test" href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&ModuleInstanceID=1105&ViewID=ed695a1c-ef13-4546-b4eb-4fefcdd4f389&RenderLoc=0&FlexDataID=1039&PageID=1&Comments=true"><span>Comments (-1)</span></a>
			
		</div>
		<div class="clear"></div>
	</div>
</li>
<li>
    <div class="ui-article">
		<div class="ui-article-description">
			<span><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></span>
		</div>
		<div class="ui-article-controls">
        	<a class="sub-link" title="Go to comments for Title here" href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&ModuleInstanceID=1105&ViewID=ed695a1c-ef13-4546-b4eb-4fefcdd4f389&RenderLoc=0&FlexDataID=1038&PageID=1&Comments=true"><span>Comments (-1)</span></a>
			
		</div>
		<div class="clear"></div>
	</div>
</li>
</ul>
</div>
	<div class="ui-widget-footer">
		
		<div class="app-level-social-follow"></div> <div class="app-level-social-rss"><a title='Subscribe to RSS Feed - Announcements' tabindex='0' class='ui-btn-toolbar rss' href="https://mpatzem.schoolwires.net/site/Default.aspx?PageType=4&DomainID=4&ModuleInstanceID=1105&PageID=1&PMIID=1946"><span>Subscribe to RSS Feed - Announcements </span></a></div>
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div>
                </div>
            </div>
            <div class="content-wrap extra-row">
                <div class="hp-inner-row" data-region="g">
                    <div id="sw-content-container6" class="region ui-hp"><div id='pmi-1947'>



<div id='sw-module-15650'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '4';
            var PageID = '1';
            var RenderLoc = '0';
            var MIID = '1565';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1565" >
<div class="ui-widget app headlines">
	
	<div class="ui-widget-header">
		<h1>District News</h1>
	</div>
	
	<div class="ui-widget-detail" id="sw-app-headlines-1565">
		<ul class="ui-articles">
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" >
            <span class="img">
                <img alt="kid " height="320" width="434" src="https://mpatzem.schoolwires.net/..//cms/lib/SWCS000010/Centricity/Domain/4/test-image.png" />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1565&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1449&PageID=1"><span>Star Wars</span></a>
            </h1> 
        <p class="ui-article-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras fringilla interdum sem, in imperdiet ipsum consectetur tristique.</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Star Wars" href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1565&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1449&PageID=1&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" >
            <span class="img">
                <img alt=" kid" height="319" width="434" src="https://mpatzem.schoolwires.net/..//cms/lib/SWCS000010/Centricity/Domain/4/test-image-2.png" />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1565&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1448&PageID=1"><span>The Adventures of Sherlock Holmes</span></a>
            </h1> 
        <p class="ui-article-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras fringilla interdum sem.</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for The Adventures of Sherlock Holmes" href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1565&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1448&PageID=1&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" >
            <span class="img">
                <img alt=" food" height="301" width="437" src="https://mpatzem.schoolwires.net/..//cms/lib/SWCS000010/Centricity/Domain/4/Asset 7.png" />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1565&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1447&PageID=1"><span>The Maltese Falcon</span></a>
            </h1> 
        <p class="ui-article-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras fringilla interdum sem.</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for The Maltese Falcon" href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1565&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1447&PageID=1&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
<li>  
    <div  class="ui-article">   
        <span class="ui-article-thumb" >
            <span class="img">
                <img alt=" glasses" height="301" width="437" src="https://mpatzem.schoolwires.net/..//cms/lib/SWCS000010/Centricity/Domain/4/Asset 8.png" />
            </span>
        </span>   
        
        <h1 class="ui-article-title">
        	<a href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1565&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1446&PageID=1"><span>Moonrise Kingdom</span></a>
            </h1> 
        <p class="ui-article-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>   
        <div class="ui-article-controls">                                    
            <a class="sub-link" title="Go to comments for Moonrise Kingdom" href="https://mpatzem.schoolwires.net/../site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=1565&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=1446&PageID=1&Comments=true"><span>Comments (-1)</span></a>
               
        </div>   
        <div class="clear"></div>  
    </div>
</li>
</ul><div class="ui-read-more"><a id='MoreLinkButton1565' class='more-link' aria-label='Go to more records' onclick='MoreViewClick(this);' href='https://mpatzem.schoolwires.net/site/default.aspx?PageType=14&DomainID=4&PageID=1&ModuleInstanceID=1565&ViewID=c83d46ac-74fe-4857-8c9a-5922a80225e2&IsMoreExpandedView=True'><span>more</span></a><div class='more-link-under'>&nbsp;</div></div>
</ul>
	</div>

	<div class="ui-widget-footer">
		
		
		<div class="clear"></div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
        /*$(document).on('click', 'a.ui-article-thumb', function() {
        	window.location = $(this).attr('href');
    	});*/
    		
		$('#sw-app-headlines-1565').find('img').each(function() {
			if ($.trim(this.src) == '' ) {
				$(this).parent().parent().remove();
			}
		});
        
        // Jason Smith - 12/9/2014 - Removed due to bandwidth implications
		
	});

</script>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div>
                </div>
            </div>
        </section>
        <section class="hp-row five" data-region="h">
            <div class="content-wrap">
                <div id="sw-content-container7" class="region ui-hp"><div id='pmi-1952'>



<div id='sw-module-17890'>
    <script type="text/javascript">
        $(document).ready(function() {
            var DomainID = '4';
            var PageID = '1';
            var RenderLoc = '0';
            var MIID = '1789';

            //added to check to make sure moderated content doesn't bleed through the dialog
            if ($('#dialog-overlay-WindowLargeModal-body.moderateContent').length > 0) {
                $("#module-content-" + MIID).find(".ui-widget-detail").find(".ui-article").append("<p>&nbsp;</p>");
            }
        });
    </script>

    <script type="text/javascript">$(document).ready(function() {CheckScript('ModuleView');CheckScript('Mustache');
 });</script>
    
    <div id="module-content-1789" >
<link rel="stylesheet" type="text/css" href="//extend.schoolwires.com/creative/css/sw-icon-library/sw-icon-library.min.css" />
<link rel="stylesheet" type="text/css" href="//extend.schoolwires.com/creative/module_library/icons/horizontal/css/icons-horizontal.min.css" />
<style type="text/css">
    #module-content-1789 .border-box {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
    #module-content-1789 .flex {
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
    }
	#module-content-1789 .flex-justify-center {
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
	}
    #module-content-1789 .flex-items-center {
        -webkit-align-items: center;
        -ms-flex-align: center;
        align-items: center;
    }
    
    #module-content-1789 .ui-articles {
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-flex-wrap: wrap;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        margin: 0 -20px;
    }
    #module-content-1789 .ui-articles > li {
    	width: 100%;
        min-width: calc(25% - 40px);
        max-width: 150px;
        margin: 0 20px 10px;
        padding: 0;
    }
    #module-content-1789 .ui-articles > li .ui-article {
    	padding: 5px 0;
        text-align: center;
    }
    #module-content-1789 .ui-articles > li .ui-article .icon {
    	margin: 0 0 10px 0;
    }
    #module-content-1789 .ui-articles > li .ui-article h2 {
    	margin: 0 0 10px 0;
    }
    #module-content-1789 .ui-articles > li .ui-article .icon,
    #module-content-1789 .ui-articles > li .ui-article h2[role="button"] {
        cursor: pointer;
    }
    #module-content-1789 .ui-articles > li .ui-article .icon {
    	position: relative;
        top: 0;
        -webkit-transition: all 0.25s;
        -moz-transition: all 0.25s;
        -ms-transition: all 0.25s;
        -o-transition: all 0.25s;
        transition: all 0.25s;        
    }
    #module-content-1789 .ui-articles > li .ui-article .icon:hover,
    #module-content-1789 .ui-articles > li .ui-article .icon:focus {
    	top: -5px;
    }
    #module-content-1789 .ui-articles > li .ui-article-overlay-outer {
        width: 100vw;
        height: 100vh;
    	display: none;
        position: fixed;
        top: 0;
        left: 0;
        z-index: 99999;
        background: rgba(0,0,0,0.5);
    }
    #module-content-1789 .ui-articles > li .ui-article-overlay {
		width: 100%;
        height: 100%;
    }
    #module-content-1789 .ui-articles > li .ui-article-overlay-inner {
    	width: 50vw;
        margin: 0 auto;
    	padding: 10px 20px 39px;
        background: #fff;
        -webkit-box-shadow: 0 0 3px 0 rgba(0,0,0,0.5);
        box-shadow: 0 0 3px 0 rgba(0,0,0,0.5);
    }
    #module-content-1789 .ui-articles > li .ui-article-overlay-close-button {
    	width: 30px;
        height: 30px;
        margin: 0 0 10px auto;
        cursor: pointer;
    }
    #module-content-1789 .ui-articles > li .ui-article-overlay-close-button:before {
        content: "\2715";
        color: #757575;
        font-size: 30px;
        font-weight: 300;
    }
    #module-content-1789 .ui-articles > li .ui-article-overlay-content {
    	max-height: calc(100vh - 200px);
        overflow-x: hidden;
        overflow-y: auto;
    }
    #module-content-1789 .ui-articles > li .ui-article-overlay-content * {
    	color: #000;
    }
    [data-primary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Primary_Color_Box,
    [data-primary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Primary_Color_Box .H1_Template,
    [data-primary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Primary_Color_Box .H2_Template,
    [data-primary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Primary_Color_Box .H3_Template,
    [data-primary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Primary_Color_Box .H4_Template,
    [data-primary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Primary_Color_Box a,
    [data-secondary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Secondary_Color_Box,
    [data-secondary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Secondary_Color_Box .H1_Template,
    [data-secondary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Secondary_Color_Box .H2_Template,
    [data-secondary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Secondary_Color_Box .H3_Template,
    [data-secondary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Secondary_Color_Box .H4_Template,
    [data-secondary-text-color="White"] #module-content-1789 .ui-articles > li .ui-article-overlay-content .Secondary_Color_Box {
    	color: #fff;
    }
	
    /* Responsive */
    #module-content-1789 .mobile-large-view .ui-articles > li {
        min-width: calc(50% - 40px);
    }
    #module-content-1789 .mobile-large-view .ui-articles > li .ui-article-overlay-inner {
    	width: calc(80vw - 20px);
    }
    #module-content-1789 .mobile-large-view .ui-articles > li .ui-article-overlay-content {
    	max-height: calc(100vh - 100px);
        overflow-x: hidden;
        overflow-y: auto;
    }
    #module-content-1789 .mobile-small-view .ui-articles {
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
    }
    #module-content-1789 .mobile-small-view .ui-articles > li {
    	min-width: calc(100% - 40px);
    }
</style>

<script tyle="text/javascript" src="//extend.schoolwires.com/creative/scripts/creative/tools/hex-to-filter/css-hex-to-filter.min.js"></script>

<div class="ui-widget app lightbox">
	<div class="ui-widget-header">
		<h1>Media</h1>
	</div>
	
	<div class="ui-widget-detail">
		<ul class="ui-articles">
<li data-id="1526">

	<!-- Record Article -->
	<div class="ui-article">
        
        	
            	<!-- Custom Image Is Selected -->
            	<div role="button" class="icon custom" aria-label="Open lightbox content for Test Lightbox Header." aria-controls="ui-article-overlay-1526" tabindex="0"><img src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Domain/4/new-pdcst.png" alt="" /></div>
             
        
		
        <!-- Record Title -->
        
        	<!-- Record Title Is Hidden -->
        
        <!-- End Record Title -->

		<!-- Record Description -->
        
            
            	<!-- Record Decription Text Is Blank -->
            
        
        <!-- End Record Description -->
    </div>
    
    <!-- Record Overlay -->
    <div id="ui-article-overlay-1526" class="ui-article-overlay-outer" aria-hidden="true" role="dialog">
    	<div class="ui-article-overlay flex flex-justify-center flex-items-center">
            <div class="ui-article-overlay-inner border-box">
                <div class="ui-article-overlay-close-button flex flex-justify-center flex-items-center" role="button" aria-label="Close the lightbox content." aria-controls="ui-article-overlay-1526" tabindex="0"></div>
                <div class="ui-article-overlay-content">
                	<div class="ui-article-overlay-content-inner" tabindex="0">
                   		<div style="padding: 56.25% 0 0 0; position: relative;"><iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://player.vimeo.com/video/161801298?h=376fe72dbd&amp;color=f7f7f7&amp;title=0&amp;byline=0&amp;portrait=0" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen="allowfullscreen"></iframe></div>
<p>
<script src="https://player.vimeo.com/api/player.js"></script>
</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Record Icon Size -->
    <style type="text/css">    
    	
        	#module-content-1789 .ui-articles > li[data-id="1526"] {
                max-width: 150px;
                max-width: px;
            }
            #module-content-1789 .ui-articles > li[data-id="1526"] .icon.custom img {
                max-width: 150px;
                max-height: 150px;
                max-width: px;
                max-height: px;
            }
                
    </style>
    
    <!-- If Custom Image -->
    
    	<!-- If Use Icon Color -->
        
        
        
        <!-- End If Use Icon Color -->
    
</li>
<li data-id="1528">

	<!-- Record Article -->
	<div class="ui-article">
        
        	
            	<!-- Custom Image Is Selected -->
            	<div role="button" class="icon custom" aria-label="Open lightbox content for Test." aria-controls="ui-article-overlay-1528" tabindex="0"><img src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity//Domain/4/test-lightbox.png" alt="" /></div>
             
        
		
        <!-- Record Title -->
        
        	<!-- Record Title Is Hidden -->
        
        <!-- End Record Title -->

		<!-- Record Description -->
        
            
            	<!-- Record Decription Text Is Blank -->
            
        
        <!-- End Record Description -->
    </div>
    
    <!-- Record Overlay -->
    <div id="ui-article-overlay-1528" class="ui-article-overlay-outer" aria-hidden="true" role="dialog">
    	<div class="ui-article-overlay flex flex-justify-center flex-items-center">
            <div class="ui-article-overlay-inner border-box">
                <div class="ui-article-overlay-close-button flex flex-justify-center flex-items-center" role="button" aria-label="Close the lightbox content." aria-controls="ui-article-overlay-1528" tabindex="0"></div>
                <div class="ui-article-overlay-content">
                	<div class="ui-article-overlay-content-inner" tabindex="0">
                   		<div style="padding: 56.25% 0 0 0; position: relative;"><iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://player.vimeo.com/video/336892239?h=1335474bbe&amp;color=f7f7f7&amp;title=0&amp;byline=0&amp;portrait=0" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen="allowfullscreen"></iframe></div>
<p>
<script src="https://player.vimeo.com/api/player.js"></script>
</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Record Icon Size -->
    <style type="text/css">    
    	
        	#module-content-1789 .ui-articles > li[data-id="1528"] {
                max-width: 150px;
                max-width: px;
            }
            #module-content-1789 .ui-articles > li[data-id="1528"] .icon.custom img {
                max-width: 150px;
                max-height: 150px;
                max-width: px;
                max-height: px;
            }
                
    </style>
    
    <!-- If Custom Image -->
    
    	<!-- If Use Icon Color -->
        
        
        
        <!-- End If Use Icon Color -->
    
</li>
<li data-id="1529">

	<!-- Record Article -->
	<div class="ui-article">
        
        	
            	<!-- Custom Image Is Selected -->
            	<div role="button" class="icon custom" aria-label="Open lightbox content for Test 3." aria-controls="ui-article-overlay-1529" tabindex="0"><img src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity//Domain/4/test-lightbox.png" alt="" /></div>
             
        
		
        <!-- Record Title -->
        
        	<!-- Record Title Is Hidden -->
        
        <!-- End Record Title -->

		<!-- Record Description -->
        
            
            	<!-- Record Decription Text Is Blank -->
            
        
        <!-- End Record Description -->
    </div>
    
    <!-- Record Overlay -->
    <div id="ui-article-overlay-1529" class="ui-article-overlay-outer" aria-hidden="true" role="dialog">
    	<div class="ui-article-overlay flex flex-justify-center flex-items-center">
            <div class="ui-article-overlay-inner border-box">
                <div class="ui-article-overlay-close-button flex flex-justify-center flex-items-center" role="button" aria-label="Close the lightbox content." aria-controls="ui-article-overlay-1529" tabindex="0"></div>
                <div class="ui-article-overlay-content">
                	<div class="ui-article-overlay-content-inner" tabindex="0">
                   		<div style="padding: 56.25% 0 0 0; position: relative;"><iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://player.vimeo.com/video/336892239?h=1335474bbe&amp;color=f7f7f7&amp;title=0&amp;byline=0&amp;portrait=0" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen="allowfullscreen"></iframe></div>
<p>
<script src="https://player.vimeo.com/api/player.js"></script>
</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Record Icon Size -->
    <style type="text/css">    
    	
        	#module-content-1789 .ui-articles > li[data-id="1529"] {
                max-width: 150px;
                max-width: px;
            }
            #module-content-1789 .ui-articles > li[data-id="1529"] .icon.custom img {
                max-width: 150px;
                max-height: 150px;
                max-width: px;
                max-height: px;
            }
                
    </style>
    
    <!-- If Custom Image -->
    
    	<!-- If Use Icon Color -->
        
        
        
        <!-- End If Use Icon Color -->
    
</li>
<li data-id="1530">

	<!-- Record Article -->
	<div class="ui-article">
        
        	
            	<!-- Custom Image Is Selected -->
            	<div role="button" class="icon custom" aria-label="Open lightbox content for Test 4." aria-controls="ui-article-overlay-1530" tabindex="0"><img src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity//Domain/4/test-lightbox.png" alt="" /></div>
             
        
		
        <!-- Record Title -->
        
        	<!-- Record Title Is Hidden -->
        
        <!-- End Record Title -->

		<!-- Record Description -->
        
            
            	<!-- Record Decription Text Is Blank -->
            
        
        <!-- End Record Description -->
    </div>
    
    <!-- Record Overlay -->
    <div id="ui-article-overlay-1530" class="ui-article-overlay-outer" aria-hidden="true" role="dialog">
    	<div class="ui-article-overlay flex flex-justify-center flex-items-center">
            <div class="ui-article-overlay-inner border-box">
                <div class="ui-article-overlay-close-button flex flex-justify-center flex-items-center" role="button" aria-label="Close the lightbox content." aria-controls="ui-article-overlay-1530" tabindex="0"></div>
                <div class="ui-article-overlay-content">
                	<div class="ui-article-overlay-content-inner" tabindex="0">
                   		<div style="padding: 56.25% 0 0 0; position: relative;"><iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://player.vimeo.com/video/336892239?h=1335474bbe&amp;color=f7f7f7&amp;title=0&amp;byline=0&amp;portrait=0" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen="allowfullscreen"></iframe></div>
<p>
<script src="https://player.vimeo.com/api/player.js"></script>
</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Record Icon Size -->
    <style type="text/css">    
    	
        	#module-content-1789 .ui-articles > li[data-id="1530"] {
                max-width: 150px;
                max-width: px;
            }
            #module-content-1789 .ui-articles > li[data-id="1530"] .icon.custom img {
                max-width: 150px;
                max-height: 150px;
                max-width: px;
                max-height: px;
            }
                
    </style>
    
    <!-- If Custom Image -->
    
    	<!-- If Use Icon Color -->
        
        
        
        <!-- End If Use Icon Color -->
    
</li>
<li data-id="1531">

	<!-- Record Article -->
	<div class="ui-article">
        
        	
            	<!-- Custom Image Is Selected -->
            	<div role="button" class="icon custom" aria-label="Open lightbox content for Test 5." aria-controls="ui-article-overlay-1531" tabindex="0"><img src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity//Domain/4/test-lightbox.png" alt="" /></div>
             
        
		
        <!-- Record Title -->
        
        	<!-- Record Title Is Hidden -->
        
        <!-- End Record Title -->

		<!-- Record Description -->
        
            
            	<!-- Record Decription Text Is Blank -->
            
        
        <!-- End Record Description -->
    </div>
    
    <!-- Record Overlay -->
    <div id="ui-article-overlay-1531" class="ui-article-overlay-outer" aria-hidden="true" role="dialog">
    	<div class="ui-article-overlay flex flex-justify-center flex-items-center">
            <div class="ui-article-overlay-inner border-box">
                <div class="ui-article-overlay-close-button flex flex-justify-center flex-items-center" role="button" aria-label="Close the lightbox content." aria-controls="ui-article-overlay-1531" tabindex="0"></div>
                <div class="ui-article-overlay-content">
                	<div class="ui-article-overlay-content-inner" tabindex="0">
                   		<div style="padding: 56.25% 0 0 0; position: relative;"><iframe style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" src="https://player.vimeo.com/video/161801298?h=376fe72dbd&amp;color=f7f7f7&amp;title=0&amp;byline=0&amp;portrait=0" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen="allowfullscreen"></iframe></div>
<p>
<script src="https://player.vimeo.com/api/player.js"></script>
</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Record Icon Size -->
    <style type="text/css">    
    	
        	#module-content-1789 .ui-articles > li[data-id="1531"] {
                max-width: 150px;
                max-width: px;
            }
            #module-content-1789 .ui-articles > li[data-id="1531"] .icon.custom img {
                max-width: 150px;
                max-height: 150px;
                max-width: px;
                max-height: px;
            }
                
    </style>
    
    <!-- If Custom Image -->
    
    	<!-- If Use Icon Color -->
        
        
        
        <!-- End If Use Icon Color -->
    
</li>
</ul><div class="ui-read-more"><a id='MoreLinkButton1789' class='more-link' aria-label='Go to more records' onclick='MoreViewClick(this);' href='https://mpatzem.schoolwires.net/site/default.aspx?PageType=14&DomainID=4&PageID=1&ModuleInstanceID=1789&IsMoreExpandedView=True'><span>more</span></a><div class='more-link-under'>&nbsp;</div></div>
</ul>
	</div>
	<div class="ui-widget-footer ui-clear"></div>
</div>

<script tyle="text/javascript" src="//extend.schoolwires.com/creative/module_library/icons/horizontal/js/icons-horizontal.min.js"></script>
<script tyle="text/javascript" src="//extend.schoolwires.com/creative/module_library/lightbox/js/cs.lightbox.app.min.js"></script>
<script type="text/javascript">
    $(function() {
        CsIconsHorizontalApp.Init(1952);
        CsLightboxApp.Init(1952, 1789);
    });
</script>
<script type="text/javascript">
$(document).ready(function (){
$(".tag-list li a").keypress(function(e) { if(e.which == 13) { $(this).click();   }});
});
function LoadGroupedData(container, MIID, PMI, groupYear, groupMonth, groupBy, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&EnableQuirksMode=0&ViewID=' + viewToUse + '&Tag=' + tag, container, 2, 'chkSidebar();');
}
function LoadData(container, MIID, PMI, flexDataID, groupYear, groupMonth, groupBy, targetView, tag) {
  if(targetView !== undefined || targetView.Length() == 0){
  //targetView looks at the hidden Detail View defined in the Builder View.
      targetView = $('#hid-'+MIID+'-DetailView').val();
   }
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&FlexDataID=' + flexDataID + '&GroupYear=' + groupYear + '&GroupMonth=' + groupMonth + '&GroupByField=' + groupBy + '&RenderLoc=0&FromRenderLoc=0&EnableQuirksMode=0&Tag=' + tag + '&ViewID=' + targetView, container, 2, 'chkSidebar();');
}
function LoadTaggedData(container, MIID, PMI, tag) {
  //ViewToUse looks at the hidden Sidebar List View defined in the Builder View.
  var viewToUse = "";
    if($('#hid-'+MIID+'-SidebarListView').length > 0){
      viewToUse = $("#hid-" + MIID + "-SidebarListView").val();
    };
  GetContent('https://mpatzem.schoolwires.net//cms/UserControls/ModuleView/ModuleViewRendererWrapper.aspx?DomainID=4&PageID=1&ModuleInstanceID=' + MIID + '&PageModuleInstanceID=' + PMI + '&RenderLoc=0&FromRenderLoc=0&Tag=' + tag + '&EnableQuirksMode=0&ViewID='+viewToUse, container, 2, 'chkSidebar();');
  setTimeout(function(){ $("#module-content-"+ MIID +"").find('[tabindex]:first').focus(); }, 200);
}
</script>
</div>

</div></div>
</div>
            </div>
        </section>
        <section class="hp-row six">
            <div class="content-wrap">
                <h1 class="mg-header hide640">
                    <span class="mg-head-1">Magic</span>
                    <div class="mg-header-icon-1024 show1024">
                        <img class="mg-wand-icon" src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/mg-wand.svg" alt="">
                        <img class="mg-star i1" src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/mg-bt-l.svg" alt="">
                        <img class="mg-star i2" src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/mg-tp-l.svg" alt="">
                        <img class="mg-star i3" src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/mg-tp-m.svg" alt="">
                        <img class="mg-star i4" src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/mg-md-r.svg" alt= "">
                        <img class="mg-star i5" src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/mg-bt-r.svg" alt= "">
                    </div>
                    <img class="mg-header-icon hide1024" src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/magic-wand.svg" alt="magic section icon">
                    <span class="mg-head-2">Happens Here</span>
                </h1>
                <h1 class="mg-header mobile hide1024 show640">
                    <span class="mg-head-1">Magic</span>
                    <span class="mg-header-wrap">
                        <img class="mg-header-icon" src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/magic-wand.svg" alt="magic section icon">
                        <span class="mg-head-2">Happens Here</span>
                    </span>
                </h1>
                <div id="mg-section" class="flex-cont jc-sb ai-fs show1024 hide768" data-toggle='true'>
                <!-- Tab links -->
                    <div id="mg-titles" class="tab flex-cont"></div>
                <!-- Tab content -->
                    <div id="mg-descriptions" class="tabpanel"></div>
                </div>
                <div id="magic-mobile" class="hide1024 show768">
                </div>
            </div>
            <div id="animation-graphic">
                <div id="city-bg">
                    <img id="cloud-1" class="mg-svgs" src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/cloud-1.svg" alt="">

                    <img id="cloud-2" class="mg-svgs" src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/cloud-2.svg" alt=""> 

                    <img id="cloud-3" class="mg-svgs" src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/cloud-3.svg" alt="">

                    <img id="car" class="mg-svgs" src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/car.svg" alt="">

                    <img id="windmill-1"class="mg-svgs" src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/Left-TurbineTop.svg" alt="">

                    <img id="windmill-2" class="mg-svgs" src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/Middle-TurbineTop.svg" alt="">

                    <img id="windmill-3" class="mg-svgs" src="https://mpatzem.schoolwires.net/cms/lib/SWCS000010/Centricity/Template/56/SVG/FarRight-TurbineTop.svg" alt="">    

                </div>

                <div id="city-bg-mobile"></div>

            </div>
        </section>
    </main>

    <footer id="gb-footer">
        <div class="content-wrap">
            <div class="gb-footer-row one flex-cont jc-sb ai-fs">
                <div class="gb-logo-footer"></div>
                <div class="footer-address flex-cont jc-fs">
                    <div class="footer-info-wrap top">
                        <h2 class="footer-info-header visit">Visit Us</h2>
                        <div class="footer-info-wrapper">
                            <p>
                                <span class="info-head">Arthur R. Sypek Center:<br></span>
                                <a class="address-wrap" href="https://goo.gl/maps/PgCzNFytBzLZeAka8" target="_blank">
                                    <span>2490 Hilborn Road<br></span>
                                    <span>Fairfield, CA 94534</span>
                                </a>

                            </p>
                            <p>
                                <span class="info-head">Assunpink Center:<br></span>
                                <a class="address-wrap" href="https://goo.gl/maps/KLBWnPo8Y9yQkHen8" target="_blank">
                                    <span>1085 Old Trenton Road<br></span>
                                    <span>Trenton, NJ 08690</span>
                                </a>

                            </p>
                        </div>
                    </div>
                    <div class="footer-info-wrap bottom">
                        <h2 class="footer-info-header contact">Contact Us</h2>
                        <div class="footer-info-wrapper">
                            <p>
                                <span class="info-head">Arthur R. Sypek Center:<br></span>
                                <span>707-399-5000</span>
                            </p>
                            <p>
                                <span class="info-head">Assunpink Center:<br></span>
                                <span>609.586.5144</span>
                            </p>
                            <p>
                                <span class="info-head">Adult Evening School:<br></span>
                                <span>609.586.5146</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="footer-link-container">

                    <ul id="footer-link-list">
                        <li><a href="https://mpatzem.schoolwires.net/site/Default.aspx?PageType=15&SiteID=4&SectionMax=15&DirectoryType=6" target="_self"><span>Site Map</span></a> 
</li>
                        <li data-toggle="true">
                            <a href="#" target="_blank">
                                <span>Custom Link 1</span>
                            </a>
                        </li> 
                        <li data-toggle="true">
                            <a href="#" target="_blank">
                                <span>Custom Link 2</span>
                            </a>
                        </li> 
                        <li data-toggle="true">
                            <a href="#" target="_blank">
                                <span>Custom Link 3</span>
                            </a>
                        </li> 
                        <li data-toggle="true">
                            <a href="#" target="_blank">
                                <span>Custom Link 4</span>
                            </a>
                        </li>
                        <li data-toggle="true">
                            <a href="#" target="_blank">
                                <span>Custom Link 5</span>
                            </a>
                        </li>
                        <li data-toggle="true">
                            <a href="#" target="_blank">
                                <span>Custom Link 6</span>
                            </a>
                        </li>
                        <li data-toggle="true">
                            <a href="#" target="_blank">
                                <span>Custom Link 7</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="gb-footer-row two">
                    <div id="footer-disclaimer" data-toggle="true"><p>DISCLAIMER AREA Lorem ipsum dolor sit amet, consectetur adipiscing elit. In erat urna, cursus eget bibendum quis, rhoncus ac justo. Proin justo eros, porttitor ac fringilla et, imperdiet non velit. In mauris erat, pulvinar in faucibus eu, convallis sed ex. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse et pellentesque augue. Quisque vel diam ante. Ut ut mi in sem porta auctor. Pellentesque a tortor elit. Nulla feugiat in magna id vestibulum. Quisque ipsum lectus, lobortis quis velit a, pharetra aliquam felis. Praesent venenatis dui co.</p></div>
                    <div id="disclaimer-row" class="flex-cont jc-fe">
                        <div class="cs-mystart-dropdown user-options">
                            <button class="cs-dropdown-selector" tabindex="0" aria-label='User Options' role="button" aria-expanded="false" aria-haspopup="true">
                                <span>User Options</span>
                            </button>
                            <div class="cs-dropdown" aria-hidden="true" style="display:none;">
                                <ul class="cs-dropdown-list"></ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div id="gb-icons-sticky"></div>
</div><!-- BEGIN - STANDARD FOOTER -->
<div id='sw-footer-outer'>
<div id='sw-footer-inner'>
<div id='sw-footer-left'></div>
<div id='sw-footer-right'>
<div id='sw-footer-links'>
<ul>
<li><a title='Click to email the primary contact' href='mailto:noreply@schoolwires.com'>Questions or Feedback?</a> | </li>
<li><a href='https://www.blackboard.com/blackboard-web-community-manager-privacy-statement' target="_blank">Blackboard Web Community Manager Privacy Policy (Updated)</a> | </li>
<li><a href='https://help.blackboard.com/Terms_of_Use' target="_blank">Terms of Use</a></li>
</ul>
</div>
<div id='sw-footer-copyright'>Copyright &copy; 2002-2023 Blackboard, Inc. All rights reserved.</div>
<div id='sw-footer-logo'><a href='http://www.blackboard.com' title="Blackboard, Inc. All rights reserved.">
<img src='https://mpatzem.schoolwires.net/Static//GlobalAssets/Images/Navbar/blackboard_logo.png'
 alt="Blackboard, Inc. All rights reserved."/>
</a></div>
</div>
</div>
</div>
<!-- END - STANDARD FOOTER -->
<script type="text/javascript">
   $(document).ready(function(){
      var beaconURL='https://analytics.schoolwires.com/analytics.asmx/Insert?AccountNumber=6boZxfPfyUa2SJk0fiN5BA%3d%3d&SessionID=15375777-516f-4cc8-82a0-70a1cad0a7db&SiteID=4&ChannelID=0&SectionID=0&PageID=1&HitDate=2%2f15%2f2023+3%3a40%3a45+PM&Browser=Chrome+87.0&OS=Unknown&IPAddress=10.61.78.175';
      try {
         $.getJSON(beaconURL + '&jsonp=?', function(myData) {});
      } catch(err) { 
         // prevent site error for analytics
      }
   });
</script>

    <input type="hidden" id="hid-pageid" value="1" />

    

    <div id='dialog-overlay-WindowMedium-base' class='ui-dialog-overlay-base' ><div id='WindowMedium' role='dialog' tabindex='-1'  class='ui-dialog-overlay medium' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowMedium-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowMedium-body' ></div><button class='ui-dialog-overlay-close' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowMedium");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowMedium-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowMedium-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowSmall-base' class='ui-dialog-overlay-base' ><div id='WindowSmall' role='dialog' tabindex='-1'  class='ui-dialog-overlay small' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowSmall-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowSmall-body' ></div><button class='ui-dialog-overlay-close' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowSmall");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowSmall-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowSmall-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowLarge-base' class='ui-dialog-overlay-base' ><div id='WindowLarge' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowLarge-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowLarge-body' ></div><button class='ui-dialog-overlay-close' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowLarge");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowLarge-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowLarge-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>

    <div id='dialog-overlay-WindowMediumModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowMediumModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay medium' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowMediumModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowMediumModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowMediumModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowMediumModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowMediumModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowSmallModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowSmallModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay small' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowSmallModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowSmallModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowSmallModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowSmallModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowSmallModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowLargeModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowLargeModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowLargeModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowLargeModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowLargeModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowLargeModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowLargeModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-WindowXLargeModal-base' class='ui-dialog-overlay-base-modal' ><div id='WindowXLargeModal' role='dialog' tabindex='-1'  class='ui-dialog-overlay xlarge' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-WindowXLargeModal-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-WindowXLargeModal-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("WindowXLargeModal");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-WindowXLargeModal-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-WindowXLargeModal-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>

    <div id='dialog-overlay-MyAccountSubscriptionOverlay-base' class='ui-dialog-overlay-base-modal' ><div id='MyAccountSubscriptionOverlay' role='dialog' tabindex='-1'  class='ui-dialog-overlay medium' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-MyAccountSubscriptionOverlay-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-MyAccountSubscriptionOverlay-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("MyAccountSubscriptionOverlay");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-MyAccountSubscriptionOverlay-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-MyAccountSubscriptionOverlay-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>

    <div id='dialog-overlay-InsertOverlay-base' class='ui-dialog-overlay-base-modal' ><div id='InsertOverlay' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-InsertOverlay-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-InsertOverlay-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("InsertOverlay");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-InsertOverlay-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-InsertOverlay-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    <div id='dialog-overlay-InsertOverlay2-base' class='ui-dialog-overlay-base-modal' ><div id='InsertOverlay2' role='dialog' tabindex='-1'  class='ui-dialog-overlay large' ><div class='ui-dialog-overlay-title-bar' id='dialog-overlay-InsertOverlay2-title-bar' ></div><div class='ui-dialog-overlay-body' id='dialog-overlay-InsertOverlay2-body' ></div><button class='ui-dialog-overlay-close modal' title='Close' aria-label='Close' onclick='CloseDialogOverlay("InsertOverlay2");' ></button><div class='ui-dialog-overlay-footer' id='dialog-overlay-InsertOverlay2-footer' ></div></div></div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dialog-overlay-InsertOverlay2-base').appendTo('body');
        
        $('body').on('keydown', '.ui-dialog-overlay-base-modal, .ui-dialog-overlay, .ui-sw-alert', function (e) {
            var swAlertOpen = $(".ui-sw-alert").length;
            if (swAlertOpen > 1) {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    //get id of open alert
                    var alertboxid = $('.ui-sw-alert').attr('id');
                    //click ok or no
                    if ($('#' + alertboxid + 'ok').length > 0) {
                        $('#' + alertboxid + 'ok').click();
                    } else {
                        $('#' + alertboxid + 'no').click();
                    }
                }
            } else {
                if (e.keyCode == 27) {//escape key
                    e.stopImmediatePropagation();
                    e.preventDefault();
                    // Check if ESC was pressed on DatePicker
                    var raisedByDatepicker = e.target.classList.contains("datepicker");
                    if (!raisedByDatepicker) {
                        e.stopImmediatePropagation();
                        e.preventDefault();
                        // Close Dialog
                        $('.ui-dialog-overlay-close.modal:visible').last().click();
                    } else {
                        // Remove focus
                        e.target.blur();
                        e.target.classList.remove("focus");
                    }
                }
            }
        });
 		
        

    });
</script>
    
    <div id="videowrapper" class="ui-helper-hidden">
        <div id="videodialog" role="application">
            <a id="videodialog-close" role="button" href="javascript:;" aria-label="Close Overlay" class="close-btn" onclick="closeVideoDialog();">CLOSE</a>
            <div id="videodialog-video" ></div>
            <div id="videodialog-foot" tabindex="0"></div>
        </div>
    </div>
    <div id="attachmentwrapper" class="ui-helper-hidden">
        <div id="attachmentdialog" role="application">
            <a id="attachmentdialog-close" role="button" href="javascript:;" aria-label="Close Overlay" class="close-btn" onclick="closeAttachmentDialog();">CLOSE</a>
            <div id="attachmentdialog-container"></div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function () {

            removeBrokenImages();
            checkSidebar();
            RemoveCookie();

            $('div.bullet').attr('tabindex', '');

            $('.navigation li.collapsible').each(function () {
                if ($(this).find('ul').length == 0) {
                    $(this).removeClass('collapsible');
                }
            });

            // find page nav state cookie and add open chevron
            var arrValues = GetCookie('SWPageNavState').split('~');

            $.each(arrValues, function () {
                if (this != '') {
                    $('#' + this).addClass('collapsible').prepend("<div class='bullet collapsible' aria-label='Close Page Submenu'/>");
                }
            });

            // find remaining sub menus and add closed chevron and close menu
            $('.navigation li > ul').each(function () {
                var list = $(this);

                if (list.parent().hasClass('active') && !list.parent().hasClass('collapsible')) {
                    // open sub for currently selected page                    
                    list.parent().addClass('collapsible').prepend("<div class='bullet collapsible'aria-label='Close Page Submenu' />");
                } else {
                    if (list.parent().hasClass('collapsible') && !list.siblings('div').hasClass('collapsible')) {
                        // open sub for page with auto expand
                        list.siblings('div.expandable').remove();
                        list.parent().prepend("<div class='bullet collapsible' aria-label='Close Page Submenu' />");
                    }
                }

                if (!list.siblings('div').hasClass('collapsible')) {
                    // keep all closed that aren't already set to open
                    list.parent().addClass('expandable').prepend("<div class='bullet expandable' aria-label='Open Page Submenu' />");
                    ClosePageSubMenu(list.parent());
                } else {
                    OpenPageSubMenu(list.parent());
                }
            });

            // remove bullet from hierarchy if no-bullet set
            $('.navigation li.collapsible').each(function () {
                if ($(this).hasClass('no-bullet')) {
                    if (!$(this).hasClass('navigationgroup')) { $(this).removeClass('collapsible'); }
                    $(this).children('div.collapsible').remove();
                }
            });

            $('.navigation li.expandable').each(function () {
                if ($(this).hasClass('no-bullet')) {
                    if (!$(this).hasClass('navigationgroup')) { $(this).removeClass('expandable'); }
                    $(this).children('div.expandable').remove();
                }
            });

            $('.navigation li:not(.collapsible,.expandable,.no-bullet)').each(function () {
                $(this).prepend("<div class='bullet'/>");
            });

            $('.navigation li.active').parents('ul').each(function () {
                if (!$(this).hasClass('page-navigation')) {
                    OpenPageSubMenu($(this).parent());
                }
            });

            // Set aria ttributes
            $('li.collapsible').each(function () {
                $(this).attr("aria-expanded", "true");
                $(this).find('div:first').attr('aria-pressed', 'true');
            });

            $('li.expandable').each(function () {
                $(this).attr("aria-expanded", "false");
                $(this).find('div:first').attr('aria-pressed', 'false');
            });

            $('div.bullet').each(function () {
                $(this).attr("aria-hidden", "true");
            });

            // set click event for chevron
            $(document).on('click', '.navigation div.collapsible', function () {
                ClosePageSubMenu($(this).parent());
            });

            $(document).on('click', '.navigation div.expandable', function () {
                OpenPageSubMenu($(this).parent());
            });

            // set navigation grouping links
            $(document).on('click', '.navigationgroup.collapsible > a', function () {
                ClosePageSubMenu($(this).parent());
            });

            $(document).on('click', '.navigationgroup.expandable > a', function () {
                OpenPageSubMenu($(this).parent());
            });

            //SW MYSTART DROPDOWNS
            $(document).on('click', '.sw-mystart-dropdown', function () {
                $(this).children(".sw-dropdown").css("display", "block");
            });

            $(".sw-mystart-dropdown").hover(function () { }, function () {
                $(this).children(".sw-dropdown").hide();
                $(this).blur();
            });

            //SW ACCOUNT DROPDOWN
            $(document).on('click', '#sw-mystart-account', function () {
                $(this).children("#sw-myaccount-list").show();
                $(this).addClass("clicked-state");
            });

            $("#sw-mystart-account, #sw-myaccount-list").hover(function () { }, function () {
                $(this).children("#sw-myaccount-list").hide();
                $(this).removeClass("clicked-state");
                $("#sw-myaccount").blur();
            });

            // set hover class for page and section navigation
            $('.ui-widget.app.pagenavigation, .ui-widget.app.sectionnavigation').find('li > a').hover(function () {
                $(this).addClass('hover');
            }, function () {
                $(this).removeClass('hover');
            });

            //set aria-label for home
            $('#navc-HP > a').attr('aria-label', 'Home');

            // set active class on channel and section
            var activeChannelNavType = $('input#hidActiveChannelNavType').val();
            if (activeChannelNavType == -1) {
                // homepage is active
                $('#navc-HP').addClass('active');
            } else if (activeChannelNavType == 1) {
                // calendar page is active
                $('#navc-CA').addClass('active');
            } else {
                // channel is active - set the active class on the channel
                var activeSelectorID = $('input#hidActiveChannel').val();
                $('#navc-' + activeSelectorID).addClass('active');

                // set the breadcrumb channel href to the channel nav href
                $('li[data-bccID=' + activeSelectorID + '] a').attr('href', $('#navc-' + activeSelectorID + ' a').attr('href'));
                $('li[data-bccID=' + activeSelectorID + '] a span').text($('#navc-' + activeSelectorID + ' a span').first().text());

                // set the active class on the section
                activeSelectorID = $('input#hidActiveSection').val();
                $('#navs-' + activeSelectorID).addClass('active');

                // set the breadcrumb section href to the channel nav href
                $('li[data-bcsID=' + activeSelectorID + '] a').attr('href', $('#navs-' + activeSelectorID + ' a').attr('href'));
                if ($('#navs-' + activeSelectorID + ' a').attr('target') !== undefined) {
                    $('li[data-bcsID=' + activeSelectorID + '] a').attr('target', $('#navs-' + activeSelectorID + ' a').attr('target'));
                }
                $('li[data-bcsID=' + activeSelectorID + '] span').text($('#navs-' + activeSelectorID + ' a span').text());

                if ($('.sw-directory-columns').length > 0) {
                    $('ul.ui-breadcrumbs li:last-child').remove();
                    $('ul.ui-breadcrumbs li:last-child a').replaceWith(function() { return $('span', this); });
                    $('ul.ui-breadcrumbs li:last-child span').append(' Directory');
                }
            }
        }); // end document ready

        function OpenPageSubMenu(li) {
            if (li.prop('tagName').toLowerCase() == "li") {
                if (li.hasClass('expandable')) {
                    li.removeClass('expandable').addClass('collapsible');
                }
                if (li.find('div:first').hasClass('expandable')) {
                    li.find('div:first').removeClass('expandable').addClass('collapsible').attr('aria-pressed', 'true').attr('aria-label','Close Page Submenu');
                }
                li.find('ul:first').attr('aria-hidden', 'false').show();

                li.attr("aria-expanded", "true");

                PageNavigationStateCookie();
            }
        }

        function ClosePageSubMenu(li) {
            if (li.prop('tagName').toLowerCase() == "li") {
                li.removeClass('collapsible').addClass('expandable');
                li.find('div:first').removeClass('collapsible').addClass('expandable').attr('aria-pressed', 'false').attr('aria-label','Open Page Submenu');
                li.find('ul:first').attr('aria-hidden', 'true').hide();

                li.attr("aria-expanded", "false");

                PageNavigationStateCookie();
            }
        }

        function PageNavigationStateCookie() {
            var strCookie = "";

            $('.pagenavigation li > ul').each(function () {
                var item = $(this).parent('li');
                if (item.hasClass('collapsible') && !item.hasClass('no-bullet')) {
                    strCookie += $(this).parent().attr('id') + '~';
                }
            });

            SetCookie('SWPageNavState', strCookie);
        }

        function checkSidebar() {
            $(".ui-widget-sidebar").each(function () {
                if ($.trim($(this).html()) != "") {
                    $(this).show();
                    $(this).siblings(".ui-widget-detail").addClass("with-sidebar");
                }
            });
        }

        function removeBrokenImages() {
            //REMOVES ANY BROKEN IMAGES
            $("span.img img").each(function () {
                if ($(this).attr("src") !== undefined && $(this).attr("src") != '../../') {
                    $(this).parent().parent().show();
                    $(this).parent().parent().siblings().addClass("has-thumb");
                }
            });
        }

        function LoadEventDetailUE(moduleInstanceID, eventDateID, userRegID, isEdit) {
            (userRegID === undefined ? userRegID = 0 : '');
            (isEdit === undefined ? isEdit = false : '');
            OpenDialogOverlay("WindowMediumModal", { LoadType: "U", LoadURL: "https://mpatzem.schoolwires.net//site/UserControls/Calendar/EventDetailWrapper.aspx?ModuleInstanceID=" + moduleInstanceID + "&EventDateID=" + eventDateID + "&UserRegID=" + userRegID + "&IsEdit=" + isEdit });
        }

        function RemoveCookie() {
            // There are no sub page            
            if ($('.pagenavigation li li').length == 0) {
                //return false;
                PageNavigationStateCookie();
            }
        }
    </script>

    <script type="text/javascript">

        function AddOffCanvasMenuHeightForSiteNav() {
            var sitenavulHeight = 0;

            if ($('#sw-pg-sitenav-ul').length > 0) {
                sitenavulHeight = parseInt($("#sw-pg-sitenav-ul").height());
            }

            var swinnerwrapHeight = 0;

            if ($('#sw-inner-wrap').length > 0) {
                swinnerwrapHeight = parseInt($("#sw-inner-wrap").height());
            }

            // 360px is abount 5 li height
            if (sitenavulHeight + 360 >= swinnerwrapHeight) {
                $("#sw-inner-wrap").height(sitenavulHeight + 360);
            }
        }

        function AddOffCanvasMenuHeightForSelectSchool() {
            var selectschoolulHeight = 0;

            if ($('#sw-pg-selectschool-ul').length > 0) {
                selectschoolulHeight = parseInt($("#sw-pg-selectschool-ul").height());
            }

            var swinnerwrapHeight = 0;

            if ($('#sw-inner-wrap').length > 0) {
                swinnerwrapHeight = parseInt($("#sw-inner-wrap").height());
            }

            // 360px is abount 5 li height
            if (selectschoolulHeight + 360 >= swinnerwrapHeight) {
                $("#sw-inner-wrap").height(selectschoolulHeight + 360);
            }
        }

        $(document).ready(function () {
            if ($("#sw-pg-sitenav-a").length > 0) {
                $(document).on('click', '#sw-pg-sitenav-a', function () {
                    if ($("#sw-pg-sitenav-ul").hasClass('sw-pgmenu-closed')) {
                        AddOffCanvasMenuHeightForSiteNav();

                        $("ul.sw-pgmenu-toplevel").removeClass('sw-pgmenu-open').addClass('sw-pgmenu-closed');
                        $("#sw-pg-sitenav-ul").removeClass('sw-pgmenu-closed');
                        $("#sw-pg-sitenav-ul").addClass('sw-pgmenu-open');
                    } else {
                        $("#sw-pg-sitenav-ul").removeClass('sw-pgmenu-open');
                        $("#sw-pg-sitenav-ul").addClass('sw-pgmenu-closed');
                    }
                });

                $(document).on('click', '#sw-pg-selectschool-a', function () {
                    if ($("#sw-pg-selectschool-ul").hasClass('sw-pgmenu-closed')) {
                        AddOffCanvasMenuHeightForSelectSchool();

                        $("ul.sw-pgmenu-toplevel").removeClass('sw-pgmenu-open').addClass('sw-pgmenu-closed');
                        $("#sw-pg-selectschool-ul").removeClass('sw-pgmenu-closed');
                        $("#sw-pg-selectschool-ul").addClass('sw-pgmenu-open');
                    } else {
                        $("#sw-pg-selectschool-ul").removeClass('sw-pgmenu-open');
                        $("#sw-pg-selectschool-ul").addClass('sw-pgmenu-closed');
                    }
                });

                $(document).on('click', '#sw-pg-myaccount-a', function () {
                    if ($("#sw-pg-myaccount-ul").hasClass('sw-pgmenu-closed')) {
                        $("ul.sw-pgmenu-toplevel").removeClass('sw-pgmenu-open').addClass('sw-pgmenu-closed');
                        $("#sw-pg-myaccount-ul").removeClass('sw-pgmenu-closed');
                        $("#sw-pg-myaccount-ul").addClass('sw-pgmenu-open');
                    } else {
                        $("#sw-pg-myaccount-ul").removeClass('sw-pgmenu-open');
                        $("#sw-pg-myaccount-ul").addClass('sw-pgmenu-closed');
                    }
                });

                $(document).on('click', '.pg-list-bullet', function () {
                    $(this).prev().toggle();

                    if ($(this).hasClass('closed')) {
                        AddOffCanvasMenuHeightForSiteNav();

                        $(this).removeClass('closed');
                        $(this).addClass('open');
                    } else {
                        $(this).removeClass('open');
                        $(this).addClass('closed');
                    }
                });

                $(document).on('mouseover', '#sw-pg-selectschool', function () {
                    $("#sw-pg-selectschool-firstli").removeClass('sw-pg-selectschool-firstli-mouseout').addClass('sw-pg-selectschool-firstli-mouseover');
                    $("#sw-pg-selectschool-firstli a").addClass('sw-pg-selectschool-firstli-a-mouseover').removeClass('sw-pg-selectschool-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-selectschool', function () {
                    $("#sw-pg-selectschool-firstli").removeClass('sw-pg-selectschool-firstli-mouseover').addClass('sw-pg-selectschool-firstli-mouseout');
                    $("#sw-pg-selectschool-firstli a").addClass('sw-pg-selectschool-firstli-a-mouseout').removeClass('sw-pg-selectschool-firstli-a-mouseover');
                });

                $(document).on('mouseover', '#sw-pg-myaccount', function () {
                    $("#sw-pg-myaccount-firstli").removeClass('sw-pg-myaccount-firstli-mouseout').addClass('sw-pg-myaccount-firstli-mouseover');
                    $("#sw-pg-myaccount-firstli a").addClass('sw-pg-myaccount-firstli-a-mouseover').removeClass('sw-pg-myaccount-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-myaccount', function () {
                    $("#sw-pg-myaccount-firstli").removeClass('sw-pg-myaccount-firstli-mouseover').addClass('sw-pg-myaccount-firstli-mouseout');
                    $("#sw-pg-myaccount-firstli a").addClass('sw-pg-myaccount-firstli-a-mouseout').removeClass('sw-pg-myaccount-firstli-a-mouseover');
                });

                $(document).on('mouseover', '#sw-pg-sitenav', function () {
                    $("#sw-pg-sitenav-firstli").removeClass('sw-pg-sitenav-firstli-mouseout').addClass('sw-pg-sitenav-firstli-mouseover');
                    $("#sw-pg-sitenav-firstli a").addClass('sw-pg-sitenav-firstli-a-mouseover').removeClass('sw-pg-sitenav-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-sitenav', function () {
                    $("#sw-pg-sitenav-firstli").removeClass('sw-pg-sitenav-firstli-mouseover').addClass('sw-pg-sitenav-firstli-mouseout');
                    $("#sw-pg-sitenav-firstli a").addClass('sw-pg-sitenav-firstli-a-mouseout').removeClass('sw-pg-sitenav-firstli-a-mouseover');
                });

                $(document).on('mouseover', '#sw-pg-district', function () {
                    $("#sw-pg-district-firstli").removeClass('sw-pg-district-firstli-mouseout').addClass('sw-pg-district-firstli-mouseover');
                    $("#sw-pg-district-firstli a").addClass('sw-pg-district-firstli-a-mouseover').removeClass('sw-pg-district-firstli-a-mouseout');
                });

                $(document).on('mouseout', '#sw-pg-district', function () {
                    $("#sw-pg-district-firstli").removeClass('sw-pg-district-firstli-mouseover').addClass('sw-pg-district-firstli-mouseout');
                    $("#sw-pg-district-firstli a").addClass('sw-pg-district-firstli-a-mouseout').removeClass('sw-pg-district-firstli-a-mouseover');
                });
            }
        });


    </script>
    <script src='https://mpatzem.schoolwires.net/Static//GlobalAssets/Scripts/min/jquery-ui-1.12.0.min.js' type='text/javascript'></script>
    <script src="https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/min/SW-UI_2680.min.js" type='text/javascript'></script>
    <script src="https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/jquery.sectionlayer.js" type='text/javascript'></script>
    
    <script src="https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/min/swfobject.min.js" type="text/javascript"></script>
    <script src="https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/min/jquery.ajaxupload_2440.min.js" type="text/javascript"></script>

    <!-- Begin swuc.CheckScript -->
  <script type="text/javascript" src="https://mpatzem.schoolwires.net/Static/GlobalAssets/Scripts/ThirdParty/json2.js"></script>
  
<script>var homeURL = location.protocol + "//" + window.location.hostname;

function parseXML(xml) {
    if (window.ActiveXObject && window.GetObject) {
        var dom = new ActiveXObject('Microsoft.XMLDOM');
        dom.loadXML(xml);
        return dom;
    }

    if (window.DOMParser) {
        return new DOMParser().parseFromString(xml, 'text/xml');
    } else {
        throw new Error('No XML parser available');
    }
}

function GetContent(URL, TargetClientID, Loadingtype, SuccessCallback, FailureCallback, IsOverlay, Append) {
    (Loadingtype === undefined ? LoadingType = 3 : '');
    (SuccessCallback === undefined ? SuccessCallback = '' : '');
    (FailureCallback === undefined ? FailureCallback = '' : '');
    (IsOverlay === undefined ? IsOverlay = '0' : '');
    (Append === undefined ? Append = false : '');

    var LoadingHTML;
    var Selector;

    switch (Loadingtype) {
        //Small
        case 1:
            LoadingHTML = "SMALL LOADER HERE";
            break;
            //Large
        case 2:
            LoadingHTML = "<div class='ui-loading large' role='alert' aria-label='Loading content'></div>";
            break;
            // None
        case 3:
            LoadingHTML = "";
            break;
    }

    Selector = "#" + TargetClientID;

    ajaxCall = $.ajax({
        url: URL,
        cache: false,
        beforeSend: function () {
            if (Loadingtype != 3) { BlockUserInteraction(TargetClientID, LoadingHTML, Loadingtype, 0, IsOverlay); }
        },
        success: function (strhtml) {

            // check for calendar and empty div directly surrounding eventlist uc first 
            //      to avoid memory error in IE (Access violation reading location 0x00000018)
            //      need to figure out exactly why this is happening..
            //      Partially to do with this? http://support.microsoft.com/kb/927917/en-us
            //      The error/crash happens when .empty() is called on Selector 
            //          (.html() calls .empty().append() in jquery)
            //      * no one has come across this issue anywhere else in the product so far

            if ($(Selector).find('#calendar-pnl-calendarlist').length > 0) {
                $('#calendar-pnl-calendarlist').empty();
                $(Selector).html(strhtml);
            } else if (Append) {
                $(Selector).append(strhtml);
            }
            else {
                $(Selector).html(strhtml);
            }


            // check for tabindex 
            if ($(Selector).find(":input[tabindex='1']:first").length > 0) {
                $(Selector).find(":input[tabindex='1']:first").focus();
            } else {
                $(Selector).find(":input[type='text']:first").not('.nofocus').focus();
            }

            //if (CheckDirty(Selector) === true) { BindSetDirty(Selector); }
            //CheckDirty(Selector);
            BlockUserInteraction(TargetClientID, '', '', 1);
            (SuccessCallback != '' ? eval(SuccessCallback) : '');
        },
        failure: function () {
            BlockUserInteraction(TargetClientID, '', '', 1);
            (FailureCallback != '' ? eval(FailureCallback) : '');
        }
    });
}

function BlockUserInteraction(TargetClientID, LoadingHTML, Loadingtype, Unblock, IsOverlay) {
    if (LoadingHTML === undefined) {
        LoadingHTML = "<div class='ui-loading large'></div>";
    }

    if (Unblock == 1) {
        $('#' + TargetClientID).unblockinteraction();
    } else {
        if (IsOverlay == 1) {
            $('#' + TargetClientID).blockinteraction({ message: LoadingHTML, type: Loadingtype, isOverlay: true });
        } else {
            $('#' + TargetClientID).blockinteraction({ message: LoadingHTML, type: Loadingtype });
        }
    }
}

function OpenUltraDialogOverlay(OverlayClientID, options, Callback) {
    lastItemClicked = document.activeElement;
    var defaults = {
        LoadType: "U",
        LoadURL: "",
        TargetDivID: "",
        LoadContent: "",
        NoResize: false,
        ScrollTop: false,
        CloseCallback: undefined
    };

    jQuery.extend(defaults, options);

    // check what browser/version we're on
    var isIE = GetIEVersion();

    if (isIE == 0) {
        if ($.browser.mozilla) {
            //Firefox/Chrome
            $("body").css("overflow", "hidden");
        } else {
            //Safari
            $("html").css("overflow", "hidden");
        }
    } else {
        // IE
        $("html").css("overflow", "hidden");
    }

    var OverlaySelector;
    var BodyClientID;
    var TargetDivSelector;
    var CurrentScrollPosition;

    if (defaults.ScrollTop) {
        $.scrollTo(0, { duration: 0 });
    }

    OverlaySelector = "#dialog-ultra-overlay-" + OverlayClientID + "-base";
    BodyClientID = "dialog-ultra-overlay-" + OverlayClientID + "-holder";
    TargetDivSelector = "#" + defaults.TargetDivID;

    if ($.trim($("#dialog-ultra-overlay-" + OverlayClientID + "-holder").html()) != "") {
        CloseUltraDialogOverlay(OverlayClientID);
    }

    // U = URL
    // H - HTML
    // D - Divider

    var success = "";

    if (Callback !== undefined) {
        success += Callback;
    }

    // block user interaction
    BlockUserInteraction(BodyClientID, "<div class='ui-loading large'></div>", 2, 0, 1);

    switch (defaults.LoadType) {
        case 'U':
            GetContent(defaults.LoadURL, BodyClientID, 3, success);
            break;
        case 'H':
            $("#" + BodyClientID).html(defaults.LoadContent);
            break;
        case 'D':
            $("#" + BodyClientID).html($(TargetDivSelector).html());
            break;
    };

    // open the lateral panel
    var browserWidth = $(document).width();

    if (OverlayClientID == "UltraOverlayLarge") {
        browserWidth = browserWidth - 200;
    } else {
        browserWidth = browserWidth - 280;
    }

    $("#dialog-ultra-overlay-" + OverlayClientID + "-body").css("width", browserWidth);
    $(OverlaySelector).addClass("is-visible");

    // hide 'X' button if second window opened
    if (OverlayClientID == "UltraOverlayMedium") {
        $("#dialog-ultra-overlay-UltraOverlayLarge-close").hide();
    }
}

function CloseUltraDialogOverlay(OverlayClientID) {
    $("#dialog-ultra-overlay-" + OverlayClientID + "-base").removeClass("is-visible");

    // show 'X' button if second window closed
    if (OverlayClientID == "UltraOverlayMedium") {
        $("#dialog-ultra-overlay-UltraOverlayLarge-close").show();
    }

    if (OverlayClientID != "UltraOverlayMedium") {
        // check what browser/version we're on
        var isIE = GetIEVersion();

        if (isIE == 0) {
            if ($.browser.mozilla) {
                //Firefox/Chrome
                $("body").css("overflow", "auto");
            } else {
                //Safari
                $("html").css("overflow", "auto");
            }
        } else {
            // IE
            $("html").css("overflow", "auto");
        }
    }
    //focus back on the element clicked to open the dialog
    if (lastItemClicked !== undefined) {
        lastItemClicked.focus();
        lastItemClicked = undefined;
    }
}

//Save the last item clicked when opening a dialog overlay so the focus can go there upon close
var lastItemClicked;
function OpenDialogOverlay(OverlayClientID, options, Callback) {
    lastItemClicked = document.activeElement;
    var defaults = {
        LoadType: 'U',
        LoadURL: '',
        TargetDivID: '',
        LoadContent: '',
        NoResize: false,
        ScrollTop: false,
        CloseCallback: undefined
    };

    jQuery.extend(defaults, options);

    //check what browser/version we're on
    var isIE = GetIEVersion();

    //if (isIE == 0) {
    //    if ($.browser.mozilla) {
    //        //Firefox/Chrome
    //        $('body').css('overflow', 'hidden');
    //    } else {
    //        //Safari
    //        $('html').css('overflow', 'hidden');
    //    }
    //} else {
    //    // IE
    //    $('html').css('overflow', 'hidden');
    //}
    $('html').css('overflow', 'hidden');

    var OverlaySelector;
    var BodyClientID;
    var TargetDivSelector;

    if (defaults.ScrollTop) {
        $.scrollTo(0, { duration: 0 });
    }

    OverlaySelector = "#dialog-overlay-" + OverlayClientID + "-base";
    BodyClientID = "dialog-overlay-" + OverlayClientID + "-body";
    TargetDivSelector = "#" + defaults.TargetDivID;

    $(OverlaySelector).appendTo('body');

    $("#" + OverlayClientID).css("top", "5%");

    $("#" + BodyClientID).html("");

    if (isIEorEdge()) {
        $(OverlaySelector).show();
    } else {
        $(OverlaySelector).fadeIn();
    }

    if ($.trim($('#dialog-overlay-' + OverlayClientID + '-body').html()) != "") {
        CloseDialogOverlay(OverlayClientID);
    }

    // U = URL
    // H - HTML
    // D - Divider

    var success = "";

    if (Callback !== undefined) {
        success += Callback;
    }

    // Block user interaction
    $('#' + BodyClientID).css({ 'min-height': '100px' });
    BlockUserInteraction(BodyClientID, "<div class='ui-loading large'></div>", 2, 0, 1);

    switch (defaults.LoadType) {
        case 'U':
            GetContent(defaults.LoadURL, BodyClientID, 3, success);
            break;
        case 'H':
            $("#" + BodyClientID).html(defaults.LoadContent);
            break;
        case 'D':
            $("#" + BodyClientID).html($(TargetDivSelector).html());
            break;

    };

    if (defaults.CloseCallback !== undefined) {
        $(OverlaySelector + ' .ui-dialog-overlay-close').attr('onclick', 'CloseDialogOverlay(\'' + OverlayClientID + '\',' + defaults.CloseCallback + ')')
    }

    // check for tabindex 
    if ($("#" + BodyClientID).find(":input[tabindex='1']:first").length > 0) {
        $("#" + BodyClientID).find(":input[tabindex='1']:first").focus();
    } else if ($("#" + BodyClientID).find(":input[type='text']:first").length > 0) {
        $("#" + BodyClientID).find(":input[type='text']:first").focus();
    } else {
        $("#" + BodyClientID).parent().focus();
    }

    $('#dialog-overlay-' + OverlayClientID + '-base').css({'overflow': 'auto', 'top': '0', 'width': '100%', 'left': '0' });
}

function GetIEVersion() {
    var sAgent = window.navigator.userAgent;
    var Idx = sAgent.indexOf("MSIE");

    if (Idx > 0) {
        // If IE, return version number
        return parseInt(sAgent.substring(Idx + 5, sAgent.indexOf(".", Idx)));
    } else if (!!navigator.userAgent.match(/Trident\/7\./)) {
        // If IE 11 then look for Updated user agent string
        return 11;
    } else {
        //It is not IE
        return 0;
    }
}

function isIEorEdge() {
    var agent = window.navigator.userAgent;

    var ie = agent.indexOf('MSIE ');
    if (ie > 0) {
        // IE <= 10
        return parseInt(agent.substring(ie + 5, uaagentindexOf('.', ie)), 10);
    }

    var gum = agent.indexOf('Trident/');
    if (gum > 0) {
        // IE 11
        var camper = agent.indexOf('rv:');
        return parseInt(agent.substring(camper + 3, agent.indexOf('.', camper)), 10);
    }

    var linkinPark = agent.indexOf('Edge/');
    if (linkinPark > 0) {
        return parseInt(agent.substring(linkinPark + 5, agent.indexOf('.', linkinPark)), 10);
    }

    // other browser
    return false;
}

function SendEmail(to, from, subject, body, callback) {

    var data = "{ToEmailAddress: '" + to + "', " +
        "FromEmailAddress: '" + from + "', " +
        "Subject: '" + subject + "', " +
        "Body: '" + body + "'}";
    var url = homeURL + "/GlobalUserControls/SE/SEController.aspx/SE";

    $.ajax({
        type: "POST",
        url: url,
        data: data,
        async: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (msg) {
            callback(msg.d);
        },
        headers: { 'X-Csrf-Token': GetCookie('CSRFToken') }
    });
}

// DETERMINE TEXT COLOR 

function rgbstringToTriplet(rgbstring) {
    var commadelim = rgbstring.substring(4, rgbstring.length - 1);
    var strings = commadelim.split(",");
    var numeric = [];

    for (var i = 0; i < 3; i++) {
        numeric[i] = parseInt(strings[i]);
    }

    return numeric;
}

function adjustColour(someelement) {
    var rgbstring = someelement.css('background-color');
    var triplet = [];
    var newtriplet = [];

    if (rgbstring != 'transparent') {
        if (/rgba\(0, 0, 0, 0\)/.exec(rgbstring)) {
            triplet = [255, 255, 255];
        } else {
            if (rgbstring.substring(0, 1).toLowerCase() != 'r') {
                CheckScript('RGBColor', staticURL + '/GlobalAssets/Scripts/ThirdParty/rgbcolor.js');
                // not rgb, convert it
                var color = new RGBColor(rgbstring);
                rgbstring = color.toRGB();
            }
            triplet = rgbstringToTriplet(rgbstring);
        }
    } else {
        triplet = [255, 255, 255];
    }

    // black or white:
    var total = 0; for (var i = 0; i < triplet.length; i++) { total += triplet[i]; }

    if (total > (3 * 256 / 2)) {
        newtriplet = [0, 0, 0];
    } else {
        newtriplet = [255, 255, 255];
    }

    var newstring = "rgb(" + newtriplet.join(",") + ")";

    someelement.css('color', newstring);
    someelement.find('*').css('color', newstring);

    return true;
}


// END DETERMINE TEXT color

function CheckScript2(ModuleName, ScriptSRC) {
    $.ajax({
        url: ScriptSRC,
        async: false,
        //context: document.body,
        success: function (html) {
            var script =
				document.createElement('script');
            document.getElementsByTagName('head')[0].appendChild(script);
            script.text = html;
        }
    });
}

// AREA / SCREEN CODES

function setCurrentScreenCode(screenCode) {
    SetCookie('currentScreenCode', screenCode);
    AddAnalyticsEvent(getCurrentAreaCode(), screenCode, 'Page View');
}

function getCurrentScreenCode() {
    var cookieValue = GetCookie('currentScreenCode');
    return (cookieValue != '' ? cookieValue : 0);
}

function setCurrentAreaCode(areaCode) {
    SetCookie('currentAreaCode', areaCode);
}

function getCurrentAreaCode() {
    var cookieValue = GetCookie('currentAreaCode');
    return (cookieValue != '' ? cookieValue : 0);
}

// END AREA / SCREEN CODES

// CLICK HOME TAB IN HEADER SECTION

function GoHome() {
    window.location.href = homeURL + "/cms/Workspace";
}

// END CLICK HOME TAB IN HEADER SECTION

// HELP PANEL

function OpenHelpPanel() {
    var URLScreenCode = getCurrentScreenCode();
    
    if (URLScreenCode == "" || URLScreenCode == 0) {
        URLScreenCode = getCurrentAreaCode();
        if (URLScreenCode == 0) {
            URLScreenCode = "";
        }
    } 

    AddAnalyticsEvent("Help", "How Do I...?", URLScreenCode);

    //help site url stored in webconfig, passed in to BBHelpURL from GlobalJSVar and GlobalJS.cs
    var HelpURL = BBHelpURL + URLScreenCode;
    window.open(HelpURL,"_blank");
}

// END HELP PANEL

// COOKIES
function SetCookie(name, value, days, ms) {
    var expires = "";

    if (ms) {
        var date = new Date();

        date.setMilliseconds(date.getMilliseconds() + ms);
        expires = "; expires=" + date.toGMTString();
    } else if (days) {
        var date = new Date();

        date.setDate(date.getDate() + days);
        expires = "; expires=" + date.toGMTString();
    }

    document.cookie = name + "=" + escape(value) + expires + "; path=/";
}

function GetCookie(name) {
    var value = "";

    if (document.cookie.length > 0) {
        var start = document.cookie.indexOf(name + "=");

        if (start != -1) {
            start = start + name.length + 1;

            var end = document.cookie.indexOf(";", start);

            if (end == -1) end = document.cookie.length;
            value = unescape(document.cookie.substring(start, end));
        }
    }

    return value;
}

function DeleteCookie(name) {
    SetCookie(name, '', -1);
}

function SetUnescapedCookie(name, value, days, ms) {
    var expires = "";

    if (ms) {
        var date = new Date();

        date.setMilliseconds(date.getMilliseconds() + ms);
        expires = "; expires=" + date.toGMTString();
    } else if (days) {
        var date = new Date();

        date.setDate(date.getDate() + days);
        expires = "; expires=" + date.toGMTString();
    }

    document.cookie = name + "=" + value + expires + "; path=/";
}
// END COOKIES



// IFRAME FUNCTIONS
function BindResizeFrame(FrameID) {
    $(document).on('load', "#" + FrameID, function () {
        var bodyHeight = $("#" + FrameID).contents().height() + 40;
        $("#" + FrameID).attr("height", bodyHeight + "px");
    });
}

function AdjustLinkTarget(FrameID) {
    $(document).on('load', "#" + FrameID, function () {
        $("#" + FrameID).contents().find("a").attr("target", "_parent");
    });
}

function ReloadDocViewer(moduleInstanceID, retryCount) {
    var iframe = document.getElementById('doc-viewer-' + moduleInstanceID);

    //if document failed to load and retry count is less or equal to 3, reload document and check again
    if (iframe.contentWindow.frames.length == 0 && retryCount <= 3) {
        retryCount = retryCount + 1;

        //reload the iFrame
        document.getElementById('doc-viewer-' + moduleInstanceID).src += '';

        //Check if document loaded again in 7.5 seconds
        setTimeout(ReloadDocViewer, (1000 * retryCount), moduleInstanceID, retryCount);

    } else if (iframe.contentWindow.frames.length == 0 && retryCount > 3) {
        $('#doc-viewer-' + moduleInstanceID).css('background', '');
        iframe.src = "/Errors/ReloadPage.aspx";
        iframe.height = 200;
    }

    if (iframe.contentWindow.frames.length == 1) {
        $('#doc-viewer-' + moduleInstanceID).css('background', '');
    }
}
// END IFRAME FUNCTIONS


// SCROLL TOP

function ScrollTop() {
    $.scrollTo(0, { duration: 1000 });
}

// END SCROLL TOP

// ANALYTICS TRACKING

function AddAnalyticsEvent(category, action, label) {
    ga('BBTracker.send', 'event', category, action, label);
}

// END ANYLYTICS TRACKING


// BEGIN INCLUDE DOC READY SCRIPTS

function IncludeDocReadyScripts() {
    var arrScripts = [
		staticURL + '/GlobalAssets/Scripts/min/external-combined.min.js',
        staticURL + '/GlobalAssets/Scripts/Utilities_2560.js'

    ];

    var script = document.createElement('script');
    script.type = 'text/javascript';
    $.each(arrScripts, function () {script.src = this;$('head').append(script);;
        
    });

}
// END INCLUDE DOC READY SCRIPTS

//BEGIN ONSCREEN ALERT SCRIPTS
function OnScreenAlertDialogInit() {
    $("#onscreenalert-message-dialog").hide();
    $(".titleMessage").hide();
    $("#onscreenalert-ctrl-msglist").hide();
    $(".onscreenalert-ctrl-footer").hide();

    $(".icon-icon_alert_round_message").addClass("noBorder");
    $('.onscreenalert-ctrl-modal').addClass('MoveIt2 MoveIt1 Smaller');
    $('.onscreenalert-ctrl-modal').css({ "top": "88%" });
    $("#onscreenalert-message-dialog").show();
    $('#onscreenalert-message-dialog-icon').focus();
}

function OnScreenAlertGotItDialog(serverTime) {
    var alertsID = '';
    alertsID = GetCookie("Alerts");
    var arr = [];

    if (alertsID.length > 0) {
        arr = alertsID.split(',')
    }

    $("#onscreenalertdialoglist li").each(function () {
        arr.push($(this).attr('id'));
    });

    arr.sort();
    var newarr = $.unique(arr);

    SetUnescapedCookie("Alerts", newarr.join(','), 365);
    OnScreenAlertMaskShow(false);

    $('.onscreenalert-ctrl-modal').removeClass("notransition");
    $(".onscreenalert-ctrl-footer").slideUp(200);
    $("#onscreenalert-ctrl-msglist").slideUp(200);

    // check what browser/version we're on
    // if IE 9 or less, don't do CSS 3 transitions (not supported)
    var isIE = GetIEVersion();

    if (isIE == 0) {
        // not IE
        hideOnScreenAlertTransitions();
    } else {
        // IE
        if (isIE == 8 || isIE == 9) {
            hideOnScreenAlertNoTransitions();
        } else if (isIE >= 10) {
            hideOnScreenAlertTransitions();
        }
    }
}

function OnScreenAlertOpenDialog(SiteID) {
    $('#sw-important-message-tooltip').hide();
    var onscreenAlertCookie = GetCookie('Alerts');

    if (onscreenAlertCookie != '' && onscreenAlertCookie != undefined) {
        GetContent(homeURL + "/cms/Tools/OnScreenAlerts/UserControls/OnScreenAlertDialogListWrapper.aspx?OnScreenAlertCookie=" + onscreenAlertCookie + "&SiteID=" + SiteID, "onscreenalert-ctrl-cookielist", 2, "OnScreenAlertCookieSuccess();");
        $('.onscreenalert-ctrl-content').focus();
    }
}

function OnScreenAlertCookieSuccess() {
    OnScreenAlertMaskShow(true);

    $('.onscreenalert-ctrl-modal').removeClass("notransition");
    $('.onscreenalert-ctrl-modal').removeClass("Smaller");

    // check what browser/version we're on
    // if IE 9 or less, don't do CSS 3 transitions (not supported)
    var isIE = GetIEVersion();

    if (isIE == 0) {
        // not IE
        showOnScreenAlertTransitions();
    } else {
        // IE
        if (isIE == 8 || isIE == 9) {
            showOnScreenAlertNoTransitions();
        } else if (isIE >= 10) {
            showOnScreenAlertTransitions();
        }
    }

    $('.onscreenalert-ctrl-modal').addClass('MoveIt3');
    $('.onscreenalert-ctrl-modal').attr('style', '');

    // move div to top of viewport
    $('.onscreenalert-ctrl-modal').one('transitionend', function (e) {
        var top = $('.onscreenalert-ctrl-modal').offset().top;

        $('.onscreenalert-ctrl-modal').addClass("notransition").removeClass('MoveIt1').addClass('MoveIt4');

        $('.onscreenalert-ctrl-modal').removeClass('MoveIt2');
    });
}

function showOnScreenAlertTransitions() {
    $('.onscreenalert-ctrl-modal').one('transitionend', function (e) {
        $(".icon-icon_alert_round_message").removeClass("noBorder");
        $(".titleMessage").show();
        $("#onscreenalert-ctrl-msglist").slideDown(200);
        $(".onscreenalert-ctrl-footer").slideDown(200);
    });
}

function showOnScreenAlertNoTransitions() {
    $(".icon-icon_alert_round_message").removeClass("noBorder");
    $(".titleMessage").show();
    $("#onscreenalert-ctrl-msglist").slideDown(200);
    $(".onscreenalert-ctrl-footer").slideDown(200);
}

function hideOnScreenAlertTransitions() {
    $(".titleMessage").fadeOut(200, function () {
        $('.onscreenalert-ctrl-modal').addClass("notransition").removeClass('MoveIt4').addClass('MoveIt1');
        $('.onscreenalert-ctrl-modal').attr('style', '');
        $('.onscreenalert-ctrl-modal').addClass("Smaller");
        $(".icon-icon_alert_round_message").addClass("noBorder");

        $('.onscreenalert-ctrl-modal').one('transitionend', function (e) {
            $('.onscreenalert-ctrl-modal').removeClass("notransition");
            $('.onscreenalert-ctrl-modal').removeClass('MoveIt3').addClass('MoveIt2');
            $('.onscreenalert-ctrl-modal').css({
                "top": "88%"
            });
        });
    });
}

function hideOnScreenAlertNoTransitions() {
    $(".titleMessage").fadeOut(200, function () {
        $('.onscreenalert-ctrl-modal').addClass("notransition").removeClass('MoveIt4').addClass('MoveIt1');
        $('.onscreenalert-ctrl-modal').attr('style', '');
        $('.onscreenalert-ctrl-modal').addClass("Smaller");
        $(".icon-icon_alert_round_message").addClass("noBorder");

        $('.onscreenalert-ctrl-modal').removeClass("notransition");
        $('.onscreenalert-ctrl-modal').removeClass('MoveIt3').addClass('MoveIt2');
        $('.onscreenalert-ctrl-modal').css({
            "top": "88%"
        });
    });
}

function OnScreenAlertCheckListItem() {
    var ShowOkGotIt = $("#onscreenalertdialoglist-hid-showokgotit").val();
    var ShowStickyBar = $("#onscreenalertdialoglist-hid-showstickybar").val();

    if (ShowOkGotIt == "True" && ShowStickyBar == "False") {
        OnScreenAlertShowCtrls(true, true, false);
    } else if (ShowOkGotIt == "False" && ShowStickyBar == "True") {
        OnScreenAlertShowCtrls(true, false, true);
    } else {
        OnScreenAlertShowCtrls(false, false, false);
    }
}

function OnScreenAlertShowCtrls(boolOkGotIt, boolMask, boolStickyBar) {
    if (boolOkGotIt == false) {
        $("#onscreenalert-message-dialog").hide()
    }

    (boolMask == true) ? (OnScreenAlertMaskShow(true)) : (OnScreenAlertMaskShow(false));

    if (boolStickyBar == true) {
        OnScreenAlertDialogInit();
    }
    else {
        $('.onscreenalert-ctrl-content').focus();
    }
    
}

function OnScreenAlertMaskShow(boolShow) {
    if (boolShow == true) {
        $("#onscreenalert-ctrl-mask").css({ "position": "absolute", "background": "#000", "filter": "alpha(opacity=70)", "-moz-opacity": "0.7", "-khtml-opacity": "0.7", "opacity": "0.7", "z-index": "9991", "top": "0", "left": "0" });
        $("#onscreenalert-ctrl-mask").css({ "height": function () { return $(document).height(); } });
        $("#onscreenalert-ctrl-mask").css({ "width": function () { return $(document).width(); } });
        $("#onscreenalert-ctrl-mask").show();
        $('body').css('overflow', 'hidden');
    } else {
        $("#onscreenalert-ctrl-mask").hide();
        $('body').css('overflow', 'scroll');
    }
}
// END ONSCREEN ALERT SCRIPTS

// Encoding - obfuscation
function swrot13(s) {
    return s.replace(/[a-zA-Z]/g, function(c) {return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);})
}

// START SIDEBAR TOUR SCRIPTS
/* 
 *  Wrapping sidebar tour in a function 
 *  to be called when needed.
 */

var hasPasskeys = false;
var hasStudents = false;
var isParentLinkStudent = false;
var hasNotifications = false;

function startSidebarTour() {
    // define tour
    var tour = new Shepherd.Tour({
        defaults: {
            classes: 'shepherd-theme-default'
        }
    });

    if ($('#dashboard-sidebar-student0').length > 0) {
        hasStudents = true;
    }

    if ($('#dashboard-sidebar-profile').length > 0) {
        isParentLinkStudent = true;
    }

    if ($("#dashboard-sidebar-passkeys").get(0)) {
        hasPasskeys = true;
    }

    if ($("#dashboard-sidebar-notification").get(0)) {
        hasNotifications = true;
    }


    // define steps

    tour.addStep('Intro', {
        //title: '',
        text: '<div id="tour-lightbulb-icon"></div><p>Introducing your new personalized dashboard. Would you like to take a quick tour?</p>',
        attachTo: 'body',
        classes: 'dashboard-sidebar-tour-firststep shepherd-theme-default',
        when: {
            show: function () {
                AddAnalyticsEvent('Dashboard', 'Tour', 'View');
            }
        },
        buttons: [
          {
              text: 'Yes! Let\'s go.',
              action: function () {
                  AddAnalyticsEvent('Dashboard', 'Tour', 'Continue');
                  tour.next();
                  TourADAFocus();
              },
              events: {
                  'keydown': function (e) {
                      TourButtonPress(e, tour);
                  }
              }
          },
          {
              text: 'Maybe later.',
              classes: 'shepherd-button-sec',
              action: function () {
                  AddAnalyticsEvent('Dashboard', 'Tour', 'Hide For Now');
                  tour.show('Later');
                  TourADAFocus();
              },
              events: {
                  'keydown': function (e) {
                      TourButtonPress(e, tour, 'Later');
                  }
              }
          },
          {
              text: 'No thanks. I\'ll explore on my own.',
              classes: 'shepherd-button-suboption',
              action: function () {
                  AddAnalyticsEvent('Dashboard', 'Tour', 'Hide Forever');
                  tour.show('Never');
                  TourADAFocus();
              },
              events: {
                  'keydown': function (e) {
                      TourButtonPress(e, tour, 'Never');
                  }
              }
          }
        ]
    }).addStep('Avatar', {
        text: 'Click on your user avatar to access and update your personal information and subscriptions.',
        attachTo: '#dashboard-sidebar-avatar-container right',
        when: {
            show: function() {
                $('h3.shepherd-title').attr('role', 'none'); //ADA compliance
            }
        },
        classes: 'shepherd-theme-default shepherd-element-custom',
        buttons: [
          {
              text: 'Continue',
              action: function () {
                tour.next();
                TourADAFocus();

              },
              events: {
                  'keydown': function (e) {
                      if (hasPasskeys) {
                          TourButtonPress(e, tour);
                      } else {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
          }
        ]
    }).addStep('Stream', {
        text: 'Go to your stream to see updates from your district and schools.',
        attachTo: '#dashboard-sidebar-stream right',
        buttons: [
          {
              text: 'Continue',
              action: function () {
                  if (hasPasskeys) {
                      tour.next();
                      TourADAFocus();
                  } else {
                      tour.show('Finish');
                      TourADAFocus();
                  }
              },
              events: {
                  'keydown': function (e) {
                      if(hasPasskeys){
                          TourButtonPress(e, tour);
                      } else {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
          }
        ]
    });

    if (hasPasskeys) {
        tour.addStep('Passkeys', {
            text: 'Use your passkeys to log into other district applications or websites.',
            attachTo: '#dashboard-sidebar-passkeys right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      if (hasNotifications) {
                          tour.show('Notifications');
                          TourADAFocus();
                      }
                      else if (hasStudents) {
                          tour.show('Students');
                          TourADAFocus();
                      }
                      else if (isParentLinkStudent) {
                          tour.show('ParentLinkStudent');
                          TourADAFocus();
                      }
                      else {
                          tour.show('Finish');
                          TourADAFocus();
                      }
                  },
                  events: {
                      'keydown': function (e) {
                          if (hasStudents) {
                              TourButtonPress(e, tour, 'Notifications');
                          }
                          else {
                              TourButtonPress(e, tour, 'Finish');
                          }
                      }
                  }
              }
            ]
        })
    }

    if (hasNotifications) {
        tour.addStep('Notifications', {
            text: 'Open your notifications to review messages.',
            attachTo: '#dashboard-sidebar-notification right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      if (hasStudents) {
                          tour.show('Students');
                          TourADAFocus();
                      }
                      else if (isParentLinkStudent) {
                          tour.show('ParentLinkStudent');
                          TourADAFocus();
                      }
                      else {
                          tour.show('Finish');
                          TourADAFocus();
                      }
                  },
                  events: {
                      'keydown': function (e) {
                          if (hasStudents) {
                              TourButtonPress(e, tour, 'Students');
                          }
                          else {
                              TourButtonPress(e, tour, 'Finish');
                          }
                      }
                  }
              }
            ]
        })
    }

    if (hasStudents) {
        tour.addStep('Students', {
            text: 'Select a student to view his or her information and records.',
            attachTo: '#dashboard-sidebar-student0 right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      tour.show('Finish');
                      TourADAFocus();
                  },
                  events: {
                      'keydown': function (e) {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
            ]
        })
    }

    if (isParentLinkStudent) {
        tour.addStep('ParentLinkStudent', {
            text: 'View your information or other helpful resources.',
            attachTo: '#dashboard-sidebar-profile right',
            buttons: [
              {
                  text: 'Continue',
                  action: function () {
                      tour.show('Finish');
                      TourADAFocus();
                  },
                  events: {
                      'keydown': function (e) {
                          TourButtonPress(e, tour, 'Finish');
                      }
                  }
              }
            ]
        })
    }

    tour.addStep('Later', {
        text: 'Ok, we\'ll remind you later. You can access the tour here any time you want.',
        attachTo: '#dashboard-sidebar-help right',
        classes: 'shepherd-theme-default shepherd-element-custom-bottom',
        buttons: [
          {
              text: 'Finish',
              action: function () {
                  SetCookie("DashboardSidebarTour", "true");
                  tour.cancel();
              },
              events: {
                  'keydown': function (e) {
                      e.stopImmediatePropagation();
                      if (e.keyCode == 13) {
                          SetCookie("DashboardSidebarTour", "true");
                          tour.cancel();
                      }
                  }
              }
          }
        ]

    }).addStep('Never', {
        text: 'Ok, we won\'t ask you again. If you change your mind, you can access the tour here any time you want.',
        attachTo: '#dashboard-sidebar-help right',
        classes: 'shepherd-theme-default shepherd-element-custom-bottom',
        buttons: [
          {
              text: 'Finish',
              action: function () {
                  SetCookie("DashboardSidebarTour", "true");
                  DenyTour();
              },
              events: {
                  'keydown': function (e) {
                      e.stopImmediatePropagation();
                      if (e.keyCode == 13) {
                          SetCookie("DashboardSidebarTour", "true");
                          DenyTour();
                      }
                  }
              }
          }
        ]

    }).addStep('Finish', {
        text: 'All finished! Go here any time to view this tour again.',
        attachTo: '#dashboard-sidebar-help right',
        classes: 'shepherd-theme-default shepherd-element-custom-bottom',
        buttons: [
          {
              text: 'Finish',
              action: function () {
                  SetCookie("DashboardSidebarTour", "true");
                  FinishTour();
              },
              events: {
                  'keydown': function (e) {
                      e.stopImmediatePropagation();
                      if (e.keyCode == 13) {
                          SetCookie("DashboardSidebarTour", "true");
                          FinishTour();
                      }
                  }
              }
          }
        ]
    });

    // start tour
    tour.start();
    TourADA();
}

function TourADA() {
    $(".shepherd-content").attr("tabindex", "0");
    $(".shepherd-button").each(function () {
        var $this = $(this);
        $this.attr("title", $this.text());
        $this.attr("tabindex", "0");
    });
}

function TourADAFocus() {
    TourADA();
    $(".shepherd-content").focus();
}

function TourButtonPress(e, tour, next) {
    e.stopImmediatePropagation();
    if (e.keyCode == 13) {
        if (next == undefined) {
            tour.next();
        } else {
            tour.show(next);
        }
        TourADAFocus();
    }
}

function FinishTour() {
    var data = "";
    var success = 'ActiveTourCancel();';
    var failure = 'CallControllerFailure(result[0].errormessage);';
    CallController(homeURL + "/GlobalUserControls/DashBoardSideBar/DashBoardSideBarController.aspx/FinishTour", data, success, failure);
}

function DenyTour() {
    var data = "";
    var success = 'ActiveTourCancel();';
    var failure = 'CallControllerFailure(result[0].errormessage);';
    CallController(homeURL + "/GlobalUserControls/DashBoardSideBar/DashBoardSideBarController.aspx/DenyTour", data, success, failure);
}

function ActiveTourCancel() {
    Shepherd.activeTour.cancel();
}
// END SIDEBAR TOUR SCRIPTS

// BEGIN DOCUMENT READY
$(document).ready(function () {

    if ($('#sw-sidebar').height() > 1500) {
        $('#sw-page').css('min-height', $('#sw-sidebar').height() + 'px');
        $('#sw-inner').css('min-height', $('#sw-sidebar').height() + 'px');
    } else {
        $('#sw-page').css('min-height', $(document).height() + 'px');
        $('#sw-inner').css('min-height', $(document).height() + 'px');
    }

    $('#sw-footer').show();

    // add focus class to textboxes for IE
    $(document).on('focus', 'input', function () {
        $(this).addClass('focus');
    });

    $(document).on('blur', 'input', function () {
        $(this).removeClass('focus');
    });

    // default ajax setup
    $.ajaxSetup({
        cache: false,
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //swalert(XMLHttpRequest.status + ' ' + textStatus + ': ' + errorThrown, 'Ajax Error', 'critical', 'ok');
            swalert("Something went wrong. We're sorry this happened. Please refresh your page and try again.", "Error", "critical", "ok");
        }
    });

    // make :Contains (case-insensitive version of :contains)
    jQuery.expr[':'].Contains = function (a, i, m) {
        return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
    };

    // HIDE / SHOW DETAILS IN LIST SCREEN
    $(document).on('click', 'span.ui-show-detail', function () {
        var $this = $(this);

        if ($this.hasClass('open')) {
            // do nothing
        } else {
            $this.addClass('open').parent('div.ui-article-header').nextAll('div.ui-article-detail').slideDown(function () {
                // add close button
                $this.append("<span class='ui-article-detail-close'></span>");
            });
        }
    });

    $(document).on('click', 'span.ui-article-detail-close', function () {
        $this = $(this);
        $this.parents('.ui-article-header').nextAll('.ui-article-detail').slideUp(function () {
            $this.parent('.ui-show-detail').removeClass('open');
            // remove close button
            $this.remove();
        });
    });


    // LIST/EXPANDED VIEW ON LIST PAGES
    $(document).on('click', '#show-list-view', function () {
        $(this).addClass('ui-btn-toolbar-primary').removeClass('ui-btn-toolbar');
        $('#show-expanded-view').addClass('ui-btn-toolbar').removeClass('ui-btn-toolbar-primary');
        $('div.ui-article-detail').slideUp(function () {
            // remove close buttons
            $(this).prevAll('.ui-article-header').find('.ui-article-detail-close').remove();
            //$this.children('.ui-article-detail-close').remove();
        });
        $('span.ui-show-detail').removeClass('open');
    });

    $(document).on('click', '#show-expanded-view', function (i) {
        $(this).addClass('ui-btn-toolbar-primary').removeClass('ui-btn-toolbar');
        $('#show-list-view').addClass('ui-btn-toolbar').removeClass('ui-btn-toolbar-primary');
        $('div.ui-article-detail').slideDown('', function () {
            // add close buttons
            $(this).prevAll('.ui-article-header').children('.ui-show-detail').append("<span class='ui-article-detail-close'></span>");
        });
        $('span.ui-show-detail').addClass('open');
    });

    //Important Message Ok, got it tab=0
    $('#onscreenalert-ctrl-gotit').keydown(function (e) {
        e.stopImmediatePropagation();
        if (e.keyCode == 13) {
            $(this).click();
        }
    });

    $('#onscreenalert-message-dialog-icon').keydown(function (e) {
        e.stopImmediatePropagation();
        if (e.keyCode == 13) {
            $(this).click();
        }
    });
    
}); // end document ready

// load scripts after everything else
$(window).on('load',  IncludeDocReadyScripts);
﻿/// <reference path="../../../../Scripts/Slick/slick.min.js" />
/// <reference path="../../../../Scripts/Slick/slick.min.js" />
/// <reference path="../../../../Scripts/Slick/slick.min.js" />
/// <reference path="../../../../Scripts/Slick/slick.min.js" />
// Check for included script
function CheckScript(ModuleName, ScriptSRC, FunctionName) {

    var loadScriptFile = true;

    switch (ModuleName.toLowerCase()) {
        case 'sectionrobot':
            FunctionName = 'CheckSectionRobotScript';
            ScriptSRC = homeURL + '/cms/Tools/SectionRobot/SectionRobot.js';
            break;
        case 'assignments':
            FunctionName = 'CheckAssignmentsScript';
            ScriptSRC = homeURL + '/cms/Module/Assignments/Assignments.js';
            break;
        case 'spacedirectory':
            FunctionName = 'CheckSpaceDirectoryScript';
            ScriptSRC = homeURL + '/cms/Module/SpaceDirectory/SpaceDirectory.js';
            break;
        case 'calendar':
            FunctionName = 'CheckCalendarScript';
            ScriptSRC = homeURL + '/cms/Module/Calendar/Calendar.js';
            break;
        case 'fullcalendar':
            FunctionName = '$.fn.fullCalendar';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/jquery.fullcalendar1.6.1.js';
            break;
        case 'links':
            FunctionName = 'CheckLinksScript';
            ScriptSRC = staticURL + '/cms/Module/Links/Links.js';
            break;
        case 'minibase':
            FunctionName = 'CheckMinibaseScript';
            ScriptSRC = homeURL + '/cms/Module/Minibase/Minibase.js';
            break;
        case 'moduleinstance':
            var randNum = Math.floor(Math.random() * (1000 - 10 + 1) + 1000);
            FunctionName = 'CheckModuleInstanceScript';
            ScriptSRC = homeURL + '/cms/Module/ModuleInstance/ModuleInstance.js?rand=' + randNum;
            break;
        case 'photogallery':
            FunctionName = 'CheckPhotoGalleryScript';
            ScriptSRC = staticURL + '/cms/Module/PhotoGallery/PhotoGallery_2520.js';
            break;
        case 'comments':
            FunctionName = 'CheckModerateCommentsScript';
            ScriptSRC = homeURL + '/cms/Tools/ModerateComments/ModerateComments.js';
            break;
        case 'postings':
            FunctionName = 'CheckModeratePostingsScript';
            ScriptSRC = homeURL + '/cms/Tools/ModerateContribution/ModerateContribution.js';
            break;
        case 'myaccount':
            FunctionName = 'CheckMyAccountScript';
            ScriptSRC = homeURL + '/cms/UserControls/MyAccount/MyAccount.js';
            break;
        case 'formsurvey':
            FunctionName = 'CheckFormSurveyScript';
            ScriptSRC = homeURL + '/cms/tools/FormsAndSurveys/Surveys.js';
            break;
        case 'alerts':
            FunctionName = 'CheckAlertsScript';
            ScriptSRC = homeURL + '/cms/tools/Alerts/Alerts.js';
            break;
        case 'onscreenalerts':
            FunctionName = 'CheckOnScreenalertsScript';
            ScriptSRC = homeURL + '/cms/tools/OnScreenAlerts/OnScreenAlerts.js';
            break;
        case 'workspace':
            FunctionName = 'CheckWorkspaceScript';
            ScriptSRC = staticURL + '/cms/Workspace/PageList_2680.js';
            break;
        case 'moduleview':
            FunctionName = 'CheckModuleViewScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/ModuleViewRenderer_2640.js';
            break;
        case 'pageeditingmoduleview':
            FunctionName = 'CheckPageEditingModuleViewScript';
            //ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/PageEditingModuleViewRenderer.js';
            ScriptSRC = homeURL + '/cms/UserControls/ModuleView/PageEditingModuleViewRenderer_2640.js';
            break;
        case 'editarea':
            FunctionName = 'CheckEditAreaScript';
            ScriptSRC = homeURL + '/GlobalUserControls/EditArea/edit_area_full.js';
            break;
        case 'rating':
            FunctionName = '$.fn.rating';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/jquery.rating.min.js';
            break;
        case 'metadata':
            FunctionName = '$.fn.metadata';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/min/jquery.metadata.min.js';
            break;
        case 'pwcalendar':
            FunctionName = 'CheckPWCalendarScript';
            ScriptSRC = homeURL + '/myview/UserControls/Calendar/Calendar.js';
            break;
        case 'importwizard':
            FunctionName = 'CheckImportWizardScript';
            ScriptSRC = homeURL + '/cms/UserControls/ImportDialog/ImportWizard.js';
            break;
        case 'mustache':
            FunctionName = 'CheckMustacheScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/mustache.js';
            break;
        case 'slick':
            FunctionName = 'CheckSlickScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/Slick/slick.min.js';
            break;
        case 'galleria':
            FunctionName = 'CheckGalleriaScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/galleria-custom-129_2520/galleria-1.2.9.min.js';
            break;
        case 'fine-uploader':
            FunctionName = 'CheckFineUploaderScript';
            ScriptSRC = staticURL + '/GlobalAssets/Scripts/ThirdParty/fine-uploader/fine-uploader.min.js';
            break;
        case 'tinymce':
            FunctionName = 'tinyMCE';
            ScriptSRC = homeURL + '/cms/Module/selectsurvey/ClientInclude/tinyMCE/jquery.tinymce.min.js';
            break;
        case 'attachmentview':
            FunctionName = 'CheckAttachmentScript';
            ScriptSRC = homeURL + '/GlobalUserControls/Attachment/AttachmentView.js';
            break;
        default:
            // module name not found
            if (ScriptSRC !== undefined && ScriptSRC !== null && ScriptSRC.length > 0) {
                // script src was specified in parameter
                if (FunctionName === undefined && ModuleName.length > 0) {
                    // default the function name for lookup from the module name
                    FunctionName = 'Check' + ModuleName + 'Script';
                }
            } else {
                // can't load a script file without at least a src and function name to test
                loadScriptFile = false;
            }
            break;
    }

    if (loadScriptFile === true) {
        try {
            if (eval("typeof " + FunctionName + " == 'function'")) {
                // do nothing, it's already included
            } else {
                var script = document.createElement('script');script.type = 'text/javascript';script.src = ScriptSRC;$('head').append(script);;
            }
        } catch (err) {
        }
    }
}
// End Check for included script
</script><!-- End swuc.CheckScript -->


    <!-- Server Load Time (01): 0.3917828 Seconds -->

    

    <!-- off-canvas menu enabled-->
    

    <!-- Ally Alternative Formats Configure START   -->
    
    <!-- Ally Alternative Formats Configure END     -->

</body>
</html>
