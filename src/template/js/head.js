$(window).on("load", function() { CreativeTemplate.WindowLoad(); });

$(function() {
    CreativeTemplate.Init();
});

    window.CreativeTemplate = {
        //PROPERTIES
        "KeyCodes": { 
            'tab': 'Tab', 
            'enter': 'Enter', 
            'esc': 'Escape', 
            'space': ' ', 
            'end': 'End', 
            'home': 'Home', 
            'left': 'ArrowLeft', 
            'up': 'ArrowUp', 
            'right': 'ArrowRight', 
            'down': 'ArrowDown' 
        },
        "IsMyViewPage": false,
        "ShowDistrtictHome": false,
        "ShowSchoolList": true,
        "ShowTranslate": true,
        "DefaultLogoSrc": "/src/template/assets/default.png",

        //METHODS
        "Init": function() {
            //FOR SCOPE
            var template = this;

            this.SetTemplateProps();
            this.MyStart();
            this.RegisterLinks();
            this.JsMediaQueries();
            this.Header();
            this.RsMenu();
            this.ChannelBar();
            if ($("#gb-page.hp").length) {
                this.Homepage();
                this.Magic();
                this.Headlines();
                this.Animations();
                this.MoveButtons();
            }
            this.Body();
            this.GlobalIcons();
            this.SocialIcons();
            this.MegaMenu();
            this.MMGPlugin();
            this.ModEvents();
            this.Search();
            this.Footer();
            
            //WINDOW RESIZE AND SCROLL METHOD
            $(window).resize(function() { template.WindowResize(); });
            $(window).scroll(function() { template.WindowScroll(); });
            
        },
        
        "WindowLoad": function() {

        },

        "WindowResize": function() {
            //RUN QUERIES ON RESIZE
            this.JsMediaQueries();
            this.MoveButtons();

        },

        "WindowScroll": function() {

        },

        "JsMediaQueries": function() {
            switch(this.GetBreakPoint()) {
                case "desktop":
                    
                break;
                case "768":

                break;
                case "640":


                break;
                case "480":


                break;
                case "320":

                break;
            }
        },
        
        "SetTemplateProps": function SetTemplateProps () {
            // FOR SCOPE
            var template = this;
            
            //MYVIEW PAGE CHECK
            if ($('#pw-body').length) this.IsMyViewPage = true;

            //SCHOOL LIST CHECK
            if($(".sw-mystart-dropdown.schoollist").length) this.ShowSchoolList = true;
        },

        "MyStart": function() {
            //FOR SCOPE
            var template = this;

            //BUILD USER OPTIONS DROPDOWN
            var userOptionsItems = "";

            [$IF LOGGED IN$]
                if ($(".sw-mystart-button.manage").length) {
                  $(".sw-mystart-button.manage a").attr("onclick", $(".sw-mystart-button.manage a").attr("onclick") + "return false;");
                  userOptionsItems += '<li id="user-options-manage"><a href="#" onclick="' + $(".sw-mystart-button.manage a").attr("onclick") + '"><span>Site Manager</span></a></li>';
                } // MY ACCOUNT BUTTONS


                $("#sw-myaccount-list > li:first-child a span").text("My Account");
                $("#sw-myaccount-list > li").each(function () {
                  userOptionsItems += "<li>" + $(this).html() + "</li>";
                }); // MY PASSKEYS BUTTON

                if ($("#sw-mystart-mypasskey").length) {
                  $("#sw-mystart-mypasskey").removeClass("sw-mystart-button").addClass("cs-mystart-dropdown passkeys");
                  $("#ui-btn-mypasskey").addClass("cs-dropdown-selector");
                  $("#sw-mystart-mypasskey").insertBefore(".cs-mystart-dropdown.user-options");
                }
            [$ELSE IF LOGGED$]

                // SIGNIN BUTTON
                if($(".sw-mystart-button.signin").length) {
                    userOptionsItems += "<li>" + $(".sw-mystart-button.signin").html() + "</li>";
                }

                // REGISTER BUTTON
                if($(".sw-mystart-button.register").length) {
                    userOptionsItems += "<li>" + $(".sw-mystart-button.register").html() + "</li>";
                }
            [$END IF LOGGED IN$]

            // ADD USER OPTIONS DROPDOWN TO THE DOM
            $(".cs-mystart-dropdown.user-options .cs-dropdown-list").html(userOptionsItems);

            //CLONE AND DUMP SCHOOLLIST INTO DOM
            $('.sw-mystart-dropdown.schoollist .sw-dropdown-list').clone().appendTo('#school-list');

            //MOVE SCHOOLS FROM OFFSCREEN
            $("#cs-schools").on('click', function(){
                //FOR SCOPE
                var subList = $(this).children('ul');
                const itemPosLeft = $(this).offset().left;
                const itemPosRight = itemPosLeft + subList.outerWidth();

                if( itemPosRight >= $(window).width() ){
                    $('#school-list', this)
                        .addClass('overflowing')
                        .css({
                            'left': -(itemPosRight - $(window).width())
                        });
                }else if(itemPosLeft < 0){
                    $('#school-list', this)
                        .addClass('overflowing')
                        .css({
                            'left': -(itemPosLeft)
                        });
                }else{
                    $('#school-list', this)
                        .removeClass('overflowing')
                        .css({
                            'left': ''
                        });
                }
            });

            //RUN TRANSLATE FUNCTION
            this.Translate();

            // BIND DROPDOWN EVENTS
            template.DropdownActions({ //USER OPTIONS
                "dropdownParent": ".cs-mystart-dropdown.user-options",
                "dropdownSelector": ".cs-dropdown-selector",
                "dropdown": ".cs-dropdown",
                "dropdownList": ".cs-dropdown-list"
            });
            template.DropdownActions({ //TRANSLATE
                "dropdownParent": "#gb-translate",
                "dropdownSelector": ".translate-selector",
                "dropdown": "#translate-dropdown",
            });
            template.DropdownActions({ //A&B DROPDOWN
                "dropdownParent": "#cs-ab",
                "dropdownSelector": ".mystart-selector",
                "dropdown": "#ab-dropdown"
            });
            template.DropdownActions({ //SCHOOLS DROPDOWN
                "dropdownParent": "#cs-schools",
                "dropdownSelector": ".mystart-selector",
                "dropdown": "#school-dropdown",
                "dropdownList": "#school-list"
            });
            template.DropdownActions({ //A&R DROPDOWN
                "dropdownParent": "#cs-ar",
                "dropdownSelector": ".mystart-selector",
                "dropdown": "#ar-dropdown",
                "dropdownList": "#ar-list"
            });
            template.DropdownActions({ //A&R MOBILE DROPDOWN
                "dropdownParent": "#cs-ar-mobile",
                "dropdownSelector": ".mystart-selector",
                "dropdown": "#ar-dropdown-mobile",
                "dropdownList": "#ar-list-mobile"
            });
        },

        "RegisterLinks": function(){
            //FOR SCOPE
            var template = this;

            //BUILD LINK LIST ARRAY
            var listItem = [
                [//LINK LIST ONE
                   '<SWCtrl controlname="Custom" props="Name:showarLink1" />', //LINK TOGGLE
                   '<SWCtrl controlname="Custom" props="Name:arLink1" />', //LINK NAME
                   '<SWCtrl controlname="Custom" props="Name:arLink1Url" />', //LINK URL
                   '<SWCtrl controlname="Custom" props="Name:arLink1Target" />' //LINK TARGET
                ],
                [//LINK LIST TWO
                   '<SWCtrl controlname="Custom" props="Name:showarLink2" />', //LINK TOGGLE
                   '<SWCtrl controlname="Custom" props="Name:arLink2" />', //LINK NAME
                   '<SWCtrl controlname="Custom" props="Name:arLink2Url" />', //LINK URL
                   '<SWCtrl controlname="Custom" props="Name:arLink2Target" />' //LINK TARGET
                ],
                [//LINK LIST THREE
                   '<SWCtrl controlname="Custom" props="Name:showarLink3" />', //LINK TOGGLE
                   '<SWCtrl controlname="Custom" props="Name:arLink3" />', //LINK NAME
                   '<SWCtrl controlname="Custom" props="Name:arLink3Url" />', //LINK URL
                   '<SWCtrl controlname="Custom" props="Name:arLink3Target" />' //LINK TARGET
                ],
                [//LINK LIST FOUR
                   '<SWCtrl controlname="Custom" props="Name:showarLink4" />', //LINK TOGGLE
                   '<SWCtrl controlname="Custom" props="Name:arLink4" />', //LINK NAME
                   '<SWCtrl controlname="Custom" props="Name:arLink4Url" />', //LINK URL
                   '<SWCtrl controlname="Custom" props="Name:arLink4Target" />' //LINK TARGET
                ],
                [//LINK LIST FIVE
                   '<SWCtrl controlname="Custom" props="Name:showarLink5" />', //LINK TOGGLE
                   '<SWCtrl controlname="Custom" props="Name:arLink5" />', //LINK NAME
                   '<SWCtrl controlname="Custom" props="Name:arLink5Url" />', //LINK URL
                   '<SWCtrl controlname="Custom" props="Name:arLink5Target" />' //LINK TARGET
                ],
                [//LINK LIST SIX
                   '<SWCtrl controlname="Custom" props="Name:showarLink6" />', //LINK TOGGLE
                   '<SWCtrl controlname="Custom" props="Name:arLink6" />', //LINK NAME
                   '<SWCtrl controlname="Custom" props="Name:arLink6Url" />', //LINK URL
                   '<SWCtrl controlname="Custom" props="Name:arLink6Target" />' //LINK TARGET
                ],
                [//LINK LIST SEVEN
                   '<SWCtrl controlname="Custom" props="Name:showarLink7" />', //LINK TOGGLE
                   '<SWCtrl controlname="Custom" props="Name:arLink7" />', //LINK NAME
                   '<SWCtrl controlname="Custom" props="Name:arLink7Url" />', //LINK URL
                   '<SWCtrl controlname="Custom" props="Name:arLink7Target" />' //LINK TARGET
                ],
                [//LINK LIST EIGHT
                   '<SWCtrl controlname="Custom" props="Name:showarLink8" />', //LINK TOGGLE
                   '<SWCtrl controlname="Custom" props="Name:arLink8" />', //LINK NAME
                   '<SWCtrl controlname="Custom" props="Name:arLink8Url" />', //LINK URL
                   '<SWCtrl controlname="Custom" props="Name:arLink8Target" />' //LINK TARGET
                ]
            ]

            //BUILD LINKS BY LOOPING EACH ITEM IN ARRAY
            var linkHTML = '';

            $.each(listItem, function(index){

                //BUILD LIST ITEMS
                linkHTML +=  '<li class="ar-list=item" data-toggle="'+ listItem[index][0] +'"><a class="ar-link" href="' + listItem[index][2] + '" target="'+ listItem[index][3] +'"  aria-label="'+ listItem[index][1] +'"><span>'+ listItem[index][1] +'</span></a></li>';

            });

            //PUSH HTML TO DOM
            $('#ar-list').html(linkHTML);
            $('#ar-list-mobile').html(linkHTML);
        },

        "DropdownActions": function(params) {
            // FOR SCOPE
            var template = this;

            var dropdownParent = params.dropdownParent;
            var dropdownSelector = params.dropdownSelector;
            var dropdown = params.dropdown;
            var dropdownList = params.dropdownList;

            $(dropdownParent + " " + dropdownList + " a").attr("tabindex", "-1");

            // MYSTART DROPDOWN SELECTOR CLICK EVENT
            $(dropdownParent).on("click", dropdownSelector, function(e) {
                e.preventDefault();

                if($(this).parent().hasClass("open")){
                    $("+ " + dropdownList + " a").attr("tabindex", "-1");
                    $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                } else {
                    $(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden","false").slideDown(300, "swing");
                }
            });

            // MYSTART DROPDOWN SELECTOR KEYDOWN EVENTS
            $(dropdownParent).on("keydown", dropdownSelector, function(e) {
                // CAPTURE KEY CODE
                switch(e.key) {
                    // CONSUME LEFT AND UP ARROWS
                    case template.KeyCodes.enter:
                    case template.KeyCodes.space:
                        e.preventDefault();

                        // IF THE DROPDOWN IS OPEN, CLOSE IT
                        if($(dropdownParent).hasClass("open")){
                            $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
                            $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                        } else {
                            $(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden", "false").slideDown(300, "swing", function(){
                                if($(dropdownParent).hasClass("translate")){
                                    $("#cs-branded-translate-dropdown").focus();
                                } else {
                                    $(dropdownList + " li:first-child a", this).attr("tabindex", "0").focus();
                                }
                            });
                        }
                    break;

                    // CONSUME TAB KEY
                    case template.KeyCodes.tab:
                        if($("+ " + dropdown + " " + dropdownList + " a").length) {
                            $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
                            $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                        }
                    break;

                    // CONSUME LEFT AND UP ARROWS
                    case template.KeyCodes.down:
                    case template.KeyCodes.right:
                        e.preventDefault();

                        $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
                        $("+ " + dropdown + " " + dropdownList + " li:first-child > a", this).attr("tabindex", "0").focus();
                    break;
                }
            });

            // MYSTART DROPDOWN LINK KEYDOWN EVENTS
            $(dropdownParent).on("keydown", dropdownList + " li a", function(e) {
                // CAPTURE KEY CODE
                switch(e.key) {
                    // CONSUME LEFT AND UP ARROWS
                    case template.KeyCodes.left:
                    case template.KeyCodes.up:
                        e.preventDefault();

                        // IS FIRST ITEM
                        if($(this).parent().is(":first-child")) {
                            // FOCUS DROPDOWN BUTTON
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).closest(dropdownParent).find(dropdownSelector).focus();
                        } else {
                            // FOCUS PREVIOUS ITEM
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).parent().prev("li").find("> a").attr("tabindex", "0").focus();
                        }
                    break;

                    // CONSUME RIGHT AND DOWN ARROWS
                    case template.KeyCodes.right:
                    case template.KeyCodes.down:
                        e.preventDefault();

                        // IS LAST ITEM
                        if($(this).parent().is(":last-child")) {
                            // FOCUS FIRST ITEM
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).closest(dropdownList).find("li:first-child > a").attr("tabindex", "0").focus();
                        } else {
                            // FOCUS NEXT ITEM
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).parent().next("li").find("> a").attr("tabindex", "0").focus();
                        }
                    break;

                    // CONSUME TAB KEY
                    case template.KeyCodes.tab:
                        if(e.shiftKey) {
                            e.preventDefault();

                            // FOCUS DROPDOWN BUTTON
                            $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                            $(this).closest(dropdownParent).find(dropdownSelector).focus();
                        }
                    break;

                    // CONSUME HOME KEY
                    case template.KeyCodes.home:
                        e.preventDefault();

                        // FOCUS FIRST ITEM
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(this).closest(dropdownList).find("li:first-child > a").attr("tabindex", "0").focus();
                    break;

                    // CONSUME END KEY
                    case template.KeyCodes.end:
                        e.preventDefault();

                        // FOCUS LAST ITEM
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(this).closest(dropdownList).find("li:last-child > a").attr("tabindex", "0").focus();
                    break;

                    // CONSUME ESC KEY
                    case template.KeyCodes.esc:
                        e.preventDefault();

                        // FOCUS DROPDOWN BUTTON AND CLOSE DROPDOWN
                        $(this).closest(dropdownParent).find(dropdownSelector).focus();
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(dropdownSelector).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                    break;
                }
            });

            $(dropdownParent).mouseleave(function() {
                $(dropdownList + " a", this).attr("tabindex", "-1");
                $(dropdownSelector, this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
            }).focusout(function() {
                var thisDropdown = this;

                setTimeout(function () {
                    if(!$(thisDropdown).find(":focus").length) {
                        $(dropdownSelector, thisDropdown).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                    }
                }, 500);
            });           

        },

        "CheckElementPosition": function(element){ //CALL FOR SCROLL ANIMATIONS
            var template = this;

            //CHECK IF ELEMENT IS IN VIEWPORT
            var elementTop = $(element).offset().top;
            var elementLeft = $(element).offset().left;
            var elementBottom = elementTop + $(element).outerHeight();
            var viewportTop = $(window).scrollTop();
            var viewportBottom = viewportTop + $(window).height();
            var viewportWidth = $(window).width();

            return elementBottom > viewportTop && elementTop < viewportBottom && elementLeft < viewportWidth;

        },

        "ChannelBar": function() {
            $(".sw-channel-item").off('hover');
            $(".sw-channel-item").hover(function(){
                
                $("ul", this).stop(true, true);
                var subList = $(this).children('ul');
                if ($.trim(subList.html()) !== "") {
                    subList.slideDown(300, "swing");
                }
                $(this).addClass("hover");
                const itemPosLeft = $(this).offset().left;
                const itemPosRight = itemPosLeft + subList.outerWidth();

                if( itemPosRight >= $(window).width() ){
                    $('.sw-channel-dropdown', this)
                        .addClass('overflowing')
                        .css({
                            'left': -(itemPosRight - $(window).width())
                        });
                }else if(itemPosLeft < 0){
                    $('.sw-channel-dropdown', this)
                        .addClass('overflowing')
                        .css({
                            'left': -(itemPosLeft)
                        });
                }else{
                    $('.sw-channel-dropdown', this)
                        .removeClass('overflowing')
                        .css({
                            'left': ''
                        });
                }
            }, function(){
                $(".sw-channel-dropdown").slideUp(300, "swing");
                $(this).removeClass("hover");
            });

        },
        
        "Header": function() {
            // FOR SCOPE
            var template = this;
            
            //ADD SITE URL
            const homeLink = '<a style="display:inline-block" href="[$SiteAlias$]">';
            
            //LOGO DIMENSIONS
            var imgWidth = 255;
            var imgHeight = 165;
            
            //ADD LOGO TO HEADER
            var logoSrc = $.trim('<SWCtrl controlname="Custom" props="Name:schoolLogo" />');
            var srcSplit = logoSrc.split("/");
            var srcSplitLen = srcSplit.length;
            
            //CHECK IF LOGO IS EMPTY
            if((logoSrc == '') || (srcSplit[srcSplitLen - 1] == 'default-man.jpg')) {
                logoSrc = this.DefaultLogoSrc;
            }
            
            //ADD CUSTOM SCHOOL LOGO
            $('#gb-logo').append(homeLink + '<img width="' + imgWidth + '" height="' + imgHeight + '" src="' + logoSrc + '" alt="[$SiteName$] Logo"></a>');
            //WRAP IN H1
            $('#gb-logo > a').wrap('<h1 />');

            //ADD TRANSLATE HOVER STATE FOR ICON
            $('.translate-selector').hover(function(){
                $('.translate-icon').addClass('hover');
            }, function(){
                $('.translate-icon').removeClass("hover");
            }); 
        },

        "Homepage": function() {
            //FOR SCOPE
            var template = this;
            
            //LOOP EACH APP ON H1
            $(".app").each(function(){
                //FOR SCOPE
                var thisApp = $(this);

                //CHECK FOR H1
                if ($(thisApp).find(".ui-widget-header h1").length){
                    //IF H1 EXISTS ADD CLASS
                    ($(thisApp).find(".ui-widget-header").addClass("icon"));
                } else {
                    //IF H1 DOESN'T EXIST REMOVE CLASS
                    ($(thisApp).find(".ui-widget-header").removeClass("icon"));
                }
            });
            //LOOP EACH ARTICLES IN LIGHTBOX
            $(".app.lightbox .ui-articles").each(function(props){
                //FOR SCOPE
                var thisApp = $(this);

                //CHECK FOR H1
                if ($(thisApp).find(".ui-article img").length){
                    ($(thisApp).find(".ui-article img", props.element).wrap("<span class='light-box-img'></span>"));
                    //IF H1 EXISTS ADD CLASS
                    $('.ui-article').hover(function(){
                        ($(thisApp).find(".ui-article").addClass("hover"));
                    },  function(){
                        ($(thisApp).find(".ui-article").removeClass("hover"));
                    });
                }
            });

            //ADD BUTTON IN REGION E
            var buttonToggle    = '<SWCtrl controlname="Custom" props="Name:regionEbtn" />';
            var buttonLink      = '<SWCtrl controlname="Custom" props="Name:regionEbtnLink" />';
            var buttonTarget    = '<SWCtrl controlname="Custom" props="Name:regionEbtnTarget" />';
            var buttonText      = '<SWCtrl controlname="Custom" props="Name:regionEbtnName" />';

            if ($('.hp-row.four .ui-widget.app').length){
                $(".hp-column.two").append("<div class='regionE-button' role='button' data-toggle='"+ buttonToggle +"'><a href='" + buttonLink + "' target='" + buttonTarget + "'><span>" + buttonText +"</span></a></div>");
            }
        },

        "Animations": function() {
            //FOR SCOPE
            var template = this;

            //SCROLL ANIMATIONS
            //VAR FOR HOW FAR HEADER LOGO NEED TO DISAPPEAR
            const checkpoint = 155;

            switch(this.GetBreakPoint()) {
                case "desktop":
                    
                    if($(".hp").length) {
                        //FIRE GB ICONS ANIMATIONS SINCE THEY ARE ALWAYS IN VIEW ON LOAD
                        $(document).ready(function() {
                            $("#gb-icons ").addClass("hello");
                        }); 
                        //ON SCROLL
                        $(window).scroll(function(){
                            //CHECK Y SCROLL DISTANCE
                            const currentScroll = window.pageYOffset;
                            if (currentScroll <= checkpoint) { //HAVE WE HIT CHECKPOINT
                                opacity = 1 - currentScroll / checkpoint;
                            } else { //HAVE WE GONE PAST CHECK POINT
                                opacity = 0;
                            }
                            $('#gb-logo').css('opacity', opacity); //PASS VARIABLE TO CSS

                            //CHECK FOR GB ICONS
                            if (!(template.CheckElementPosition($("#gb-icons")))) {
                                setTimeout(function(){
                                    $('#gb-icons-sticky').addClass('hello');
                                }, 100)
                            } else {
                                $('#gb-icons-sticky').removeClass('hello');
                            }
                        
                            //CHECK FOR HP ROW THREE
                            if (template.CheckElementPosition($('.hp-row.three'))) {
                                setTimeout(function(){
                                    $('.hp-row.three .ui-widget.app').each(function() {
                                        if(template.CheckElementPosition($(".ui-widget-detail", this)) && !$(this).hasClass("hello")) {
                                            $(this).addClass("hello");	
                                        }                        
                                    });
                                }, 300)
                            }
                            //CHECK FOR HP ROW FOUR
                            if (template.CheckElementPosition($('.hp-row.four'))) {
                                setTimeout(function(){
                                    $('.hp-row.four .ui-widget.app').each(function() {
                                        if(template.CheckElementPosition($(".ui-widget-detail", this)) && !$(this).hasClass("hello")) {
                                            $(this).addClass("hello");	
                                        }                        
                                    });
                                }, 300)
                            }
                            //CHECK FOR HP ROW FIVE
                            if (template.CheckElementPosition($('.hp-row.five'))) {
                                setTimeout(function(){
                                    $('.hp-row.five .ui-widget.app').each(function() {
                                        if(template.CheckElementPosition($(".ui-widget-detail", this)) && !$(this).hasClass("hello")) {
                                            $(this).addClass("hello");	
                                        }                        
                                    });
                                }, 300)
                            }
                            //CHECK FOR HP ROW SIX
                            if (template.CheckElementPosition($('.hp-row.six'))) {
                                setTimeout(function(){
                                    if(template.CheckElementPosition($(".mg-header-icon-1024")) && !$('.mg-header-icon-1024').hasClass("hello")) {
                                        $('.mg-header-icon-1024').addClass("hello");	
                                    }         
                                    if(template.CheckElementPosition($(".mg-header")) && !$('.mg-header').hasClass("hello")) {
                                        $('.mg-header').addClass("hello");	
                                    }                                          
                                    //CHECK MAGIC SECTION  
                                    if(template.CheckElementPosition($("#animation-graphic")) && !$('#animation-graphic').hasClass("hello")) {
                                        $('#animation-graphic').addClass("hello");	
                                    }  
                                }, 300)
                            }
                        });
                    }
                    
                break;
                case "768":


                break;
                case "640":


                break;
                case "480":


                break;
                case "320":

                break;
            }


        },

        "MoveButtons": function() {
            switch(this.GetBreakPoint()) {
                case "desktop": case "768":
                    //MOVE LINKS TO HEADERS
                    $("[data-region='c'] .app, [data-region='g'] .app, [data-region='h'] .app").each(function(){
                        //FOR SCOPE
                        var thisApp = $(this);
                        
                        //MOVE CALENDAR LINK IF IT EXISTS
                        if ($(thisApp).find(".view-calendar-link").length){
                            //IF IT ISNT WHERE IT SHOULD BE
                            if (!$(thisApp).find(".ui-widget-header .view-calendar-link").length) {
                                // move
                                $(thisApp).find(".view-calendar-link").insertAfter($(thisApp).find(".ui-widget-header h1"));
                            } //else do nothing
                        }
                        
                        //MOVE MORE LINK IF IT EXISTS
                        if ($(thisApp).find(".more-link").length){
                            //IF IT ISNT WHERE IT SHOULD BE
                            if (!$(thisApp).find(".ui-widget-header .more-link").length) {
                                // move
                                $(thisApp).find(".more-link").insertAfter($(thisApp).find(".ui-widget-header h1"));
                            } //else do nothing
                        }
                        //MOVE RSS LINK IF IT EXISTS
                        if ($(thisApp).find(".app-level-social-rss").length){
                            if (!$(thisApp).find(".ui-widget-header .app-level-social-rss").length) {
                                // move
                                $(thisApp).find(".app-level-social-rss").insertAfter($(thisApp).find(".ui-widget-header h1"));
                            } //else do nothing
                        }
                    });

                break;

                case "640":
                    //MOVE LINKS TO HEADERS
                    $("[data-region='g'] .app").each(function(){
                        //FOR SCOPE
                        var thisApp = $(this);
                        
                        //MOVE CALENDAR LINK IF IT EXISTS
                        if ($(thisApp).find(".view-calendar-link").length){
                            //IF IT ISNT WHERE IT SHOULD BE
                            if (!$(thisApp).find(".ui-widget-header .view-calendar-link").length) {
                                // move it
                                $(thisApp).find(".view-calendar-link").insertAfter($(thisApp).find(".ui-widget-header h1"));
                            } //else do nothing
                        }

                        //MOVE MORE LINK IF IT EXISTS
                        if ($(thisApp).find(".more-link").length){
                            //IF IT ISNT WHERE IT SHOULD BE
                            if (!$(thisApp).find(".ui-widget-header .more-link").length) {
                                // move
                                $(thisApp).find(".more-link").insertAfter($(thisApp).find(".ui-widget-header h1"));
                            } //else do nothing
                        }
                        
                        //MOVE RSS LINK IF IT EXISTS
                        if ($(thisApp).find(".app-level-social-rss").length){
                            //IF IT ISNT WHERE IT SHOULD BE
                            if (!$(thisApp).find(".ui-widget-header .app-level-social-rss").length) {
                                // move
                                $(thisApp).find(".app-level-social-rss").insertAfter($(thisApp).find(".ui-widget-header h1"));
                            } //else do nothing
                        }
                    });

                    //MOVE LINKS TO FOOTERS
                    $("[data-region='c'] .app, [data-region='h'] .app").each(function(){
                        //FOR SCOPE
                        var thisApp = $(this);

                        //MOVE LINK IF IT EXISTS
                        if ($(thisApp).find(".view-calendar-link").length){
                            //IF IT ISNT WHERE IT SHOULD BE
                            if (!$(thisApp).find(".ui-widget-footer .view-calendar-link").length) {
                                // move
                                $(thisApp).find(".view-calendar-link").appendTo($(thisApp).find(".ui-widget-footer"));
                            } //else do nothing
                        }

                        //MOVE LINK IF IT EXISTS
                        if ($(thisApp).find(".more-link").length){
                            //IF IT ISNT WHERE IT SHOULD BE
                            if (!$(thisApp).find(".ui-widget-footer .more-link").length) {
                                // move
                                $(thisApp).find(".more-link").appendTo($(thisApp).find(".ui-widget-footer"));
                            } //else do nothing
                        }

                        //MOVE LINK IF IT EXISTS
                        if ($(thisApp).find(".app-level-social-rss").length){
                            //IF IT ISNT WHERE IT SHOULD BE
                            if (!$(thisApp).find(".ui-widget-footer .app-level-social-rss").length) {
                                // move
                                $(thisApp).find(".app-level-social-rss").appendTo($(thisApp).find(".ui-widget-footer"));
                            } //else do nothing
                        }
                    });
                    
                break;

                case "480": case "320":

                    //MOVE LINKS TO FOOTERS
                    $("[data-region='c'] .app, [data-region='g'] .app, [data-region='h'] .app").each(function(){
                        //FOR SCOPE
                        var thisApp = $(this);

                        //MOVE LINK IF IT EXISTS
                        if ($(thisApp).find(".view-calendar-link").length){
                            //IF IT ISNT WHERE IT SHOULD BE
                            if (!$(thisApp).find(".ui-widget-footer .view-calendar-link").length) {
                                // move
                                $(thisApp).find(".view-calendar-link").appendTo($(thisApp).find(".ui-widget-footer"));
                            } //else do nothing
                        }

                        //MOVE LINK IF IT EXISTS
                        if ($(thisApp).find(".more-link").length){
                            //IF IT ISNT WHERE IT SHOULD BE
                            if (!$(thisApp).find(".ui-widget-footer .more-link").length) {
                                // move
                                $(thisApp).find(".more-link").appendTo($(thisApp).find(".ui-widget-footer"));
                            } //else do nothing
                        }

                        //MOVE LINK IF IT EXISTS
                        if ($(thisApp).find(".app-level-social-rss").length){
                            //IF IT ISNT WHERE IT SHOULD BE
                            if (!$(thisApp).find(".ui-widget-footer .app-level-social-rss").length) {
                                // move
                                $(thisApp).find(".app-level-social-rss").appendTo($(thisApp).find(".ui-widget-footer"));
                            } //else do nothing
                        }
                        
                    });
                break;
            }
        },

        "Headlines": function() {
            //HEADLINE CARDS
            $("[data-region='g'] .headlines .ui-article").each(function(articleIndex, article){//LOOP THROUGH EACH ARTICLE
                $('.ui-article-controls, .clear', article).remove(); //REMOVE
                //WRAP LIST ITEMS THAT DONT HAVE IMGS
                $(article).children().not('.ui-article-thumb').wrapAll('<div class="headline-description flex-cont jc-c ai-c fd-col"><div></div></div>');
                //SET TABINDEX
                $('.headline-description a', article).attr('tabindex', -1);
            });
            //LOOP THROUGH EACH ARTICLE
            $("[data-region='g'] .headlines").each(function(appIndex, app){
                $('.ui-article', app).each(function(articleIndex, article){
                    const imgSrc = ($('.ui-article-thumb img', article).attr('src') == undefined) ? '' : $('.ui-article-thumb img', article).attr('src');
        
                    if(imgSrc != '' && imgSrc.indexOf('default-man') < 0){
                        $(article).css('background-image', 'url("' + imgSrc + '")');

                        //SET UNIQUE ID FOR ALLY CONTROL
                        const uniqId = 'hp-headlines-' + appIndex + '-desc-' + articleIndex;
                        $('.headline-description', article)
                            .attr('id', uniqId)
                            .before('<button class="headline-card-trigger" aria-controls="' + uniqId + '" aria-expanded="false">Toggle Description</button>');
                    }else{
                        $('.headline-description', article).addClass('no-image');
                    }
                });
            });
            //CLICK EVENTS FOR HEADLINES
            $("[data-region='g'] .headline-card-trigger").on('click', function(e){
                const ctrlObj = $('#' + $(this).attr('aria-controls'));

                if($(this).attr('aria-expanded') == 'true'){
                    ctrlObj.removeClass('focused');
                    $(this).attr('aria-expanded', false);
                    $('a', ctrlObj).attr('tabindex', -1);
                }else{
                    ctrlObj.addClass('focused');
                    $(this).attr('aria-expanded', true);
                    $('a', ctrlObj).attr('tabindex', 0);
                }
            });
        },

        "Magic": function() {
            //FOR SCOPE
            var template = this;

            var magicContent = [
                {
                    'show'          : <SWCtrl controlname="Custom" props="Name:igToggle1" />,
                    'text'          : '<SWCtrl controlname="Custom" props="Name:igTitle1" />',
                    'description'   : '<SWCtrl controlname="Custom" props="Name:igDescription1" />'
                },
                {
                    'show'          : <SWCtrl controlname="Custom" props="Name:igToggle2" />,
                    'text'          : '<SWCtrl controlname="Custom" props="Name:igTitle2" />',
                    'description'   : '<SWCtrl controlname="Custom" props="Name:igDescription2" />'
                },
                {
                    'show'          : <SWCtrl controlname="Custom" props="Name:igToggle3" />,
                    'text'          : '<SWCtrl controlname="Custom" props="Name:igTitle3" />',
                    'description'   : '<SWCtrl controlname="Custom" props="Name:igDescription3" />'
                },
                {
                    'show'          : <SWCtrl controlname="Custom" props="Name:igToggle4" />,
                    'text'          : '<SWCtrl controlname="Custom" props="Name:igTitle4" />',
                    'description'   : '<SWCtrl controlname="Custom" props="Name:igDescription4" />'
                }
            ];

            //CREATE VARIABLE FOR ITEMS
            var igTitles = '';
            var igDescrip = '';
            var igMobile = '';

            //LOOP THROUGH EACH ITEM
            $.each(magicContent, function(index, items) {
                //BUILD ITEMS
                if (items.show) {//IS TOGGLED ON

                    //FOR DESKTOP
                    if (index === 0) { //IS BUTTON 1
                        //BUILD TITLE
                        igTitles += '<button id="title-'+index+'" class="tablink active" aria-labelledby="title-'+index+'" aria-selected="true" tabindex="0" aria-controls="tabpanel-'+index+'"><p>'+items.text+'</p><svg id="title-0_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 181.19 21.91"><g><path class="cls t0_1" d="m2.81,18.95S36.95-2.8,33.27,6.13c-5.61,13.63-8.82,14.43,15.58,0,15.78-9.33,6.06,3.21,6.87,10.43s12.03-2.41,24.86-10.42c12.83-8.02,9.62,10.42,28.06,12.03,18.44,1.6,17.01-13.83,28.06-4.01,7.22,6.41,41.69,1.6,41.69,1.6"/></g></svg><svg id="title-0_2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 59.52 48.25"><g><line class="cls t0_2" x1="51.79" y1="23.31" x2="56.72" y2="2.81"/><line class="cls t0_2" x1="28.27" y1="26.55" x2="18.26" y2="5.56"/><line class="cls t0_2" x1="24.72" y1="45.45" x2="2.81" y2="37.68"/></g></svg></button>'

                        //BUILD DESCRIPTION
                        igDescrip += ' <div id="tabpanel-'+index+'" class="tabcontent show" aria-expanded="true" aria-labelledby="tabpanel-'+index+'"><p>'+items.description+'</p></div>'
                    } else {
                        //BUILD TITLE
                        igTitles += '<button id="title-'+index+'" class="tablink" aria-selected="false" tabindex="-1" aria-labelledby="title-'+index+'" aria-controls="tabpanel-'+index+'"><p>'+items.text+'</p>'
                        
                        if (index === 1) { //IS BUTTON 2
                            igTitles += '<svg id="title-1_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 35.35 35.61"><g><line class="cls t1_1" x1="16.68" y1="2.81" x2="18.68" y2="32.81"/><line class="cls t1_1" x1="32.54" y1="15.62" x2="2.81" y2="20"/></g></svg><svg id="title-1_2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 333.61 26.44"><g><path class="cls t1_2" d="m2.81,23.64S176.8-6.36,330.8,5.64"/></g></svg>'
                        } 

                        if (index === 2) { //IS BUTTON 3
                            igTitles += '<svg id="title-2_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 73.61 37.61"><g><line class="cls t2_1" x1="2.81" y1="24.8" x2="61.81" y2="16.8"/><polyline class="cls t2_1" points="39.81 2.81 70.8 14.81 52.81 34.81 49.81 10.81"/></g></svg><svg id="title-2_2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 362.61 24.19"><g><path class="cls t2_2" d="m2.81,19.6S45.81-5.4,38.81,5.6c-7,11-14,18,17,4,31-14-11,17,13,11,24-6,291-12,291-12l-27,9"/></g></svg>'

                        } 

                        if (index === 3) { //IS BUTTON 4
                            igTitles += '<svg id="title-3_1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 47.25 60.56"><g><line class="cls t3_1" x1="23.15" y1="8.35" x2="2.81" y2="2.81"/><line class="cls t3_1" x1="25.68" y1="31.96" x2="4.38" y2="41.33"/><line class="cls t3_1" x1="44.45" y1="36.09" x2="36" y2="57.75"/></g></svg><svg id="title-3_2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 393.61 75.61"><g><polyline class="cls t3_2"points="326.8 72.8 2.81 72.8 2.81 2.81 390.8 2.81"/></g></svg>'
                        } 

                        igTitles += '</button>'
    
                        //BUILD DESCRIPTION
                        igDescrip += '<div id="tabpanel-'+index+'" class="tabcontent no-show" aria-expanded="false"  aria-labelledby="tabpanel-'+index+'"><p>'+items.description+'</p></div>'
                    }

                    //FOR MOBILE
                        igMobile += '<div class="mg-mobile-wrapper"><div class="mg-title-mobile"><h2>'+ items.text +'</h2></div><div class="mg-description-mobile"><p>'+ items.description +'</p></div></div>'
                
                }
            });

            //IF ITEMS EXIST, PUSH
            if (igTitles.length && igDescrip.length) {
                $('#mg-titles').prepend(igTitles);
                $('#mg-descriptions').prepend(igDescrip);

                //PUSH FOR MOBILE
                $('#magic-mobile').prepend(igMobile);
            }

            //SPOTLIGHT TABS INTERACTION
            $(".tablink").on('focus mouseover', function(e){
                var currentTab = $(this); //curent tab
                var currentTabPanel = $('#' + $(this).attr('aria-controls')); //corresponding tabpanel

                $(".tablink").removeClass('active'); //remove active from all tabs
                $(".tablink").attr({
                    'aria-selected' : 'false',
                    'tabindex'     : '-1'
                }); //update all tabs to selected false

                currentTab.addClass('active'); //find current tab, make active
                currentTab.attr({
                    'aria-selected': 'true',
                    'tabindex': '0',
                }); //find current tab, make selected

                $(".tabcontent").addClass('no-show'); //add no-show to all tabpanels
                $(".tabcontent").removeClass('show'); //remove show from all tabpanels
                $(".tabcontent").attr('aria-expanded', 'false'); //update all tabpanels to selected false
                currentTabPanel.removeClass('no-show'); //remove no-show from current panel
                currentTabPanel.addClass('show'); //add show to current panel
                currentTabPanel.attr('aria-expanded', 'true'); //update all tabpanels to selected false
            });
            //SPOTLIGHT TAB INTERACTION ONCE FOCUSED
            $(".tablink").on("keydown", function(e){
                var currentTab = $(this); //curent tab
                var currentTabPanel = $('#' + $(this).attr('aria-controls')); //corresponding tabpanel

                //ARROW KEY MOVEMENTS
                switch(e.key) {
    
                    //CONSUME DOWN ARROW KEY
                    case template.KeyCodes.right:
                    case template.KeyCodes.down:
                        e.preventDefault();
                        if($(currentTab).is(":last-child")) { //IF WE ARE AT BOTTOM OF LIST
                            $(".tablink:first-child").focus(); //GO TO FIRST
                        } else {
                            $(currentTab).next().focus(); //IF NOT GO TO NEXT DOWN
                        } 
                    break;

                    //CONSUME UP ARROW KEY
                    case template.KeyCodes.left:
                    case template.KeyCodes.up:
                        e.preventDefault();
                        if($(currentTab).is(":first-child")) { //IF WE ARE AT TOP OF LIST
                            $(".tablink:last-child").focus(); //GO TO FIRST
                        } else {
                            $(currentTab).prev().focus(); //ESLE GO
                        }
                    break;
                }
            });

        },
        
        "Body": function() {
            // FOR SCOPE
            var template = this;
            
            //AUTO FOUCS SIGN IN FIELD
            $("swsignin-txt-username").focus();

            // APPLY RESPONSIVE DIMENSIONS TO CONTENT IMAGES
            $('div.ui-widget.app .ui-widget-detail img')
                .not($('div.ui-widget.app.cs-rs-multimedia-rotator .ui-widget-detail img'))
                .not($('div.ui-widget.app.gallery.json .ui-widget-detail img'))
                .each(function() {
                    if ($(this).attr('width') !== undefined && $(this).attr('height') !== undefined) { // IMAGE HAS INLINE DIMENSIONS
                        $(this).css({'display': 'inline-block', 'width': '100%', 'max-width': $(this).attr('width') + 'px', 'height': 'auto', 'max-height': $(this).attr('height') + 'px'});
                    }
            });

            // ADJUST FIRST BREADCRUMB
            $('li.ui-breadcrumb-first > a > span').text('Home');

            // CHECK PAGELIST HEADER
            if($.trim($(".ui-widget.app.pagenavigation .ui-widget-header").text()) == "") {
                $(".ui-widget.app.pagenavigation .ui-widget-header").html("<h1>[$ChannelName$]</h1>");
            }
            
            //MOVE BUTTONS INTO UI-WIDGET-FOOTER
            $('.sp div.ui-widget.app, .spn div.ui-widget.app').each(function(appIndex, app){
                //more link
                if( $('.more-link', app).length > 0 ){
                    $('.more-link', app).prependTo( $('.ui-widget-footer', app) );
                    $('.more-link-under', app).parent().remove();
                }
                
                //view calendar link
                if( $('.view-calendar-link', app).length > 0 ){
                    $('.view-calendar-link', app).prependTo( $('.ui-widget-footer', app) );
                }
                
                if( $('.app-level-social-follow', app).length > 0 && $('.app-level-social-follow', app).children().length > 0 ){
                    $('.app-level-social-follow', app).prependTo( $('.ui-widget-footer', app) );
                }else{
                    $('.app-level-social-follow', app).remove();
                }
    
                //Hide empty footer
                if( $('.ui-widget-footer', app).children().not('.clear').length <= 0 ){
                    $('.ui-widget-footer', app).addClass('no-children').hide();
                }else{
                    $('.ui-widget-footer', app).addClass('has-children');
                    $('.ui-widget-footer .clear', app).remove();
                }

                //move button to header on accordion app
                if ($(app).hasClass("content-accordion")) {
                    const thisApp = $(app);
                    $('.content-accordion-toolbar', app).appendTo( $('.ui-widget-header', app) );
                }
                
            });

            //CONST FOR TOGGLE ATTR
            const toggle = '<SWCtrl controlname="Custom" props="Name:sideNavToggle" />';
            //FIND SIDE NAV
            if ($('.sp-column.one .pagenavigation').length) {
                //ADD ATTR
                $('.sp-column.one .pagenavigation .ui-widget-header').attr('data-toggle', toggle);
            }

            $(".sp-column.one").csAppAccordion({
                "accordionBreakpoints" : [768, 640, 480, 320]
            });
        },

        "GlobalIcons": function() {
            $("#gb-icons, #gb-icons-sticky").creativeIcons({
                "iconNum"           : '<SWCtrl controlname="Custom" props="Name:numOfIcons" />',
                "defaultIconSrc"    : '',
                "icons"             : [
                    {
                        "image"     : '<SWCtrl controlname="Custom" props="Name:Icon1Image" />', 
                        "showText"  : true,
                        "text"      : '<SWCtrl controlname="Custom" props="Name:Icon1Text" />',
                        "url"       : '<SWCtrl controlname="Custom" props="Name:Icon1Link" />',
                        "target"    : '<SWCtrl controlname="Custom" props="Name:Icon1Target" />'
                    },
                    {
                        "image"     : '<SWCtrl controlname="Custom" props="Name:Icon2Image" />', 
                        "showText"  : true,
                        "text"      : '<SWCtrl controlname="Custom" props="Name:Icon2Text" />',
                        "url"       : '<SWCtrl controlname="Custom" props="Name:Icon2Link" />',
                        "target"    : '<SWCtrl controlname="Custom" props="Name:Icon2Target" />'
                    },
                    {
                        "image"     : '<SWCtrl controlname="Custom" props="Name:Icon3Image" />', 
                        "showText"  : true,
                        "text"      : '<SWCtrl controlname="Custom" props="Name:Icon3Text" />',
                        "url"       : '<SWCtrl controlname="Custom" props="Name:Icon3Link" />',
                        "target"    : '<SWCtrl controlname="Custom" props="Name:Icon3Target" />'
                    },
                    {
                        "image"     : '<SWCtrl controlname="Custom" props="Name:Icon4Image" />', 
                        "showText"  : true,
                        "text"      : '<SWCtrl controlname="Custom" props="Name:Icon4Text" />',
                        "url"       : '<SWCtrl controlname="Custom" props="Name:Icon4Link" />',
                        "target"    : '<SWCtrl controlname="Custom" props="Name:Icon4Target" />'
                    },
                    {
                        "image"     : '<SWCtrl controlname="Custom" props="Name:Icon5Image" />', 
                        "showText"  : true,
                        "text"      : '<SWCtrl controlname="Custom" props="Name:Icon5Text" />',
                        "url"       : '<SWCtrl controlname="Custom" props="Name:Icon5Link" />',
                        "target"    : '<SWCtrl controlname="Custom" props="Name:Icon5Target" />'
                    },
                    {
                        "image": '<SWCtrl controlname="Custom" props="Name:Icon6Image" />',
                        "showText": true,
                        "text": '<SWCtrl controlname="Custom" props="Name:Icon6Text" />',
                        "url": '<SWCtrl controlname="Custom" props="Name:Icon6Link" />',
                        "target": '<SWCtrl controlname="Custom" props="Name:Icon6Target" />'
                    },
                    {
                        "image": '<SWCtrl controlname="Custom" props="Name:Icon7Image" />',
                        "showText": true,
                        "text": '<SWCtrl controlname="Custom" props="Name:Icon7Text" />',
                        "url": '<SWCtrl controlname="Custom" props="Name:Icon7Link" />',
                        "target": '<SWCtrl controlname="Custom" props="Name:Icon7Target" />'
                    },
                    {
                        "image": '<SWCtrl controlname="Custom" props="Name:Icon8Image" />',
                        "showText": true,
                        "text": '<SWCtrl controlname="Custom" props="Name:Icon8Text" />',
                        "url": '<SWCtrl controlname="Custom" props="Name:Icon8Link" />',
                        "target": '<SWCtrl controlname="Custom" props="Name:Icon8Target" />'
                    },
                    {
                        "image": '<SWCtrl controlname="Custom" props="Name:Icon9Image" />',
                        "showText": true,
                        "text": '<SWCtrl controlname="Custom" props="Name:Icon9Text" />',
                        "url": '<SWCtrl controlname="Custom" props="Name:Icon9Link" />',
                        "target": '<SWCtrl controlname="Custom" props="Name:Icon9Target" />'
                    },
                    {
                        "image": '<SWCtrl controlname="Custom" props="Name:Icon10Image" />',
                        "showText": true,
                        "text": '<SWCtrl controlname="Custom" props="Name:Icon10Text" />',
                        "url": '<SWCtrl controlname="Custom" props="Name:Icon10Link" />',
                        "target": '<SWCtrl controlname="Custom" props="Name:Icon10Target" />'
                    }
                ],
                    "siteID"        : "[$SiteID$]",
                    "siteAlias"     : "[$SiteAlias$]",
                    "calendarLink"  : "[$SiteCalendarLink$]",
                    "contactEmail"  : "[$SiteContactEmail$]",
                    "allLoaded"     : function(){ }
            });

        },

        "SocialIcons": function() {
            var socialIcons = [{
                "show":<SWCtrl controlname="Custom" props="Name:showFacebook" />,
                "label": "Facebook",
                "class": "facebook",
                "url":'<SWCtrl controlname="Custom" props="Name:FacebookUrl" />',
                "target":'<SWCtrl controlname="Custom" props="Name:FacebookTarget" />'
              }, {
                "show":<SWCtrl controlname="Custom" props="Name:showTwitter" />,
                "label": "Twitter",
                "class": "twitter",
                "url":'<SWCtrl controlname="Custom" props="Name:TwitterUrl" />',
                "target":'<SWCtrl controlname="Custom" props="Name:TwitterTarget" />'
              }, {
                "show":<SWCtrl controlname="Custom" props="Name:showYouTube" />,
                "label": "YouTube",
                "class": "youtube",
                "url":'<SWCtrl controlname="Custom" props="Name:YouTubeUrl" />',
                "target":'<SWCtrl controlname="Custom" props="Name:YouTubeTarget" />'
              }, {
                "show":<SWCtrl controlname="Custom" props="Name:showInstagram" />,
                "label": "Instagram",
                "class": "instagram",
                "url":'<SWCtrl controlname="Custom" props="Name:InstagramUrl" />',
                "target":'<SWCtrl controlname="Custom" props="Name:InstagramTarget" />'
              }, {
                "show":<SWCtrl controlname="Custom" props="Name:showLinkedIn" />,
                "label": "LinkedIn",
                "class": "linkedin",
                "url":'<SWCtrl controlname="Custom" props="Name:LinkedInUrl" />',
                "target":'<SWCtrl controlname="Custom" props="Name:LinkedInTarget" />'
              }, {
                "show":<SWCtrl controlname="Custom" props="Name:showVimeo" />,
                "label": "Vimeo",
                "class": "vimeo",
                "url":'<SWCtrl controlname="Custom" props="Name:VimeoUrl" />',
                "target":'<SWCtrl controlname="Custom" props="Name:VimeoTarget" />'
              }, {
                "show":<SWCtrl controlname="Custom" props="Name:showFlickr" />,
                "label": "Flickr",
                "class": "flickr",
                "url":'<SWCtrl controlname="Custom" props="Name:FlickrUrl" />',
                "target":'<SWCtrl controlname="Custom" props="Name:FlickrTarget" />'
              }, {
                "show":<SWCtrl controlname="Custom" props="Name:showPinterest" />,
                "label": "Pinterest",
                "class": "pinterest",
                "url":'<SWCtrl controlname="Custom" props="Name:PinterestUrl" />',
                "target":'<SWCtrl controlname="Custom" props="Name:PinterestTarget" />'
              }, {
                "show":<SWCtrl controlname="Custom" props="Name:showPeachjar" />,
                "label": "Peachjar",
                "class": "peachjar",
                "url":'<SWCtrl controlname="Custom" props="Name:PeachjarUrl" />',
                "target":'<SWCtrl controlname="Custom" props="Name:PeachjarTarget" />'
              }];

              //CREATE ICON VARIABLE
              var icons = "";

              //LOOP THROUGH EACH AND BUILD
              $.each(socialIcons, function(index, icon) {
                if(icon.show) {
                    icons += '<li class="gb-social-icon" data-toggle=' + icon.show + '><a class="gb-social-icons ' + icon.class + ' district" href="' + icon.url + '" target="' + icon.target + '" aria-label="' + icon.label + '"></a></li>';
                }
              });

            //AFTER LOOP PUSH ICONS TO DOM
            if(icons.length) {
                $(".gb-socials").prepend(icons); 
            }

        },

        
        // "StickyScroll": function() {
        //     var template = this;
        //     //CHECK IF ELEMENT IS IN VIEWPORT
        //     $.fn.isInViewport = function(offset) {
        //         let elementTriggerPoint = $(this).offset().top + ($(this).outerHeight()/2);
        //         let viewportMiddle = $(window).scrollTop() + ($(window).height()/2);
        //         let offsetSize = $(window).height() * offset;
        //         return elementTriggerPoint >= (viewportMiddle - offsetSize) && elementTriggerPoint <= (viewportMiddle + offsetSize);
        //     };

        //     if(this.GetBreakPoint() === 'desktop') {

        //         console.log('desktop');

        //         if(!$('#gb-icons-sticky').isInViewport(0.42)) {
        //             console.log('notInView');
        //             $(this).addClass('hello');
        //         } else {
        //             $(this).removeClass('hello');
        //             console.log('inView');
        //         }
        //     }
            
        // },

        "MegaMenu": function() {
            $("#sw-channel-list-container").megaChannelMenu({
                "numberOfChannels"					: 10, // integer 
                "maxSectionColumnsPerChannel"		: 3, // integer (this does NOT include the extra, non-section column)
                "extraColumn"						: false, // boolean
                "extraColumnTitle"					: false, //bolean
                "extraColumnDescription"			: false, //bolean
                "extraColumnImage"					: false, //bolean
                "extraColumnLink"					: false, //bolean
                "extraColumnPosition"				: "ABOVE", // string, "ABOVE", "BELOW", "LEFT", "RIGHT"
                "sectionHeadings"					: true, // boolean
                "sectionHeadingLinks"				: false, // boolean
                "maxNumberSectionHeadingsPerColumn"	: 3, // integer 
                "megaMenuElements"					: [
                    { //CHANNEL 1
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel1Name" />',
                        "channelColumns"		: [
                            [ //COLUMN 1
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column1Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column1Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column1Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column1Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column1Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column1Heading3Sections" />'
                                }
                            ],
                            [ //COLUMN 2
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column2Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column2Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column2Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column2Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column2Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column2Heading3Sections" />'
                                }
                            ],
                            [ //COLUMN 3
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column3Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column3Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column3Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column3Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column3Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel1Column3Heading3Sections" />'
                                }
                            ]
                        ]
                    },
                    { //CHANNEL 2
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel2Name" />',
                        "channelColumns"		: [
                            [ //COLUMN 1
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column1Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column1Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column1Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column1Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column1Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column1Heading3Sections" />'
                                }
                            ],
                            [ //COLUMN 2
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column2Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column2Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column2Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column2Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column2Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column2Heading3Sections" />'
                                }
                            ],
                            [ //COLUMN 3
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column3Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column3Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column3Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column3Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column3Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel2Column3Heading3Sections" />'
                                }
                            ]
                        ]
                    },
                    { //CHANNEL 3
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel3Name" />',
                        "channelColumns"		: [
                            [ //COLUMN 1
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column1Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column1Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column1Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column1Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column1Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column1Heading3Sections" />'
                                }
                            ],
                            [ //COLUMN 2
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column2Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column2Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column2Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column2Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column2Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column2Heading3Sections" />'
                                }
                            ],
                            [ //COLUMN 3
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column3Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column3Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column3Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column3Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column3Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel3Column3Heading3Sections" />'
                                }
                            ]
                        ]
                    },
                    { //CHANNEL 4
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel4Name" />',
                        "channelColumns"		: [
                            [ //COLUMN 1
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column1Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column1Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column1Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column1Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column1Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column1Heading3Sections" />'
                                }
                            ],
                            [ //COLUMN 2
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column2Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column2Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column2Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column2Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column2Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column2Heading3Sections" />'
                                }
                            ],
                            [ //COLUMN 3
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column3Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column3Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column3Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column3Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column3Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel4Column3Heading3Sections" />'
                                }
                            ]
                        ]
                    },
                    { //CHANNEL 5
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel5Name" />',
                        "channelColumns"		: [
                            [ //COLUMN 1
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column1Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column1Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column1Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column1Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column1Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column1Heading3Sections" />'
                                }
                            ],
                            [ //COLUMN 2
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column2Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column2Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column2Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column2Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column2Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column2Heading3Sections" />'
                                }
                            ],
                            [ //COLUMN 3
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column3Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column3Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column3Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column3Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column3Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel5Column3Heading3Sections" />'
                                }
                            ]
                        ]
                    },
                    { //CHANNEL 6
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel6Name" />',
                        "channelColumns"		: [
                            [ //COLUMN 1
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column1Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column1Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column1Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column1Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column1Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column1Heading3Sections" />'
                                }
                            ],
                            [ //COLUMN 2
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column2Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column2Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column2Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column2Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column2Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column2Heading3Sections" />'
                                }
                            ],
                            [ //COLUMN 3
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column3Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column3Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column3Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column3Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column3Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel6Column3Heading3Sections" />'
                                }
                            ]
                        ]
                    },
                    { //CHANNEL 7
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel7Name" />',
                        "channelColumns"		: [
                            [ //COLUMN 1
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column1Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column1Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column1Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column1Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column1Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column1Heading3Sections" />'
                                }
                            ],
                            [ //COLUMN 2
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column2Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column2Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column2Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column2Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column2Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column2Heading3Sections" />'
                                }
                            ],
                            [ //COLUMN 3
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column3Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column3Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column3Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column3Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column3Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel7Column3Heading3Sections" />'
                                }
                            ]
                        ]
                    },
                    { //CHANNEL 8
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel8Name" />',
                        "channelColumns"		: [
                            [ //COLUMN 1
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column1Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column1Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column1Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column1Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column1Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column1Heading3Sections" />'
                                }
                            ],
                            [ //COLUMN 2
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column2Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column2Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column2Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column2Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column2Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column2Heading3Sections" />'
                                }
                            ],
                            [ //COLUMN 3
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column3Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column3Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column3Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column3Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column3Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel8Column3Heading3Sections" />'
                                }
                            ]
                        ]
                    },
                    { //CHANNEL 9
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel9Name" />',
                        "channelColumns"		: [
                            [ //COLUMN 1
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column1Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column1Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column1Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column1Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column1Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column1Heading3Sections" />'
                                }
                            ],
                            [ //COLUMN 2
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column2Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column2Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column2Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column2Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column2Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column2Heading3Sections" />'
                                }
                            ],
                            [ //COLUMN 3
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column3Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column3Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column3Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column3Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column3Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel9Column3Heading3Sections" />'
                                }
                            ]
                        ]
                    },
                    { //CHANNEL 10
                        "channelName"			: '<SWCtrl controlname="Custom" props="Name:csmChannel10Name" />',
                        "channelColumns"		: [
                            [ //COLUMN 1
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column1Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column1Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column1Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column1Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column1Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column1Heading3Sections" />'
                                }
                            ],
                            [ //COLUMN 2
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column2Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column2Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column2Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column2Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column2Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column2Heading3Sections" />'
                                }
                            ],
                            [ //COLUMN 3
                                { //SECTION 1
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column3Heading1Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column3Heading1Sections" />'
                                },
                                { //SECTION 2
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column3Heading2Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column3Heading2Sections" />'
                                },
                                { //SECTION 3
                                    "heading"	: [
                                        {
                                            "text"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column3Heading3Text" />'
                                        }
                                    ],
                                    "sections"	: '<SWCtrl controlname="Custom" props="Name:csmChannel10Column3Heading3Sections" />'
                                }
                            ]
                        ]
                    }
                ],
                "allLoaded"	: function allLoaded() {}
            })
        },
        
        "MMGPlugin": function MMGPlugin() {
            // FOR SCOPE
            var template = this;
            
            if($("#sw-content-container10 .ui-widget.app.multimedia-gallery").length) {
                var mmg = eval("multimediaGallery" + $("#sw-content-container10 .ui-widget.app.multimedia-gallery:first").attr("data-pmi"));
                mmg.props.defaultGallery = false;
                
                $("#sw-content-container10 .ui-widget.app.multimedia-gallery:first").csMultimediaGallery({
                    "efficientLoad" : true,
                    "imageWidth" : 1200,
                    "imageHeight" : 655,
                    "mobileDescriptionContainer": [960, 768, 640, 480, 320], // [960, 768, 640, 480, 320]
                    "galleryOverlay" : false,
                    "linkedElement" : [], // ["image", "title", "overlay"]
                    "playPauseControl" : true,
                    "backNextControls" : true,
                    "bullets" : false,
                    "thumbnails" : false,
                    "thumbnailViewerNum": [4, 4, 3, 3, 2], // NUMERICAL - [960 view, 768 view, 640 view, 480 view, 320 view]
                    "autoRotate" : false,
                    "hoverPause" : true,
                    "transitionType" : "slide", // fade, slide, custom
                    "transitionSpeed" : 1.5,
                    "transitionDelay" : 4,
                    "fullScreenRotator" : false,
                    "fullScreenBreakpoints" : [960], // NUMERICAL - [960, 768, 640, 480, 320]
                    "onImageLoad" : function(props) {}, // props.element, props.recordIndex, props.mmgRecords
                    "allImagesLoaded" : function(props) {}, // props.element, props.mmgRecords
                    "onTransitionStart" : function(props) {
                        $('.mmg-description-wrapper', props.element).addClass('hello');

                    }, // props.element, props.currentRecordIndex, props.currentGalleryIndex, props.nextRecordIndex, props.nextGalleryIndex, props.mmgRecords
                    "onTransitionEnd" : function(props) {
                        $('.mmg-description-wrapper', props.element).removeClass('hello');
                        
                    }, // props.element, props.currentRecordIndex, props.currentGalleryIndex, props.mmgRecords
                    "allLoaded" : function(props) {

                        //WRAP DESCRIPTION IN WRAPPER
                        $('.mmg-description-outer', props.element).wrap('<div class="mmg-description-wrapper flex-cont fd-col jc-sb" />');

                        //IF CONTROLS EXIST, THEN WRAP THEM
                        if( $('.mmg-control', props.element).length > 0 ){
                            $('.mmg-description-outer', props.element).append('<div class="mmg-controls flex-cont ai-c jc-c"></div>');
    
                            $('.mmg-controls', props.element).append($('#hp-slideshow .mmg-control.back'));
                            $('.mmg-controls', props.element).append($('#hp-slideshow .mmg-control.play-pause'));
                            $('.mmg-controls', props.element).append($('#hp-slideshow .mmg-control.next'));
                        }

                    }, // props.element, props.mmgRecords
                    "onWindowResize": function(props) {} // props.element, props.mmgRecords
                });
            }
        },

        "ModEvents": function() {
            // FOR SCOPE
            var template = this;

            $(".ui-widget.app.upcomingevents").modEvents({
                columns     : "yes",
                monthLong   : "yes",
                dayWeek     : "yes"
            });

            eventsByDay(".upcomingevents .ui-articles");

            function eventsByDay(container) {
                $(".ui-article", container).each(function(){
                    if (!$(this).find("h1.ui-article-title.sw-calendar-block-date").size()){
                        var moveArticle = $(this).html();
                        $(this).parent().prev().children().children().next().append(moveArticle);
                        $(this).parent().remove();
                    };
                }); 

                $(".ui-article", container).each(function(){
                    var newDateTime = $('.upcoming-column.left h1', this).html().toLowerCase().split("</span>");
                    var newDate = '';
                    var dateIndex = ((newDateTime.length > 2) ? 2 : 1); //if the dayWeek is set to yes we need to account for that in our array indexing
                    
                    //if we have the day of the week make sure to include it.
                    if( dateIndex > 1 ){ 
                        newDate += newDateTime[0] + "</span>";
                    }
                    
                    //add in the month
                    newDate += newDateTime[dateIndex - 1] + '</span>'; //the month is always the in the left array position to the date
                    
                    //wrap the date in a new tag
                    newDate += '<span class="jeremy-date">' + newDateTime[dateIndex] + '</span>';
                    
                    //append the date and month back into their columns
                    $('.upcoming-column.left h1', this).html(newDate);  
                    
                    
                    //add an ALL DAY label if no time was given
                    $('.upcoming-column.right .ui-article-description', this).each(function(){
                        if( $('.sw-calendar-block-time', this).length < 1 ){ //if it doesnt exist add it
                            $(this).prepend('<span class="sw-calendar-block-time">ALL DAY</span>');
                        }
                        var text = $(".sw-calendar-block-title", this).text();
                        var text = text.split("@");
                        if(text.length == 2) {
                            $(".sw-calendar-block-title a", this).text(text[0]);
                            $(this).append("<span class='sw-calendar-block-location'>"+text[1]+"</span>");
                        }
                        $(".sw-calendar-block-time", this).appendTo($(this));
                    });
                    
                    //WRAP DATE AND MONTH IN A CONTAINER
                    $(".jeremy-date, .joel-month", this).wrapAll("<span class='adam-hug'></span>");
                    
                    var eventDay = $.trim($(this).find(".joel-day").html().toLowerCase());
                    var eventMonth = $.trim($(this).find(".joel-month").text().toLowerCase());
                    var eventNumberDay = $.trim($(this).find(".jeremy-date").text());
                    var ariaMonth = "";
                    var ariaDate = "";

                    switch (eventMonth) {
                      case "jan":
                        ariaMonth = "January";
                        break;

                      case "feb":
                        ariaMonth = "February";
                        break;

                      case "mar":
                        ariaMonth = "March";
                        break;

                      case "apr":
                        ariaMonth = "April";
                        break;

                      case "may":
                        ariaMonth = "May";
                        break;

                      case "jun":
                        ariaMonth = "June";
                        break;

                      case "jul":
                        ariaMonth = "July";
                        break;

                      case "aug":
                        ariaMonth = "August";
                        break;

                      case "sep":
                        ariaMonth = "September";
                        break;

                      case "oct":
                        ariaMonth = "October";
                        break;

                      case "nov":
                        ariaMonth = "November";
                        break;

                      case "dec":
                        ariaMonth = "December";
                        break;

                      default:
                        ariaMonth = eventMonth;
                        break;
                    }

                    switch (eventNumberDay) {
                      case "1":
                        ariaDate = "First";
                        break;

                      case "2":
                        ariaDate = "Second";
                        break;

                      case "3":
                        ariaDate = "Third";
                        break;

                      case "4":
                        ariaDate = "Fourth";
                        break;

                      case "5":
                        ariaDate = "Fifth";
                        break;

                      case "6":
                        ariaDate = "Sixth";
                        break;

                      case "7":
                        ariaDate = "Seventh";
                        break;

                      case "8":
                        ariaDate = "Eighth";
                        break;

                      case "9":
                        ariaDate = "Ninth";
                        break;

                      case "10":
                        ariaDate = "Tenth";
                        break;

                      case "11":
                        ariaDate = "Eleventh";
                        break;

                      case "12":
                        ariaDate = "Twelveth";
                        break;

                      case "13":
                        ariaDate = "Thirteenth";
                        break;

                      case "14":
                        ariaDate = "Fourteenth";
                        break;

                      case "15":
                        ariaDate = "Fifteenth";
                        break;

                      case "16":
                        ariaDate = "Sixteenth";
                        break;

                      case "17":
                        ariaDate = "Seventeenth";
                        break;

                      case "18":
                        ariaDate = "Eighteenth";
                        break;

                      case "19":
                        ariaDate = "Nineteenth";
                        break;

                      case "20":
                        ariaDate = "Twentieth";
                        break;

                      case "21":
                        ariaDate = "Twenty-first";
                        break;

                      case "22":
                        ariaDate = "Twenty-second";
                        break;

                      case "23":
                        ariaDate = "Twenty-third";
                        break;

                      case "24":
                        ariaDate = "Twenty-fourth";
                        break;

                      case "25":
                        ariaDate = "Twenty-fifth";
                        break;

                      case "26":
                        ariaDate = "Twenty-sixth";
                        break;

                      case "27":
                        ariaDate = "Twenty-seventh";
                        break;

                      case "28":
                        ariaDate = "Twenty-eighth";
                        break;

                      case "29":
                        ariaDate = "Twenty-ninth";
                        break;

                      case "30":
                        ariaDate = "Thirtieth";
                        break;

                      case "31":
                        ariaDate = "Thirty-first";
                        break;

                      default:
                        ariaDate = eventNumberDay;
                        break;
                    }


					//ADD MORE EVENTS LINK
                    if ($(this).find(".ui-article-description").length > 2) {
                      var moreEventsLink = $(this).find(".ui-article-description:nth-child(1) a").attr("href");
                      moreEventsLink = moreEventsLink.split("/").slice(0, -2).join("/");
                      moreEventsLink = moreEventsLink + "/day";
                      $(this).find(".upcoming-column.right").append("<a href='" + moreEventsLink + "' target='blank' aria-label='View all events for " + eventDay + ", " + ariaMonth + " " + ariaDate + ".' class='all-events-link'></a>");
                    }
                }) 
            }

            //ADD NO EVENTS TEXT
            $(".upcomingevents").each(function(){
                if(!$(this).find(".ui-article").length){
                    $("<li><p>There are no upcoming events to display.</p></l1>").appendTo($(this).find(".ui-articles"));
                }
            });
    
        },


        "RsMenu": function() {
            //FOR SCOPE
            var template = this;
            var usedSearchLinks = [];
            var searchLinks = [
                {
                    "text"      : '<SWCtrl controlname="Custom" props="Name:Nav1Text" />',
                    "url"       : '<SWCtrl controlname="Custom" props="Name:Nav1Url" />',
                    "target"    : '<SWCtrl controlname="Custom" props="Name:Nav1Target" />'
                },
                {
                    "text"      : '<SWCtrl controlname="Custom" props="Name:Nav2Text" />',
                    "url"       : '<SWCtrl controlname="Custom" props="Name:Nav2Url" />',
                    "target"    : '<SWCtrl controlname="Custom" props="Name:Nav2Target" />'
                },
                {
                    "text"      : '<SWCtrl controlname="Custom" props="Name:Nav3Text" />',
                    "url"       : '<SWCtrl controlname="Custom" props="Name:Nav3Url" />',
                    "target"    : '<SWCtrl controlname="Custom" props="Name:Nav3Target" />'
                },

            ];

            searchLinks.forEach(checkIfUsed);

            var extraOptions = {};
            //ARE THEY USED, IF SO ASSIGN THEM
            if (usedSearchLinks.length > 0){
                Object.assign(extraOptions, {"Quick Links": usedSearchLinks});
            };

            //CHECK IF ASSIGNED
            function checkIfUsed(item) {
                //TRIM
                if ($.trim(item.text) != "") {
                    var itemToPush = {
                        "text"      :   item.text,
                        "url"       :   item.url,
                        "target"    :   item.target                       
                    };

                    //PLACE USED LINKS
                    usedSearchLinks.push(itemToPush);
                }
            }
            $.csRsMenu({
                "breakPoint" : 768, // SYSTEM BREAK POINTS - 768, 640, 480, 320
                "slideDirection" : "left-to-right", // OPTIONS - left-to-right, right-to-left
                "menuButtonParent" : "#gb-header-mobile",
                "menuBtnText" : "Menu",
                "colors": {
                    "pageOverlay": '#000000',
                    "menuBackground": '#FFFFFF',
                    "menuText": '#333333',
                    "menuTextAccent": '#333333',
                    "dividerLines": '#E6E6E6',
                    "buttonBackground": '#E6E6E6',
                    "buttonText": '#333333'
                },
                "showDistrictHome": template.ShowDistrictHome,
                "districtHomeText": "District",
                "showSchools" : template.ShowSchoolList,
                "schoolMenuText": "Schools",
                "showTranslate" : true,
                "translateMenuText": "Translate",
                "translateVersion": 2, // 1 = FRAMESET, 2 = BRANDED
                "translateId" : "",
                "translateLanguages": [ // ["ENGLISH LANGUAGE NAME", "TRANSLATED LANGUAGE NAME", "LANGUAGE CODE"]
                    ["Afrikaans", "Afrikaans", "af"],
                    ["Albanian", "shqiptar", "sq"],
                    ["Amharic", "አማርኛ", "am"],
                    ["Arabic", "العربية", "ar"],
                    ["Armenian", "հայերեն", "hy"],
                    ["Azerbaijani", "Azərbaycan", "az"],
                    ["Basque", "Euskal", "eu"],
                    ["Belarusian", "Беларуская", "be"],
                    ["Bengali", "বাঙালি", "bn"],
                    ["Bosnian", "bosanski", "bs"],
                    ["Bulgarian", "български", "bg"],
                    ["Burmese", "မြန်မာ", "my"],
                    ["Catalan", "català", "ca"],
                    ["Cebuano", "Cebuano", "ceb"],
                    ["Chichewa", "Chichewa", "ny"],
                    ["Chinese Simplified", "简体中文", "zh-CN"],
                    ["Chinese Traditional", "中國傳統的", "zh-TW"],
                    ["Corsican", "Corsu", "co"],
                    ["Croatian", "hrvatski", "hr"],
                    ["Czech", "čeština", "cs"],
                    ["Danish", "dansk", "da"],
                    ["Dutch", "Nederlands", "nl"],
                    ["Esperanto", "esperanto", "eo"],
                    ["Estonian", "eesti", "et"],
                    ["Filipino", "Pilipino", "tl"],
                    ["Finnish", "suomalainen", "fi"],
                    ["French", "français", "fr"],
                    ["Galician", "galego", "gl"],
                    ["Georgian", "ქართული", "ka"],
                    ["German", "Deutsche", "de"],
                    ["Greek", "ελληνικά", "el"],
                    ["Gujarati", "ગુજરાતી", "gu"],
                    ["Haitian Creole", "kreyòl ayisyen", "ht"],
                    ["Hausa", "Hausa", "ha"],
                    ["Hawaiian", "ʻŌlelo Hawaiʻi", "haw"],
                    ["Hebrew", "עִברִית", "iw"],
                    ["Hindi", "हिंदी", "hi"],
                    ["Hmong", "Hmong", "hmn"],
                    ["Hungarian", "Magyar", "hu"],
                    ["Icelandic", "Íslenska", "is"],
                    ["Igbo", "Igbo", "ig"],
                    ["Indonesian", "bahasa Indonesia", "id"],
                    ["Irish", "Gaeilge", "ga"],
                    ["Italian", "italiano", "it"],
                    ["Japanese", "日本語", "ja"],
                    ["Javanese", "Jawa", "jw"],
                    ["Kannada", "ಕನ್ನಡ", "kn"],
                    ["Kazakh", "Қазақ", "kk"],
                    ["Khmer", "ភាសាខ្មែរ", "km"],
                    ["Korean", "한국어", "ko"],
                    ["Kurdish", "Kurdî", "ku"],
                    ["Kyrgyz", "Кыргызча", "ky"],
                    ["Lao", "ລາວ", "lo"],
                    ["Latin", "Latinae", "la"],
                    ["Latvian", "Latvijas", "lv"],
                    ["Lithuanian", "Lietuvos", "lt"],
                    ["Luxembourgish", "lëtzebuergesch", "lb"],
                    ["Macedonian", "Македонски", "mk"],
                    ["Malagasy", "Malagasy", "mg"],
                    ["Malay", "Malay", "ms"],
                    ["Malayalam", "മലയാളം", "ml"],
                    ["Maltese", "Malti", "mt"],
                    ["Maori", "Maori", "mi"],
                    ["Marathi", "मराठी", "mr"],
                    ["Mongolian", "Монгол", "mn"],
                    ["Myanmar", "မြန်မာ", "my"],
                    ["Nepali", "नेपाली", "ne"],
                    ["Norwegian", "norsk", "no"],
                    ["Nyanja", "madambwe", "ny"],
                    ["Pashto", "پښتو", "ps"],
                    ["Persian", "فارسی", "fa"],
                    ["Polish", "Polskie", "pl"],
                    ["Portuguese", "português", "pt"],
                    ["Punjabi", "ਪੰਜਾਬੀ ਦੇ", "pa"],
                    ["Romanian", "Română", "ro"],
                    ["Russian", "русский", "ru"],
                    ["Samoan", "Samoa", "sm"],
                    ["Scottish Gaelic", "Gàidhlig na h-Alba", "gd"],
                    ["Serbian", "Српски", "sr"],
                    ["Sesotho", "Sesotho", "st"],
                    ["Shona", "Shona", "sn"],
                    ["Sindhi", "سنڌي", "sd"],
                    ["Sinhala", "සිංහල", "si"],
                    ["Slovak", "slovenský", "sk"],
                    ["Slovenian", "slovenski", "sl"],
                    ["Somali", "Soomaali", "so"],
                    ["Spanish", "Español", "es"],
                    ["Sundanese", "Sunda", "su"],
                    ["Swahili", "Kiswahili", "sw"],
                    ["Swedish", "svenska", "sv"],
                    ["Tajik", "Тоҷикистон", "tg"],
                    ["Tamil", "தமிழ்", "ta"],
                    ["Telugu", "తెలుగు", "te"],
                    ["Thai", "ไทย", "th"],
                    ["Turkish", "Türk", "tr"],
                    ["Ukrainian", "український", "uk"],
                    ["Urdu", "اردو", "ur"],
                    ["Uzbek", "O'zbekiston", "uz"],
                    ["Vietnamese", "Tiếng Việt", "vi"],
                    ["Welsh", "Cymraeg", "cy"],
                    ["Western Frisian", "Western Frysk", "fy"],
                    ["Xhosa", "isiXhosa", "xh"],
                    ["Yiddish", "ייִדיש", "yi"],
                    ["Yoruba", "yorùbá", "yo"],
                    ["Zulu", "Zulu", "zu"]
                ],
                "showAccount": true,
                "accountMenuText": "User Options",
                "usePageListNavigation": false,
                "extraMenuOptions": extraOptions,
                "siteID": "[$siteID$]",
                "allLoaded": function(){}
            });
        },

        "Translate": function() {
            $('#translate-dropdown').creativeTranslate({
                'type': 2, // 1 = FRAMESET, 2 = BRANDED, 3 = API
                'advancedOptions': {
                    'addMethod': 'append',
                    'removeBrandedDefaultStyling': false,
                    'useTranslatedNames': true, // ONLY FOR FRAMESET AND API VERSIONS - true = TRANSLATED LANGUAGE NAME, false = ENGLISH LANGUAGE NAME
                },
                'translateLoaded': function() {} // CALLBACK TO RUN YOUR CUSTOM CODE AFTER EVERYTHING IS READY               
            });
        },

        "Search": function() {
            const defSearchText = 'Search...';
        
            const searchBox = $('#gb-search-input');
            
            searchBox
                .attr('placeholder', '')
                .val(defSearchText)
                .focus(function() {
                    if($(this).val() == defSearchText) {
                        $(this).val('');
                    }
                })
                .blur(function() {
                    if($(this).val() == '') {
                        $(this).val(defSearchText);
                    }
                });
                
            $('#gb-search-form').submit(function(e){
                e.preventDefault();
        
                if($('#gb-search-input').val() != '' && $('#gb-search-input').val() != defSearchText) {
                    window.location.href = '/site/Default.aspx?PageType=6&SiteID=[$SiteID$]&SearchString=' + $('#gb-search-input').val();
                }
            });
        },
        
        "Footer": function() {
            //FOR SCOPE
            var template = this;
            
            //ADD LOGO
            const homeLink = '<a style="display:inline-block" href="[$SiteAlias$]">'; //SITE URL
            
            //LOGO DIMENSIONS
            var imgWidth = 255;
            var imgHeight = 165;
            
            //ADD LOGO TO HEADER
            var logoSrc = $.trim('<SWCtrl controlname="Custom" props="Name:schoolLogo" />');
            var srcSplit = logoSrc.split("/");
            var srcSplitLen = srcSplit.length;
            
            //CHECK IF LOGO IS EMPTY
            if((logoSrc == '') || (srcSplit[srcSplitLen - 1] == 'default-man.jpg')) {
                logoSrc = this.DefaultLogoSrc;
            }
            
            //ADD CUSTOM SCHOOL LOGO
            $('.gb-logo-footer').append(homeLink + '<img width="' + imgWidth + '" height="' + imgHeight + '" src="' + logoSrc + '" alt="[$SiteName$] Logo"></a>');
            
            //ADD BLACKBOARD FOOTER
            $(document).csBbFooter({
                'footerContainer'   : '#disclaimer-row',
                'useDisclaimer'     : false,
                'disclaimerText'    : ''
            });
        },       
        
        "AllyClick": function(event) {
            if(event.type == 'click') {
                return true;
            } else if(event.type == 'keydown' && (event.key == this.KeyCodes.space || event.key == this.KeyCodes.enter)) {
                return true;
            } else {
                return false;
            }
        },

        "GetBreakPoint": function() {
            return window.getComputedStyle(document.querySelector("body"), ":before").getPropertyValue("content").replace(/"|'/g, "");/*"*/
        },
    }