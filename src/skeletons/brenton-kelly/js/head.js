$(function() {
    CreativeTemplate.Init();
});

var CreativeTemplate = {
    // PROPERTIES
    "KeyCodes": { "tab": [9, "Tab"], "enter": [13, "Enter"], "esc": [27, "Escape"], "space": [32, "Space"], "end": [35, "End"], "home": [36, "Home"], "left": [37, "ArrowLeft"], "up": [38, "ArrowUp"], "right": [39, "ArrowRight"], "down": [40, "ArrowDown"] },
    "IsMyViewPage": false,
    
    // METHODS
    "Init": function() {
        this.SetTemplateProps();
        this.JsMediaQueries();
        this.MyStart();
        this.Header();
        this.ChannelBar();
        this.Body();
        this.GlobalIcons();
        this.SocialIcons();
        this.ModEvents();
        this.Footer();
        this.Slideshow();
        this.RsMenu();
        this.AppAccordion();
        this.Search();
        this.GlobalJs("NewWindowLinks");
        
        $(window).on("load", () => { this.WindowLoad(); });
        $(window).on("resize", () => { this.WindowResize(); });
    },
    
    "SetTemplateProps": function() {
        // MYVIEW PAGE CHECK
        if($(".myview").length) this.IsMyViewPage = true;
    },

    "WindowLoad": function() {
        this.GlobalJs("NewWindowLinks");
    },

    "WindowResize": function() {
        this.JsMediaQueries();
    },
    
    "GlobalJs": function(action) {
        if(action.indexOf("TcwMarkdown") > -1) {
            csGlobalJs.TcwMarkDown();
        }
        
        if(action.indexOf("NewWindowLinks") > -1) {
            csGlobalJs.OpenInNewWindowWarning();
        }
    },

    "JsMediaQueries": function() {
        switch(this.GetBreakPoint()) {
            case "desktop": 

            break;
            case "768":

            break;
            case "640":

            break;
            case "480":

            break;
            case "320":

            break;
        }
    },

    "MyStart": function() {
        // ADD TRANSLATE
        $(".cs-template__mystart-section.cs-template--section-one").creativeTranslate({
            "type": 2
        });
    },
    
    "DropdownActions": function({ dropdownParent, dropdownSelector, dropdown, dropdownList }) {
        /* ASSUMES THE FOLLOWING STRUCTURE
        <div class="cs-template__mystart-control user-options">
            <div class="cs-template__mystart-control-selector" tabindex="0" aria-label="User Options" role="button" aria-expanded="false" aria-haspopup="true">User Options</div>
            <div class="cs-template__mystart-dropdown" aria-hidden="true" style="display:none;">
                <ul class="cs-template__mystart-dropdown-list">
                    <li class="cs-template__mystart-dropdown-list-item">
                        <a href="#" class="cs-template__mystart-dropdown-link">Link</a>
                    </li>
                </ul>
            </div>
        </div>
        */
        
        // FOR SCOPE
        var _this = this;

        var dropdownParent = dropdownParent;
        var dropdownSelector = dropdownSelector;
        var dropdown = dropdown;
        var dropdownList = dropdownList;
        
        $(dropdownParent + " " + dropdownList + " a").attr("tabindex", "-1");
        
        // MYSTART DROPDOWN SELECTOR CLICK EVENT
        $(dropdownParent).on("click", dropdownSelector, function(e) {
            e.preventDefault();
            
            if($(this).parent().hasClass("open")){
                $("+ " + dropdownList + " a").attr("tabindex", "-1");
                $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing"); 
            } else {
                $(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden","false").slideDown(300, "swing");
            }
        });
        
        // MYSTART DROPDOWN SELECTOR KEYDOWN EVENTS
        $(dropdownParent).on("keydown", dropdownSelector, function(e) {
            // CAPTURE KEY CODE
            var key = e.code || e.keyCode;
            switch(key) {
                // CONSUME LEFT AND UP ARROWS
                case _this.KeyCodes.enter[0]:
                case _this.KeyCodes.enter[1]:
                case _this.KeyCodes.space[0]:
                case _this.KeyCodes.space[1]:
                    e.preventDefault();

                    // IF THE DROPDOWN IS OPEN, CLOSE IT
                    if($(dropdownParent).hasClass("open")){
                        $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
                        $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing"); 
                    } else {
                        $(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden", "false").slideDown(300, "swing", function(){
                            $(dropdownList + " li:first-child a", this).attr("tabindex", "0").focus();
                        });
                    }
                break;

                // CONSUME TAB KEY
                case _this.KeyCodes.tab[0]:
                case _this.KeyCodes.tab[1]:
                    if($("+ " + dropdown + " " + dropdownList + " a").length) {
                        $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
                        $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                    }
                break;

                // CONSUME LEFT AND UP ARROWS
                case _this.KeyCodes.down[0]:
                case _this.KeyCodes.down[1]:
                case _this.KeyCodes.right[0]:
                case _this.KeyCodes.right[1]:
                    e.preventDefault();
                    
                    $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
                    $("+ " + dropdown + " " + dropdownList + " li:first-child > a", this).attr("tabindex", "0").focus();
                break;
            }
        });
        
        // MYSTART DROPDOWN LINK KEYDOWN EVENTS
        $(dropdownParent).on("keydown", dropdownList + " li a", function(e) {
            // CAPTURE KEY CODE
            switch(e.keyCode) {
                // CONSUME LEFT AND UP ARROWS
                case _this.KeyCodes.left[0]:
                case _this.KeyCodes.left[1]:
                case _this.KeyCodes.up[0]:
                case _this.KeyCodes.up[1]:
                    e.preventDefault();

                    // IS FIRST ITEM
                    if($(this).parent().is(":first-child")) {
                        // FOCUS DROPDOWN BUTTON
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(this).closest(dropdownParent).find(dropdownSelector).focus();
                    } else {
                        // FOCUS PREVIOUS ITEM
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(this).parent().prev("li").find("> a").attr("tabindex", "0").focus();
                    }
                break;

                // CONSUME RIGHT AND DOWN ARROWS
                case _this.KeyCodes.right[0]:
                case _this.KeyCodes.right[1]:
                case _this.KeyCodes.down[0]:
                case _this.KeyCodes.down[1]:
                    e.preventDefault();

                    // IS LAST ITEM
                    if($(this).parent().is(":last-child")) {
                        // FOCUS FIRST ITEM
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(this).closest(dropdownList).find("li:first-child > a").attr("tabindex", "0").focus();
                    } else {
                        // FOCUS NEXT ITEM
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(this).parent().next("li").find("> a").attr("tabindex", "0").focus();
                    }
                break;
                
                // CONSUME TAB KEY
                case _this.KeyCodes.tab[0]:
                case _this.KeyCodes.tab[1]:
                    if(e.shiftKey) {
                        e.preventDefault();
                        
                        // FOCUS DROPDOWN BUTTON
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(this).closest(dropdownParent).find(dropdownSelector).focus();
                    }
                break;

                // CONSUME HOME KEY
                case _this.KeyCodes.home[0]:
                case _this.KeyCodes.home[1]:
                    e.preventDefault();

                    // FOCUS FIRST ITEM
                    $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                    $(this).closest(dropdownList).find("li:first-child > a").attr("tabindex", "0").focus();
                break;

                // CONSUME END KEY
                case _this.KeyCodes.end[0]:
                case _this.KeyCodes.end[1]:
                    e.preventDefault();

                    // FOCUS LAST ITEM
                    $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                    $(this).closest(dropdownList).find("li:last-child > a").attr("tabindex", "0").focus();
                break;
                
                // CONSUME ESC KEY
                case _this.KeyCodes.esc[0]:
                case _this.KeyCodes.esc[1]:
                    e.preventDefault();

                    // FOCUS DROPDOWN BUTTON AND CLOSE DROPDOWN
                    $(this).closest(dropdownParent).find(dropdownSelector).focus();
                    $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                    $(dropdownSelector).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                break;
            }
        });
        
        $(dropdownParent).mouseleave(function(e) {
            if(e.target.nodeName.toLowerCase() !== "select") {
                $(dropdownList + " a", this).attr("tabindex", "-1");
                $(dropdownSelector, this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
            }
        }).focusout(function() {
            var thisDropdown = this;
            
            setTimeout(function () {
                if(!$(thisDropdown).find(":focus").length) {
                    $(dropdownSelector, thisDropdown).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                }
            }, 500);
        });
    },

    "Header": function() {
        // ADD LOGO
        var showLogo = <SWCtrl controlname="Custom" props="Name:showLogo" />;
        var logoSrc = $.trim("<SWCtrl controlname="Custom" props="Name:logoSrc" />");
        if(showLogo) {
            if((logoSrc != "") && logoSrc.indexOf("default-man") == -1) {
                $(".cs-template__logo").append('<a href="/[$SITEALIAS$]" class="cs-template__logo-link"><img src="' + logoSrc + '" alt="[$SiteName$] logo" class="cs-template__logo-image" /></a>');
            } else {
                $(".cs-template__logo").append('<a href="/[$SITEALIAS$]" class="cs-template__logo-link"><img src="/cms/lib9/SWCS000001/Centricity/template/83/defaults/phoenix.svg" width="63" height="102" alt="[$SiteName$] logo" class="cs-template__logo-image" /></a>');
            }
        }

        // ADD SITENAME
        var siteNameOne = $.trim("<SWCtrl controlname="Custom" props="Name:siteNameOne" />");
        var siteNameTwo = $.trim("<SWCtrl controlname="Custom" props="Name:siteNameTwo" />");
        var siteNameBegin = '';
        var siteNameEnd = '';

        if((siteNameOne == "") && (siteNameTwo == "")) {
            var splitLen = 2;
            var siteName = "[$SiteName$]";
            siteName = siteName.split(" ");
            var siteNameLength = siteName.length;
            if(siteNameLength > 2){
                siteNameEnd = $.trim(siteName.splice(-splitLen, siteName.length).toString().replace(/,/g, " "));
            } else {
                siteNameEnd = $.trim(siteName.splice(-1, siteName.length).toString().replace(/,/g, " "));
            }
            siteNameBegin = $.trim(siteName.toString().replace(/,/g, " "));
            $(".cs-template__site-name").prepend("<h1 class='cs-template__site-name-h1'>" + siteNameBegin + "<span>" + siteNameEnd + "</span></h1>");
        } else if((siteNameOne != "") && (siteNameTwo == "")) {
            $(".cs-template__site-name").prepend("<h1 class='cs-template__site-name-h1'>" + siteNameOne + "</h1>");
        } else if((siteNameOne == "") && (siteNameTwo != "")) {
            $(".cs-template__site-name").prepend("<h1 class='cs-template__site-name-h1'>" + siteNameTwo + "</h1>");
        } else if((siteNameOne != "") && (siteNameTwo != "")) {
            $(".cs-template__site-name").prepend("<h1 class='cs-template__site-name-h1'>" + siteNameBegin + "<span>" + siteNameEnd + "</span></h1>");
        }
    },

    "ChannelBar": function() {
        $(".sw-channel-item").unbind("hover");
        $(".sw-channel-item").hover(function(){
            $(".sw-channel-item ul").stop(true, true);
            var subList = $(this).children('ul');
            if ($.trim(subList.html()) !== "") {
                subList.slideDown(300, "swing");
            }
            $(this).addClass("hover");
        }, function(){
            $(".sw-channel-dropdown").slideUp(300, "swing");
            $(this).removeClass("hover");
        });
    },

    "Body": function() {
        // AUTO FOCUS SIGN IN FIELD
        $("#swsignin-txt-username").focus();

        // ADD READ FULL STORY BUTTON TO EACH HEADLINE
        $(".ui-widget.app.headlines").each(function() {
            $(".ui-article", this).each(function() {
                var headlineURL = $("h1 a", this).attr("href");
                $(".ui-article-description", this).append('<a class="custom-read-more" href="' + headlineURL + '" aria-hidden="true" tabindex="-1">Read Full Story »</a>');
            });
        });

        // APPLY RESPONSIVE DIMENSIONS TO CONTENT IMAGES
        $("div.ui-widget.app .ui-widget-detail img")
            .not($("div.ui-widget.app.multimedia-gallery .ui-widget-detail img"))
            .not($("div.ui-widget.app.gallery.json .ui-widget-detail img"))
            .each(function() {
                if ($(this).attr('width') !== undefined && $(this).attr('height') !== undefined) { // IMAGE HAS INLINE DIMENSIONS
                    $(this).css({"display": "inline-block", "width": "100%", "max-width": $(this).attr("width") + "px", "height": "auto", "max-height": $(this).attr("height") + "px"});
                }
        });

        // ADJUST FIRST BREADCRUMB
        $("li.ui-breadcrumb-first > a > span").text("Home");

        // USE CHANNEL NAME FOR PAGELIST NAV HEADER IF ONE IS NOT PRESENT
        if(!$(".cs-template__subpage-column.cs-template--column-one .ui-widget-header h1").length) {
            $(".cs-template__subpage-column.cs-template--column-one .ui-widget-header").append("<h1>[$ChannelName$]</h1>");
        }
    },
    
    "GlobalIcons": function() {
        
    },
    
    "SocialIcons": function() {
        var socialIcons = [
            
        ];

        var icons = '';
        $.each(socialIcons, function(index, icon) {
            if(icon.show) {
                icons += '<li class="cs-template__social-media-list-item"><a class="cs-template__social-media-icon cs-template--social-media-' + icon.class + '" href="' + icon.url + '" target="' + icon.target + '" aria-label="' + icon.label + '"></a></li>';
            }
        });

        if(icons.length) {
            $(".cs-template__social-media-icons").html('<ul class="cs-template__social-media-list">' + icons + '</ul>');
        }
    },
    
    "ModEvents": function() {
        $(".ui-widget.app.upcomingevents").modEvents({
            columns     : "yes",
            monthLong   : "yes",
            dayWeek     : "yes"
        });
        
        $(".ui-widget.app.upcomingevents .ui-article").each(function() {
            if(!$(".ui-article-title.sw-calendar-block-date", this).length && $.trim($(".ui-article-description", this).text()) != "There are no upcoming events to display.") {
                var moveArticle = $(this).html();
                $(this).parent().prev("li").find(".ui-article").append(moveArticle);
                $(this).parent().remove();
            } else {
                var eventMonth = $(".joel-month", this).detach();
                var eventDay = $(".joel-day", this).detach();
                var eventDate = $.trim($(".ui-article-title", this).text());
                if(eventDate.length == 1) eventDate = "0" + eventDate;
                
                $(".ui-article-title", this).html(eventMonth).prepend('<span class="joel-date" aria-hidden="true">' + eventDate + '</span>').append('<span class="joel-day" aria-hidden="true">' + $(eventDay).text() + '</span>').attr("aria-label", $(eventDay).text() + ", " + $(eventMonth).text() + " " + eventDate);
                $(".joel-month", this).attr("aria-hidden", "true");
            }
        });
    },

    "Footer": function() {
        // MOVE Bb FOOTER STUFF
        $(".cs-template__branded-footer-logo").html($("#sw-footer-logo").html());
        var brandedLinks = '';
        brandedLinks += '<li class="cs-template__branded-footer-links-list-item">' + $.trim($("#sw-footer-links li:eq(0)").html().replace("|", "")) + '</li>';
        brandedLinks += '<li class="cs-template__branded-footer-links-list-item>' + $.trim($("#sw-footer-links li:eq(2)").html().replace("|", "")) + '</li>';
        brandedLinks += '<li class="cs-template__branded-footer-links-list-item>' + $.trim($("#sw-footer-links li:eq(1)").html().replace("|", "")) + '</li>';
        $(".cs-template__branded-footer-links").append('<ul class="cs-template__branded-footer-links-list">' + brandedLinks + '</ul>');
        $(".cs-template__branded-footer-copyright").append($("#sw-footer-copyright").html());
    },

    "Slideshow": function() {
        if($("#sw-content-container10.ui-hp .ui-widget.app.multimedia-gallery").length) {
            var mmg = eval("multimediaGallery" + $("#sw-content-container10.ui-hp .ui-widget.app.multimedia-gallery:first").attr("data-pmi"));
            mmg.props.defaultGallery = false;

            $("#sw-content-container10.ui-hp .ui-widget.app.multimedia-gallery:first").csMultimediaGallery({
                "efficientLoad" : true,
                "imageWidth" : 960,
                "imageHeight" : 500,
                "mobileDescriptionContainer": [640, 480, 320], // [960, 768, 640, 480, 320]
                "galleryOverlay" : false,
                "linkedElement" : [], // ["image", "title", "overlay"]
                "playPauseControl" : false,
                "backNextControls" : false,
                "bullets" : false,
                "thumbnails" : false,
                "thumbnailViewerNum": [4, 4, 3, 3, 2], // NUMERICAL - [960 view, 768 view, 640 view, 480 view, 320 view]
                "autoRotate" : true,
                "hoverPause" : true,
                "transitionType" : "fade", // fade, slide, custom
                "transitionSpeed" : 1.5,
                "transitionDelay" : 4,
                "fullScreenRotator" : false,
                "fullScreenBreakpoints" : [960], // NUMERICAL - [960, 768, 640, 480, 320]
                "onImageLoad" : function(props) {}, // props.element, props.recordIndex, props.mmgRecords
                "allImagesLoaded" : function(props) {}, // props.element, props.mmgRecords
                "onTransitionStart" : function(props) {}, // props.element, props.currentRecordIndex, props.currentGalleryIndex, props.nextRecordIndex, props.nextGalleryIndex, props.mmgRecords
                "onTransitionEnd" : function(props) {}, // props.element, props.currentRecordIndex, props.currentGalleryIndex, props.mmgRecords
                "allLoaded" : function(props) {}, // props.element, props.mmgRecords
                "onWindowResize": function(props) {} // props.element, props.mmgRecords
            });
        }
    },

    "RsMenu": function() {
        $.csRsMenu({
            "breakPoint" : 480, // SYSTEM BREAK POINTS - 768, 640, 480, 320
            "slideDirection" : "left-to-right", // OPTIONS - left-to-right, right-to-left
            "menuButtonParent" : "#sw-mystart-left",
            "menuBtnText" : "Menu",
            "colors": {
                "pageOverlay": "#000000", // DEFAULT #000000
                "menuBackground": "#FFFFFF", // DEFAULT #FFFFFF
                "menuText": "#333333", // DEFAULT #333333
                "menuTextAccent": "#333333", // DEFAULT #333333
                "dividerLines": "#E6E6E6", // DEFAULT #E6E6E6
                "buttonBackground": "#E6E6E6", // DEFAULT #E6E6E6
                "buttonText": "#333333" // DEFAULT #333333
            },
            "showSchools" : false,
            "schoolMenuText": "Our Schools",
            "showTranslate" : false,
            "translateMenuText": "Translate Language",
            "translateVersion": 1, // 1 = FRAMESET, 2 = BRANDED
            "translateId" : "",
            "showAccount": true,
            "accountMenuText": "User Options",
            "usePageListNavigation": true,
            "extraMenuOptions": {
                /*
                "Menu Name": [
                    {
                        "text": "",
                        "url": "",
                        "target": ""
                    }
                ]
                */
            }, 
            "siteID": "[$siteID$]",
            "allLoaded": function(){}
        });
    },

    "AppAccordion": function() {
        $(".cs-template__homepage-content").csAppAccordion({
            "accordionBreakpoints" : [640, 480, 320]
        });

        $(".cs-template__subpage-column.cs-template--column-one").csAppAccordion({
            "accordionBreakpoints" : [480, 320]
        });
    },

    "Search": function() {
        var _this = this;
        
        $(document).on("click keydown", ".cs-template__search-button", function(e){
            if(_this.AllyClick(e)) {
                e.preventDefault();
                
                $(".cs-template__search-form").submit();
            }
        });
        
        $(".cs-template__search-form").submit(function(e){
            e.preventDefault();
            
            if($.trim($(".cs-template__search-input").val()) != "" && $.trim($(".cs-template__search-input").val()) != "Search...") {
                window.location.href = "/site/Default.aspx?PageType=6&SiteID=[$SiteID$]&SearchString=" + $(".cs-template__search-input").val();
            }
        });
    },

    "AllyClick": function(event) {
        var key = event.code || event.keyCode;

        if(event.type == "click") {
            return true;
        } else if(event.type == "keydown" && ((
                key == this.KeyCodes.space[0] || key == this.KeyCodes.space[1]
            ) || (
                key == this.KeyCodes.enter[0] || key == this.KeyCodes.enter[1]
            ))) {
            return true;
        }
        return false;
    },

    "GetBreakPoint": function() {
        // 320
        if(window.matchMedia("(max-width: 479px)").matches) { return "320"; }

        // 480
        else if(window.matchMedia("(max-width: 639px)").matches) { return "480"; }

        // 640
        else if(window.matchMedia("(max-width: 767px)").matches) { return "640"; }

        // 768
        else if(window.matchMedia("(max-width: 1023px)").matches) { return "768"; }

        // DESKTOP
        else { return "desktop"; }
    }
};